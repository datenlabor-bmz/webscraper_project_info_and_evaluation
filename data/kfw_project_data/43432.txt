"UNICEF Irak, Bildung, Kinderschutz und WASH, Phase 5"

Der deutsche Beitrag zur Finanzierung von UNICEF im Irak soll in 2020 im Rahmen einer weiteren Phase um 26 Mio. EUR erhöht werden. Wie bereits bei den Vorgänger-Vorhaben erfolgt eine Finanzierung im Rahmen des UNICEF-Landesprogramms Irak, das mit der irakischen Regierung abgestimmt und vereinbart wurde.

Ziel der Phase 5 ist unverändert die Verbesserung des Zugangs sowie der Qualität öffentlicher Bildungsangebote im Primar- und Sekundarschulbereich, die Schaffung von Betreuungs- und Beratungsangeboten im psychologischen und psychosozialen Bereich sowie die Verbesserung der Qualität der öffentlichen Wasserversorgung und Abwasserentsorgung sowie von Sanitäranlagen in Schulen und Gesundheitseinrichtungen (WASH). Auf der Policy-Ebene sollen soziale Reformen und Inklusion von Kinder und Jugendlichen gefördert werden. Darüber hinaus sollen Maßnahmen zur Verhinderung der weiteren Ausbreitung COVID-19 gefördert werden.

Land / Region / Institution Irak Nummer 43432 Schwerpunkt Friedensentwicklung und Krisenprävention Sektor 11220 - Grundschulbildung USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 26 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner UNITED NATIONS CHILDREN'S FUND Zuständige Abteilung Afghanistan, Pakistan
