  




 

Hochwasserschutz und Entwässerung mittelgroßer Küstenstädte Vietnams
Flood Management and Drainage of Medium-Sized Cities
Project details
Project number:2011.9769.8
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Dr. Tim McGrath tim.mcgrath@giz.de
Partner countries: Viet Nam, Viet Nam
Summary
Objectives:

Die Anpassung an den Klimawandel im Bereich städtische Überflutung ist verbessert

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bauministerium (Ministry of Construction)

Financing organisation:

not available

 
Project value
Total financial commitment:11 191 653 Euro
Financial commitment for this project number:11 191 653 Euro
Cofinancing

Staatssekretariat für Wirtschaft (SECO): 6 000 000Euro


 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:08.10.2012 - 30.01.2021
Actual project:01.01.2013 - 30.01.2021
other participants
Arge IGIP - VIWASE
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Wassersektorpolitik und -verwaltung

Evaluation
not available
 
Project description (DE)

Ausgangssituation
Der Weltklimarat (Intergovernmental Panel on Climate Change, IPCC) hat Vietnam als eines der am stärksten vom Klimawandel betroffenen Länder identifiziert. Vietnamesische Küstenstädte sind durch Extremwetterereignisse, steigende Meeresspiegel und hohe Grundwasserstände besonders bedroht. In den gefährdeten Gebieten leben insbesondere arme Bevölkerungsschichten, die von den Folgen besonders betroffen sind. Zunehmende Starkregenfälle und die daraus resultierenden Überschwemmungen führen zu einer Schädigung und zum Ausfall der lokalen Infrastruktur. Zugleich verursacht der hohe Wasserpegel eine Fließrichtungsumkehr in der Kanalisation und bedingt den Austritt von unbehandelten Abwässern, die Menschen und Umwelt gefährden. Während in Großstädten bereits internationale Projekte zur Anpassung an den Klimawandel vorhanden sind, wurden mittelgroße Städte bislang vernachlässigt. Weder in der nationalen Gesetzgebung noch in der Stadt- und Raumplanung der verantwortlichen Kommunal-, Provinz- und Nationalbehörden wird den veränderten hydrologischen und meteorologischen Bedingungen Rechnung getragen. Maßnahmen zur Anpassung der Infrastruktur an den Klimawandel fanden bislang nicht statt.
Ziel
Im Hinblick auf Anpassung an häufigere und stärkere städtische Überflutungen im Zuge des Klimawandels sind Kompetenzen, Ressourcen und Leistungsfähigkeit staatlicher Behörden und der Bevölkerung verbessert.


Vorgehensweise
Das Projekt unterstützt die Berücksichtigung der Klimawandelauswirkungen in der städtischen Entwässerungsplanung sowie die Katastrophenvorsorge und den Katastrophenschutz.
Die Regierung wird hinsichtlich der Verbesserung der Sektorpolitik zur städtischen Entwässerung beraten, in enger Abstimmung mit dem Bauministerium. Die systematische Berücksichtigung zukünftiger Klimaveränderungen spielt dabei eine zentrale Rolle.


Das Projekt leistet Politikberatung in den fünf Provinzen Phu Yen, Binh Dinh, Khanh Hoa, Quang Ngai und Soc Trang. Beim Mainstreaming von Klimawandelanpassung in die städtische Entwässerungsplanung arbeitet es mit den Provinzen Phu Yen, Binh Dinh und Soc Trang eng zusammen. Dabei werden Lücken in der Gesetzgebung aufgezeigt, was der Arbeit auf nationaler Ebene und den anderen Provinzen zugutekommt.
Für die Verbesserung des Katastrophenschutzes legt das Projekt den Schwerpunkt auf die Provinzen Phu Yen und Binh Dinh. Dort fördert es die Einrichtung und die Verbesserung von Frühwarnsystemen. In Zusammenarbeit mit dem Deutschen Roten Kreuz und dem Vietnamesischen Roten Kreuz erreicht das Projekt die Bevölkerung in besonders gefährdeten Gebieten der Provinzhauptstädte direkt. Durch Trainings zur Katastrophenvorsorge und zum Verhalten im Katastrophenfall lernt die Bevölkerung, mit Hochwasserereignissen besser umzugehen. Außerdem werden die organisatorischen Abläufe bei der Notfallplanung gemeinsam mit den zuständigen Behörden überprüft und verbessert.


Trainings und Politikdialoge in den fünf Provinzen und auf nationaler Ebene runden die gemeinsame Arbeit ab. Dabei soll auch die Kommunikation zwischen den nationalen Behörden und den Behörden in den Provinzen gestärkt werden.
Wirkung
Bei den Behörden in den Projektprovinzen ist das Bewusstsein dafür gestiegen, dass eine Beschränkung auf traditionelle Infrastrukturlösungen für die Anpassung an den Klimawandel nicht zielführend ist. Die Projektpartner arbeiten deshalb an nachhaltigen Konzepten, beispielsweise für eine Verringerung der Flächenversiegelung oder die Einrichtung von Parks und Grünflächen, die auch als Wasserrückhaltebecken dienen.


In Tuy Hoa und Quy Nhon entwickeln Arbeitsgruppen Leitfäden zum Katastrophenschutz für die zuständigen Behörden. Eine weitere Arbeitsgruppe beschäftigt sich mit der Nachhaltigkeit von Frühwarnsystemen.
In der Gesetzgebung für die Stadtentwässerung und zur Berücksichtigung der Auswirkungen des Klimawandels wurden Lücken identifiziert, die nun vom Bauministerium bearbeitet werden. 

 
Project description (EN)

Context
The Intergovernmental Panel on Climate Change (IPCC) has identified Viet Nam as one of the countries that is most severely affected by climate change. The coastal towns and cities of Viet Nam are particularly vulnerable to extreme weather events, rising sea levels and high groundwater levels. At-risk areas are primarily populated by poorer communities who are often badly affected by the impacts of these events. Heavy rainfall is becoming increasingly common and the resultant floods damage and destroy the local infrastructure. In addition, high water levels mean that the flow of water through the sewerage system is reversed, causing untreated wastewater to be released, which endangers people and the environment. Although several international projects in the field of adaptation to climate change are being implemented in the major cities, medium-sized towns and cities have so far been neglected. Neither national legislation nor the urban and land use programmes run by the relevant municipal, provincial and national authorities adequately address the altered hydrological and meteorological conditions. No measures have so far been taken to make the infrastructure more resilient to climate change.

Objective
Urban planning has been adapted to make provision for the increasing number of floods caused by climate change. The negative effects of these floods on the population and urban infrastructure have been reduced. National policies and strategies prioritise adapting to climate change more highly.

Approach
The project is active in Central and Southern Viet Nam and supports the provincial capitals Quy Nhon, Tuy Hoa, Soc Trang, Quang Ngai and Nha Trang in implementing their strategies for adapting to climate change. The project tailors its activities to meet the specific needs of each province and is active in the following areas:
Providing training and organisational advice to the relevant ministries and local authorities in the urban planning and water management sectors so as to support adaptation to climate change
Integrating concepts for adapting to climate change into national legislation regarding the urban flood protection sector
Creating drainage plans and advising on structural and non-structural measures to protect infrastructure from floods
Applying instruments for sustainable management of urban drainage systems
Providing advisory services on improving disaster prevention, risk mapping and establishing or improving early warning systems
Carrying out awareness-raising activities for those who are particularly affected by floods

The project offers practical solutions to the challenge of adapting to climate change. One of the project’s main tasks is to promote communication and cooperation between the various decision-makers to facilitate effective flood protection.

The project is being run at national and regional levels in cooperation with the Vietnamese Ministry of Construction, the Ministry of Natural Resources and Environment, the Ministry of Agriculture and Rural Development, the People’s Committees, the Central Committee for Flood and Storm Control and the Climate Change Coordination Office.

Awareness-raising measures are being implemented to increase the local population’s knowledge of the potential dangers of extreme weather events, the effects of climate change in each city and the measures being taken at household and community levels to adapt to them.
 





 
