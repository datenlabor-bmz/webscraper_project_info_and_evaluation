  




 

Lokale Regierungsführung und Bürgerbeteiligung
Participatory local governance (PLG)
Project details
Project number:2021.2122.6
Status:laufendes Projekt
Responsible Organisational unit: 2B00 Asien II
Contact person:Sanjeev Pokharel sanjeev.pokharel@giz.de
Partner countries: Pakistan, Pakistan
Summary
Objectives:

Dienstleistungskapazitäten der lokalen Behörden in den Regionen Khyber Pakhtunkhwa und Punjab entsprechen den lokalen Bedürfnissen und Prioritäten.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Finance and Economic Affairs, Economic Affairs Division Islamabad (EAD)

Financing organisation:

not available

 
Project value
Total financial commitment:10 000 000 Euro
Financial commitment for this project number:10 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:05.04.2022 - 31.12.2025
Actual project:01.07.2022 - 31.12.2025
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Dezentralisierung und Förderung subnatio Gebietskörpers

Evaluation
not available
 




 
