Employment and Skills Developement Programme (Beschäftigungsförderung)

Das Ziel der SI-Maßnahme ist die Schaffung von Beschäftigungsmöglichkeiten für syrische Geflüchtete (Syrians under Temporary Protection) in der Türkei und benachteiligte, türkische Staatsbürger in den Aufnahmegemeinden. Dies erfolgt zum einen über einen institutionellen Kapazitätsaufbau der türkischen Arbeitsmarktagentur ISKUR sowie über den Ausbau von ISKUR-Service-Zentren in 16 Provinzen (Hatay, Gaziantep, Sanliurfa, Kilis, Istanbul). Dadurch wird ein Beitrag zur Verbesserung der Vermittlung von SuTP in den formellen Arbeitsmarkt geleistet.

Zum anderen erfolgt Beschäftigungsförderung über sog. SME-Capability Center, in denen Beratungsleistungen und Trainingsprogramme für kleinste, kleine und mittlere Unternehmen (KKMU) angeboten werden, um deren Wettbewerbsfähigkeit und Produktivität nachhaltig zu stärken. Dadurch soll ein Beitrag zur Schaffung zusätzlicher Arbeitsplätze, vor allem für SuTP, im formellen Sektor geleistet werden. Neben einem bereits bestehenden SME-Capability Center in Ankara werden gemeinsam mit den türkischen Regierungsstellen und lokalen Handelskammern zwei weitere Zentren in den Städten Konya und Kayseri eingerichtet. Über den deutschen Beitrag werden Digitalisierungskomponenten für die Zentren finanziert.

Durch verbesserte Perspektiven, sowohl für syrische Flüchtlinge als auch für die lokale türkische Bevölkerung, soll ein Beitrag zur Stabilisierung vom Flüchtlingszuzug am stärksten betroffenen türkischen Provinzen, zur langfristig verbesserten Resilienz lokaler Gemeinden und zur Beschäftigung geleistet werden.

Die Projektumsetzung erfolgt über das United Nations Development Program (UNDP) in enger Abstimmung mit der türkischen Regierung, der nationalen Arbeitsagentur (ISKUR), dem Ministry of Industry and Technology (MoIT), den lokalen Behörden und lokalen Industrie- und Handelskammern

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde entsprechend der Nachhaltigkeitsrichtlinie der KfW Entwicklungsbank in die Umwelt- und Sozialrisikokategorie C eingeordnet, da vom Vorhaben voraussichtlich keine oder nur geringe nachteilige Auswirkungen und Risiken auf Umwelt und soziale Belange ausgehen. Es ist daher kein spezifischer Umwelt- und Sozialmanagementplan erforderlich. Der Partner wurde zur Einführung eines Beschwerdemechanismus verpflichtet und muss die KfW unverzüglich über umwelt- und sozialrelevante besondere Vorkommnisse informieren.

Land / Region / Institution Türkei Nummer 37906 Schwerpunkt Wirtschaft und Finanzsektor Sektor 11330 - Berufliche Bildung USV-Kategorie C Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 8,8 Mio. EUR Status abgeschlossen Auftraggeber BMZ Projektpartner UNITED NATIONS DEVELOPMENT PROGRAMME (UNDP) Zuständige Abteilung Südosteuropa und Türkei
