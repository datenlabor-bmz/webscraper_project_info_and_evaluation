Türkei - ILO: "Beschäftigungsförderung für syrische Flüchtlinge und die heimische Bevölkerung in der Türkei"

Die Türkei hat mit Unterstützung der internationalen Gemeinschaft seit Beginn der syrischen Flüchtlingskrise große Anstrengungen unternommen, Syrer:innen n einen vorübergehenden Auf-enthalt in der Türkei zu ermöglichen. Auch Jahre nach Beginn der ersten Migrationswelle ist eine Reintegration von Syrer:innen in ihre Heimat noch nicht absehbar. Wegen des anhaltenden innenpolitischen Konflikts in Syrien ist die Stabilisierung der Aufnahmeregion und die Integration von Syrer: :innen in der Türkei ein prioritäres entwicklungspolitisches Ziel.

Etwa 3,77 Millionen syrische Flüchtlinge sind derzeit in der Türkei registriert. Die Integration der Syrer:innen in den formellen Arbeitsmarkt ist eine der größten Herausforderungen. Nach Schätzungen sind aktuell rund 1 Mio. Syrer:innen im informellen Sektor tätig.

Der hohe Grad an informeller Beschäftigung hat zur Folge, dass Mindestlöhne nicht gezahlt so-wie häufig Sozialstandards und Arbeitsplatzbestimmungen missachtet werden. Gleichzeitig ist zu beobachten, dass sich diese Situation auch negativ auf die Beschäftigungs- und Einkommensverhältnisse der türkischen Bevölkerung auswirkt, was wiederum zu steigenden sozialen Spannungen zwischen der Gastgeberbevölkerung und den syrischen Flüchtlingen führt. Insbesondere in urbanen Gebieten mit einer hohen Dichte an marginalisierten Bevölkerungsgruppen und hoher Jugendarbeitslosigkeit ist ein nachhaltiger Anstieg von Konflikten zwischen den einzelnen sozialen Gruppen bemerkbar.

Die Gründe für die fehlende Integration der Syrer:innen in den formellen Arbeitsmarkt sind viel-schichtig. Neben fehlenden formellen und anerkannten Qualifikationen und fehlender Sprachkompetenz in der türkischen Landessprache sind auch bürokratische Hindernisse sowie die negative Anreizwirkung der Sozialtransfers (ESSN) zu nennen. Das Projekt adressiert den Abbau einiger dieser Hemmnisse.

Das Projekt setzt sich aus drei Maßnahmenpaketen zusammen:

(i) Qualifizierungsmaßnahmen für formelle Beschäftigung über ein bezahltes Work-Based-Learning (WBL) Programm,
(ii) die Formalisierung informeller Arbeitsplätze durch Registrierung von Kleinstunternehmen mittels Einrichtung von Informationszentren (sog. BILMER) sowie
(iii) die Eingliederung von SuTP und Türk:innen in den ersten Arbeitsmarkt durch Zahlung der Sozialversicherungsbeiträgen und Kosten für die Arbeitserlaubnis für SuTP

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde entsprechend der Nachhaltigkeitsrichtlinie der KfW Entwicklungsbank in die Umwelt- und Sozialrisikokategorie C eingeordnet, da vom Vorhaben voraussichtlich keine oder nur geringe nachteilige Auswirkungen und Risiken auf Umwelt und soziale Belange ausgehen. Es ist daher kein spezifischer Umwelt- und Sozialmanagementplan erforderlich. Der Partner wurde zur Einführung eines Beschwerdemechanismus verpflichtet und muss die KfW unverzüglich über umwelt- und sozialrelevante besondere Vorkommnisse informieren.

Land / Region / Institution Türkei Nummer 39510 Schwerpunkt Friedensentwicklung und Krisenprävention Sektor 11330 - Berufliche Bildung USV-Kategorie C Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 9,4 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner INT'L LABOUR ORGANIZATION EUROPE AND CENTRAL ASIA OFFICE Zuständige Abteilung Südosteuropa und Türkei
