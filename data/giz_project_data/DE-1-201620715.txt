  




 

Programm Erneuerbare Energie und Energieeffizienz
Renewable Energy and Energy Efficiency Programme
Project details
Project number:2016.2071.5
Status:Projekt beendet
Responsible Organisational unit: 2B00 Asien II
Contact person:Dr. Frank Fecher frank.fecher@giz.de
Partner countries: Bangladesh, Bangladesh
Summary
Objectives:

Die konzeptionellen Grundlagen zur Verbreitung erneuerbarer Energien und zur Steigerung der Energieeffizienz in Bangladesch werden erfolgreich genutzt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Power Division of the Ministry of Power, Energy and Mineral Resources (MPEMR)

Financing organisation:

not available

 
Project value
Total financial commitment:26 490 001 Euro
Financial commitment for this project number:4 158 183 Euro
Cofinancing

not available

 
Previous projects
2012.2097.9Programm eneuerbare Energie und Energieeffizienz
Follow-on projects
not available
 
Term
Entire project:12.01.2007 - 02.03.2022
Actual project:01.11.2018 - 02.03.2022
other participants
GFA Consulting Group GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungSonstige unmittelbare Armutsbekämpfung
CRS code
Solarenergie für Verbundnetze

Evaluation
not available
 
Project description (DE)

Ausgangssituation
Bangladesch ist eine der am schnellsten wachsenden Volkswirtschaften der Welt. Bis 2021 soll der Status eines Landes mit mittlerem Einkommen erreicht werden. Innerhalb desselben Zeitraums will die Regierung von Bangladesch die 160 Millionen Einwohner des Landes mit Strom versorgen. Zur Deckung dieses Bedarfs werden im Stromsektor Erzeugungskapazitäten in Höhe von 24 Gigawatt benötigt.
Als eines der am stärksten vom Klimawandel betroffenen Länder der Welt unternimmt Bangladesch eigene Anstrengungen zur Verringerung der Auswirkungen auf das Weltklima. Das Land hat sich verpflichtet, die Treibhausgasemissionen in den Sektoren Stromerzeugung, Industrie und Verkehr bis 2030 durch externe Unterstützungsmaßnahmen gegenüber einem Business-as-ususal-Szenario um 15 Prozent zu senken. Zudem plant Bangladesch, bis 2021 10 Prozent des gesamten Energiebedarfs durch erneuerbare Energien zu decken und im Vergleich zu 2013 den Energieverbrauch um 15 Prozent zu senken.
Die Förderung von Energieeffizienz und Energieeinsparungen, die Nutzung von integrierten erneuerbaren Energielösungen sowie die Steigerung der Handlungsfähigkeit der relevanten Stakeholder zur Fortführung der Anstrengungen haben für Bangladesch daher oberste Priorität.
Ziel
Die Integration von nachhaltigen Energieerzeugungslösungen wird unter Führung der Behörde für nachhaltige und erneuerbare Energien (Sustainable and Renewable Energy Development Authority, SREDA) durch mehrstufige Capacity-Building-Initiativen gestärkt. Das Bewusstsein für die Notwendigkeit von Energieeffizienz und Energieeinsparungen wird durch eine Kampagne auf landesweiter Ebene geschärft. Die Verbreitung des Programms für Aufdach-Photovoltaik-Anlagen wurde optimiert und die erzielten Fortschritte werden überwacht.
Vorgehensweise
REEEP II zielt darauf ab, mit der Regierung, der Privatwirtschaft und Akteuren der Zivilgesellschaft zusammenzuarbeiten, um die Entwicklung von nachhaltigen Energiesystemen in Bangladesch zu stärken.
Im Rahmen des Projekts werden die genauen Bedarfe für die Organisationsentwicklung von SREDA und der relevanten Stakeholder der Sektoren ermittelt. Außerdem wird die Zusammenarbeit zwischen diesen Akteuren gefördert, um das nationale Ziel für erneuerbare Energien und Energieeffizienz zu erreichen. Darüber hinaus wird das Programm die Regierung bei der Entwicklung von geeigneten energiepolitischen Maßnahmen und Vorschriften unterstützen, damit nachhaltige entwicklungspolitische Wirkungen erzielt werden.
Zur größtmöglichen Nutzung der Solarenergie strebt die Regierung die großflächige Verbreitung netzgekoppelter Aufdach-Photovoltaik-Anlagen im Rahmen des Net-Metering-Konzepts an. Das Projekt unterstützt diese Bemühungen durch die Einrichtung einer speziellen staatlichen Anlaufstelle für Aufdach-PV-Anlagen bei der SREDA sowie eines begleitenden Online-Portals für Verbraucher*innen und Dienstleister. Darüber hinaus sind begleitende Maßnahmen zur Qualitätssicherung und Marktförderung vorgesehen.
Technische Maßnahmen zur Steigerung der Energieeffizienz können nur langfristig wirken, wenn das öffentliche und institutionelle Bewusstsein und Engagement gesichert ist. Das Projekt unterstützt die Ausarbeitung einer nationalen Strategie für die Bewusstseinsbildung hinsichtlich Energieeffizienz und Energieeinsparung. Relevante öffentliche, private und zivilgesellschaftliche Akteure werden aktiv an der daran anschließenden Kampagne beteiligt, um die Wirkung der Initiativen zu maximieren.
Der Gesamtfortschritt der Entwicklung in den Sektoren wird qualitativ und quantitativ überwacht und in das zentrale Überwachungssystem bei SREDA integriert.

 

 
Project description (EN)

Context
Bangladesh is one of the fastest growing economies in the world, headed towards earning middle income status by 2021. Within this same timeframe, Bangladesh is committed to providing electricity for its 160 million citizens. To cater to this demand 24 Gigawatts of electricity will be needed.
Being one of the most climate vulnerable countries in the world, Bangladesh is committed to make its own efforts to reduce impact on global climate change. Bangladesh has pledged to reduce Green House Gas emissions in the power, industry and transport sectors by 15% compared to ‘business-as-usual’ levels within 2030 through external support. By 2021 Bangladesh also aims to generate 10% of its total energy from renewable sources and to save 15% energy consumption compared to 2013 levels.
Promotion of energy efficiency and conservation, utilisation of renewable energy integrated options, and building the capacity of relevant stakeholders to sustain the effort is therefore a priority for Bangladesh.
Objective
Integration of Sustainable Energy options has been strengthened under leadership of Sustainable and Renewable Energy Development Authority (SREDA) with multilevel capacity building initiatives. Awareness regarding energy efficiency and conservation has been increased through a national level campaign. Dissemination of solar rooftop programme has been streamlined and the progress has been monitored.
Approach
REEEP II aims to work with the government, private sector and civil society actors to strengthen sustainable energy development in Bangladesh.
The project will identify specific organisational development needs for the Sustainable and Renewable Energy Development Authority (SREDA) and relevant sector stakeholders and will promote cooperation among those actors to achieve the national target for renewable energy and energy efficiency. Moreover, the programme will support the government to develop conducive energy policies and regulations to sustain developmental impacts.
To maximise the utilisation of solar energy, the government aims to disseminate grid connected solar rooftops at a large scale through net-metering scheme. The project will support this endeavour through facilitating the establishment of a dedicated national rooftop service desk at SREDA and accompanying online portal for consumers and service providers. There will also be accompanied measures on quality assurance and market facilitation.
Technical measures regarding energy efficiency can only be sustained when public and institutional awareness and commitment is ensured. The project will support the formulation of a national awareness raising strategy for energy efficiency and conservation. Relevant public, private and civil society actors will be actively engaged with the subsequent campaign to maximise impact of the initiatives.
The overall progress in sector development will be monitored both qualitatively and quantitively and will be integrated with centralized monitoring system at SREDA.

 





 
