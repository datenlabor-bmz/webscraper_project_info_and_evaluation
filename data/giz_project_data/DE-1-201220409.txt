  




 

Regionale Koordination Frieden und Sicherheit
Regional coordination for Peace and Security
Project details
Project number:2012.2040.9
Status:Projekt beendet
Responsible Organisational unit: 1700 Afrika Überregional und Horn von Afrika
Contact person:Bernadette Schulz bernadette.schulz@giz.de
Partner countries: AFRICA
Summary
Objectives:

Die Unterstützungsleistungen der deutschen EZ für die Afrikanische Friedens- uns Sicherheitsarchitektur (APSA) sind kohärent, effektiv und sichtbar.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:3 800 000 Euro
Financial commitment for this project number:1 610 000 Euro
Cofinancing

not available

 
Previous projects
2009.2260.9Regionaler Koordinator Frieden und Sicherheit
Follow-on projects
not available
 
Term
Entire project:29.10.2004 - 14.10.2016
Actual project:02.11.2012 - 14.10.2016
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Zivile Friedensentw., Krisenpräv. und Konfliktlösung

Evaluation
Afrika überregional: Regionale Koordination deutscher Beiträge zu Frieden und Sicherheit in Afrika überregional. Projektevaluierung: Kurzbericht
 
Project description (DE)

Ausgangssituation
Seit 2005 unterstützt das Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) auf vielfältige Weise den Aufbau der Afrikanischen Friedens- und Sicherheitsarchitektur (APSA). Seit 2008 ergänzt das Auswärtige Amt dieses Engagement durch umfangreiche Fördermaßnahmen. Die meisten Vorhaben der Bundesregierung setzt die GIZ um, in Zusammenarbeit mit der Afrikanischen Union (AU), mehreren afrikanischen Regionalorganisationen und Trainingszentren. Zentrale Aufgaben sind der Aufbau von kontinentalen und regionalen Krisenfrühwarnsystemen, von Kompetenzen für Konfliktmediation sowie der zivilen und polizeilichen Elemente der Afrikanischen Bereitschaftstruppe (African Standby Force, ASF).

Auch die KfW Entwicklungsbank wurde mit der Durchführung zweier Vorhaben zur Unterstützung der Afrikanischen Friedens- und Sicherheitsarchitektur beauftragt. Schwerpunkt ist dabei der Wiederaufbau in Postkonfliktländern. Darüber hinaus ist die deutsche Bundesregierung mit rund 20 Prozent an der umfangreichen Unterstützung der Europäischen Union für Frieden und Sicherheit in Afrika beteiligt. Die Vielzahl der Akteure auf Partner- und Geberseite setzt ein hohes Maß an Abstimmung voraus. Nur so kann der Beitrag der Bundesregierung dem Aufbau einer starken und einheitlichen afrikanischen Friedens- und Sicherheitsarchitektur zugutekommen. Das von der GIZ durchgeführte Vorhaben „Regionale Koordination von Frieden und Sicherheit in Afrika" trägt hierzu bei.

Ziel
Die Unterstützungsleistungen der deutschen Entwicklungszusammenarbeit für die Afrikanische Friedens- und Sicherheitsarchitektur sind kohärent, effektiv und sichtbar.
Vorgehensweise

Das Vorhaben „Regionale Koordination Frieden und Sicherheit in Afrika" berät das BMZ bei der Konzeptentwicklung, der Öffentlichkeitsarbeit und der Profilierung in ressortübergreifenden und internationalen Prozessen. Außerdem vernetzt es die von der GIZ durchgeführten Vorhaben zur Unterstützung der AU und der afrikanischen regionalen Wirtschaftsgemeinschaften beim Aufbau der Friedens- und Sicherheitsarchitektur für den Kontinent. Das Vorhaben bereitet Informationen, Erfahrungen und Erkenntnisse auf und unterstützt die Kooperation zwischen den Vorhaben und mit anderen Organisationen im deutschen und internationalen Raum. Die Wirkungen der deutschen Beiträge zur Entwicklung der Instrumente für die Friedens- und Sicherheitsarchitektur und die darauf gestützten Interventionen von AU und regionalen Wirtschaftsgemeinschaften in Konfliktländern werden langfristig beobachtet. Das Vorhaben erstellte in Zusammenarbeit mit dem Forschungsinstitut European Center for Development Policy Management (ECDPM) zwei Berichte. Neben Informationen zum Stand ausgewählter APSA-Instrumente geben sie einen Gesamtüberblick zu Umfang und Wirkung der Interventionen der AU und Regionalorganisationen in afrikanischen Konfliktländern zwischen 2008 und 2013.

Wirkung – Was bisher erreicht wurde
International und seitens der afrikanischen Partnerorganisationen erfährt der deutsche Beitrag zur Unterstützung der Afrikanischen Friedens- und Sicherheitsarchitektur zunehmende Anerkennung. Dazu tragen die Präsenz auf verschiedenen Ebenen der APSA und das enge Zusammenspiel dieser Ebenen entscheidend bei. Kontinentale und regionale Konfliktfrühwarnsysteme wurden entwickelt, die ihre Daten untereinander austauschen und Politikentscheidungen untermauern helfen. In der SADC (Southern African Development Community) wurden Mediationsstrukturen aufgebaut. Die zivile und polizeiliche Komponente der ASF wird durch den Aufbau einer vernetzten Datenbank für qualifizierte zivile Fachkräfte, die in afrikanischen Friedensmissionen eingesetzt werden können, gestärkt.

 

 
Project description (EN)

Context
The German Federal Ministry for Economic Cooperation and Development (BMZ) has been supporting the development of the African Peace and Security Architecture (APSA) in a variety of ways since 2005. In 2008, the Federal Foreign Office also launched extensive support measures. Most of the German Government's projects and programmes are being implemented by GIZ, in cooperation with the African Union (AU), and various regional African organisations and training centres. Their primary tasks are to establish continental and regional conflict early warning systems, develop conflict mediation capacities and provide civilian and police support to the African Standby Force (ASF).

KfW development bank has similarly been charged with implementing two projects in support of the APSA. These focus on reconstruction in post-conflict countries. In addition, the German Government funds around 20 per cent of the European Union's extensive support for peace and security in Africa. With so many partners and donors involved, it is essential to ensure a high degree of coordination. Only then can the German Government's contribution underpin the development of a strong and united African Peace and Security Architecture. The ‘Regional coordination of peace and security in Africa’ project implemented by GIZ contributes to that cohesion.

Objective
The support provided by German development cooperation for the African Peace and Security Architecture is coherent, effective and visible.

Approach
The GIZ project advises BMZ on strategy development, public relations and thematic positioning in inter-agency and international processes. It also maintains a network of all GIZ projects and programmes that are assisting the AU and the African regional economic communities (RECs) in establishing the APSA. The project processes and makes available information, knowledge and expertise, and promotes cooperation between the various projects, programmes and organisations in Germany and abroad.

The results of the German activities to develop APSA instruments will be monitored over the long term, as will the interventions of the AU and the regional economic communities that use these instruments in conflict countries.

The project has published two reports in cooperation with the research institute, the European Centre for Development Policy Management. As well as information on the current status of specific instruments of the APSA, the reports provide a general overview of the extent and the impacts of interventions by the AU and RECs in Africa’s conflict-affected countries for the period 2008-2013.

Results achieved so far

The German contribution to the APSA is gaining greater recognition internationally and among African partner organisations. This is largely due to our close involvement at various levels in the Architecture, and the intensive interaction between the different levels. Continental and regional conflict early warning systems that share data and help underpin policy decisions are now in place, and mediating structures have been set up in the Southern African Development Community (SADC). The civil and police components of the ASF are being strengthened with the development of a networked database of qualified civilian specialists who can be deployed in African peace missions.

 





 
