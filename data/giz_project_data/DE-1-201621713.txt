  




 

Offener Regionalfonds SOE - Außenwirtschaftsberatung
Open Regional Fund for Southeastern Europe - Foreign Trade
Project details
Project number:2016.2171.3
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Johanna Wohlmeyer johanna.wohlmeyer@giz.de
Partner countries: Western balkans/Eastern Partnership, Albania, Bosnia Herzeg., Montenegro, North Macedonia, Serbia, Kosovo
Summary
Objectives:

Die Rahmenbedingungen für Handel in Südosteuropa sind im Einklang mit den Anforderungen des EU-Annäherungsprozesses verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Sekretariat des Regionalen Kooperationsrats

Financing organisation:

not available

 
Project value
Total financial commitment:34 837 412 Euro
Financial commitment for this project number:7 999 977 Euro
Cofinancing

Europäische Union (EU): 3 000 000Euro


 
Previous projects
2012.2466.6ORF Außenwirtschaftsberatung SOE
Follow-on projects
2019.2353.1Offener Regionalfonds SOE - Aussenwirtschaftsberatung
 
Term
Entire project:23.10.2006 - 30.04.2025
Actual project:01.02.2017 - 30.06.2020
other participants
GFA Consulting Group GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Regionale Handelsabkommen

Evaluation
not available
 
Project description (DE)



Ausgangssituation
Alle am Offenen Regionalfonds für Außenwirtschaftsberatung beteiligten südosteuropäischen Länder streben die EU-Mitgliedschaft an. Eine wichtige Voraussetzung für den EU-Beitritt ist jedoch die Umsetzung des Mitteleuropäischen Freihandelsabkommens (CEFTA), das diese Länder – sowie die Republik Moldawien – 2006 unterzeichnet haben. Das Ziel des Abkommens besteht darin, tarifäre und nichttarifäre Handelshemmnisse abzubauen und so den Warenverkehr zu erleichtern. Zwar wurden in den letzten Jahren sichtbare Fortschritte bei der Umsetzung des CEFTA erzielt, doch ist noch ein gutes Stück Weg zurückzulegen, bevor alle im Abkommen gesetzten Ziele erreicht sind. Hinzu kommt, dass mehrere Länder nur sehr langsam Fortschritte dabei machen, ihre Vorschriften an die Handelsstandards und -richtlinien der EU anzugleichen. Die Folge ist, dass nach wie vor rechtliche und administrative Schranken den Handel behindern. Ebenfalls wichtig für den Handel sind nachfrageorientierte Dienstleistungen für Unternehmen. Das derzeitige Angebot an handelsförderlichen Dienstleistungen wird jedoch den Bedürfnissen der betroffenen exportorientierten Unternehmen nicht gerecht. Der rechtliche und der institutionelle Rahmen reichen folglich zur Förderung des regionalen und internationalen Handels nicht aus, zumal sie in vielen Bereichen immer noch nicht die EU-Konvergenzkriterien erfüllen.
Ziel
Ziel des Projektes ist es, im Zusammenhang mit dem EU-Konvergenzprozess die Rahmenbedingungen für den Handel in Südosteuropa zu verbessern.
Vorgehensweise
Der Offene Regionalfonds Außenwirtschaftsberatung (ORF-FT) unterstützt regionale Initiativen zur Verbesserung der Voraussetzungen für freien Handel. Die vom ORF-FT geförderten Projekte betreffen mindestens drei Länder und werden in Zusammenarbeit mit verschiedenen Akteuren geplant und umgesetzt. Ihr Ziel besteht darin, die Umsetzung von in der Region vereinbarten Reformen zur Handels-erleichterung sowie von Maßnahmen zur Handelsförderung zu verbessern und Unternehmen einen leichteren Zugang zu relevanten Informationen und Know-how zu verschaffen. Gleichzeitig verbessern diese Projekte den öffentlich-privaten Dialog: Zum einen liegt ein Schwerpunkt auf Handelshemmnissen, die eine Priorität für den privatwirtschaftlichen Sektor darstellen; zum anderen wird die Nutzung regionaler Plattformen für die gemeinsame Entwicklung oder Verbreitung von auf Good Practices beruhenden Lösungen gefördert. Wichtige Projektpartner sind das CEFTA (CEFTA-Sekretariat und -Ausschüsse sowie die nationalen CEFTA-Ansprechpartner), Fachministerien, für den Handel relevante staatliche Stellen (z. B. der Zoll) und privatwirtschaftliche Organisationen.
Der von der EU kofinanzierte ORF-FT ist für die Umsetzung des Projekts ‚Unterstützung für Handelserleichterungen zwischen CEFTA-Parteien‘ (‚Support to Facilitation of Trade between CEFTA Parties‘) verantwortlich. Das Ziel dieses Projekts besteht darin, den Handel in der CEFTA-Region zu erleichtern. Dazu sollen in zwei ausgewählten Lieferketten Handelsabläufe vereinfacht und nichttarifäre Maßnahmen mit der stärksten wettbewerbsverzerrenden Wirkung reduziert und letztlich abgeschafft werden. Mit einem produktorientierten Ansatz werden die drei wichtigsten Dimensionen untersucht, die den freien Warenverkehr in der CEFTA-Region beeinträchtigen: 1) nichttarifäre Maßnahmen, die von nicht harmonisierten Rechtsvorschriften und Vorgehensweisen herrühren; 2) redundante und sich überschneidende Anforderungen für die von handelnden Unternehmen einzureichenden Dokumente und Daten; 3) übertriebene und redundante physische Kontrollen an der Grenze.
Wirkungen
Der Offene Regionalfonds Außenwirtschaftsberatung hat in verschiedener Weise zum Abbau von Handelshemmnissen in der CEFTA-Region beigetragen und die Kapazitäten relevanter Akteure gestärkt. Ein besonders bemerkenswertes Ergebnis ist die gestiegene Transparenz im Zusammenhang mit Handelshemmnissen. Dies ist der Einrichtung eines umfassenden CEFTA-Informations- und Berichtssystems zu Handelsanforderungen und -hemmnissen zu verdanken (http://transparency.cefta.int/). Außerdem wurden die handelspolitischen Kapazitäten verstärkt, wie an den handelsdiplomatischen Strategien der einzelnen Länder deutlich wird. Es sind jetzt vorteilhaftere Regeln zur Bestimmung des Ursprungslandes in Kraft, von denen letztlich die Unternehmen in der Projektregion profitieren. Darüber hinaus hat das Projekt dazu beigetragen, regionale handelsbezogene Netzwerke zu stärken, darunter auch Netzwerke und Kooperationsvereinbarungen zwischen Unternehmensverbänden (IT-Branche, ökologische Landwirtschaft), und die Zusammenarbeit zwischen den Marktaufsichtsbehörden in Südosteuropa zu verbessern.

Durch die intensive Zusammenarbeit mit den Handelskammern ist es dem Offenen Regionalfonds Außenwirtschaftsberatung ferner gelungen, das Dienstleistungsangebot für exportorientierte Unternehmen in der Region zu verbessern, etwa durch die Einführung eines Studiengangs im Bereich Exportmanagement in drei der betroffenen Länder.
 

 
Project description (EN)

Context
The South-East European countries taking part in the Open Regional Fund for Foreign Trade Promotion all aspire to become EU members. An important requirement for EU accession however is the implementation of the Central European Free Trade Agreement (CEFTA) which these countries – and the Republic of Moldova – signed in 2006. The objective of the agreement is to eliminate tariff and non-tariff trade barriers and so facilitate the movement of goods. While there has been visible progress in CEFTA implementation over recent years, there is still quite a way to go before all the targets which are set out in the agreement will have been reached. Also, several countries are making very slow progress in harmonising their regulations with EU trade standards and directives. As a result, legal and administrative barriers continue to obstruct trade. Also important for the trading environment are demand-oriented services for companies. However, the current range of trade promotion services falls short of the needs articulated by the relevant export-oriented businesses. Thus, both the legal and the institutional frameworks are not sufficient for boosting regional and international trade and, in many areas, they still fail to comply with EU convergence criteria.
Objective
The project aims to improve framework conditions for trade in South-East Europe as part of the EU convergence process.
Approach
The Open Regional Fund for Foreign Trade Promotion (ORF-FT) supports regional initiatives designed to improve conditions for free trade. Individual ORF FT-backed projects involve at least three countries and are planned and implemented with various actors. Their aim is to improve the rollout of regionally agreed trade facilitation reforms and promotion measures while giving companies better access to relevant information and know-how. At the same time, these projects enhance public-private dialogue by specifically addressing trade barriers prioritised by the private sector and by promoting the use of regional platforms for the joint development or dissemination of good practice-based solutions. Key project partners include CEFTA (CEFTA Secretariat and Committees and the national CEFTA Contact Points), line ministries, state authorities relevant for trade (e.g. customs), and private sector organisations.
Co-financed by the EU, the ORF FT is implementing the project ‘Support to Facilitation of Trade between CEFTA Parties’ whose goal is to facilitate trade in the CEFTA region by simplifying trading procedures and reducing and eventually removing the most distortive non-tariff measures in two selected supply chains. Adopting a product-oriented approach, the project examines the three main dimensions impacting the free trade of goods in the CEFTA region: 1) Non-tariff measures stemming from non-harmonised legislation and practices; 2) Redundant and overlapping document and data submission requirements for traders; 3) Excessive and redundant physical inspections at the border.

Results
The Open Regional Fund for Foreign Trade has contributed to the removal of trade barriers in the CEFTA region in several ways and has strengthened the capacities of relevant actors in the process. One particularly notable achievement is the increased level of transparency surrounding trade barriers. This is thanks to the establishment of comprehensive CEFTA information and reporting system on trade requirements and barriers (http://transparency.cefta.int/). In addition, trade policy capacity has been strengthened, as evidenced by the respective countries’ trade diplomacy strategies. Now more advantageous rules for determining the country of origin are in place which have ultimately benefited the companies in the project region. Moreover, the project has helped strengthen regional trade-related networks, including networks and cooperation arrangements between business associations (IT sector, organic farming), and has also improved cooperation between market surveillance institutions in South-East Europe. Through its intensive work with Chambers of Commerce, the Open Regional Fund for Foreign Trade has succeeded in improving the services available for export-oriented companies in the region, e.g. by introducing an export management course in three of the countries concerned.
 





 
