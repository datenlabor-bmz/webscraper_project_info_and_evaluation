  




 

Studien- und Fachkräftefonds
Studies and Experts Fund
Project details
Project number:1995.3503.0
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Nicole Lindau nicole.lindau@giz.de
Partner countries: Malaysia, Malaysia
Summary
Objectives:

Vorbereitung und Prüfung von Vorhaben der Technischen Zusammenarbeit (TZ), Finanzierung von Studien, Gutachten sowie Durchführung von TZ-Maßnahmen geringen Umfangs.

Client:

BMZ

Project partner:

Ministry of Plantation Industries and Commodities

Financing organisation:

not available

 
Project value
Total financial commitment:7 828 059 Euro
Financial commitment for this project number:3 349 097 Euro
Cofinancing

Ministry of Agriculture, Nature and Food Quality/Niederlande: 300 000Euro


 
Previous projects
1988.2066.4Studien- und Fachkräftefonds
Follow-on projects
not available
 
Term
Entire project:05.11.1979 - 31.12.2017
Actual project:01.01.1995 - 31.12.2017
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
