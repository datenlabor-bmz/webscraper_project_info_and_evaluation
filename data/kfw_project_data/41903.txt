Unterstützung für freiwillige syrische Lehrer in der Türkei (Phase IV)

Ziel des FZ-Vorhabens ist es, einen verbesserten Zugang zu Schulbildung für syrische Flüchtlingskinder in der Türkei zu ermöglichen. Das Vorhaben wird von UNICEF im Rahmen der übergreifenden Initiative "Preventing a 'lost generation' in Turkey" umgesetzt. Aus Projektmitteln der Phase IV werden für das Schuljahr 2019/2020 monatliche Anreizzahlungen für ca. 12.000 syrische Lehrkräfte bezahlt. Dadurch profitieren rund 200.000 Schulkinder von dem Vorhaben. Das Vorhaben ist Teil der Beschäftigungsoffensive Nahost im Rahmen der Sonderinitiative "Fluchtursachen bekämpfen, Flüchtlinge reintegrieren".

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde entsprechend der Nachhaltigkeitsrichtlinie der KfW Entwicklungsbank in die Umwelt- und Sozialrisikokategorie C eingeordnet, da vom Vorhaben voraussichtlich keine oder nur geringe nachteilige Auswirkungen und Risiken auf Umwelt und soziale Belange ausgehen. Es ist daher kein spezifischer Umwelt- und Sozialmanagementplan erforderlich. Der Partner wurde zur Einführung eines Beschwerdemechanismus verpflichtet und muss die KfW unverzüglich über umwelt- und sozialrelevante besondere Vorkommnisse informieren.

Land / Region / Institution Türkei Nummer 41903 Schwerpunkt Sonstige Sektor 72010 - Materielle Nothilfe USV-Kategorie C Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 40 Mio. EUR Status abgeschlossen Auftraggeber BMZ Projektpartner UNITED NATIONS CHILDREN'S FUND Zuständige Abteilung Südosteuropa und Türkei
