  




 

Stärkung der Klimaresilienz
Strengthening climate adaptation and climate resilience, Pakistan
Project details
Project number:2021.2124.2
Status:laufendes Projekt
Responsible Organisational unit: 2B00 Asien II
Contact person:Dr. Baptiste Chatre baptiste.chatre@giz.de
Partner countries: Pakistan, Pakistan
Summary
Objectives:

Klimaanpassung und Klimarisikomanagement mit spezifischem Fokus auf vulnerablen Bevölkerungsgruppen (insb. Frauen) sind in ausgewählten Provinzen sowie auf nationaler Ebene verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Climate Change

Financing organisation:

not available

 
Project value
Total financial commitment:10 000 000 Euro
Financial commitment for this project number:10 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:30.11.2021 - 30.04.2025
Actual project:01.11.2021 - 30.04.2025
other participants
ARGE Oxford Policy Management
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektziel-Ebene: Projekt zielt vor allem auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Umweltpolitik und -verwaltung

Evaluation
not available
 




 
