  




 

Deutsch-Indische CSR Initiative
Indo-German CSR Initiative
Project details
Project number:2003.2457.4
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Wolfgang Leidig wolfgang.leidig@giz.de
Partner countries: India, India
Summary
Objectives:

Öffentliche und private Institutionen setzen die Nationalen Richtlinien zur verantwortungsvollen Unternehmensführung und die nationale CSR-Agenda um und greifen auf die Leistungen des IICA als führende nationale Plattform zurück.

Client:

BMZ

Project partner:

Ministry of Corporate Affairs

Financing organisation:

not available

 
Project value
Total financial commitment:2 600 000 Euro
Financial commitment for this project number:2 600 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:23.10.2007 - 05.02.2015
Actual project:01.01.2008 - 05.02.2015
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Geschäftspolitik und -verwaltung

Evaluation
not available
 




 
