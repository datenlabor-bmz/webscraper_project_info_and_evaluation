Programm ländliche Entwicklung

Das "Programm Ländliche Infrastruktur" (Rural Infrastructure Programme) umfasst den schnell umsetzbaren und wirksamen Aufbau ländlicher Infrastruktur durch Ausbau ländlicher Wege und kleinerer Infrastrukturmaßnahmen, wie z. B. Märkte, im südlichen Shan-Staat Myanmars, beginnend mit dem Distrikt Taunggyi. Durch eine flankierende Begleitmaßnahme sollen primär Themen wie Instandhaltung, Planungs- und Vergabeverfahren eingeübt werden. Eine Koordination mit den TZ-Maßnahmen ist vorgesehen.

Ziel der Maßnahme ist die nachhaltige und ganzjährige Nutzung der ausgebauten ländlichen Wege und der anderen Infrastrukturmaßnahmen durch die Zielgruppe zur besseren Erreichbarkeit von Märkten, Schulen, Gesundheitseinrich-tungen, Arbeitsstätten.

Zielgruppe sind die verschiedenen Straßennutzer und die im Einzugsgebiet der Straßen lebende ländliche, oftmals arme Bevölkerung im Taunggyi-Distrikt, die überwiegend ethnischen Minderheiten angehört.

Das Vorhaben soll einen Beitrag zur langfristigen Erschließung ländlicher Räume leisten und den Zugang zu Märkten, beruflicher Qualifizierung, Finanzdienstleistungen, Kliniken und Bildungseinrichtungen verbessern. Durch die Förderung von Handel und Austausch können Beschäftigungsmöglichkeiten und Einkommen geschaffen und damit die marktwirtschaftliche Entwicklung gefördert bzw. erst möglich gemacht werden. Indem ethnische Gruppen einen Zugang zu lokalen Wirtschaftszentren erhalten, wird ein Beitrag zu nationalen Aussöhnung und Friedenssicherung in Myanmar geleistet.

Land / Region / Institution Myanmar Nummer 31869 Schwerpunkt Transport und Kommunikation Sektor 21020 - Straßenverkehrswesen Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber Eigenbeitrag d. Landes/Own contribution of country Deutscher Finanzierungsbeitrag 7 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner MINISTRY OF LIVESTOCK, FISHERI ES AND RURAL DEVELOPMENT Zuständige Abteilung Ostasien und Pazifik
