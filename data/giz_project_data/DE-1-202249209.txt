  




 

Verbesserung des Bevölkerungsschutzes in kommunalen Partnerschaften mit der Ukraine
Improving civil protection in municipal partnerships with Ukraine
Project details
Project number:2022.4920.9
Status:laufendes Projekt
Responsible Organisational unit: 3930 Region Ost
Contact person:Maria Koenig maria.koenig@giz.de
Partner countries: Ukraine
Summary
Objectives:

Der kommunale Bevölkerungsschutz in der Ukraine ist im Rahmen von kommunalen Partnerschaften zwischen deutschen und ukrainischen Städten und Gemeinden gestärkt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ohne politischen Träger - Direkt mit Bevölkerung oder Nichtregierungsorganisationen (NGO)

Financing organisation:

not available

 
Project value
Total financial commitment:16 200 000 Euro
Financial commitment for this project number:16 200 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:21.07.2022 - 31.03.2024
Actual project:01.08.2022 - 31.03.2024
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
Armutsorientierungnot available
CRS code
Materielle Nothilfe

Evaluation
not available
 




 
