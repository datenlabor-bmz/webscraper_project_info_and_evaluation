  




 

Sektorprogramm Agenda 2030, Armutsbekämpfung und Reduzierung von Ungleichheit
2030 Agenda for Sustainable Development, Poverty and Inequality
Project details
Project number:2021.2004.6
Status:laufendes Projekt
Responsible Organisational unit: G410 Global Policy
Contact person:Dr. Christiane Schuppert christiane.schuppert@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh., ZZZ
Summary
Objectives:

Unterstützung des Bundesministeriums für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ), die Agenda 2030 für nachhaltige Entwicklung in Deutschland, mit Partnerländern und auf internationaler Ebene umzusetzen. Der Fokus liegt auf der Umsetzung von SDG 1 und 10 sowie auf den Prinzipien integrierter Ansatz und Leave no one behind (LNOB).

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:37 600 000 Euro
Financial commitment for this project number:17 877 777 Euro
Cofinancing

not available

 
Previous projects
2019.2265.7Sektorvorhaben Agenda 2030 für nachhaltige Entwicklung
Follow-on projects
not available
 
Term
Entire project:10.09.2012 - 29.02.2024
Actual project:01.03.2021 - 29.02.2024
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
