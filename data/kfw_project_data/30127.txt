Programm zur Förderung der beruflichen Bildung

Das Programm zur Förderung der beruflichen Bildung und seine Begleitmaßnahme dienen dem Ziel, die Ausbildungskapazitäten von ausgewählten privaten und öffentlichen Berufsschulen zu erweitern und die Qualität der dort angebotenen Ausbildung zu verbessern. Durch einen erhöhten Praxisbezug und eine verstärkte Ausrichtung der Ausbildung am Bedarf des Arbeitsmarkts werden die Beschäftigungs- und Einkommensmöglichkeiten der dort tätigen Schüler verbessert.

Im Rahmen des Vorhabens werden zunächst in den drei Provinzen Inhabane, Manica und Sofala private und öffentliche Berufsbildungsinstitutionen, welche Curricula gemäß kompetenz-basierter Standards anbieten und/oder einführen möchten, kriterienbasiert ausgewählt, rehabilitiert bzw. erweitert und bedarfsgerecht ausgestattet. Des Weiteren werden Maßnahmen zur Stärkung des Managements der geförderten Berufsschulen finanziert.

Zielgruppe des Vorhabens sind jugendliche Schulabgänger der 10. Klasse (im Alter von ca. 16 Jahren), die erfolgreich die ersten drei Jahre der Sekundarstufe absolviert haben sowie das Management und die Angestellten der geförderten Berufsschulen.

Der FZ-Anteil beläuft sich nach Aufstockung auf 16,3 Mio. EUR mit einer geschätzten Gesamtlaufzeit von 5 Jahren. Parallel zu den durch das Vorhaben finanzierten investive Maßnahmen sind zusätzliche Mittel i.H.v. 2,0 Mio. EUR für eine Begleitmaßnahme (BM) zur weiteren Stärkung der Kapazitäten der geförderten Berufsschulen vorgesehen (BMZ-Nr. 2016 70 280). Darüber hinaus steht eine Vorratsprüfung für eine mögliche zweite Phase des Vorhabens mit einem FZ-Volumen von bis zu 28 Mio. EUR weiterhin zur Verfügung.

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde entsprechend der Nachhaltigkeitsrichtlinie der KfW Entwicklungsbank in die Umwelt- und Sozialrisikokategorie C eingeordnet, da vom Vorhaben voraussichtlich keine oder nur geringe nachteilige Auswirkungen und Risiken auf Umwelt und soziale Belange ausgehen. Es ist daher kein spezifischer Umwelt- und Sozialmanagementplan erforderlich. Der Partner wurde zur Einführung eines Beschwerdemechanismus verpflichtet und muss die KfW unverzüglich über umwelt- und sozialrelevante besondere Vorkommnisse informieren.

Land / Region / Institution Mosambik Nummer 30127 Schwerpunkt Bildung Sektor 11330 - Berufliche Bildung USV-Kategorie C Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber Eigenbeitrag d. Landes/Own contribution of country Deutscher Finanzierungsbeitrag 18,3 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner MINISTÉRIO DA CIÊNCIA E TECNOL OGIA Zuständige Abteilung Südliches Afrika
