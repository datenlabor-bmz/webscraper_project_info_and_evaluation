  




 

Armutsorientierte Kommunalentwicklung und Dezentralisierung III
Communal Development and Decentralisation
Projet de Développement Communal inclusif et de Décentralisation
Project details
Project number:2021.2078.0
Status:laufendes Projekt
Responsible Organisational unit: 1600 Westafrika 2, Madagaskar
Contact person:Anja Heuft anja.heuft@giz.de
Partner countries: Madagascar, Madagascar
Summary
Objectives:

Partnerkommunen nehmen ihre Rolle als Akteure der armutsorientierten lokalen Entwicklung, in Zusammenarbeit mit der Zivilgesellschaft und staatlichen Institutionen, effektiv wahr.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministère de l'Intérieur et de la Décentralisation

Financing organisation:

not available

 
Project value
Total financial commitment:40 420 000 Euro
Financial commitment for this project number:6 250 000 Euro
Cofinancing

not available

 
Previous projects
2017.2075.4Armutsorientierte Kommunalentwicklung und Dezentralisierung
Follow-on projects
not available
 
Term
Entire project:02.06.2015 - 31.12.2025
Actual project:01.01.2023 - 31.12.2025
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Dezentralisierung und Förderung subnatio Gebietskörpers

Evaluation
not available
 




 
