  




 

Unterstützung der grenzüberschreitenden Wasserkooperation im Nilbecken
Support to transboundary water cooperation in the Nile Basin
Project details
Project number:2016.2083.0
Status:Projekt beendet
Responsible Organisational unit: 1500 Ostafrika
Contact person:Dr. Malte Grossmann malte.grossmann@giz.de
Partner countries: AFRICA, Burundi, Dem. Rep. Congo, Egypt, Ethiopia, Kenya, Rwanda, Sudan, South Sudan, Tanzania, Uganda
Summary
Objectives:

Der Beitrag der Nilbeckeninitiative (NBI) zur Konsensbildung der Anrainerstaaten über eine nachhaltige und kooperative Bewirtschaftung und Entwicklung der Wasserressourcen des Nilbeckens ist gestärkt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Nil Becken Initiative

Financing organisation:

not available

 
Project value
Total financial commitment:32 867 751 Euro
Financial commitment for this project number:13 475 000 Euro
Cofinancing

Europäische Union (EU): 10 000 000Euro


 
Previous projects
2013.2249.4Unterstützung der grenzüberschreitenden Wasserkooperation im Nilbecken
Follow-on projects
2020.2287.9Unterstützung der grenzüberschreitenden Wasserkooperation im Nilbecken
 
Term
Entire project:06.12.2001 - 30.06.2025
Actual project:01.07.2016 - 30.10.2022
other participants
Sydro Consult GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Wassersektorpolitik und -verwaltung

Evaluation
not available
 
Project description (DE)

Ausgangssituation

Mit einer Länge von 6.695 Kilometern ist der Nil der längste Fluss der Erde. Sein Einzugsgebiet umfasst rund ein Zehntel der Fläche Afrikas und beherbergt fast ein Viertel der afrikanischen Bevölkerung, für die der Fluss das mit Abstand wichtigste Süßwasserreservoir der Region ist.

Die Nachfrage nach Wasser in der gesamten Region steigt aufgrund der wirtschaftlichen Entwicklung und des Bevölkerungswachstums stetig, die Wasserressourcen werden jedoch bereits heute intensiv genutzt. Der klimatische Wandel und Veränderungen der Landnutzung beinträchtigen darüber hinaus die Verfügbarkeit der Ressource. Das Nilbecken gilt daher als eines der besonders konfliktanfälligen Flussgebiete, bis heute herrscht unter den Anrainerstaaten keine Einigkeit über die Wasserverteilung.

Die Anrainerstaaten des Nils – heute sind dies Ägypten, Äthiopien, Burundi, DR Kongo, Kenia, Ruanda, Südsudan, Sudan, Tansania, Uganda – gründeten 1999 die Nile Basin Initiative (NBI), um den Dialog untereinander zu fördern und gemeinsame Projekte in der Wasserbewirtschaftung voranzutreiben.

Deutschland ist, in enger Koordination mit anderen Gebern, langjähriger Partner der NBI und ihrer Mitgliedsstaaten. Seit 2002 unterstützt die GIZ die Initiative im Auftrag des Bundesministeriums für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ).

Ziel

Der Beitrag der Nile Basin Initiative (NBI) zur Konsensbildung der Anrainerstaaten über eine nachhaltige und kooperative Bewirtschaftung und Entwicklung der Wasserressourcen des Nilbeckens ist gestärkt.

Vorgehensweise

In dem gemeinsamen Vorhaben unterstützt die GIZ die NBI durch Fach- und Prozessberatung in acht eng verknüpften Handlungsfeldern:

1. Ein Prozess mit den Mitgliedsstaaten wird gefördert, um eine gemeinsame Analyse der aktuellen und der projizierten, zukünftigen Wasserbilanz für das Nilbecken umzusetzen. Das Vorhaben trägt so dazu bei, Optionen für ein verbessertes beckenweites Wasserressourcenmanagement zu entwickeln.

2. Das Vorhaben schafft durch einen wissensbasierten und kooperativen Planungsprozess die Grundlagen für eine gemeinsame Nilbeckenplanung.

3. The Nilbeckeninitiative bereitet ein beckenweites, sektorübergreifendes Investitionsprogramms auf, und leistet damit einen Beitrag zur Wasser-, Energie- und Ernährungssicherheit in der Region.

4. Die Anwendung der bereits existierenden Politiken, Richtlinien und Standards in den Mitgliedsländern wird gestärkt.

5. Die Länder des östlichen Nils schaffen mit Unterstützung des Vorhabens eine Grundlage für die gemeinsame und optimierte Bewirtschaftung von Staudammkaskaden.

6. Der Aufbau eines beckenweiten hydrologischen Monitoringsystems leistet einen Beitrag zum Informationsaustausch zwischen den Ländern des Nilbeckens.

7. Das Vorhaben trägt dazu bei, die öffentliche Meinung in den Mitgliedsländern im Sinne eines kooperativen Wasserressourcenmanagements positiv zu entwickeln und somit Spielraum für politische Entscheidungsträger zu eröffnen. Das Medienteam des Nilbeckensekretariats wird dabei unterstützt, auf die aktuell im Nilbecken diskutierten Themen vermehrt zu reagieren.

Daneben stärkt das Vorhaben individuelle Kapazitäten für die Nilkooperation und organisatorische Kapazitäten der NBI-Zentren.

Wirkung

Mit Unterstützung der deutschen internationalen Zusammenarbeit und anderer Geber wurde aus einer politischen Initiative eine etablierte, geschätzte Plattform für den Dialog der Anrainer geschaffen.

Mit dem Sekretariat ist eine leistungsfähige Organisation entstanden, die die Dienstleistungen einer Flussgebietsorganisation erbringen kann: Sie unterstützt den Austausch wasserwirtschaftlicher Informationen, kann hydrologische und sozioökonomische Szenarien für die Planung bereitstellen und hat mit den Anrainerstaaten für viele Themenfelder des grenzüberschreitenden Wassermanagements gemeinsame Prinzipien und Strategien vereinbaren können. Diese Leistungsfähigkeit und die Kompetenzen werden nun verstärkt für die gemeinsame Entscheidungsfindung auf regionaler Ebene genutzt und bei der nationalen Planung der Mitgliedsstaaten angewendet.

Die Nile Basin Initiative hat seit ihrer Gründung zur Vertrauensbildung zwischen den Anrainerstaaten und zur Konfliktprävention wesentlich beigetragen. Heute arbeiten zahlreiche Wasserexperten aller beteiligten Länder gemeinsam an regionalen Lösungen zum Nutzen aller Mitgliedsstaaten. Für die Teileinzugsgebiete koordiniert die NBI Investitionen in eine regional abgestimmte Infrastruktur und Maßnahmen zum Schutz des Einzugsgebiets in einem Gesamtumfang von rund 1,4 Milliarden US-Dollar. Dabei werden beispielsweise die regionalen Stromnetze verknüpft, um den Nutzen der Wasserkraftentwicklung in einem Teil des Beckens mit anderen Anrainern teilen zu können und so den Konflikt um die Aufteilung der Wassermengen zu entschärfen.
 

 
Project description (EN)

Context

The Nile is 6,695 kilometres long, making it the longest river on earth. Its catchment area covers around a tenth of the surface of Africa, and is home to almost a quarter of the African population. For these people, the river is by far the most important freshwater reservoir in the region.

Demand for water in the entire region is constantly increasing due to economic development and population growth. However, water resources are already being intensively utilised, and climate change and land use changes are also having a negative impact on water availability. The Nile Basin is therefore classed as one of the most conflict-prone river basins. The riparian states have not yet reached any agreement on water allocation.

In 1999, the Nile Basin states, currently comprising Burundi, the Democratic Republic of the Congo, Egypt, Ethiopia, Kenya, Rwanda, South Sudan, Sudan, Tanzania and Uganda, founded the Nile Basin Initiative (NBI). Their aim was to facilitate dialogue between themselves, and advance joint water management projects.

Working in close coordination with other donors, Germany has been a partner to NBI and its member states for many years. On behalf of the German Federal Ministry for Economic Cooperation and Development (BMZ), GIZ has been working with NBI since 2002.

Objective

The Nile Basin Initiative (NBI) has greater capacity to contribute to consensus building in the Nile Basin through the sustainable and cooperative management and development of water resources.

Approach

In this joint project, GIZ supports NBI by providing technical and process advice in eight closely linked fields of activity:

1. Supporting dialogue and trust building among Nile Basin states.
Facilitating a process that enables member states to conduct a joint analysis of the current and projected future water balance for the Nile Basin, thus contributing to the development of options for better basin-wide water resource management.

2. Laying the foundations for joint Nile Basin planning by way of a knowledge-based and cooperative planning process.

3. Developing a Basin-wide, cross-sector investment programme, thereby contributing to better water, energy and food security in the region.

4. Strengthening the application of existing policies, guidelines and standards in member states.

5. Supporting Eastern Nile countries in laying the foundations for optimised joint management of dam cascades.

6. Developing a Basin-wide hydrological monitoring system, thus contributing to the exchange of information among Nile basin states.

7. Supporting efforts to positively influence public opinion in member states with the aim of encouraging cooperative water resource management. This opens up scope for political decision-makers. The media team for the Nile Basin Secretariat is receiving support in responding more effectively to issues currently being discussed in the Nile Basin.

The project also aims to strengthen individual capacities for cooperation in the Nile Basin as well as organisational capacities at NBI centres.

Results achieved so far

With the support of German international cooperation and other donors, a political initiative has been transformed into an established and well regarded platform for dialogue between the Nile Basin riparian states.

With the NBI Secretariat, an effective organisation that is capable of delivering the services expected from a river basin organization has been built: it is supporting the exchange of information on water resources, is able to provide hydrological and socio-economic planning scenarios, and has reached an agreement with the Nile Basin riparian states on common principles and strategies for many of the issues relating to transboundary water management. The organisation’s capabilities and skills are now being utilised more widely at regional level to assist in joint decision-making, and are also being applied in the member states’ own national planning processes.

Since its creation, the Nile Basin Initiative has significantly contributed to building trust, and preventing conflict among Nile Basin states. Numerous water experts from all participating countries are now working together to find regional solutions that will benefit all member states. At sub-basin level, NBI is coordinating investments in regionally coordinated infrastructure and watershed management projects with total value of around USD 1.4 billion. This includes interconnecting regional power networks so that the benefits of hydropower development in one part of the basin can be shared with other Nile Basin riparian states, thereby defusing conflicts over water allocation.
 





 
