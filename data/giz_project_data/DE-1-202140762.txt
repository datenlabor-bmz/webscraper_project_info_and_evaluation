  




 

Soziale und wirtschaftliche Teilhabe von Rückkehrenden, Binnenvertriebenen und aufnehmender Bevölkerung (SEPIN)
Social and economic participation of returnees, internally displaced persons, and host communities in northeast Nigeria
Project details
Project number:2021.4076.2
Status:laufendes Projekt
Responsible Organisational unit: 1600 Westafrika 2, Madagaskar
Contact person:Beau Davis beau.davis@giz.de
Partner countries: Nigeria
Summary
Objectives:

Die wirtschaftliche und soziale Teilhabe von Rückkehrenden, Binnenvertriebenen und vulnerablen Mitgliedern aufnehmender Gemeinden ist unter Berücksichtigung genderbezogener Rollen verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Federal Ministry for Budget and National Planning

Financing organisation:

not available

 
Project value
Total financial commitment:16 830 000 Euro
Financial commitment for this project number:16 830 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:09.12.2021 - 31.12.2027
Actual project:01.08.2022 - 31.12.2027
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt zielt vor allem auf Gleichberechtigung ab
Armutsorientierungnot available
CRS code
Demokratische Teilhabe und Zivilgesellschaft

Evaluation
not available
 




 
