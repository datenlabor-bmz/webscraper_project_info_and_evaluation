  




 

Inklusive und nutzer*innenzentrierte digitale Dienstleistungen in Jordanien
Inclusive and User-centered e-Services in Jordan
Project details
Project number:2019.4103.8
Status:laufendes Projekt
Responsible Organisational unit: 3300 Naher und Mittlerer Osten 1
Contact person:Jasmin Sadoun jasmin.sadoun@giz.de
Partner countries: Jordan
Summary
Objectives:

Die Effizienz, NutzerInnenorientierung und Inklusivität kommunaler eServices sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry for Digital Economy and Entrepreneurship, MoDEE

Financing organisation:

not available

 
Project value
Total financial commitment:4 000 000 Euro
Financial commitment for this project number:4 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:20.12.2019 - 30.04.2024
Actual project:01.01.2020 - 30.04.2024
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Politik und Verwaltung in Bezug auf den öffentl Sektor

Evaluation
not available
 




 
