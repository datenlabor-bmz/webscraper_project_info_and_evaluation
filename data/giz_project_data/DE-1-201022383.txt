  




 

Programm zur Reform der Bildungssysteme in ZAS
Reform of Educational Systems in Central Asia
RBS
Project details
Project number:2010.2238.3
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Dr. Holger Ehlers holger.ehlers@giz.de
Partner countries: C. Asia (indiv)
Summary
Objectives:

Gestärkte Nationale Kapazitäten sind in der Lage, Reformen zur Verbesserung von Qualität und Relevanz des Bildungssektors einzuleiten, um- zusetzen und durch Monitoring zu begleiten.

Client:

BMZ

Project partner:

Bildunsministerien der ZAS Länder

Financing organisation:

not available

 
Project value
Total financial commitment:13 648 757 Euro
Financial commitment for this project number:6 500 000 Euro
Cofinancing

not available

 
Previous projects
2008.2192.6Programm zur Förderung der Grundbildung in Zentralasien
Follow-on projects
2013.2221.3Reform der Bildungssysteme in Zentralasien
 
Term
Entire project:29.12.2008 - 31.12.2016
Actual project:01.01.2011 - 03.07.2014
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Bildungspolitik und Verwaltung im Bildungswesen

Evaluation
not available
 




 
