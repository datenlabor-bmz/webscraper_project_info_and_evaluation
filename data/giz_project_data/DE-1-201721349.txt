  




 

Finanzielle Inklusion in der MENA-Region (FIMENA)
Financial Inclusion in the MENA Region (FIMENA)
Project details
Project number:2017.2134.9
Status:Projekt beendet
Responsible Organisational unit: 3600 Nordafrika
Contact person:Hayder Al-Bagdadi hayder.al-bagdadi@giz.de
Partner countries: AFRICA, Egypt, Jordan, Palestinian Territories, Tunisia
Summary
Objectives:

Die Voraussetzungen für beschäftigungswirksame Investitionen von Kleinst-, kleinen und mittleren Unternehmen (KKMU) sowie Startups in der MENA-Region sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Arabischer Währungsfonds

Financing organisation:

not available

 
Project value
Total financial commitment:5 000 000 Euro
Financial commitment for this project number:5 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:29.12.2017 - 02.01.2023
Actual project:01.01.2018 - 02.01.2023
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Finanzsektorpolitik und -verwaltung

Evaluation
not available
 




 
