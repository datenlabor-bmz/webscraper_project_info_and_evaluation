  




 

Schaffung von Rechtssicherheit und Gleichbehandlung im Einwohnerwesen
Good Governance in Population Administration
Project details
Project number:2008.2096.9
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Joerg-Werner Haas joerg.haas@giz.de
Partner countries: Indonesia
Summary
Objectives:

Dienstleistungen im Einwohnerwesen werden auf der Grundlage der vom Innenministerium landesweit neu eingefuehrten Standards bereitgestellt.

Client:

BMZ

Project partner:

Innenministerium

Financing organisation:

not available

 
Project value
Total financial commitment:6 765 443 Euro
Financial commitment for this project number:3 265 443 Euro
Cofinancing

not available

 
Previous projects
2005.2140.1Schaffung von Rechtssicherheit und Gleichbehandlung im Einwohnerwesen
Follow-on projects
not available
 
Term
Entire project:27.03.2002 - 28.02.2013
Actual project:24.11.2008 - 28.02.2013
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Politik und Verwaltung in Bezug auf den öffentl Sektor

Evaluation
not available
 




 
