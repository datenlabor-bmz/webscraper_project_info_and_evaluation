  




 

Intelligente Netze für Erneuerbare Energien und Energieeffizienz
Smart grids for renewable energies and energy efficiency
Project details
Project number:2015.2081.6
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Markus Bissel markus.bissel@giz.de
Partner countries: Viet Nam, Viet Nam
Summary
Objectives:

Verbraucher und Umwelt profitieren von einer ökonomischen, ökologischen und sozial gerechten Bereitstellung und Nutzung von Energie in Vietnam

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Industry and Trade

Financing organisation:

not available

 
Project value
Total financial commitment:5 500 000 Euro
Financial commitment for this project number:5 500 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:24.04.2017 - 25.07.2022
Actual project:01.07.2017 - 25.07.2022
other participants
Arge Moeller & Poeller Engineering
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
Armutsorientierungnot available
CRS code
Energiepolitik und -verwaltung

Evaluation
not available
 
Project description (DE)

Ausgangssituation
Vietnam ist eines der am schnellsten wachsenden Länder Asiens. Die wirtschaftliche Entwicklung leidet jedoch bereits unter Engpässen in der Stromversorgung. Um diesen zu begegnen, müssen Stromerzeugung, Netzkapazitäten und Energieeffizienz (EE) gesteigert werden. Der Entwicklungsplan für den Stromsektor sieht eine Steigerung der Erzeugungskapazitäten vor: Von knapp 39.000 Megawatt (MW) im Jahr 2015 auf über 60.000 MW im Jahr 2020 und 129.500 MW im Jahr 2030. Dafür sind Investitionen in Höhe von umgerechnet 125 Milliarden Euro nötig. Ein Viertel davon ist dafür vorgesehen, Stromnetze zu modernisieren und auszubauen.

Ende 2012 wurde die Smart Grid Entwicklungsstrategie (Smart Grid Road Map - SGRM) verabschiedet. Sie legt fest, dass mit dem Netzausbau die Digitalisierung des Netzes einhergeht. Das bedeutet, dass ein intelligentes Stromversorgungssystem – ein sogenanntes Smart Grid – entsteht. So werden netzbedingte Versorgungsstörungen verringert und Effizienzpotenziale des Stromnetzes gehoben.

Bislang wurde der Netzbetrieb automatisiert und die Zuverlässigkeit des Netzes durch schnellere Fehlererkennung und -behebung erhöht. Dies bringt die teilweise veralteten Stromnetze auf den heutigen Stand der Automatisierung.

Digitalisierung und Flexibilisierung beim Um- und Ausbau von Netzen sollen die technischen Grundlagen für deutlich höhere Anteile von Strom aus volatilen Erneuerbaren Energien (vRE) schaffen und die Voraussetzungen für effiziente Energie verbessern. Dieses Vorgehen ist eine klimafreundlichere Alternative zu der derzeit noch Kohlenstoff-intensiven Stromausbauplanung Vietnams.

Ziel
Vietnamesische Stromnetz-Experten nutzen verbesserte Kapazitäten, um ein intelligentes Stromversorgungssystem (Smart Grid) zu entwickeln, das erneuerbaren Energien und der Energieeffizienz zuträglich ist.

Vorgehensweise
Das Vorhaben fördert die partizipative Entwicklung eines intelligenten Stromversorgungssystems in Vietnam. Private und öffentliche Akteure werden in ihren Beteiligungsprozessen an der Ausarbeitung von regulatorischen Rahmenvorgaben unterstützt.

Das Vorhaben hat drei zentrale Handlungsfelder:

Regulatorische Rahmenbedingungen
Der Regulierungsbehörde Electricity Regulatory Authority of Vietnam (ERAV) sollen Entscheidungsgrundlagen für den regulatorischen Rahmen für ein RE und EE zuträgliches Smart Grid vorliegen. Die Kompetenzen der Regulierungsexperten, die sich bei ERAV mit der Fortschreibung der Smart Grid Road Map und der Formulierung entsprechender Regulierungsvorschriften beschäftigen, sind gestärkt. Fachlich geht es darum, international bewährte Regelungen zu verstehen, ihre Notwendigkeit anzuerkennen und sie an Vietnam-spezifische Gegebenheiten anzupassen.
Aufbau von Fach- und Methodenwissen für private und staatliche Akteure (Human Capacity Development)
Weiterbildung und fachlicher Austausch im Rahmen eines aus vietnamesischen Fach- und Führungskräften bestehenden Smart Grid Wissensnetzwerks sollen etabliert werden. Durch Vermittlung, Ausbau und Management von Smart Grid Fachwissen wird die Vernetzung zwischen Politik, staatlicher Verwaltung, Zivilgesellschaft, Wirtschaft, sowie Forschung und Lehre gefördert.
Technologiekooperation
Experten aus Regulierung, Wirtschaft sowie Forschung und Lehre können die Eignung von Technologielösungen für ein RE und EE zuträgliches intelligentes Stromversorgungssystem besser einschätzen. Sie bekommen einen fundierten Einblick, welche international verfügbaren Technologien unter vietnamesischen Gegebenheiten funktionieren. Dies erfolgt in einem dreistufigen Ansatz. Im ersten Schritt werden einzelne Technologien theoretisch bewertet, dann werden einzelne Technologien pilothaft praktisch erprobt und aus mehreren Technologien bestehende Systemkonfigurationen beurteilt.

Wirkungen
Das Projekt verbessert die Wirtschaftlichkeit, Zuverlässigkeit und Nachhaltigkeit von netzbezogenen Serviceleistungen. Die Integration von Erneuerbaren Energien in das existierende Stromnetz wird erleichtert.

Die regulatorischen Rahmenbedingungen werden verbessert, die Expertenkenntnisse im Bereich Smart Grids werden vertieft. Progressive Smart Grid Lösungen werden eingeführt. Diese umfassen eine fortgeschrittene Infrastruktur für Netto-Messung und Nachfragesteuerung.

Um der wachsenden Stromnachfrage der vietnamesischen Wirtschaft und Bevölkerung entgegenzukommen, trägt das Projekt zur Entwicklung eines flexiblen, responsiven und nachhaltigen Stromversorgungssystems bei. 

 
Project description (EN)

Context
Viet Nam is one of the fastest growing countries in Asia. The economic development is restrained by shortages in power supply. Increasing power generation, grid capacity and energy efficiency are necessary to solve this problem. Hence the Vietnamese government is planning to increase production capacities from approximately 39,000 MW in 2015 to 60,000 in 2020 and 1,290,500 MW in 2030. The required investment volume is estimated to reach 125 billion Euros, of which one quarter is designated to the modernization and expansion of the power grids.

Grid expansion and its digitization and thus its transition to an intelligent power supply system (Smart Grid) is to be conducted to reduce grid related supply shortages and to exploit efficiency potentials. At the end of 2012 the Smart Grid Road Map (SGRM) has been introduced for this matter.

So far the introduced measures essentially served to automatize the grid operation and to increase the reliability by decreasing the time needed for error discovery and elimination, thus updating the partly outdated power grids to the latest automatization standards.

The project aims to increase the amount of power that comes from renewable energy sources. Digital technologies will be used to make power grids more flexible and expand their capacities. This will make them fit for the integration of more volatile renewable energy sources. The approach is an environmentally friendly alternative compared to the current plans for improving electricity supplies in Viet Nam, which would carry along a higher degree of carbon emission.

Objective
Vietnamese power grid experts are utilizing improved capacities for the development of an intelligent power supply system (Smart Grid) which enhances energy efficiency (EE) and facilitates the use of renewable energies.

Approach
The project focuses on three main Action Areas that promote the participatory development of smart grid solutions, namely:

Legal and regulatory framework
The objective of this action area is to provide the Electricity Regulatory Authority of Viet Nam (ERAV) with information for improving the regulatory framework for a Smart Grid, which facilitates Renewable Energies and increases Energy Efficiency. Experts who update the Smart Grid Road Map (SGRM) and shape respective regulatory requirements receive support through training. Technically this means understanding the advantages of internationally proven legal and regulatory requirements and adjusting them to the conditions in Viet Nam.
Human capacity development
One of the project’s aims is to establish a Smart Grid knowledge hub, which helps Vietnamese experts and stakeholders to exchange knowledge about the development and management of Smart Grids, state-of-the-art technologies and international approaches. The exchanges also aim to promote awareness about Smart Grids between the government, policy makers, business, research institutes and civil society.
Technology cooperation
Through activities in this action area, power sector experts will be presented with and exchange on available technology solutions for an intelligent power supply system, which facilitates the integration of Renewable Energies and improves Energy Efficiency. The experts will learn more about the technologies that are available on an international level and will get insights into the benefits these technologies can have for the Vietnamese power sector. This will happen by evaluating the technologies theoretically, developing and testing pilot schemes and then testing and evaluating system configurations which integrate different technologies.

Results
The project will not only help improving the efficiency, reliability, and sustainability of electricity services, but also support the increased integration of renewable energies into the existing power grid.

The regulatory framework for Smart Grids is expected to be improved, expert knowledge on Smart Grids will be enhanced and Smart Grid solutions will be introduced, which include an advanced metering infrastructure and demand side management.

The project will contribute to the development of a flexible and responsive, yet, sustainable power system that satisfies the rapidly increasing energy demand of the Vietnamese economy and population. 





 
