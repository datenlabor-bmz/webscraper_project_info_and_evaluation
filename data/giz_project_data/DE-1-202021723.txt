  




 

Aufarbeitung der Vergangenheit zur Konsolidierung des Friedens ProPaz II
Dealing with the past to consolidate peace in Colombia
Project details
Project number:2020.2172.3
Status:laufendes Projekt
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Rebekka Rust rebekka.rust@giz.de
Partner countries: Colombia, Colombia
Summary
Objectives:

Schutz, Förderung und Garantien von Opferrechten sind im Rahmen der Prozesse für Wahrheit, Gerechtigkeit und Entschädigung auf nationaler, r

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Agencia Presidencial de Cooperación Internacional de Colombia

Financing organisation:

not available

 
Project value
Total financial commitment:49 702 644 Euro
Financial commitment for this project number:16 588 866 Euro
Cofinancing

not available

 
Previous projects
2014.2170.0Programm zur Unterstützung des Friedensprozesses in Kolumbien
Follow-on projects
not available
 
Term
Entire project:12.03.2015 - 31.01.2025
Actual project:01.02.2021 - 31.01.2025
other participants
ARGE GOPA-CISP-IDEABORN
Arge AMBERO Consulting Gesellschaft
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Zivile Friedensentw., Krisenpräv. und Konfliktlösung

Evaluation
not available
 




 
