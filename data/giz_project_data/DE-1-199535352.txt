  




 

Studien- und Fachkräftefonds
Studies and Experts Fund
Fonds d'édtudes
Project details
Project number:1995.3535.2
Status:Projekt beendet
Responsible Organisational unit: 1100 Westafrika 1
Contact person:Christophe di Marco christophe.dimarco@giz.de
Partner countries: Mauritania, Mauritania
Summary
Objectives:

Vorbereitung und Prüfung von Vorhaben der Technischen Zusammenarbeit (TZ), Finanzierung von Studien, Gutachten sowie Durchführung von TZ-Maßnahmen geringen Umfangs.

Client:

BMZ

Project partner:

Planungsministerium

Financing organisation:

not available

 
Project value
Total financial commitment:3 839 975 Euro
Financial commitment for this project number:1 030 081 Euro
Cofinancing

not available

 
Previous projects
1992.2177.1Studien- und Fachkräftefonds
Follow-on projects
2018.2258.4Studien- und Fachkräftefonds II
 
Term
Entire project:03.08.1983 - 31.12.2023
Actual project:01.01.1995 - 14.07.2021
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt zielt vor allem auf Gleichberechtigung ab
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
