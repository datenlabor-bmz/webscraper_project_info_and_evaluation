  




 

Studien- und Fachkräftefonds
Studies and Experts Fund
Fonds d'études et d'experts
Project details
Project number:1995.3536.0
Status:Projekt beendet
Responsible Organisational unit: 3600 Nordafrika
Contact person:Matthias Giegerich matthias.giegerich@giz.de
Partner countries: Tunisia, Tunisia
Summary
Objectives:

Vorbereitung und Prüfung von Vorhaben der Technischen Zusammenarbeit (TZ), Finanzierung von Studien, Gutachten sowie Durchführung von TZ-Maßnahmen geringen Umfangs.

Client:

BMZ

Project partner:

Ministerium für Wirtschaft, Finanzen und Investitionen

Financing organisation:

not available

 
Project value
Total financial commitment:10 679 334 Euro
Financial commitment for this project number:3 555 394 Euro
Cofinancing

not available

 
Previous projects
1990.2045.4Studien- und Fachkräftefonds
Follow-on projects
2013.3523.1Studien- und Fachkräftefonds
 
Term
Entire project:16.07.1990 - 31.12.2023
Actual project:01.01.1995 - 18.04.2017
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
