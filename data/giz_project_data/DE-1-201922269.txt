  




 

Aus- und Fortbildung von Medienschaffenden in Afrika
Education and training of media professionals in Africa
Project details
Project number:2019.2226.9
Status:laufendes Projekt
Responsible Organisational unit: 1730 Digitale Transformation in Afrika
Contact person:Victor Chudal-Linden victor.linden@giz.de
Partner countries: AFRICA, Kenya, Rwanda
Summary
Objectives:

Die Filmwirtschaft in Kenia und Ruanda gestärkt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Wird mit der jeweiligen Einzelmaßnahme bestimmt

Financing organisation:

not available

 
Project value
Total financial commitment:15 790 000 Euro
Financial commitment for this project number:4 390 000 Euro
Cofinancing

not available

 
Previous projects
2013.2010.0Aus- und Fortbildung von Medienschaffenden in Afrika - DW-Akademie
Follow-on projects
not available
 
Term
Entire project:21.04.2011 - 30.06.2023
Actual project:01.01.2021 - 30.06.2023
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Fortbildung von Fach- und Führungskräften

Evaluation
not available
 




 
