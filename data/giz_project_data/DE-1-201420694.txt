  




 

Programm ländliche Entwicklung und produktive Landwirtschaft - Förderung der produktiven Landwirtschaft (PROMAP)
Rural development and productive agriculture Niger
Project details
Project number:2014.2069.4
Status:Projekt beendet
Responsible Organisational unit: 1100 Westafrika 1
Contact person:Matthias Banzhaf matthias.banzhaf@giz.de
Partner countries: Niger, Niger
Summary
Objectives:

Das wirtschaftliche Potential der Kleinbewässerung ist nachhaltig in Wert gesetzt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministère du Plan, de l'Amenagement du Territoire et du Développement Communautaire

Financing organisation:

not available

 
Project value
Total financial commitment:92 696 717 Euro
Financial commitment for this project number:15 010 426 Euro
Cofinancing

not available

 
Previous projects
2011.2100.3Ländliche Entwicklung und produktive Landwirtschaft
Follow-on projects
2017.2093.7Programm ländliche Entwicklung und produktive Landwirtschaft - Förderung der produktiven Landwirtschaft(PROMAP) Phase II
 
Term
Entire project:31.10.2006 - 31.12.2024
Actual project:01.01.2016 - 31.03.2019
other participants
Arge ECO / AFC
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungSelbsthilfeorientierte Armutsbekämpfung
CRS code
Landwirtschaftsentwicklung

Evaluation
not available
 




 
