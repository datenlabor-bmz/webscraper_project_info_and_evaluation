  




 

Förderung der Energieeffizienz
Promotion of Energy Efficiency
Project details
Project number:2016.2148.1
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Sibylle Strahl sibylle.strahl@giz.de
Partner countries: Bosnia Herzeg., Bosnia Herzeg.
Summary
Objectives:

Die Strategien und rechtlichen Vorgaben für Energieeffizienz im Gebäudesektor werden umgesetzt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministerium für Außenhandel und wirtschaftliche Beziehungen

Financing organisation:

not available

 
Project value
Total financial commitment:8 250 000 Euro
Financial commitment for this project number:2 250 000 Euro
Cofinancing

not available

 
Previous projects
2011.2042.7Energieeffizienz-Beratung
Follow-on projects
not available
 
Term
Entire project:03.11.2009 - 30.01.2020
Actual project:01.01.2017 - 30.01.2020
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Energieeinsparung und Effizienz (Nachfrageseite)

Evaluation
not available
 




 
