  




 

Allianz für Integrität (Afln)
Alliance for Integrity
Project details
Project number:2015.6253.7
Status:Projekt beendet
Responsible Organisational unit: G420 Governance, Menschenrechte
Contact person:Susanne Friedrich susanne.friedrich@giz.de
Partner countries: Internat. Zusammenarbeit mit Regionen für Nachhalt, Brazil, Ghana, Indonesia, India, Mexico
Summary
Objectives:

Unternehmen in ausgewählten Partnerländern haben verbesserte Kapazitäten geschaffen, praxiserprobte Lösungsansätze zur Prävention und Bekämpfung von Korruption umzusetzen.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:15 000 000 Euro
Financial commitment for this project number:5 500 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2018.6253.1Allianz für Integrität (Afln)
 
Term
Entire project:25.08.2015 - 30.11.2023
Actual project:25.08.2015 - 30.06.2018
other participants
not available
 
Contact
Project websiteswww.allianceforintegrity.org
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
Armutsorientierungnot available
CRS code
Korruptionsbekämpfungs-organisationen und -institution.

Evaluation
not available
 




 
