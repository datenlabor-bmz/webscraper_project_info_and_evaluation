Resilienzprogramm beschäftigungsintensive Maßnahmen

Das "FZ-Vorhaben Resilienzprogramm beschäftigungsintensive Maßnahmen" ist Teil der Sonderinitiative MENA, die im Jahr 2014 vom BMZ ins Leben gerufen wurde.

Die FZ-Mittel dieses Vorhabens werden über den Social Fund for Development (SFD) im Jemen umgesetzt und sind ausschließlich für arbeitsintensive Infrastrukturmaßnahmen mit einem Lohnanteil von 60% vorgesehen. Ziel der FZ-Maßnahme ist es, einen Beitrag zur Stabilisierung des Jemen und Verbesserung der Resilienz in der gegenwärtigen Krise für arme Bevölkerungsgruppen in ländlichen Gebieten des Jemen zu leisten. Durch die armutsorientierte Bereitstellung von Basisinfrastruktur leistet der SFD einen wesentlichen Beitrag zur Linderung der Folgen der politischen Krise und gewaltsamen Konflikte für die Zielgruppe im Jemen und trägt zur wirtschaftlichen Stabilisierung bei.

Im Rahmen der FZ-Maßnahme wird ein Finanzierungsbeitrag in Höhe von 5,0 Mio. EUR zur Verfügung gestellt.

Land / Region / Institution Jemen Nummer 34524 Schwerpunkt Sonstige Sektor 43010 - Multisektorale Hilfe Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 5 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner SOCIAL FUND FOR DEVELOPMENT Zuständige Abteilung Nahost
