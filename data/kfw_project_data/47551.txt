Human Capital Development Programme (HCD)

In Indonesien gibt es starke regionale sozioökonomische Unterschiede, die wiederum entsprechend angepasste staatliche Interventionen, u.a. im Humankapitalentwicklungsbereich, erfordern. Derzeit verfügt die indonesische Regierung über noch keine angemessene Datengrundlage, staatliche Interventionen zielgerichtet planen und umsetzen zu können. Das indonesische Berufsausbildungssystem ist quantitativ, aber vor allem qualitativ derzeit noch unzureichend entwickelt. Aufgrund der bisher unzureichend entwickelten Arbeitslosenversicherungssysteme sind viele Familien durch die Folgen der Covid-19 Pandemie in die Armut abgerutscht.

Das "Boosting Productivity through Human Capital Development Programme" (BPHCD) unterstützt die Republik Indonesien bei der Identifizierung und Umsetzung von Reformen zur Erhöhung der Arbeitsproduktivität für ein stärkeres und stabiles Wachstum. ADB und KfW stellen hierfür u.a. Kredite und fachliche Unterstützungsleistungen zur Verfügung. Die geplanten Reformen haben aufgrund der negativen Auswirkungen der Covid-19 Pandemie auf die indonesische Volkswirtschaft zusätzlich an Bedeutung gewonnen. Die Durchführung des BPHCDP ist an die Umsetzung einer Vielzahl von Einzelreformen geknüpft.

Das Programm fokussiert auf Einflussfaktoren der Humankapitalentwicklung, die durch staatliche Interventionen positiv beeinflusst werden können. Die Reformmatrix des Programms ist in die folgenden drei Reformbereiche unterteilt:
1) Stärkung der Richtlinien und Strategien zur besseren Überwachung (Monitoring) und Finanzierung der nachhaltigen Entwicklungsziele (SDG)
2) Verstärkung der Leistungsfähigkeit des Bildungssystems sowie der beruflichen Kompetenzentwicklung
3) Verbesserung der sozialen Sicherungssysteme sowie des öffentlichen Gesundheitssystems

Durch das Vorhaben sollen die Entwicklungsmöglichkeiten von jungen Menschen sowie von Armut bedrohten Familien verbessert werden.

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde entsprechend der Nachhaltigkeitsrichtlinie der KfW Entwicklungsbank in die Umwelt- und Sozialrisikokategorie C eingeordnet, da vom Vorhaben voraussichtlich keine oder nur geringe nachteilige Auswirkungen und Risiken auf Umwelt und soziale Belange ausgehen. Es ist daher kein spezifischer Umwelt- und Sozialmanagementplan erforderlich. Der Partner wurde zur Einführung eines Beschwerdemechanismus verpflichtet und muss die KfW unverzüglich über umwelt- und sozialrelevante besondere Vorkommnisse informieren.

Land / Region / Institution Indonesien Nummer 47551 Schwerpunkt Wirtschaft und Finanzsektor Sektor 11330 - Berufliche Bildung USV-Kategorie C Finanzierungsinstrument Förderkredit Weitere Geber - Deutscher Finanzierungsbeitrag 400 Mio. EUR Status abgeschlossen Auftraggeber BMZ Projektpartner MINISTRY OF FINANCE OF THE REPUBLIC OF INDONESIA Zuständige Abteilung Ostasien und Pazifik
