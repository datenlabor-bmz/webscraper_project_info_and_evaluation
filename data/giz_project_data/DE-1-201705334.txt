  




 

Durchführung der BMZ-Präsenz im Rahmen der Fachmesse Intersolar Europe 2017
Intersolar Europe
Project details
Project number:2017.0533.4
Status:Projekt beendet
Responsible Organisational unit: G310 Energie, Wasser, Verkehr
Contact person:Jens Burgtorf jens.burgtorf@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh.
Summary
Objectives:

Sicherstellung der Präsenz des Bundesministeriums für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) im Rahmen der Fachveranstaltung Intersolar Europe.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:29 052 Euro
Financial commitment for this project number:29 052 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:05.05.2017 - 31.12.2017
Actual project:05.05.2017 - 31.12.2017
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
Armutsorientierungnot available
CRS code
Energiepolitik und -verwaltung

Evaluation
not available
 




 
