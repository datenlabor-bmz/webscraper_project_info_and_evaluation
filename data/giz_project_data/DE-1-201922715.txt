  




 

Förderung von on- und off-grid Solartechnologien für eine inklusive Wirtschaftsentwicklung
Support of on- and off-grid solar technology for inclusive economic development
Project details
Project number:2019.2271.5
Status:laufendes Projekt
Responsible Organisational unit: 2A00 Asien I
Contact person:Diego Senoner diego.senoner@giz.de
Partner countries: Nepal, Nepal
Summary
Objectives:

Die Voraussetzungen zur Verbreitung von Solartechnologien für eine inklusive, klimafreundliche Wirtschaftsentwicklung sind in ausgewählten Provinzen verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry for Energy, Water Resources and Irrigation

Financing organisation:

not available

 
Project value
Total financial commitment:4 000 000 Euro
Financial commitment for this project number:4 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:17.11.2020 - 31.12.2023
Actual project:01.06.2021 - 31.12.2023
other participants
INTEGRATION
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Solarenergie für Inselnetze und autarke Anlagen

Evaluation
not available
 




 
