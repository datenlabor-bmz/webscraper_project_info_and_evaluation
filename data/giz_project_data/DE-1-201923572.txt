  




 

Studien- und Fachkräftefonds
Studies and Experts Fund
Project details
Project number:2019.2357.2
Status:laufendes Projekt
Responsible Organisational unit: 1600 Westafrika 2, Madagaskar
Contact person:Katja Lehmann katja.lehmann@giz.de
Partner countries: Nigeria, Nigeria
Summary
Objectives:

Vorbereitung und Prüfung von Vorhaben der Technischen Zusammenarbeit (TZ), Finanzierung von Studien, Gutachten sowie Durchführung von TZ-Maßnahmen geringen Umfangs.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

National Planning Commission

Financing organisation:

not available

 
Project value
Total financial commitment:3 500 000 Euro
Financial commitment for this project number:3 500 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:20.12.2019 - 31.12.2025
Actual project:16.12.2019 - 31.12.2025
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungSonstige unmittelbare Armutsbekämpfung
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
