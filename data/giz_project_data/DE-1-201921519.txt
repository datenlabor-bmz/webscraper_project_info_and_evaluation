  




 

Bund-Länder-Pilotprogramm: Neue Formate der Zusammenarbeit in der TZ
Federal Government- Federal States Programme
Project details
Project number:2019.2151.9
Status:laufendes Projekt
Responsible Organisational unit: 3940 Region Nord
Contact person:Dieter Anders dieter.anders@giz.de
Partner countries: Weltweite Massnahmen, Albania, Bosnia Herzeg., Germany, Ethiopia, Ghana, India, Iraq, North Macedonia, Malawi, Mozambique, Namibia, Nigeria, Peru, Serbia, Rwanda, Togo, Tunisia, Tanzania, Uganda, Viet Nam, Kosovo, South Africa
Summary
Objectives:

Partnerländer der deutschen EZ profitieren von einer stärkeren strategisch-thematischen Zusammenarbeit zwischen Bund und Bundesländernin der

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:30 168 974 Euro
Financial commitment for this project number:17 555 630 Euro
Cofinancing

Staatsministerium Baden-Württemberg: 49 310Euro


Ministerium f. Bildung, Wissenschaft und Kultur: 27 270Euro


Sächs. Staatsmin. für Energie, Klimaschutz,Umwelt+Landwirtschaft: 119 995Euro


Bay. Staatsministerium für Wirtschaft, Landesentwicklung u. Energie: 279 828Euro


Sächsisches Oberbergamt: 299 996Euro


Senatskanzlei Freie Hansastadt Bremen: 255 000Euro


Nieders. Ministerium für Umwelt, Energie, Bauen u. Klimaschutz: 124 513Euro


Hessen Agentur GmbH: 289 000Euro


Bayr. Staatsministerium f. Wirtschaft, Landesentwicklung u. Energie: 218 951Euro


Ministerium f. Wirtschaft, Verkehr, Landwirtschaft u. Weinbau: 55 000Euro


Staatskanzlei des Landes Nordrhein-Westfalen: 310 554Euro


Niedersächsisches Landesamt für Soziales, Jugend und Familie: 50 000Euro


Bayerische Staatskanzlei: 818 779Euro


Bayerisches Staatsministerium f. Umwelt u. Verbraucherschutz: 89 770Euro


Staatskanzlei Schleswig-Holstein: 95 000Euro


Ministerium f. Bildung, Wissenschaft u. Kultur: 72 730Euro


Ministerium für Kultur und Wissenschaft: 99 933Euro


Sächsische Staatskanzlei: 160 000Euro


 
Previous projects
2015.2138.4Bund-Länder-Programm
Follow-on projects
not available
 
Term
Entire project:17.12.2013 - 30.06.2023
Actual project:09.09.2019 - 30.06.2023
other participants
Minds & Makers GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
