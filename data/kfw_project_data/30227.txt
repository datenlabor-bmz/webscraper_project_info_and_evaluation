GeT Fit

Im Rahmen des GET FiT-Programms sollen private Investitionen in Erneuerbare Energien (EE) in Uganda gefördert werden. Ziel des Programms ist es, durch eine innovative Bündelung von ergebnisorientierten Prämienzahlungen auf den Einspeisetarif (GET FiT Premium Payment Mechanismus, GFPPM) sowie einer Zusammenarbeit mit privaten Banken, die Rahmenbedingungen für private Investitionen in EE zu verbessern.
So sollen 17 kleinere EE-Kraftwerke (Wasser, Solar, Bagasse) mit einer installierten Kapazität von jeweils zwischen 1MW und 20 MW ans Netz gebracht werden. Insgesamt werden dadurch dem ugandischen Verbundnetz bis zu 160 MW bzw. knapp 760 GWh Erzeugung p.a. an zusätzlicher Kapazität zugeführt.

Uganda wird so bei der Umsetzung eines klimafreundlichen Entwicklungspfades unterstützt indem ein Beitrag zu nachhaltigem Wachstum, Armutsreduzierung, einer Reduzierung der Abhängigkeit von fossilen Energien und einer Mitigierung des Klimawandels geleistet wird.

Land / Region / Institution Uganda Nummer 30227 Schwerpunkt Energie Sektor 23210 - Energieerzeugung, erneuerbare Quellen Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber European Investment Bank (EIB), NORAD, Department for International Development (DFID) Deutscher Finanzierungsbeitrag 15 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner MINISTRY OF ENERGY AND MINERAL DEVELOPMENT Zuständige Abteilung Ostafrika und Afrikanische Union
