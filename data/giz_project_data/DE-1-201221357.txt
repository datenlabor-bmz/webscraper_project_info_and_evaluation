  




 

Stärkung integraler Bürgersicherheit und gesellschaftlicher Konfliktbearbeitung
Promotion of integral civil security and transformation of social conflicts
Fomento de la seguridad ciudadana y de la transformación de conflictos sociales
Project details
Project number:2012.2135.7
Status:Projekt beendet
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Dr. Gerhard Schmalbruch gerhard.schmalbruch@giz.de
Partner countries: Guatemala, Guatemala
Summary
Objectives:

Staatliche und nichtstaatliche Akteure sind in der Lage, gemeinsam integrale Strategien der Bürgersicherheit (einschließlich Strategien zur konstruktiven Bearbeitung von sozialen Konflikten) auf nationaler und sub-nationaler Ebene umzusetzen.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministerio de Gobernación (Innenministerium)

Financing organisation:

not available

 
Project value
Total financial commitment:16 750 000 Euro
Financial commitment for this project number:4 750 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2014.2165.0Stärkung integraler Bürgersicherheit und gesellschaftlicher Konfliktbearbeitung
 
Term
Entire project:20.12.2012 - 15.08.2022
Actual project:20.12.2012 - 13.09.2017
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Zivile Friedensentw., Krisenpräv. und Konfliktlösung

Evaluation
Guatemala: Förderung der integralen Bürgersicherheit und Transformation sozialer Konflikte (FOSIT). Projektevaluierung: Kurzbericht
Guatemala: Promotion of integral citizens´ security and transformation of social conflicts (FOSIT). Project evaluation: summary report
 
Project description (DE)

Ausgangssituation
Guatemala gehört fast zwei Jahrzehnte nach Unterzeichnung der Friedensverträge (1996) immer noch zu den Ländern mit den höchsten Gewaltraten. Seit dem Ende des 36-jährigen Bürgerkrieges wird Gewalt zunehmend von privaten Akteuren wie Drogenkartellen und Jugendbanden ausgeübt.

Bei der Verbesserung der Bürgersicherheit wurden bislang nur bescheidene Fortschritte erzielt; die meisten strukturellen Konfliktursachen bleiben ungelöst. Mangelnde Präsenz und Professionalität der staatlichen Sicherheitsorgane, einhergehend mit der vierthöchsten Mordrate in Mittelamerika, weitgehender Straflosigkeit und zunehmender Selbstjustiz machen Bürgersicherheit und Gewaltprävention zu prominenten innenpolitischen Dauerthemen.

Weiteres Konfliktpotenzial ist durch neue Großprojekte, vor allem Infrastruktur- und Bergbauprojekt oder Wasserkraftwerke, entstanden. Auch im privaten und familiären Bereich ist Gewalt weit verbreitet, die Zahl der Femizide erschreckend hoch.

Ziel
Die Bürgersicherheit ist verbessert: sowohl objektiv als auch in der individuellen Wahrnehmung der Bürger. Soziale Konflikte um Investitionen im ländlichen Raum werden gewaltfrei und partizipativ gelöst.

Vorgehensweise
Die guatemaltekische Regierung hat im „Pakt für Sicherheit, Justiz und Frieden" zwei wesentliche Erfolgsfaktoren benannt: Entwicklung erfolgreicher Strategien zur Gewaltprävention und die Einbeziehung aller Interessengruppen in die Umsetzung der Präventionsmaßnahmen (Inklusion). Das Programm FOSIT unterstützt Strategien und Maßnahmen zur Vorbeugung der Gewalt gegen Kinder, Heranwachsende, Jugendliche und Frauen sowie gegen Gewalt mit Waffen. Es unterstützt Strategien und Maßnahmen auf nationaler Ebene sowie in den Provinzen Alta Verapaz, Baja Verapaz und Quiché.

Das Vorhaben fördert das Innenministerium bei der Planung und Umsetzung von Gewaltpräventionsstrategien sowie bei der Zusammenarbeit mit weiteren staatlichen Institutionen. Bei der Entwicklung und dem Aufbau von Dialogmethoden und -mechanismen werden gleichzeitig zivilgesellschaftliche Akteure wie Nichtregierungsorganisationen und Wirtschaftsverbände unterstützt.

Lokale Netzwerke staatlicher und zivilgesellschaftlicher Akteure sind ein wichtiger Erfolgsfaktor. Ihre Akzeptanz ist die Voraussetzung für den Aufbau eines Dialogs aller Beteiligten über Bürgersicherheit und die konstruktive Lösung sozialer Konflikte. Systematisch aufgearbeitete Erfahrungen aus der Konfliktbearbeitung und nachahmenswerte Beispiele sind Grundlage für die Zusammenarbeit und die Entwicklung gemeinschaftlicher Gewaltpräventionsstrategien von Staat und Zivilgesellschaft.

Die Zivilgesellschaft sowie Verbände und Interessenvertretungen werden in ihren Anstrengungen, einen integrativen, gesamtgesellschaftlichen Dialog zu führen, unterstützt sowie in ihrer Koordinierung und ihrem Rollenverständnis gestärkt. Gewaltprävention und friedliche Konfliktaustragung werden so zur gemeinsamen Aufgabe aller sozialen und ethnischen Gruppen.

Wirkung
Unterstützt durch die Programme FOSIT und PREVENIR liegt in Guatemala seit Mai 2014 erstmals ein langfristiger nationaler Politikrahmen für Gewaltprävention vor.

Das Bewusstsein, dass alle Gruppen der Gesellschaft an der Überwindung der Gewalt mitarbeiten müssen, wurde gestärkt. Beispiele positiver gemeinsamer Konfliktlösung und Angebote wie objektivierende Begleitung oder Analyse von Konfliktsituationen und ihren Ursachen stoßen auf positive Resonanz bei der Bevölkerung. Auf nationaler wie lokaler Ebene werden konkrete, konzertierte Strategien der Bürgersicherheit und Gewaltprävention umgesetzt und gute Erfahrungen gesammelt.

Beispiele aus der Praxis
Verschiedene Kampagnen zum Thema Gewalt gegen Frauen in Lateinamerika wurden ausgewertet und die Ergebnisse in den Regionen vorgestellt. Daraufhin haben sich Initiativen von Gemeindeverwaltungen und der Zivilgesellschaft, beispielsweise Frauenorganisationen, gegründet, um die Erfahrungen aus anderen Ländern auf die Situation in Guatemala zu übertragen und Aktivitäten mit Unterstützung von FOSIT umzusetzen. Prominentes Beispiel ist die Kampagne „Briefe von Frauen": Berichte von Frauen, die anonym über ihre Gewalterfahrungen in der Familie berichten, werden diskutiert und veröffentlicht, um die Vereinzelung der Opfer zu überwinden und eine gesellschaftliche Diskussion anzustoßen.

13 lokale Gemeinden wurden bisher bei der Gründung von Sicherheitskommissionen und der Entwicklung von Plänen zur Gewaltprävention unterstützt. Auch hier gilt der Bekämpfung von Gewalt gegen Frauen im öffentlichen Raum und in der Familie besondere Aufmerksamkeit.

In Zusammenarbeit mit der Polizeiakademie und zwei Universitäten wurden Fernlehrgänge, Seminare und Diplome entwickelt. Fast 400 Polizisten, Lehrer, öffentliche Angestellte und Akteure zivilgesellschaftlicher Organisationen wurden bisher in Deeskalationstechniken und systematischen Gewaltpräventionsansätzen geschult.

In Pilotprojekten wird Gewaltfreiheit, Mediation und politische Partizipation von Kindern und Jugendlichen gefördert. An zwei Schulzentren wird in Zusammenarbeit mit Eltern und Lehrern ein Curriculum für Gewaltfreiheit erarbeitet, das später in den nationalen Lehrplan eingehen soll.

Um der Konflikteskalation um Großprojekte vorzubeugen, unterstützt FOSIT den Aufbau eines nationalen Dialogsystems. Die beteiligten Institutionen, wie die präsidiale Menschenrechtskommission, das Sekretariat für Agrarangelegenheiten und das Energie- und Bergbauministerium koordinierten ihre Arbeit bislang wenig. Im Oktober 2014 fand das erste nationale Treffen mit rund 140 staatlichen Funktionären statt. Die teilnehmenden Institutionen tauschten sich über nachahmenswerte Praktiken in der Konfliktbearbeitung aus und erarbeiteten Vorschläge für eine bessere Zusammenarbeit untereinander.

Sicherheitskommissionen und Netzwerke zum Konfliktmanagement werden auf lokaler Ebene von Entwicklungshelfern beraten. So werden in gemeinsamen Seminaren Konfliktbeteiligte und ihre Interessenvertreter in friedlichen Kommunikationstechniken und Kompromissfindungsstrategien geschult.

Der Nationale Verband zur Förderung sozialer Verantwortung bei Unternehmen (CENTRARSE) hat mit Unterstützung von FOSIT ein Programm zum nachhaltigen Dialog entwickelt. An den drei Vorstellungen auf Departementebene waren jeweils 100 bis 120 Vertreter aus Staat, Zivilgesellschaft und Privatsektor beteiligt.

Das zentralamerikanische Treffen der Unternehmer (CONVERTIRSE), hatte 2014 in Guatemala, unterstützt von FOSIT, das Thema: Soziale Verantwortung zum Dialog als Strategie der Nachhaltigkeit. Ziel war eine Sensibilisierung der Unternehmen, sich für soziale Dialogprozesse einzusetzen und soziale Verantwortung im Alltag zu übernehmen.
 

 
Project description (EN)

Context
Almost two decades after the signing of the peace agreements (1996), Guatemala continues to have one of the highest rate of violence in the world. Since the end of the 36-year civil war, violence is increasingly being perpetrated by private actors such as drug cartels and youth gangs.

As yet only modest progress has been made in improving civil security; most structural causes of conflict remain unresolved. The insufficient presence and professionalism of state security forces coupled with the fourth-highest homicide rate in Central America, extensive impunity and increasing vigilantism make civil security and the prevention of violence prominent and ongoing topics of domestic policy.
New large-scale projects, in particular infrastructure and mining projects as well as hydroelectric power stations, have created further potential for conflict. Violence is also widespread in the private sphere and in families; the number of femicides is frighteningly high.

Objective
Civic security is improved, both on an objective level and in the individual perception of citizens. Social conflicts about investments in the rural regions are solved in a participatory manner and without violence.

Approach
In its Pact for Security, Justice and Peace, the Guatemalan Government identified two essential factors for success: the development of successful strategies for preventing violence and the inclusion of all interest groups in the implementation of prevention measures. The FOSIT programme supports strategies and measures for preventing violence against children, youths, young adults and women as well as against violence with weapons. It supports strategies and measures at a national level and in the provinces of Alta Verapaz, Baja Verapaz and Quiché.

The programme supports the Ministry of the Interior in planning and implementing strategies for preventing violence and for cooperation with other state institutions. The development and establishment of dialogue methods and instruments also involves providing support to civil society stakeholders such as non-governmental organisations and industry associations.

Local networks of public and civil society stakeholders are an important success factor. Their acceptance is the prerequisite for establishing a dialogue on civil security and the constructive resolution of social conflicts involving all stakeholders. A systematic review will be conducted of experiences in resolving conflicts and examples of good practice provide the basis for cooperation between the state and civil society and development of joint strategies for preventing violence.

Civil society, associations and special interest groups are supported in their efforts to maintain an integrative dialogue involving all spheres of society. They also receive support in their coordination and to better understand their role. This makes the prevention of violence and the peaceful resolution of conflicts a joint task involving all social and ethnic groups.

Results
Since May 2014, with support from the programmes FOSIT and PREVENIR, Guatemala for the first time has a long-term national policy framework for preventing violence.
The awareness that all spheres of society have to contribute to overcoming violence has been strengthened. Examples of positive, joint conflict resolution, provision of services such as objectifying support and the analysis of conflict situations and their root causes have received positive feedback from the population. Specific, concerted strategies for civil security and the prevention of violence are being implemented with good experiences both at a national and local level.

Practical examples
Various campaigns on the issue of violence against women in Latin America have been evaluated and the results have been presented in the different regions. This has resulted in the establishment of initiatives by municipal administrations and the civil society, for example, women's organisations, to transfer the experiences from other countries to the situation in Guatemala and to implement activities with support from FOSIT. One prominent example is the campaign ‘Letters from Women: reports by women’, who anonymously describe their experiences with domestic violence in families are published and discussed to overcome the isolation of the victims and to trigger public debate.

So far, 13 local communities have received support in establishing security commissions and in their development of plans for preventing violence. Here, too, the prevention of violence against women in public and in the family is being given specific attention.

Distance learning courses, seminars and diplomas have been developed in cooperation with the police academy and two universities. Almost 400 police staff, teachers, public servants and activists from civil society organisations have so far received training in de-escalation techniques and systematic approaches to preventing violence.

Pilot projects promote non-violence, mediation and political participation for children and youths. In cooperation with parents and teachers, two school centres are developing a curriculum for non-violence with the aim of its later inclusion in the national curriculum.

FOSIT supports the development of a national dialogue system for preventing the escalation of conflicts regarding large-scale projects. So far, there has been insufficient coordination between the involved institutions such as the Presidential Coordinating Commission on Human Rights, the Secretariat for Agricultural Affairs and the Ministry of Energy and Mines. The first national meeting with about 140 government officials took place in October 2014. The participating institutions exchanged information about exemplary practices for conflict resolution and developed proposals for improving their cooperation.

At a local level, development workers are providing support to security commissions and conflict management networks. Joint seminars deliver training for people involved in conflict and those representing their interests on the topics of peaceful communication techniques and strategies for developing compromises.

With support from FOSIT, the National Centre for Promoting Social Responsibility in Enterprises (CENTRARSE) has developed a programme for lasting dialogue. The three programme presentations at departmental level each involved 100 to 120 representatives from the government, civil society and the private sector.

FOSIT also provided support to the 2014 Central American Conference of Entrepreneurs (CONVERTIRSE) in Guatemala which focused on the topic of Social Responsibility for Dialogue as a Sustainability Strategy. Its aim was to sensitise companies to committing to social dialogue processes and taking over social responsibility in their daily routine.
 





 
