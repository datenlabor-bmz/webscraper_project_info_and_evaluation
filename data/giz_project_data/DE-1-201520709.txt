  




 

Unterstützung der Fiskalpolitik II
Support for Fiscal Policy II
Apoyo a las Politicas Fiscales II
Project details
Project number:2015.2070.9
Status:Projekt beendet
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Roland von Frankenhorst roland.frankenhorst@giz.de
Partner countries: El Salvador, El Salvador
Summary
Objectives:

Die Leistungsfähigkeit des Systems der Öffentlichen Finanzen hinsichtlich Effektivität, Effizienz sowie Transparenz und Partizipation ist gestärkt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministerio de Hacienda

Financing organisation:

not available

 
Project value
Total financial commitment:8 307 950 Euro
Financial commitment for this project number:2 893 298 Euro
Cofinancing

not available

 
Previous projects
2009.2268.2Unterstützung der Reform der Fiskalpolitik
Follow-on projects
not available
 
Term
Entire project:12.07.2010 - 30.06.2018
Actual project:01.09.2015 - 30.06.2018
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Mobilisierung von Eigeneinnahmen

Evaluation
not available
 




 
