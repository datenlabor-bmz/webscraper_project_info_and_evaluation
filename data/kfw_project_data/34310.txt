Weiterentwicklung der Mikrofinanzinitiative für Sub-Sahara Afrika II (MIFFSSA), Tranche V

Bei dem Vorhaben handelt es sich um die 5. Tranche der Weiterentwicklung der Mikrofinanzinitiative für Sub-Sahara Afrika (MIFSSA II). Ziel des Vorhabens ist die Vergrößerung des nachhaltigen und effizienten Angebots von bedarfsgerechten Finanzdienstleistungen für KKMU in Sub-Sahara-Afrika. Hierfür werden durch die Vergabe von Eigenkapital und eigenkapitalähnlichen Darlehen an Finanzinstitutionen, Holdinggesellschaften und Finanzsektor-Fonds, Geschäftsmodelle gefördert, die auf die Kreditvergabe an KKMU bzw. Förderung von KKMU ausgerichtet sind. Die Finanzierung ermöglicht es den KKMU, Investitionen zu tätigen, die das Unternehmenswachstum fördern. Das Wachstum führt somit indirekt zur Erhaltung und Schaffung von Arbeitsplätzen und Einkommen und das Vorhaben leistet einen Beitrag zur Armutsbekämpfung in Sub-Sahara Afrika.

Mit dieser Tranche an MIFFSA II wurden die folgenden Projekte finanziert:
- AB Bank Rwanda (Ruanda)
- Finca Kongo
- Finca Sambia

Land / Region / Institution Afrika NA Nummer 34310 Schwerpunkt Wirtschaft und Finanzsektor Sektor 24030 - Finanzintermediäre des formellen Sektors Finanzierungsinstrument Treuhandbeteiligung Weitere Geber - Deutscher Finanzierungsbeitrag 2,994 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner AB BANK RWANDA PLC Zuständige Abteilung Beteiligungsfinanzierung
