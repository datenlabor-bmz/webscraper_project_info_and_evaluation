  




 

Arabisch-Deutsches Wörterbuch
Arabic-German Dictionary
Arabic-German Dictionary
Project details
Project number:2010.2010.6
Status:Projekt beendet
Responsible Organisational unit: 3600 Nordafrika
Contact person:Guido Zebisch guido.zebisch@giz.de
Partner countries: Middle East and North Africa
Summary
Objectives:

Die begriffliche Grundlage für den internationalen und regionalen Fachdialog in arabischen Ländern ist in ausgewählten technischen Fachgebieten verbessert.

Client:

BMZ

Project partner:

Arab League Educational, Cultural and Scientific Organization (ALECSO)

Financing organisation:

not available

 
Project value
Total financial commitment:5 551 213 Euro
Financial commitment for this project number:2 436 581 Euro
Cofinancing

not available

 
Previous projects
2007.2173.8Arabisch-Deutsches Technisches Wörterbuch
Follow-on projects
2013.2476.3Arabisch-Deutsches Technisches Wörterbuch
 
Term
Entire project:24.04.2008 - 15.06.2018
Actual project:01.08.2011 - 31.07.2014
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Berufliche Bildung

Evaluation
not available
 




 
