  




 

Unterstützung des EU-Integrationsprozesses
Support to the EU-Integration Process
Project details
Project number:2013.2156.1
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Dr. Mareike Meyn mareike.meyn@giz.de
Partner countries: Kosovo, Kosovo
Summary
Objectives:

Die EU-Integrationspolitik Kosovos wird zielführend koordiniert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministerium für europäische Integration

Financing organisation:

not available

 
Project value
Total financial commitment:13 979 666 Euro
Financial commitment for this project number:2 500 000 Euro
Cofinancing

not available

 
Previous projects
2010.2240.9Unterstützung des EU-Integrationsprozesses
Follow-on projects
2016.2222.4Unterstützung des EU-Integrationsprozesses
 
Term
Entire project:07.07.2011 - 28.02.2024
Actual project:01.07.2014 - 27.08.2018
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Politik und Verwaltung in Bezug auf den öffentl Sektor

Evaluation
Kosovo: Unterstützung des Europäischen Integrationsprozesses. Projektevaluierung: Kurzbericht
Project evaluation: summary report. Kosovo: Support to the European Integration Process
 
Project description (DE)

Ausgangssituation
Ziel der Regierung des Kosovo ist der Beitritt zur Europäischen Union (EU). Seit April 2016 ist das Stabilisierungs- und Assoziierungsabkommen mit der EU in Kraft.
Um die Zusammenarbeit zur Umsetzung der europäischen Reformagenda zwischen dem 2010 geschaffene kosovarischen Ministerium für Europäische Integration (MEI) und den Fachministerien effektiv zu koordinieren, wurden 2013 in den zentralen Ministerien Abteilungen für Europäische Integration und Politikkoordinierung eingerichtet. Sie sind bislang jedoch noch nicht ausreichend in den EU-Integrationsprozess involviert.
In jeder Gemeinde wurden darüber hinaus Büros als lokale Ansprechpartner für den EU-Integrationsprozess geschaffen. Außerdem hat das kosovarischen Parlament 2008 den Ausschuss für Europäische Integration eingerichtet. Seit Inkrafttreten des Stabilisierungs- und Assoziierungsabkommen gibt es auch einen gemeinsamen Ausschuss von kosovarischen und europäischen Parlamentariern.
Trotz wichtiger Fortschritte ist die Umsetzungsfähigkeit der kosovarischen Institutionen zur Rechtsangleichung, Übernahme und Anwendung von EU-Standards noch nicht ausreichend. Auch die Koordinations- und Kooperationsprozesse innerhalb und zwischen den Fachministerien sowie mit dem Ministerium für Europäische Integration (MEI) müssen verbessert werden, um den EU-Integrationsprozess langfristig und nachhaltig koordinieren und überwachen zu können.

Ziel
Das Ministerium für Europäische Integration (MEI) ist in der Lage, den EU-Annäherungsprozess zu planen und zu steuern, vor allem die effektive Implementierung des Stabilisierungs- und Assoziierungsabkommens. Es koordiniert sich dazu mit den zentralen Fachministerien und bindet weitere wichtige Akteure in den EU-Integrationsprozess ein, etwa das Parlament und die Zivilgesellschaft.

Vorgehensweise
Die GIZ unterstützt die kosovarische Regierung, vor allem das Ministerium für Europäische Integration (MEI), dabei, den EU-Annäherungsprozess, insbesondere die effektive Umsetzung des Stabilisierungs- und Assoziierungsabkommens, zu koordinieren, zu steuern und den Verlauf zu überwachen. Dies beinhaltet die Angleichung des kosovarischen Rechtssystems an bestehendes EU-Recht und die damit einhergehende Reform der Institutionen.
Die GIZ unterstützt das MEI mit Prozess- und Organisationsberatung sowie beim Ausbau fachlichen Know-hows. Darüber hinaus fördert das Vorhaben die Einbindung parlamentarischer und zivilgesellschaftlicher Kräfte und damit die partizipative Gestaltung des EU-Annäherungsprozesses in Kosovo.
Die Aktivitäten des Programms haben vier Schwerpunkte:

1. Stärkung der strategischen Planungsfähigkeit für den europäischen Integrationsprozess
2. Verbesserung der Koordinations- und Kooperationsfähigkeiten des MEI
3. Unterstützung bei den Schlüsselprozessen der EU-Integration, insbesondere bei der Rechtsangleichung nationaler Gesetze an das EU-Recht (acquis communautaire)
4. Stärkung der Partizipation staatlicher und nichtstaatlicher Akteure sowie Zusammenarbeit mit dem Ausschuss für Europäische Integration des kosovarischen Parlaments zur Verbesserung der Aufsichts- und Kontrollfunktion

Wirkung
Die GIZ hat durch intensive Prozessberatung und Organisationsentwicklung dazu beigetragen, zentralen Koordinierungsstrukturen und Prozesse für die Steuerung der EU-Annäherung im Kosovo aufzubauen und zu konsolidieren sowie die maßgeblichen Akteure für ihre Aufgaben weiterzubilden und zu stärken.
Mit Unterstützung des Vorhabens wurde das Nationale Programm zur Umsetzung des Stabilisierungs- und Assoziierungsabkommens erarbeitet. Mit Vertretern aus Regierung, Parlament, Gemeinden, Wissenschaft, Gebergemeinschaft, Zivilgesellschaft und Privatsektor konnte darüber hinaus ein breiter nationaler Konsens über die Ziele der EU-Integration erreicht werden.
Die GIZ hat einen Trainingsleitfaden für die schrittweise Übernahme der EU acquis communautaire in das kosovarische Gesetzgebungsverfahren entwickelt. In Zusammenarbeit mit dem Kosovarischen Institut für Öffentliche Verwaltung (KIPA) werden Trainer in den maßgeblichen Ministerien ausgebildet. Für die Kernprozesse der Annäherung an das EU-Rechts- und Regelwerk konnten so wesentliche Grundlagen geschaffen werden. 

 
Project description (EN)

Context
Accession to the European Union (EU) is a declared objective of the Government of Kosovo. For its part, the EU has reiterated that Kosovo has a clear European perspective. In October 2013 EU negotiations were initiated for a Stabilisation and Association Agreement (SAA). The negotiations continued until May 2014 and the agreement was initialled in July 2014.
In recent years, the Kosovar Government has developed resources to coordinate and implement reforms geared to EU integration. The key actor and project partner is the Ministry of European Integration (MEI), which was set up in 2010. The MEI is charged with coordinating, managing and monitoring the EU pre-accession process.
To manage the process of EU convergence the Government of Kosovo has not only set up a specific ministry but has also created departments for European integration and policy coordination in all sectoral ministries. These were formally established in 2013 but are not as yet adequately involved in the EU integration process. Offices were set up in all municipalities as contact centres for the EU integration process to ensure the desired participation at local level. The Kosovar parliament established an EU Committee in 2008.
However, in general the performance capacity and roles of the new entities have not yet evolved sufficiently to facilitate sustainable and long-term coordination and monitoring of the EU integration process.
Objective
The Ministry of European Integration is capable of steering the EU pre-accession process – in particular, implementation of the Stabilisation and Association Agreement – and implementing the necessary reforms. In doing so, it involves other key stakeholders such as other sectoral ministries, local community groups and the parliamentary EU Committee.
Approach
By providing advisory services on procedural and organisational issues, GIZ supports Kosovo in building up its own know-how with a view to managing the processes involved in EU pre-accession and implementing the necessary reforms. A key element entails strengthening the capacity of participating actors to cooperate and coordinate their activities. In addition, the project fosters the involvement of civil society groups, thereby reinforcing the participatory nature of the EU pre-accession process in Kosovo.
Results
Analyses have been conducted of the structural and institutional resources for EU convergence, the legal conditions for EU integration and the current status of the EU pre-accession process in Kosovo. These form the basis for intensive process consulting and organisational development. GIZ helped to develop and consolidate the central coordination structures and processes in Kosovo necessary for managing EU convergence and to provide further training for core actors in their tasks.
The Ministry of European Integration completed its internal reorganisation in late 2012, adapting its structures to the requirements of the EU pre-accession process. In March 2012, the President of Kosovo convened a national council and a working party on European integration. GIZ supported the ministry with setting up this council and coordinating activities of the working party. By involving representatives from the government, parliament, municipalities, the academic community, donor groups, civil society and the private sector, a broad national consensus was established regarding Kosovo’s goals for EU integration. In a participatory process, the actors involved devised a national strategy on EU pre-accession. In December 2013 the President of Kosovo presented this national strategy for European integration to the media and the general public.
The GIZ contribution has helped lay important foundations in core processes of the country’s EU harmonisation efforts. In order to support the convergence process between Kosovo’s legal system and EU legislation, for example, GIZ helped create the necessary structures and processes and trained trainers in the various ministries.
The project also helped to develop the necessary conditions and structures for visa liberalisation at both the political and technical level; these are now in operation.

 





 
