  




 

Kommunales Landmanagement/Kataster
Landmanagement/Cadastre
Project details
Project number:2013.2154.6
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Hamsi Behluli hamsi.behluli@giz.de
Partner countries: Kosovo, Kosovo
Summary
Objectives:

Die Rechtssicherhit von Land und Wohneigentum ist erhöht.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Innovationsministerium

Financing organisation:

not available

 
Project value
Total financial commitment:7 322 545 Euro
Financial commitment for this project number:777 165 Euro
Cofinancing

not available

 
Previous projects
2010.2092.4Kommunales Landmanagement/Kataster
Follow-on projects
2016.2233.1Stärkung von Raumplanung und Landmanagement
 
Term
Entire project:22.11.2010 - 30.05.2022
Actual project:24.03.2014 - 13.05.2016
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Stadtentwicklung und -verwaltung

Evaluation
not available
 




 
