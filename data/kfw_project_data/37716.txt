Armutsorientierte Wirtschaftsförderung zur Friedenskonsolidierung III

Durch die Finanzierung wirtschaftlicher Infrastruktur (ländliche Zufahrtswege, Getreidespeicher, Brunnen) entlang ausgewählter landwirtschaftlicher Wertschöpfungsketten (Kakao, Kaffee, Reis, Gemüse) leistet das Modul einen Beitrag zur Verbesserung der Beschäftigungs- und Einkommenssituation von Jugendlichen (Programmziel). Über die arbeitsintensiv ausgestaltete Bereitstellung der wirtschaftlichen Infrastruktur werden zunächst kurzfristige Einkommens- und Beschäftigungsmöglichkeiten vor allem für Jugendliche geschaffen. Darüber hinaus entstehen durch die strukturelle Verbesserung der landwirtschaftlichen Infrastruktur in den Interventionsgebieten aber auch mittel- und langfristige Einkommens- und Beschäftigungsmöglichkeiten, die gerade Jugendlichen neue Zukunftsperspektiven eröffnen und zu einem nachhaltigen Wirtschaftswachstum beitragen. Das Programm ist eine Fortführung der FZ-Vorhaben "Armutsorientierte Wirtschaftsförderung zur Friedenskonsolidierung (Pro-Poor Growth for Peace Consolidation - GPC) Phase I (BMZ-Nr.: 2004 66 284) und Phase II (BMZ-Nr.: 2009 67 141). Es knüpft zudem an das TZ-Vorhaben "Jugendbeschäftigungsförderung durch lokale Wirtschaftsentwicklung" (BMZ-Nr.: 2015 22 184) an. Projektträger des FZ-Vorhabens ist wie in den beiden ersten Phasen die National Commission for Social Action (NaCSA). Der Finanzierungsbeitrag für das Vorhaben beträgt 20,0 Mio. EUR. Aufgrund des höheren Finanzierungsbedarfs wurden zudem weitere 20,0 Mio. EUR für eine mögliche vierte Phase geprüft (Vorratsprüfung).

Land / Region / Institution Sierra Leone Nummer 37716 Schwerpunkt Friedensentwicklung und Krisenprävention Sektor 73010 - Kurzfristiger Wiederaufbau USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 20 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner NATIONAL COMMISSION FOR SOCIAL ACTION (NACSA) Zuständige Abteilung Westafrika
