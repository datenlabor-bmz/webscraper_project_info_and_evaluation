Programm Entwicklung der Agrarfinanzierung

Im Rahmen des Programms Entwicklung der Agrarfinanzierung soll der Zugang zu angepassten, nachhaltigen Agrarfinanzdienstleistungen für kleine und mittlere landwirtschaftliche Unternehmen entlang landwirtschaftlicher Wertschöpfungsketten verbessert und ein Beitrag zu erhöhten Investitionen im Agrarsektor geleistet werden. Darüber hinaus soll das Finanzierungsangebot für biodiversitätsfreundliche, privatwirtschaftliche Investitionen verbessert und Pilotinvestitionen in diesem Feld gefördert werden.

Zielgruppe sind Unternehmen entlang der landwirtschaftlichen Wertschöpfungskette, insbesondere kleinere und mittlere Unternehmen und Landwirte. Finanzinstitutionen des formellen Sektors sind Mittler. Die Biodiversitätskomponente des Programms zielt auf Unternehmen ab, die spezielles Interesse an einer Finanzierung für Investitionen mit positivem Biodiversitätsbezug haben.

Land / Region / Institution Uganda Nummer 30491 Schwerpunkt Wirtschaft und Finanzsektor Sektor 24030 - Finanzintermediäre des formellen Sektors Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 18,5 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner EAST AFRICAN DEVELOPMENT BANK Zuständige Abteilung Ostafrika und Afrikanische Union
