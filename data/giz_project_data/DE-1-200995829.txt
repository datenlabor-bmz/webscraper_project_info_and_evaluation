  




 

Ausgewählte Fragen von Handel und Entwicklung
Research project "special questions of trade and development"
Project details
Project number:2009.9582.9
Status:Projekt beendet
Responsible Organisational unit: G410 Global Policy
Contact person:Dr. Juergen Wiemann juergen.wiemann@giz.de
Partner countries: Developping countries
Summary
Objectives:

Das Vorhaben deckt den Forschungs- und Beratungsbedarf zu ausgewählten Fragen von Handel und Entwicklung

Client:

BMZ

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:490 799 Euro
Financial commitment for this project number:490 799 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:12.06.2009 - 31.05.2013
Actual project:12.06.2009 - 31.05.2013
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Handelspolitik und -verwaltung

Evaluation
not available
 




 
