  




 

Unterstützung der ergebnisorientierten Entwicklungs- und Ausgabenplanung
Advisory Assistance for results-based development and budget planning
Projet d'Appui à la Planification Développementale et Budgétaire (PAPDEV)
Project details
Project number:2015.2104.6
Status:Projekt beendet
Responsible Organisational unit: 1100 Westafrika 1
Contact person:Helen Radeke helen.radeke@giz.de
Partner countries: Senegal, Senegal
Summary
Objectives:

Die Kapazitäten von Finanz-, Wirtschafts- u. Planministerium sowie von Sektorministerien zur Haushaltsplanung und -evaluierung sind gestärkt

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministère de l'Economie, des Finances et du Plan (MEFP)

Financing organisation:

not available

 
Project value
Total financial commitment:8 155 674 Euro
Financial commitment for this project number:2 415 404 Euro
Cofinancing

not available

 
Previous projects
2012.2512.7Unterstützung der ergebnisorientierten Entwicklungs- und Ausgabenplanung
Follow-on projects
2018.2019.0Unterstützung der ergebnisorientierten Entwicklungs- und Ausgabenplanung (PAPDEV II)
 
Term
Entire project:04.12.2012 - 31.12.2023
Actual project:14.12.2015 - 31.12.2018
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Politik und Verwaltung in Bezug auf den öffentl Sektor

Evaluation
not available
 




 
