Energieeffiziente Rehabilitierung von Studentenwohnheimen in Nordmazedonien

Die vorliegende FZ-Maßnahme mit einem Gesamtvolumen von bis zu 25 Mio. EUR soll Energieeffizienzmaßnahmen in öffentlichen Studentenwohnheimen in Nordmazedonien fördern. Insgesamt soll die energetische Rehabilitierung und die Verbesserung der Wohnbedingungen in mindestens sieben öffentlichen Studentenwohnheimen vorgenommen werden, die einen enormen Rehabilitierungsaufwand aufweisen, der sich neben den Energieeffizienzmaßnahmen auch auf strukturelle Maßnahmen (bspw. Fassade oder Dach) und Komfortmaßnahmen (bspw. Anstriche und Verbesserung der Sanitärräume) erstreckt.
Neben dem FZ-Darlehen wird die Maßnahme durch einen EU-Zuschuss von 4,8 Mio. EUR aus dem Regional Energy Efficiency Programme (REEP+) gefördert.

Land / Region / Institution Nordmazedonien Nummer 44523 Schwerpunkt Energie Sektor 23183 - Energieeffizienz (nachfrageseitig) USV-Kategorie B Finanzierungsinstrument Entwicklungskredit Weitere Geber - Deutscher Finanzierungsbeitrag 20 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner MINISTRY OF EDUCATION AND SCIENCE Zuständige Abteilung Südosteuropa und Türkei
