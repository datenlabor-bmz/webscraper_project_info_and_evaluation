  




 

Berufliche Bildung in Sri Lanka
Vocational Education and Training in Sri Lanka
Project details
Project number:2018.2089.3
Status:laufendes Projekt
Responsible Organisational unit: 2A00 Asien I
Contact person:Mathis Hemberger mathis.hemberger@giz.de
Partner countries: Sri Lanka, Sri Lanka
Summary
Objectives:

Jugendliche aus allen Bevölkerungsgruppen Sri Lankas nutzen ein verbessertes Angebot kooperativer beruflicher Bildung.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Skills Development and Vocational Training

Financing organisation:

not available

 
Project value
Total financial commitment:22 285 046 Euro
Financial commitment for this project number:7 670 000 Euro
Cofinancing

Staatssekretariat für Migration: 550 000Euro


 
Previous projects
2014.2290.6Berufliche Bildung im Norden Sri Lankas
Follow-on projects
not available
 
Term
Entire project:06.10.2011 - 31.05.2023
Actual project:01.07.2019 - 31.05.2023
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Berufliche Bildung

Evaluation
not available
 




 
