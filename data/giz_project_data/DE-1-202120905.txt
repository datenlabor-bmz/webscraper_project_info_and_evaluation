  




 

Förderung einer verantwortungsvollen Regierungsführung
Promoting good governance in Mauritania
Programme pour la promotion d’une gouvernance responsable
Project details
Project number:2021.2090.5
Status:laufendes Projekt
Responsible Organisational unit: 1100 Westafrika 1
Contact person:Mohamed Said Ahmed Abdi mohamed.ahmed2@giz.de
Partner countries: Mauritania, Mauritania
Summary
Objectives:

Die Umsetzung der Dezentralisierung durch mauretanische Partnerakteure ist verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministerium für Wirtschaft und Förderung der produktiven Sektroen

Financing organisation:

not available

 
Project value
Total financial commitment:6 000 000 Euro
Financial commitment for this project number:6 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:04.10.2021 - 30.06.2024
Actual project:01.01.2022 - 30.06.2024
other participants
ARGE BiRD GmbH-GOPA Worldwide
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Dezentralisierung und Förderung subnatio Gebietskörpers

Evaluation
not available
 




 
