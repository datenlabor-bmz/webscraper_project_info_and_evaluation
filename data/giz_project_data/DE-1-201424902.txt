  




 

Digitale Welt
Sector Programme Digital World
Project details
Project number:2014.2490.2
Status:Projekt beendet
Responsible Organisational unit: G120 Nachhaltige Wirtschentw, Digitalisierung
Contact person:Bjoern Richter bjoern.richter@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh., Jordan, Kenya, Nigeria, Rwanda, South Africa
Summary
Objectives:

Die deutsche Entwicklungszusammenarbeit nutzt Chancen des Digitalen Wandels.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:46 805 185 Euro
Financial commitment for this project number:2 328 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2016.2211.7Sektorprogramm Digitalisierung für Nachhaltige Entwicklung
 
Term
Entire project:30.12.2014 - 30.11.2023
Actual project:30.12.2014 - 01.08.2017
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
