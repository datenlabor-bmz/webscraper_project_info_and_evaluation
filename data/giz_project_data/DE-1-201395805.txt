  




 

Inklusive Bildung in der Entwicklungszusammenarbeit - Angewandte Forschung zur Inklusion von Benachteiligten in Bildung
Inclusiv education - applied research regarding the inclusion of disadvantaged groups into education systems
Project details
Project number:2013.9580.5
Status:Projekt beendet
Responsible Organisational unit: G110 Gesundheit, Bildung, Soziales
Contact person:Stephanie Petrasch stephanie.petrasch@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh.
Summary
Objectives:

Das Forschungsvorhaben ist gemäß des Forschungsbedarfes des BMZ durchgeführt, stellt für die deutsche und internationale Entwicklungszusammenarbeit relevante Instrumente für die Inklusion von Benachteiligten in Bildungssysteme zur Verfügung.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:2 960 683 Euro
Financial commitment for this project number:2 960 683 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:10.01.2013 - 31.12.2016
Actual project:10.01.2013 - 31.12.2016
other participants
Arge GOPA-Institute for Special
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Bildungspolitik und Verwaltung im Bildungswesen

Evaluation
Global research project: Inclusive education in development cooperation - applied research on the inclusive design of education systems. Project evaluation: Summary report
Globales Forschungsvorhaben: Inklusive Bildung in der Entwicklungszusammenarbeit - Angewandte Forschung zur inklusiven Gestaltung von Bildungssystemen. Projektevaluierung: Kurzbericht
 




 
