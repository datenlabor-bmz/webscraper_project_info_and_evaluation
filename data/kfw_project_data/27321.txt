Naturschutzgebiete und Biodiversität

Das FZ-Vorhaben "Schutzgebiete und Biodiversität" soll einen Beitrag zur Konsolidierung und Ausweitung des nationalen Schutzgebietssystems Kolumbiens leisten (Modulziel). Das Vorhaben ordnet sich dabei in die entwicklungspolitischen Prioritäten der Bundesregierung sowie Kolumbiens im Hinblick auf den Schutz und die Erhaltung der Biodiversität und wichtiger Ökosysteme ein. Insbesondere unterstützt es die Ziele der UN Convention on Biological Diversity (CBD) zur Ausweitung von Küsten- und Meeresschutzgebieten, die auch von der Bundesregierung ratifiziert wurde.

Die FZ-Mittel in Höhe von 15,0 Mio. EUR sind für Investitionen in den folgenden drei Komponenten vorgesehen:
1) Verbesserung der Managementeffektivität in bestehenden Schutzgebieten (15 Gebiete);
2) Einrichtung neuer Küsten- und Meeresschutzgebiete (3 Gebiete);
3) Stärkung der institutionellen und finanziellen Nachhaltigkeit der im Rahmen des Vorhabens geförderten Schutzgebiete.

Das Konzept des Vorhabens beruht auf einem integrierten Ansatz, der konkrete Fördermaßnahmen in ausgewählten Schutzgebieten mit Maßnahmen zur konzeptionellen Weiterentwicklung von Management- und Förderinstrumenten im Naturschutz und der dafür erforderlichen Stärkung der national und regional zuständigen Akteure verbindet.
Unmittelbare Zielgruppe ist die innerhalb und im Umfeld der Schutzgebiete des Vorhabens lebende, überwiegend indigene Bevölkerung. In und im Umfeld dieser 15 Schutzgebiete werden voraussichtlich etwa 52.000 Menschen vom Vorhaben profitieren.

Land / Region / Institution Kolumbien Nummer 27321 Schwerpunkt Umwelt und Klima Sektor 41030 - Biodiversität USV-Kategorie B+ Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber Eigenbeitrag d. Landes/Own contribution of country Deutscher Finanzierungsbeitrag 15 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner PARQUES NACIONALES NATURALES DE COLOMBIA Zuständige Abteilung Lateinamerika und Karibik
