  




 

Raumplanung und Landmanagement
Spatial Planning and Landmanagement
Project details
Project number:2018.2239.4
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Michael Becker michael.becker@giz.de
Partner countries: Kosovo, Kosovo
Summary
Objectives:

Kommunale und nationale Institutionen erfüllen ihre Aufgaben im Bereich Raumplanung besser

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Environment and Spatal Planning

Financing organisation:

not available

 
Project value
Total financial commitment:7 322 545 Euro
Financial commitment for this project number:2 000 000 Euro
Cofinancing

not available

 
Previous projects
2016.2233.1Stärkung von Raumplanung und Landmanagement
Follow-on projects
not available
 
Term
Entire project:22.11.2010 - 30.05.2022
Actual project:01.07.2019 - 30.05.2022
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektziel-Ebene: Projekt zielt vor allem auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Stadtentwicklung und -verwaltung

Evaluation
not available
 




 
