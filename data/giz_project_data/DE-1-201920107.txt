  




 

Sektorprogramm Digitalisierung für Nachhaltige Entwicklung
sector programme digitalisation
Project details
Project number:2019.2010.7
Status:laufendes Projekt
Responsible Organisational unit: G100 Wirtschaft, Soziales, Digitalisierung
Contact person:Bjoern Richter bjoern.richter@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh., Ghana, India, Rwanda, Uganda, South Africa
Summary
Objectives:

Sektorprogramm Digitalisierung für Nachhaltige Entwicklung

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Deutsche Gesellschaft für Internationale Zusammenarbeit (GIZ) GmbH

Financing organisation:

not available

 
Project value
Total financial commitment:46 596 958 Euro
Financial commitment for this project number:32 420 163 Euro
Cofinancing

CalCEF Innovations: 116 363Euro


Europäische Union (EU): 2 753 800Euro


 
Previous projects
2016.2211.7Sektorprogramm Digitalisierung für Nachhaltige Entwicklung
Follow-on projects
not available
 
Term
Entire project:30.12.2014 - 30.11.2023
Actual project:01.07.2019 - 30.11.2023
other participants
ARGE GFA Consulting Group GmbH-UR HG Ltd
ARGE Studio GOOD digital GmbH-bleech
Arge Studio GOOD digital GmbH-
Crolla Lowis GmbH
ekipa GmbH
neues handeln AG
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Informations- und Kommunikationstechnologien (IKT)

Evaluation
not available
 




 
