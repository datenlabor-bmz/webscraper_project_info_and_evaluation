  




 

Sustainable Development Solutions Network II (SDSN II)
Support of the Sustainable Development Solutions Network - SDSN
Project details
Project number:2017.6252.5
Status:Projekt beendet
Responsible Organisational unit: G410 Global Policy
Contact person:Nina Narith Ouan nina.ouan@giz.de
Partner countries: Internat. Zusammenarbeit mit Regionen für Nachhalt, ZZZ
Summary
Objectives:

Das Sustainable Development Solutions Network (SDSN) trägt praxisorientiert ueber Netzwerke des Südens zur Umsetzung der Agenda 2030 in sektorübergreifenden Themen und Fragen der Globalen Verantwortung bei.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:11 763 156 Euro
Financial commitment for this project number:3 970 000 Euro
Cofinancing

not available

 
Previous projects
2013.6257.3Förderung des Sustainable Development Solution Network (SDSN)
Follow-on projects
2020.2162.4Sustainable Development Solutions Network - SDSN III
 
Term
Entire project:18.09.2013 - 30.06.2023
Actual project:01.01.2018 - 30.04.2021
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Multisektorale Hilfe

Evaluation
not available
 
Project description (DE)

Das Lösungsnetzwerk für nachhaltige Entwicklung (Sustainable Development Solutions Network, SDSN) wurde 2012 auf Initiative des damaligen Generalsekretärs der Vereinten Nationen Ban Ki-moon gegründet. Ziel des Netzwerks ist es, wissenschaftlich erarbeitete praktische Lösungsansätze für eine nachhaltige Entwicklung auf lokaler, nationaler, regionaler und globaler Ebene zu verbreiten. Das SDSN verfolgt hierzu vier Arbeitslinien: (1) Unterstützung der Umsetzung der Agenda 2030, einschließlich der SDGs, der Finanzierung von Entwicklung sowie dem Pariser Klimaabkommen; (2) Aufbau von thematischen Netzwerken und Förderung von Lösungsinitiativen zu den Kernthemen der Agenda 2030; (3) Aufbau nationaler und regionaler SDSN-Netzwerke zur Unterstützung praktischer Lösungsansätze für nachhaltige Entwicklung; und (4) Entwicklung einer hochwertigen, frei und online zugänglichen Bildungsplattform für Umsetzer*innen der nachhaltigen Entwicklung durch die SDG Academy.
Das IZR-Vorhaben „Stärkung des Sustainable Development Solutions Network, SDSN" verfolgt folgendes Ziel: Das Sustainable Development Solutions Network trägt praxisorientiert über Netzwerke des Südens zur Umsetzung der Agenda 2030 in sektorübergreifenden Themen und Fragen globaler Verantwortung bei.
Das Vorhaben zielt auf die Stärkung der Umsetzung der Agenda 2030 und hier insbesondere die Mitwirkung der Wissenschaft. Über die grundsätzliche Beteiligung der Wissenschaft an globalen Debatten hinaus, stärkt das Vorhaben insbesondere die Rolle der Wissenschaft für die Entwicklung integrierter und sektorübergreifender Ansätze und Fragen der globalen Verantwortung.
Zur Erreichung des Ziels ist das Vorhaben in fünf Handlungsfeldern tätig: 1) Unterstützung der Netzwerke des Südens, 2) Organisationsentwicklung, 3) Integrierte und sektorübergreifende Ansätze zur Umsetzung der Agenda 2030, 4) Globale Verantwortung (insbesondere globaler Fußabdruck) und 5) Praxisorientierung.
Das Vorhaben hat eine Laufzeit von drei Jahren (01/2018 bis 12/2020) mit Kosten des deutschen Beitrags von bis zu 3.750.000 EUR.



 

 
Project description (EN)

The Sustainable Development Solutions Network (SDSN) was established in 2012 under the patronage of then United Nations Secretary-General Ban Ki-moon. The aim of the network is to disseminate scientifically developed practical solutions for sustainable development at local, national, regional and global levels. The SDSN has four working lines: (1) supporting the implementation of the 2030 Agenda, including the SDGs, development funding and the Paris Climate Agreement; (2) setting up thematic networks and promoting solution initiatives on the core themes of the 2030 Agenda; (3) building national and regional SDSN networks to support practical approaches to sustainable development; and (4) the development of a high-quality, open and accessible educational platform for sustainable development practitioners through the SDG Academy.
The IZR project "Strengthening the Sustainable Development Solutions Network (SDSN)" pursues the following goal: Via SDSN networks of developing countries and emerging economies, the Sustainable Development Solutions Network (SDSN) makes a practice-oriented contribution to implementing the 2030 Agenda for Sustainable Development in terms of cross-sectoral topics and issues related to global responsibility.
The project aims to strengthen the implementation of the 2030 Agenda and in particular the participation of science. Beyond the fundamental involvement of science in global debates, the project particularly strengthens the role of science in the development of integrated and cross-sectoral approaches and issues of global responsibility.
To achieve the goal, the project operates in five action areas: 1) supporting networks of the global South, 2) organizational development, 3) integrated and cross-sectoral approaches to the implementation of the 2030 Agenda, 4) global responsibility (in particular global footprint) and 5) applicable recommendations.
The project has a term of three years (01/2018 to 12/2020) with costs of the German contribution of up to 3,750,000 EUR.
 





 
