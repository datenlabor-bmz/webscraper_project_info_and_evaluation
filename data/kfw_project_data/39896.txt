FPEMP Phase 2 - Fiscal and Public Expenditure Management Program (FPEMP)

Es handelt sich um die 2. von geplanten drei Phasen der Programmfinanzierung "Fiscal and Public Expenditure Management Program (FPEM)". Der Republik Indonesien, vertreten durch das Finanzministerium, wird wie in der ersten Phase (38463 / 202084143) ein Förderkredit zur Verfügung gestellt. Das Programm erfolgt in paralleler Kofinanzierung mit der ADB als "lead donor".
Die Programmfinanzierung ("policy-based"-Darlehen) zielt darauf, die indonesische Regierung bei ihrem Reformprozess im Bereich Fiskalpolitik / öffentliche Finanzen zu unterstützen. Dadurch soll ein Beitrag geleistet werden zu einem verbesserten Ausgabenmanagement insbesondere in sozialen Sektoren und im Infrastrukturbereich. Schwerpunkte des Programms sind Reformen zur Verbesserung:
- der Budgetplanung mit Orientierung an den nationalen Entwicklungszielen und UN Sustainable Development Goals (SDGs),
- des öffentlichen Ausgabenmanagements auf nationaler Ebene,
- der Systeme für fiskalische Transfers und des Ausgabenmanagements auf der Ebene der Lokalregierungen.
Die zu finanzierenden Reformen werden von ADB und KfW mit den Partnern in einer Policy Matrix vereinbart und deren Erfüllung dient als Auzahlungsvoraussetzung.

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde entsprechend der Nachhaltigkeitsrichtlinie der KfW Entwicklungsbank in die Umwelt- und Sozialrisikokategorie C eingeordnet, da vom Vorhaben voraussichtlich keine oder nur geringe nachteilige Auswirkungen und Risiken auf Umwelt und soziale Belange ausgehen. Es ist daher kein spezifischer Umwelt- und Sozialmanagementplan erforderlich. Der Partner wurde zur Einführung eines Beschwerdemechanismus verpflichtet und muss die KfW unverzüglich über umwelt- und sozialrelevante besondere Vorkommnisse informieren.

Land / Region / Institution Indonesien Nummer 39896 Schwerpunkt Wirtschaft und Finanzsektor Sektor 15111 - Management der öffentlichen Finanzen USV-Kategorie C Finanzierungsinstrument Förderkredit Weitere Geber Asian Development Bank (ADB) Deutscher Finanzierungsbeitrag 200 Mio. EUR Status abgeschlossen Auftraggeber BMZ Projektpartner - Zuständige Abteilung Ostasien und Pazifik
