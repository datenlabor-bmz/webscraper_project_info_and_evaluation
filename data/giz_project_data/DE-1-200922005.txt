  




 

Förderprogramm zur Stärkung der Privatwirtschaft - Abschlussphase
Small and Medium Sized Enterprise Development for Sustainable Employment Programme
Project details
Project number:2009.2200.5
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Dr. Volker Steigerwald volker.steigerwald@giz.de
Partner countries: Philippines
Summary
Objectives:

Die Rahmenbedingungen für die Entwicklung der Privatwirtschaft auf den Philippinen und insbesondere in der Region Visayas sind verbessert.

Client:

BMZ

Project partner:

Department of Trade and Industry (DTI)

Financing organisation:

not available

 
Project value
Total financial commitment:19 303 076 Euro
Financial commitment for this project number:3 850 000 Euro
Cofinancing

not available

 
Previous projects
2006.2036.9Förderprogramm zur Stärkung der Privatwirtschaft - Phase II
Follow-on projects
not available
 
Term
Entire project:03.07.1995 - 25.06.2013
Actual project:08.09.2009 - 25.06.2013
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Geschäftspolitik und -verwaltung

Evaluation
not available
 




 
