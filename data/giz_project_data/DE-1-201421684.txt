  




 

Nachhaltiges Ressourcenmanagement im Fokus des Klimawandels
Sustainable Management of Resources in the Focus of Climate Change
Gestion Sostenible de los Recuros Naturales con Enfoque a la Adptacion al Cambio Climático
Project details
Project number:2014.2168.4
Status:Projekt beendet
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Michael Ziegler michael.ziegler@giz.de
Partner countries: Honduras, Honduras
Summary
Objectives:

Der Schutz und die nachhaltige Nutzung der natürlichen Ressourcen sowie die Anpassung an den Klimawandel werden in der Region des Biosphärenreservats Cacique Lempira, Senor de las Montanas gestärkt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Secretaría de Relaciones Exteriores y Cooperación Internacional

Financing organisation:

not available

 
Project value
Total financial commitment:13 000 000 Euro
Financial commitment for this project number:7 029 624 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2018.2159.4Nachhaltiges Ressourcenmanagement im Fokus des Klimawandels
 
Term
Entire project:11.11.2015 - 31.12.2023
Actual project:01.05.2016 - 30.11.2020
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektziel-Ebene: Projekt zielt vor allem auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Ländliche Entwicklung

Evaluation
Zentrale Projektevaluierung - Zusammenfassung. Nachhaltiges Ressourcenmanagement im Fokus des Klimawandels (PROCAMBIO)
Evaluación central de proyecto - Resumen ejecutivo. Gestión Sostenible de Recursos Naturales con Enfoque a la Adaptación al Cambio Climático (PROCAMBIO)
Zentrale Projektevaluierung - Auf einen Blick. Nachhaltiges Ressourcenmanagement im Fokus des Klimawandels (PROCAMBIO)
Evaluación central de proyecto. Gestión Sostenible de Recursos Naturales con Enfoque a la Adaptación al Cambio Climático (PROCAMBIO), Honduras. Informe de evaluación
Evaluación central de proyecto - De un vistazo. Gestión Sostenible de Recursos Naturales con Enfoque a la Adaptación al Cambio Climático (PROCAMBIO)
 
Project description (DE)

Ausgangssituation

In Honduras spielt die Landwirtschaft eine große Rolle. Sie trägt nicht nur wesentlich zum Bruttoinlandsprodukt bei, sondern beschäftigt auch etwa ein Drittel der Bevölkerung. Auch wenn viele Nahrungsmittel angebaut werden, zum Beispiel Mais und Bohnen, ist der Kaffee wichtigstes Cash Crop, das heißt ein Erzeugnis für den Markt und nicht die Selbstversorgung. Honduras ist einer der größten Kaffeeexporteure der Welt.

Honduras ist eines der am meisten von starken Wetterereignissen betroffenen Länder. Dürren, Hurrikane, Überflutungen und Erdrutsche führen immer wieder zu Ernteausfällen und Nahrungsmittelknappheit. Das Auskommen der ärmsten Bevölkerungsgruppen, etwa Kleinbauern und indigene Völker, ist dadurch stets bedroht. Der Anbau von Monokulturen, unkontrollierte Waldbrände und Extremwetterereignisse, die durch den Klimawandel immer stärker und häufiger auftreten, führen zur Degradierung der natürlichen Ressourcen, zu Wasserknappheit, Entwaldung und schließlich einer Abnahme der Biodiversität. Besonders betroffen von den veränderten Klimaverhältnissen ist der Westen des Landes. Hier liegt das von der UNESCO zum Biosphärenreservat erklärte „Cacique Lempira – Señor de las Montañas" Das Gebiet hat mehr als 150.000 Einwohner, darunter zahlreiche Gemeinden des indigenen Volkes der Lenca, von denen die meisten traditionelle Landwirtschaft betreiben.

2010 hat Honduras eine nationale Klimawandelstrategie ausgearbeitet und sie 2014 in einem Gesetz zum Klimawandel verabschiedet. Eine verbesserte nationale und regionale Koordination ist jedoch nötig, um auch lokal eine effektive Umsetzung zu garantieren.

Ziel

Nationale, regionale und lokale Akteure setzen den Schutz und die nachhaltige Nutzung der natürlichen Ressourcen um. Im Mittelpunkt steht die Anpassung an den Klimawandel in der Region des Biosphärenreservats „Cacique Lempira – Señor de las Montañas".

Vorgehensweise

Die GIZ arbeitet eng mit den honduranischen Behörden zusammen. Ziel ist die erfolgreiche Umsetzung von Strategien zum nachhaltigen Management der natürlichen Ressourcen. Ein neu geschaffenes nationales und ein regionales Komitee sind für die Steuerung und Umsetzung der Prozesse verantwortlich. Das Vorhaben unterstützt nicht nur die nationale und regionale Koordination maßgeblicher Ministerien, beispielsweise des Umweltministeriums. Es fördert auch die Eingliederung und Teilhabe lokaler Institutionen und Organisationen, zum Beispiel Bauernorganisationen und indigene Gruppen, in Entscheidungsprozesse und bei der praktischen Umsetzung.

Der Schutz und das nachhaltige Management von natürlichen Ressourcen werden nicht nur die Lebensbedingungen den Menschen verbessern, sondern – im Einklang mit internationalen Abkommen – auch Klimarisiken vermindern und den Erhalt der Biodiversität fördern.

Wirkungen

Der Schutz natürlicher Ressourcen und der Biodiversität, vor allem angesichts sich verschlimmernder Folgen des Klimawandels, hat auf allen Ebenen hohe Priorität bekommen. Die GIZ hat nationale und regionale Schlüsselakteure ermittelt und fördert ihre Vernetzung, was für eine erfolgreiche Umsetzung unabdingbar ist.

Auch die Bewohner vor Ort profitieren bereits nachhaltig vom Projekt: Sie werden zum Thema Anpassung an den Klimawandel und nachhaltiges Ressourcenmanagement fortgebildet und sind an Entscheidungsprozessen beteiligt. Darüber hinaus haben sie mithilfe der GIZ alternative Einkommensquellen für sich entdeckt, etwa Honig, wilde Brombeeren und Balsam vom Liquidambar-Baum.
 





 
