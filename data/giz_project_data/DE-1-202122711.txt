  




 

Grüne Finanzmarktregulierung und Green Bonds (FiBraS) II
Green Financial Market Regulation and Green Bonds (FiBraS)
Regulação do mercado para financiamento verde e títulos verdes (FiBraS)
Project details
Project number:2021.2271.1
Status:laufendes Projekt
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Christine Majowski christine.majowski@giz.de
Partner countries: Brazil, Brazil
Summary
Objectives:

Die Rahmenbedingungen für die Skalierung Grüner Finanzmärkte sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministério da Fazenda

Financing organisation:

not available

 
Project value
Total financial commitment:7 400 000 Euro
Financial commitment for this project number:3 400 000 Euro
Cofinancing

not available

 
Previous projects
2016.2256.2Grüne Finanzmarktregulierung und Green Bonds (FiBraS)
Follow-on projects
not available
 
Term
Entire project:15.09.2017 - 31.12.2025
Actual project:04.07.2022 - 31.12.2025
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
Armutsorientierungnot available
CRS code
Finanzsektorpolitik und -verwaltung

Evaluation
not available
 




 
