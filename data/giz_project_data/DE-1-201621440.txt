  




 

Förderung beschäftigungswirksamer Exportaktivitäten in neue Märkte
Promotion of employment-intensive export activities in new markets
Promotion des activités d'export créatrices l'emploi vers de nouveaux marchés
Project details
Project number:2016.2144.0
Status:Projekt beendet
Responsible Organisational unit: 3600 Nordafrika
Contact person:Lisa Menucha lisa.menucha@giz.de
Partner countries: Tunisia, Tunisia
Summary
Objectives:

Der beschäftigungswirksame Zugang ausgewählter tunesischer kleinen und mittleren Unternehmen (KMU) zu neuen Exportmärkten in Afrika ist verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministère du Commerce

Financing organisation:

not available

 
Project value
Total financial commitment:12 000 000 Euro
Financial commitment for this project number:4 718 732 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2019.2106.3Förderung von Exportaktivitäten in neue Märkte Subsahara-Afrikas in Tunesien
 
Term
Entire project:23.12.2016 - 31.12.2024
Actual project:07.02.2018 - 30.01.2021
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Geschäftspolitik und -verwaltung

Evaluation
not available
 
Project description (DE)

Ausgangssituation
Um den sozialen Frieden in der jungen Demokratie zu erhalten und Fluchtbewegungen einzudämmen, braucht Tunesien Stabilität und Wachstum. Das Land steht vor der Herausforderung, mehr Arbeitsplätze zu schaffen. Der Binnenmarkt ist klein, sodass wirtschaftliches Wachstum unmittelbar mit dem Export von Waren und Dienstleistungen verbunden ist. Drei Viertel aller Exportunternehmen kommen aber aus nur drei Branchen: Textil- und Lederwaren, Lebensmittel sowie Elektronik. Die meisten wichtigen Exportprodukte haben sich seit über 20 Jahren nicht verändert. Dies hängt auch mit der starken Ausrichtung auf den europäischen Markt zusammen.
Obgleich zahlreiche subsaharische Länder die höchsten Wachstumsraten weltweit verzeichnen, gehen nur rund zehn Prozent der tunesischen Gesamtexporte auf den afrikanischen Kontinent. Die tunesische Regierung hat das Potenzial afrikanischer Märkte erkannt und will den Export und die Investitionen intensivieren.

Ziel
Der beschäftigungswirksame Zugang ausgewählter kleiner und mittlerer Unternehmen Tunesiens zu neuen Exportmärkten in Afrika ist verbessert.

Vorgehensweise
Das Projektteam arbeitet mit kleinen und mittleren Industrie- und Dienstleistungsunternehmen zusammen, die noch auf keinem oder nur wenigen afrikanischen Märkten vertreten sind.
Unternehmen der Branchen Ernährungs-, Bau und Gesundheitswirtschaft (und zukünftig IT) haben sich mit Unterstützung des Projektes und der Consultingfirma Deloitte in Exportkonsortien zusammengeschlossen, um gemeinsam neue Märkte in Afrika zu erschließen. Die Konsortien entwickeln zusammen Strategien für den Export in die fünf afrikanischen Zielmärkte: Kamerun, Elfenbeinküste, Demokratische Republik Kongo, Kenia und Nigeria.

Das Projekt spricht explizit auch deutsche Firmen an, die den Markteintritt nach Subsahara-Afrika suchen, dazu aber auf Partner*innen auf dem Kontinent angewiesen sind. Über Fachveranstaltungen, Geschäftsreisen und Messebesuche in Deutschland, Tunesien und den afrikanischen Zielmärkten können Kontakte angebahnt und Partnerschaften geknüpft werden.

Mit der staatlichen Exportagentur CEPEX, Handelskammern, Unternehmensverbänden sowie spezialisierten Beratungsunternehmen wurde die Exportplattform Think Africa gegründet. Die Plattform zeigt tunesischen Firmen Marktchancen, vor allem auch in den fünf afrikanischen Zielmärkten auf. Sie stimmt Informations-, Beratungs- und Unterstützungsangebote ihrer Mitglieder aufeinander ab.
Damit auch kleine Unternehmen ein professionelles und spezialisiertes privates Beratungsangebot für den Eintritt in afrikanische Märkte finden können, bildet das Projekt Berater*innen aus und unterstützt die Institutionalisierung eines Beraternetzwerks.

Um die Rahmenbedingungen für den Handel zwischen Tunesien und den afrikanischen Zielmärkten zu verbessern, unterstützt das Projektteam darüber hinaus Vertreter*innen des Handelsministeriums sowie weiterer relevanter Regierungsinstitutionen dabei, ihre Leistungsfähigkeit in Bezug auf Verhandlung, Umsetzung und Kontrolle von regionalen Handelsabkommen zu verbessern. Auf diese Weise wird die regionale Integration Tunesiens gestärkt, es werden Handelsschranken abgebaut und so die Exportchancen tunesischer Firmen verbessert.

Wirkungen
Die Arbeit der Exportplattform Think Africa nimmt an Fahrt auf. Mitglieder waren gemeinsam auf Geschäftsreisen, so beispielsweise auf einer Markterkundungsreise nach Kenia und einer anschließenden Roadshow in Tunesien. Über Marktchancen in Kamerun, Elfenbeinküste und der Demokratischen Republik Kongo haben sich tunesische Unternehmen bereits in neun Informations- und Matchmaking-Veranstaltungen informieren können. Die Plattform lud potenzielle Käufer*innen aus den drei Ländern ein und organisierte Geschäftstreffen mit 18 afrikanischen Geschäftsleuten. 850 Personen aus 500 tunesischen Unternehmen nahmen an den Veranstaltungen teil.


In den drei bereits aufgebauten Exportkonsortien haben sich etwa 40 kleine und mittlere Unternehmen organisiert und formalisiert. Alle haben einen Arbeitsplan zur Erschließung afrikanischer Märkte entwickelt.

Eine repräsentative Umfrage zu ersten Aktivitäten des Projekts wurde bereits durchgeführt. Demnach verzeichnet mehr als die Hälfte der 500 erreichten Unternehmen bereits nach einem Jahr Projektlaufzeit höhere Exportchancen, beispielsweise durch einen besseren Zugang zu Marktinformationen. 18 Unternehmen haben neue Geschäftsbeziehungen mit Partner*innen in Subsahara-Afrika etabliert. Vier Unternehmen haben 34 neue Arbeitsplätze geschaffen, um neue afrikanische Märkte zu erschließen. Durch den Aufbau von langfristigen Geschäftsbeziehungen fördert das Programm langfristige Beschäftigung. 

 
Project description (EN)

Context
Tunisia needs stability and growth to maintain social peace in its young democracy and to stem refugee flows. The country therefore faces the challenge of having to create more employment. Because of its small domestic market, exporting goods and services is crucial to the growth of the economy. However, three quarters of all exporters operate in just three sectors, namely textiles and leather goods, food and electronics. Most of the main exports have not changed in over 20 years. One reason for this is the country’s strong focus on the European market.
Although many sub-Saharan countries boast the world’s highest economic growth rates, only around ten per cent of Tunisia’s exports go to the African continent. The Tunisian Government has recognised the potential of African markets and intends to increase exports and investment.

Objective
Selected small and medium-sized Tunisian enterprises have better access to new export markets in Africa, resulting in more employment.

Approach
The project team primarily works with small and medium-sized enterprises in the manufacturing and service sector that have little or no representation in African markets.

With support from the project and the consulting firm Deloitte, companies in the food, construction and health industries have established export consortia to jointly tap into new markets in Africa. An IT consortium is set join shortly. The consortia are jointly developing strategies for exporting to the five African target markets: Cameroon, Côte d’Ivoire, the Democratic Republic of Congo, Kenya and Nigeria.

The project also explicitly targets German enterprises that are looking to enter the sub-Saharan market but need partners on the continent to do so. It organises business trips and visits to trade fairs in Germany, Tunisia and the African target markets in order to help establish contacts and form partnerships.

The export platform Think Africa was founded together with the public export agency CEPEX, chambers of commerce, business associations and specialised consulting firms. It points businesses to market opportunities, especially in the five African target markets. The platform aims to coordinate the information, advisory and support services of its members.
The project trains advisors and supports the institutionalisation of an advisory network to provide even small enterprises with professional and specialised private advisory services that facilitate access to African markets.

In order to improve the framework for trade between Tunisia and the African target markets, the project team also supports representatives of the Ministry of Trade and other relevant government agencies to improve their capacity to negotiate, implement and monitor regional trade agreements. The goal is to strengthen Tunisia's regional integration, dismantle trade barriers and thus improve the export opportunities of Tunisian companies.

Results
The work of the export platform Think Africa is gathering pace. Members have undertaken joint business trips, including a market exploration trip to Kenya and a subsequent roadshow in Tunisia. Tunisian companies attended nine information and matchmaking events where they learned about market opportunities in Cameroon, Côte d’Ivoire and the Democratic Republic of Congo. The platform has invited potential buyers from the three countries and organised B2B meetings with 18 African buyers. So far, 850 representatives from 500 Tunisian companies have attended these events.

Some 40 small and medium-sized enterprises have organised and formalised their collaboration efforts in the three established export consortia. All three consortia have developed work plans to access African markets.

A representative survey of the initial activities of the project showed that more than half of the 500 enterprises that were reached reported improved export opportunities after the first year, for example through better access to market information. Eighteen companies have managed to establish new business relationships with partners in sub-Saharan Africa. Four of them created 34 new jobs in order to venture into new African markets. The project is promoting sustainable employment by building long-term business relationships.
 

 
Project description (FR)

Situation initiale
Pour préserver la paix sociale dans cette jeune démocratie et endiguer les mouvements migratoires, la Tunisie a besoin de stabilité et de croissance. Le défi consiste donc pour le pays à créer plus d’emplois. Le marché intérieur est restreint, si bien que la croissance économique est directement liée aux exportations de biens et de services. Les trois quarts des entreprises exportatrices n’appartiennent actuellement qu’à trois secteurs : textile et maroquinerie, agroalimentaire et électronique. Pour la plupart, les principaux produits exportés n’ont pas changé depuis plus de vingt ans, ce qui s’explique notamment par une forte orientation vers le marché européen.
Alors que de nombreux pays subsahariens enregistrent les plus forts taux de croissance dans le monde, seulement 10 % environ de l’ensemble des exportations tunisiennes sont destinées au continent africain. Le gouvernement tunisien a pris conscience du potentiel que constituent ces marchés africains et entend intensifier les exportations et les investissements.

Objectif
Intéressant en termes de création d’emplois, l’accès à de nouveaux marchés d’exportation en Afrique pour de petites et moyennes entreprises tunisiennes sélectionnées est amélioré.

Approche
L’équipe de projet travaille essentiellement avec des petites et moyennes entreprises du secteur de l’industrie et des services qui ne sont pas ou que peu présentes sur les marchés africains.

Des entreprises issues des secteurs de l’agroalimentaire, de la construction et de la santé (et désormais des TIC) ont uni leurs forces au sein de consortiums d’exportation avec le soutien du projet ainsi que du cabinet Deloitte Conseil. Le but est de conquérir conjointement de nouveaux marchés en Afrique. Ces consortiums élaborent des stratégies d'exportation vers cinq marchés cibles africains : Cameroun, Côte d'Ivoire, RD Congo, Kenya et Nigeria.

Le projet s’adresse aussi de façon explicite à des entreprises allemandes désireuses de pénétrer les marchés de l’Afrique subsaharienne, et qui ont besoin de partenaires sur le continent pour ce faire. Grâce à des événements professionnels, des voyages d’affaires et des visites de foires et salons en Allemagne, en Tunisie et dans les pays africains visés, des contacts peuvent être noués et des partenariats, initiés.

Ensemble avec le CEPEX, le Ministère du Commerce, les chambres de commerce régionales, les fédérations d'entreprises et des cabinets de conseil spécialisés, l'équipe de projet a créé le réseau d’exportation Think Africa. Cette plateforme diffuse les opportunités de marché aux entreprises tunisiennes, notamment sur les cinq marchés cibles africains. Elle coordonne les services d'information, de conseil et de soutien proposés par ses différents membres. Pour que les petites entreprises puissent aussi trouver dans le secteur privé un conseil professionnel et spécialisé dans l’accès aux marchés africains, le projet forme des conseillers et conseillères et soutient l’institutionnalisation d’un réseau de consultants.

Afin d’améliorer les conditions encadrant le commerce entre la Tunisie et les marchés africains visés, l'équipe du projet appuie également les représentant·e·s du Ministère du Commerce et d'autres institutions gouvernementales de sorte à consolider leurs capacités à négocier, appliquer et suivre les accords commerciaux régionaux. Ainsi, l'intégration régionale de la Tunisie sera renforcée, les barrières commerciales seront démantelées et les opportunités d'exportation des entreprises tunisiennes, améliorées.

Impact
Le travail de la plateforme d'exportation Think Africa s'accélère. Ses membres ont planifié des voyages d'affaires communs, tel un voyage de prospection au Kenya. Déjà, neuf événements d'information et de mise en relation ont été organisés dans différentes régions de Tunisie en 2019 et des entreprises tunisiennes s’y sont informées sur les opportunités de marché au Cameroun, en Côte d'Ivoire et en RD Congo. La plateforme a invité des acheteurs potentiels des trois pays et a organisé des rencontres B2B avec 18 hommes et femmes d’affaires africains. 850 personnes issues de 500 entreprises tunisiennes ont assisté à ces manifestations.

Dans les trois consortiums d'exportation déjà établis, une quarantaine de petites et moyennes entreprises se sont organisées et formalisées. Les trois ont élaboré une feuille de route pour la pénétration des marchés africains ciblés.

Selon une première enquête représentative sur les activités du projet, plus de la moitié des 500 petites et moyennes entreprises tunisiennes concernées par le projet font état déjà au bout d’un an d’un meilleur accès à l'information commerciale sur les marchés cibles. 18 entreprises ont établi de nouvelles relations commerciales avec un partenaire en Afrique subsaharienne. Quatre entreprises ont créé 34 emplois afin de s’établir sur ces nouveaux marchés. En nouant des relations d'affaires à long terme, le projet contribue également à promouvoir des emplois durables.
 





 
