  




 

Förderung von Good Financial Governance
Good Financial Governance II
Buena Gobernabilidad Fiscal II
Project details
Project number:2014.2164.3
Status:Projekt beendet
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Dr. Gerhard Schmalbruch gerhard.schmalbruch@giz.de
Partner countries: Guatemala, Guatemala
Summary
Objectives:

Die Leistungsfähigkeit und Transparenz des guatemaltekischen Systems der Öffentlichen Finanzen sind auf nationaler und subnationaler Ebene im Sinne der Guten Finanziellen Regierungsführung erhöht.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministerio de Finanzas Públicas

Financing organisation:

not available

 
Project value
Total financial commitment:4 296 812 Euro
Financial commitment for this project number:2 500 000 Euro
Cofinancing

not available

 
Previous projects
2012.2137.3Förderung von Good Financial Governance
Follow-on projects
not available
 
Term
Entire project:21.12.2012 - 31.12.2018
Actual project:22.10.2015 - 31.12.2018
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Mobilisierung von Eigeneinnahmen

Evaluation
not available
 




 
