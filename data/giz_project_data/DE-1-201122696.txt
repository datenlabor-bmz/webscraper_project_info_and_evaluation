  




 

Globale entwicklungspolitische Rohstoffinitiative (GeRI)-GIZ
Global extractive Resources Initiative (GeRI) - GIZ
Project details
Project number:2011.2269.6
Status:Projekt beendet
Responsible Organisational unit: G210 Flucht, Migration, Rückkehr
Contact person:Dr. Markus Wagner markus.wagner@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh.
Summary
Objectives:

Die deutsche Entwicklungszusammenarbeit gestaltet einen partizipativen, umsetzungs- und wirkungsorientierten Strategieprozess zur Operationalisierung der entwicklungspolitischen Rohstoffinitiative.

Client:

BMZ

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:2 709 223 Euro
Financial commitment for this project number:2 709 223 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:28.12.2011 - 31.03.2016
Actual project:28.12.2011 - 31.03.2016
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Bodenschätze / Bergbaupolitik und -verwaltung

Evaluation
not available
 




 
