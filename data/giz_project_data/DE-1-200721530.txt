  




 

Stärkung kommunaler Strukturen, Maghreb
Strengthening of Municipal Structures, Maghreb
Coopération des Villes et des Municipalités au Maghreb (CoMun)
Project details
Project number:2007.2153.0
Status:Projekt beendet
Responsible Organisational unit: 3600 Nordafrika
Contact person:Meinolf Spiekermann meinolf.spiekermann@giz.de
Partner countries: N. of Sahara
Summary
Objectives:

Städte u. Kommunen, lokale Verwaltungen u. Organisationen d. Zivilgesellschaft in Algerien, Marokko, Tunesien kooperieren direkt, über ein gemeinsames Netzwerk u. intern. Plattformen in Schlüsselthemen ihrer Aufgaben zur Verbesserung der Lebensbedingungen in Städten.

Client:

BMZ

Project partner:

Städte und Kommunen

Financing organisation:

not available

 
Project value
Total financial commitment:16 525 168 Euro
Financial commitment for this project number:2 025 168 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2010.2023.9Stärkung kommunaler Strukturen, Maghreb
 
Term
Entire project:12.12.2007 - 30.01.2020
Actual project:01.08.2008 - 05.02.2013
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Stadtentwicklung und -verwaltung

Evaluation
Stärkung kommunaler Entwicklung und Demokratie im Maghreb (CoMun). Projektevaluierung: Kurzbericht
Strengthening municipal development and democracy in the Maghreb (CoMun). Project evaluation: summary report
 




 
