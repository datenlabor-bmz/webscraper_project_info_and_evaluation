Stepping Up Investments for Growth Acceleration Programme (SIGAP) Phase III

Ziel und Förderansatz: Das Sektorprogramm "Stepping Up Investments for Growth Acceleration Programme (SIGAP)" in Kofinanzierung mit der ADB hat zum Ziel, die indonesische Regierung bei der Umstrukturierung der aktuell noch stark konsumorientierten zu einer investitionsorientierten Wirtschaft zu unterstützen. Dadurch soll ein Beitrag geleistet werden zur Verbesserung der Anpassungsfähigkeit des Landes gegenüber externen Schocks und zur Stärkung der Rahmenbedingungen für nachhaltige wirtschaftliche Entwicklung. SIGAP wird sich auf die Umsetzung von Reformen zur (i) Verbesserung des Geschäftsklimas, (ii) Stärkung öffentlicher und privater Infrastrukturinvestitionen und (iii) Vereinfachung öffentlicher Vergabeverfahren konzentrieren.

Partnerstruktur: Darlehensnehmer ist die Republik Indonesien, vertreten durch das Finanzministerium. Projekt implementierende Einheit ist das Koordinierende Wirtschaftsministerium.

Kosten und Finanzierung. SIGAP umfasst drei Phasen: Phase I mit Zusage im September 2014, Phase II mit geplanter Zusage im Juli 2016 sowie Phase III mit geplanter Zusage im Juli 2018. Für Phase III plant die ADB, ein Darlehen in Höhe von 400 Mio. USD zuzusagen. Die KfW plant, einen FZ-Förderkredit mit GWR über 200 Mio. EUR für Phase III herauszulegen.

Land / Region / Institution Indonesien Nummer 37156 Schwerpunkt Sonstige Sektor 43010 - Multisektorale Hilfe Finanzierungsinstrument Förderkredit Weitere Geber Asian Development Bank (ADB) Deutscher Finanzierungsbeitrag 300 Mio. EUR Status abgeschlossen Auftraggeber BMZ Projektpartner REPUBLIK INDONESIEN Zuständige Abteilung Ostasien und Pazifik
