KMU-Förderung III

Armenische KKMU haben nach wie vor eingeschränkten Zugang zu Krediten in Lokalwährung (Kernproblem). Dies hat sich in Folge der Corona-Krise noch verschärft und kann in Verbindung mit der Belastung der armenischen Wirtschaft durch den kurzen Bergkarabach-Krieg mit dem Nachbarland Aserbaidschan zu Geschäftsaufgaben und dem Verlust vieler Arbeitsplätze führen, mit der Folge einer dramatischen Abschwächung des armutsmindernden Wirtschaftswachstums.

Das Vorhaben soll dem entgegenwirken und zur Sicherung der Liquidität kleiner und mittlerer Unternehmen durch Verbesserung der Verfügbarkeit von Lokalwährungsdarlehen für KKMU beitragen. Das Vorhaben ist komplementär zu dem Programm der armenischen Regierung "Measures to alleviate the consequences of Covid-19 outbreak" vom März 2020.

Darlehensnehmer und Projektträger sind die Armenische Zentralbank (CBA) bzw. deren Projektimplementierungseinheit (PIU) German Armenian Fund (GAF). Unmittelbare Zielgruppe des Vorhabens sind armenische Partner-Finanzinstitutionen (PFI), die Kredite an armenische KKMU (mittelbare Zielgruppe) vergeben. Das Vorhaben stellt dem GAF Refinanzierungsmittel für Kredite an KKMU zur Verfügung. Die mittelbare Zielgruppe erhält dadurch Zugang zu Finanzierungsangeboten in Lokalwährung.

Das Modul leistet einen Beitrag zum Kernthema "Ausbildung und nachhaltiges Wachstum für gute Jobs", Aktionsfeld (2) "Privatsektor- und Finanzsystementwicklung".

Umwelt- und Sozialverträglichkeit

Die Finanzierung wurde entsprechend der Nachhaltigkeitsrichtlinie der KfW Entwicklungsbank in die Umwelt- und Sozialrisikokategorie FI/C eingeordnet, da durch die Finanzierung voraussichtlich keine oder nur geringe nachteilige Auswirkungen und Risiken für Umwelt und soziale Belange entstehen. Es sind keine besonderen Umwelt- und Sozialmanagementmaßnahmen seitens des Finanzintermediärs erforderlich. Der Finanzintermediär wurde jedoch verpflichtet, adäquate Beschwerdemöglichkeiten zu schaffen und die KfW über umwelt- und sozialrelevante besondere Vorkommnisse zu informieren, sofern sich diese im Rahmen einer Finanzierung mit KfW-Mitteln ereignet haben.

Land / Region / Institution Armenien Nummer 45387 Schwerpunkt Wirtschaft und Finanzsektor Sektor 24030 - Finanzintermediäre des formellen Sektors USV-Kategorie FI/C Finanzierungsinstrument Entwicklungskredit Weitere Geber - Deutscher Finanzierungsbeitrag 70,4 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner GERMAN ARMENIAN FUND (CENTRAL BANK OF ARMENIA) Zuständige Abteilung Südosteuropa und Türkei
