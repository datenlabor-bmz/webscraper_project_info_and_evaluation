  




 

Programm zur Förderung nachhaltiger Wirtschaftsentwicklung
Promotion of Sustainable Economic Development Programme
Project details
Project number:2011.2147.4
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Ellen Michel ellen.michel@giz.de
Partner countries: Kyrgyzstan
Summary
Objectives:

Kirgisische Unternehmen in den Sektoren Agrarwirtschaft, Nahrungsindustrie, verarbeitendes und Dienstleistungsgewerbe erschließen und nutzen neue Absatzkanäle und Märkte einkommens- und beschäftigungswirksam.

Client:

BMZ

Project partner:

Ministerium für wirtschaftliche Regelung

Financing organisation:

not available

 
Project value
Total financial commitment:20 876 716 Euro
Financial commitment for this project number:5 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2013.2150.4Programm zur Förderung nachhaltige Wirtschaftsentwicklung
 
Term
Entire project:03.07.2012 - 30.07.2021
Actual project:03.07.2012 - 08.12.2015
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Geschäftspolitik und -verwaltung

Evaluation
not available
 




 
