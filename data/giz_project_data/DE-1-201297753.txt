  




 

Organisation und Durchführung des dritten GCF-Direktoriums Treffens in Berlin
Development of the GCF-Board-Meeting in Berlin
Project details
Project number:2012.9775.3
Status:Projekt beendet
Responsible Organisational unit: A210 Unknown Organisational unit: A210
Contact person:Dr. Oliver Gnad oliver.gnad@giz.de
Partner countries: Germany
Summary
Objectives:

Unterstützung bei Umsetzung des dritten GCF-Board-Treffens in Berlin

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:316 937 Euro
Financial commitment for this project number:316 937 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:16.01.2013 - 14.10.2013
Actual project:16.01.2013 - 14.10.2013
other participants
Wilde Beissel von Schmidt GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektziel-Ebene: Projekt zielt vor allem auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Umweltpolitik und -verwaltung

Evaluation
not available
 




 
