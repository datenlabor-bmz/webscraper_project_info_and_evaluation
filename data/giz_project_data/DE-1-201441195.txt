  




 

Deutsch-Tunesische Akademie für Gute Regierungsführung
German-Tunisian Academy for Good Governance
Académie tuniso-allemande pour la bonne gouvernance
Project details
Project number:2014.4119.5
Status:Projekt beendet
Responsible Organisational unit: 3600 Nordafrika
Contact person:Adelheid Uhlmann adelheid.uhlmann@giz.de
Partner countries: Tunisia
Summary
Objectives:

Die Fach- und Führungskräfte in der tunesischen Verwaltung auf der nationalen, regionalen und lokalen Ebene verfügen über Kenntnisse und Kompetenzen, um ihre Entsendeorganisation im Sinne der Prinzipien guter Regierungsführung weiterzuentwickeln

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministerium für öffentlichen Dienst und gute Regierungsführung

Financing organisation:

not available

 
Project value
Total financial commitment:4 292 254 Euro
Financial commitment for this project number:4 292 254 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:14.11.2014 - 01.07.2021
Actual project:01.01.2015 - 01.07.2021
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Dezentralisierung und Förderung subnatio Gebietskörpers

Evaluation
not available
 
Project description (DE)

Ausgangssituation

Nach der Revolution von 2011 durchläuft Tunesien einen Transformationsprozess. Für einen erfolgreichen Umbau hin zu einer freiheitlich-demokratischen Grundordnung müssen mehrere Voraussetzungen erfüllt sein: Entscheidungsträger aus Politik und Verwaltung fördern den Prozess friedlich und konstruktiv; die Bürger tragen die Veränderungen mit. Gleichzeitig muss der Verwaltungsapparat zu einem modernen und bürgernahen System umgebaut werden, das sich an Prinzipien guter Regierungsführung orientiert – etwa Transparenz, Wirtschaftlichkeit, Nachvollziehbarkeit von Entscheidungen und Rechtsstaatlichkeit.

Die große Mehrheit der Fach- und Führungskräfte in Politik und Verwaltung kann dies bislang nur unzureichend umsetzen. Viele Mitarbeiter sind noch durch das vorhergehende autokratisch-zentralistische System geprägt. Nach der Revolution neu eingestellte Mitarbeiter konnten bisher nur wenige praktische Erfahrungen sammeln. Insgesamt fehlen vielerorts Kenntnisse und Kompetenzen, um den Wandel aktiv zu gestalten.


Ziel

Nationale, regionale und lokale Fach- und Führungskräfte in Regierung und Verwaltung erreichen in ihren Behörden signifikante, nachhaltige Veränderungen im Sinne der Prinzipien guter Regierungsführung.


Vorgehensweise

Das Vorhaben arbeitet eng mit der traditionsreichen nationalen Verwaltungshochschule (ENA) zusammen. Sie verfügt über moderne Infrastruktur sowie einen Pool von 400 Lehrkräften. Schwerpunkt des Vorhabens ist die Qualifizierung von Fach- und Führungskräften sowie die Förderung des Austauschs untereinander. An der tunesischen Verwaltungshochschule wurde dazu die sogenannte Deutsch-Tunesische Werkstattakademie für gute Regierungsführung eingerichtet. Als Teilnahmebedingung am Fortbildungslehrgang der Akademie müssen die Teilnehmenden ein reales Reformprojekt mit ihrem jeweiligen Ministerium erarbeiten und umsetzen. Das Projekt soll einen für den Bürger erfahrbaren Wandel in der Verwaltung bewirken.

Die Akademie bietet ein breites Spektrum an Leistungen:

• Achtmonatiger Fortbildungslehrgang für 25-35 Teilnehmende in Tunis zu Themen guter Regierungsführung – etwa zur Dezentralisierung der Verwaltung, zur Kontrolle der Finanzen sowie zu Integrität und Korruptionsbekämpfung
• Regionale Seminare für Fach- und Führungskräfte aus den Regionen zu Themen guter Regierungsführung
• Studienreisen nach Deutschland zur Vermittlung von Praxiserfahrungen und zum Austausch mit Experten in ausgewählten Politikfeldern guter Regierungsführung
• Internationale Konferenzen und Dialogveranstaltungen zu ausgewählten Themen guter Regierungsführung in Tunis und in den tunesischen Regionen
• Förderung der Vernetzung der Alumni sowie tunesischer und deutscher Experten für gute Regierungsführung, etwa durch Veranstaltungen und Gastvorträge
• Reform-Coaching und Organisationsentwicklung

Durch den Fortbildungslehrgang, Studienreisen, Regionalseminare und eine internationale Konferenz hat die Werkstattakademie bisher mehr als 1.500 Personen aus Verwaltung, Privatsektor und Zivilgesellschaft weitergebildet, davon mehr als ein Drittel Frauen.
Kooperationspartner des Vorhabens sind die Europäische Akademie in Berlin und die Führungsakademie Baden-Württemberg. Das Vorhaben unterstützt die Internationale Akademie für Gute Regierungsführung Tunesiens und ist in den Räumen der Nationalen Verwaltungshochschule ENA in Tunis untergebracht.

Das Vorhaben ist Teil der Sonderinitiative des BMZ zur Stabilisierung und Entwicklung in Nordafrika und Nahost. Mit den Projekten der Sonderinitiative trägt das Ministerium dazu bei, wirtschaftliche und soziale Perspektiven für die Menschen in der Region zu schaffen. In diesem Rahmen stehen für Vorhaben der GIZ und anderer Durchführungsorganisationen in den Jahren 2014 bis 2021 bisher mehr als 300 Millionen Euro zusätzlich zur Verfügung. Im Fokus stehen dabei die Themenbereiche Jugend- und Beschäftigungsförderung, wirtschaftliche Stabilisierung, Demokratisierung sowie die Stabilisierung von Nachbarländern in Krisensituationen.
 

 
Project description (EN)

Context

After the revolution in 2011, Tunisia went through a process of transformation. For a successful transition towards a free and democratic basic order, several conditions need to be met: decision-makers from politics and administration must promote the process in a peaceful and constructive way, and citizens must support the changes. At the same time, the administrative machinery must be reorganised into a modern and citizen-responsive system which is guided by the principles of good governance, such as transparency (including decision-making), cost-effectiveness and the rule of law.

The great majority of experts and managers in politics and administration have not yet been able to implement this adequately. The conduct of many employees is still typical of the previous autocratic, centralist system. Individuals employed after the revolution have only been able to gain limited practical experience. Overall, the knowledge and skills needed to actively shape change are lacking in many places.


Objective

Managers and experts in government and administrative bodies at national, regional and local level achieve significant and lasting changes in accordance with the principles of good governance.


Approach

The project works closely with the National School of Administration (ENA), which has a long tradition. The school’s infrastructure is modern and it has a pool of 400 teaching staff. The focus of the project is on qualifying experts and managers and promoting mutual exchange. The German-Tunisian Academy for Good Governance has been set up at the Tunisian National School of Administration for this purpose. One of the conditions for participants to take part in the academy’s training course is that they develop and implement an actual reform project with their respective ministries. The project should achieve a transformation in administration which is tangible for the citizens.

The academy offers a wide range of activities:
- An eight-month training course in Tunis for 25 to 35 participants covering the topics of good governance, for example decentralising the administration, controlling finances, ensuring integrity and combating corruption;
- Regional seminars for experts and managers from the regions on good governance topics;
- Study trips to Germany for acquiring practical experience and interacting with experts in selected fields of good governance policy;
- International conferences and dialogue events in Tunis and the Tunisian regions on selected topics of good governance;
- Promotion of networking between alumni and Tunisian and German experts on good governance, for example through events and guest lectures;
- Reform coaching and organisational development.

With its training course, study trips, regional seminars and an international conference, the academy has now trained more than 1,500 individuals from administration, the private sector and civil society, one third of them women.

The project’s cooperation partners are the European Academy Berlin and the Leadership Academy of Baden-Württemberg. The project supports Tunisia’s International Academy for Good Governance and is housed on the premises of the National School of Administration (ENA) in Tunis.


The project is part of a special initiative run by Germany’s Federal Ministry for Economic Cooperation and Development (BMZ) designed to stabilise and promote development in North Africa and the Middle East. Through the projects that make up this special initiative, BMZ is helping to open up economic and social prospects for people in the region. Within this context, an additional sum of more than EUR 300 million has been earmarked for projects carried out by GIZ and other implementing organisations in the period from 2014 to 2021. The thematic focus is on youth and employment promotion, economic stabilisation, democracy and stabilising neighbouring countries in crisis situations.
 

 
Project description (FR)

Situation initiale

La réussite du processus de transition démocratique en Tunisie ne peut se faire sans la mise en place d’un ordre constitutionnel libéral et démocratique. Pour cela, plusieurs conditions doivent être remplies : il faut que les responsables politiques et administratifs fassent progresser le processus de manière pacifique et constructive et que les citoyennes et les citoyens s’impliquent eux aussi dans cette transition. En même temps, il est nécessaire de transformer l’appareil administratif tunisien en un système moderne et proche du citoyen, axé sur les principes de la bonne gouvernance, tels que la transparence, l’efficacité et la redevabilité, ainsi que sur les principes de l’État de droit.

Pour que les experts et les cadres administratifs soient en mesure d’appliquer ces principes de manière satisfaisante, un renforcement des connaissances et des compétences est nécessaire. Or, il n’existe pas à ce jour d’offres de qualification et de formation axées sur la pratique qui permettent à la fois d’apprendre et de mettre en œuvre la gestion des affaires publiques et de gérer activement cette transformation vers un État de droit démocratique doté d’une administration proche du citoyen.


Objectif

Le projet coopère étroitement avec l’École nationale d’administration (ENA), qui assure la formation des cadres supérieurs de l’administration tunisienne. Le projet a pour objectifs de renforcer les capacités des cadres supérieurs de l’administration tunisienne et d’accompagnement des projets de changement dans le domaine de la bonne gouvernance en Tunisie. L’Académie s’adresse en particulier aux cadres et experts de l’administration publique, mais également aux représentant-e-s du secteur privé et de la société civile au niveau national, régional et local.


Approche

L’Académie offre plusieurs prestations de soutien aux réformes en matière de bonne gouvernance. Le renforcement des capacités est assuré par une formation annuelle de longue durée qui s’adresse à 25-35 participant-e-s, est organisée à Tunis et s’accompagne de missions techniques en Allemagne ainsi que de formations de courte durée organisées dans les régions. L’échange d’expériences est également promu à travers des conférences internationales et des dialogues d’experts traitant de divers thèmes liés à la bonne gouvernance en Tunisie. La promotion du réseautage entre les anciens élèves ainsi qu’entre les experts tunisiens et allemands de la bonne gouvernance fait également partie des objectifs. Enfin le développement organisationnel et le coaching pour accompagner les personnes formées dans la mise en œuvre des projets de réforme complètent les prestations du projet.

La condition de participation consiste à élaborer et à mettre en œuvre un projet de réforme concret que le gouvernement tunisien a confié à l’Académie. Le projet doit aboutir à un changement au sein de l’administration tunisienne qui soit perceptible pour le citoyen. L’Académie veillera parallèlement à ce que les femmes et les personnes issues des régions structurellement faibles de l’intérieur du pays puissent avoir accès à ces mesures de formation et d’accompagnement.

L’Académie internationale pour la bonne gouvernance a déjà fait bénéficier plus de 1 500 personnes, dont 30% de femmes, issues de l’administration, du secteur privé et de la société civile de formations continues, de missions techniques, de séminaires régionaux et d’une conférence internationale.

La structure de tutelle du projet est la Présidence du gouvernement. D’autres partenaires de coopération sont l’Académie européenne de Berlin et l’Académie des cadres du Bade-Wurtemberg. Le projet est installé dans les locaux de l’École nationale d’administration (ENA) à Tunis.


Le projet fait partie de l’Initiative spéciale du ministère fédéral allemand de la Coopération économique et du Développement (BMZ) pour la stabilisation et le développement en Afrique du Nord et au Proche-Orient. Les projets de l’Initiative spéciale contribuent à créer des perspectives sociales et économiques pour les populations de la région. Dans ce cadre, plus de 300 millions d’euros supplémentaires ont été affectés à des projets mis en œuvre par la GIZ et d’autres agences d’exécution dans la période 2014-2021. Les thèmes visés sont la promotion des jeunes et de l’emploi, la stabilisation économique, la démocratisation ainsi que la stabilisation des pays voisins en crise.
 





 
