  




 

Stärkung der sozialen Infrastruktur für die Aufnahme von Binnenflüchtlingen in der Ukraine
Strengthening social infrastructure for the absorption of IDP´s
Project details
Project number:2016.1800.8
Status:Projekt beendet
Responsible Organisational unit: 3900 Deutschland, Europa, Südkaukasus
Contact person:Lena Flitta lena.flitta@giz.de
Partner countries: Ukraine
Summary
Objectives:

Ausgewählte Aufnahmegemeinden stellen eine verbesserte soziale Infrastruktur für Binnenvertriebene und die ansässige Bevölkerung zur Verfügung.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministerium für die Entwicklung von Gemeinden und Territorien der Ukraine

Financing organisation:

not available

 
Project value
Total financial commitment:54 883 462 Euro
Financial commitment for this project number:54 883 462 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:04.01.2016 - 30.07.2021
Actual project:16.02.2016 - 30.07.2021
other participants
Architekturbüro
Arge Architektengemeinschaft
Arge Sweco GmbH-Sweco International
O.L.T Consult GmbH
mgh ingenieure + architekten GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
Armutsorientierungnot available
CRS code
Kurzf. Wiederaufbau/Rehabilitierung nach Notsituationen

Evaluation
not available
 
Project description (DE)

Ausgangssituation
Der Konflikt im Osten der Ukraine hat etwa 2,8 Millionen Ukrainer*innen dazu gezwungen, umkämpfte Gebiete zu verlassen. Fast 1,4 Millionen Menschen sind in westliche Landesteile geflohen und sind als Binnenvertriebene (Internally Displaced Persons, IDPs) registriert, darunter circa 255.000 Menschen in den Verwaltungsbezirken (Oblasten) Charkiw, Dnipropetrowsk und Saporischschja (Stand: August 2019). Die große Zahl der IDPs stellen die aufnehmenden Gemeinden vor Probleme im Hinblick auf Unterbringung, Versorgung und Integration. Die Räumlichkeiten der sozialen Infrastruktur weisen teilweise erhebliche bauliche Mängel auf und sind auch in der Anzahl nicht ausreichend, um die benötigten Dienstleistungen zu erbringen.

Ziel
Die soziale Infrastruktur für Binnenvertriebene und die ansässige Bevölkerung ist in ausgewählten Aufnahmegemeinden verbessert.

Vorgehensweise
Das Vorhaben schafft durch Sanierung und Ausbau zusätzliche Räumlichkeiten, passt vorhandene an oder verbessert sie. Dies geschieht unter wirtschaftlichen Aspekten, auch im Hinblick auf Betriebskosten, durch Sanierungsmaßnahmen zur Energieeinsparung. Schulungen zum energieeffizienten Gebäudebetrieb ergänzen die Baumaßnahmen. Umfangreiche Finanzmittel werden von der ukrainischen Regierung und anderen Gebern für die Entwicklung der sozialen Infrastruktur bereitgestellt. Um diese nutzen zu können, werden die Gemeinden darin geschult, Bedarfsplanungen zu erstellen, Bauprojekte zu entwickeln und Förderanträge einzureichen. Unter besonderer Berücksichtigung schutzbedürftiger Gruppen und Binnenvertriebener wurden in einer Stadt integrierte Strategien für bedarfsorientierte Wohnraumkonzepte entwickelt.
Um Baumaßnahmen in den regierungskontrollierten Oblasten in Charkiw umsetzen zu können, wurden Kooperationen mit dem Danish Refugee Council, dem Luxemburgischen Roten Kreuz sowie mit dem Flüchtlingshilfswerk der Vereinten Nationen (UNHCR) abgeschlossen. Hierbei handelt es sich überwiegend um die Umsetzung von Wiederaufbaumaßnahmen kriegszerstörter Gebäude. Das Vorhaben kooperiert ebenfalls mit dem Projekt „Energieeffizienz in Kommunen II in der Ukraine".
Des Weiteren findet eine Kooperation mit der Energieagentur Odessa statt. Diese wurde im Zuge des vom Bundesministerium für Umwelt, Naturschutz und nukleare Sicherheit (BMU) finanzierten Projekts „Aufbau von Energieagenturen in der Ukraine" aufgebaut. Es werden Energieaudits und Beratungen hinsichtlich eines nachhaltigen Gebäudemanagements durchgeführt.

Wirkungen
Die Leistungen haben eine strukturbildende Wirkung. Die Qualität und Quantität der sozialen Infrastruktur ist erhöht - mit unmittelbarem Nutzen für die gesamte Bevölkerung in den Projektgemeinden.
Alle Gemeinden mit Binnenvertriebenen wurden nach Projektideen gefragt. Nach der Evaluierung von über 340 Objekten sowie weiteren Evaluierungen während der Projektlaufzeit wurden 235 Objekte für Sanierung und technische Ausstattung ausgewählt:
• 64 Schulen: mehr als 33.000 Schüler*innen profitieren von den Maßnahmen, davon knapp 1.500 Binnenvertriebene
• 35 Kindergärten: 4.500 Kinder profitieren, davon über 250 Binnenvertriebene
• 13 staatliche Heime für insgesamt rund 3.200 Personen, davon circa 120 Binnenvertriebe
• 33 medizinische Einrichtungen: 950.000 Personen profitieren, davon knapp 63.000 Binnenvertriebe
• 90 Projekte im öffentlichen Raum: mehr als 775.000 Personen profitieren, davon über 37.000 Binnenvertriebene
Mit Abschluss der Baumaßnahmen Ende 2019 profitieren mehr als 1,7 Millionen Menschen von verbesserter Infrastruktur. 

 
Project description (EN)

Context
The conflict in eastern Ukraine has forced about 2.8 million Ukrainians to leave contested areas. Almost 1.4 million people have fled to western parts of the country and are registered as internally displaced persons (IDPs), including some 255,000 people in the administrative districts (oblasts) of Kharkiv, Dnipropetrovsk and Zaporizhia (figures from August 2019). The large number of IDPs poses problems for host communities when it comes to accommodation, service provision and integration. The buildings used to support social infrastructure sometimes have considerable structural deficiencies and more premises are needed to provide the services required.

Objective
Social infrastructure for IDPs and the local population is improved in selected host communities.

Approach
The project carries out renovation and extension activities to create additional premises and adapts or improves existing buildings. To ensure cost-effectiveness, including with regard to operating costs, this work focuses on energy-saving measures. Training courses on energy-efficient building management are provided alongside these construction measures. The Ukrainian Government and other donors have made extensive financial resources available to expand social infrastructure. To help the municipalities tap these resources, the project offers training in drawing up requirements plans, developing construction projects and submitting funding applications. One city has already drafted integrated, needs-based housing strategies that focus in particular on vulnerable groups and IDPs.
In order to implement construction measures in the government-controlled oblasts in Kharkiv, cooperation agreements have been concluded with the Danish Refugee Council, the Luxembourg Red Cross and the UN Refugee Agency (UNHCR). This mainly involves reconstruction measures for war-damaged buildings. The project is also cooperating with another GIZ project in Ukraine, Energy Efficiency in Municipalities II.
In addition, it works with the Odessa Municipal Energy Agency. This partnership was initiated through the Establishment of Energy Agencies in Ukraine project, which is financed by the German Federal Ministry for the Environment, Nature Conservation and Nuclear Safety (BMU). Within the scope of this partnership, energy audits and consultations on sustainable building management are carried out.

Results
The project’s activities provide structural support. The quality and quantity of social infrastructure is increased, which directly benefits the entire population in the project communities.
All communities with IDPs were asked to submit project ideas. More than 340 buildings were assessed during the project term and further evaluations were also conducted. In the end, 235 properties were selected for rehabilitation and technical upgrading:
• 64 schools: more than 33,000 school students benefit from the measures, including almost 1,500 IDPs
• 35 kindergartens: 4,500 children benefit, more than 250 of whom are IDPs
• 13 state-run homes for around 3,200 people in total, of whom about 120 are IDPs
• 33 medical facilities: 950,000 people benefit; almost 63,000 are IDPs
• 90 projects in public spaces: more than 775,000 people benefit, including over 37,000 IDPs
Construction work was completed at the end of 2019 and more than 1.7 million people now benefit from improved infrastructure. 





 
