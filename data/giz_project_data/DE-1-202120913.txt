  




 

Integrierte wirtschaftliche Entwicklung im Rohstoffsektor II
Integrated economic development in the extractive sector II i Mauritania
Project details
Project number:2021.2091.3
Status:laufendes Projekt
Responsible Organisational unit: 1100 Westafrika 1
Contact person:Jonathon Hornbrook jonathon.hornbrook@giz.de
Partner countries: Mauritania, Mauritania
Summary
Objectives:

Die ökonomische, ökologische und soziale Nachhaltigkeit des mauretanischen Bergbausektors ist gestärkt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministère de l'Economie et de l'Industrie

Financing organisation:

not available

 
Project value
Total financial commitment:6 524 518 Euro
Financial commitment for this project number:2 000 000 Euro
Cofinancing

not available

 
Previous projects
2018.2082.8Integrierte wirtschaftliche Entwicklung im Rohstoffsektor in Mauretanien
Follow-on projects
not available
 
Term
Entire project:27.04.2015 - 31.10.2023
Actual project:19.08.2021 - 31.10.2023
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Bodenschätze / Bergbaupolitik und -verwaltung

Evaluation
not available
 




 
