  




 

Partnerschaften zur Prävention von Gewalt gegen Frauen und Mädchen im südlichen Afrika II (PfP II)
Partnerships for Prevention of Gender-Based Violence in Southern Africa II (PfP II)
Project details
Project number:2021.2026.9
Status:laufendes Projekt
Responsible Organisational unit: 1300 Südliches Afrika
Contact person:Begona Castro Vazquez begona.castro@giz.de
Partner countries: AFRICA, Lesotho, South Africa, Zambia, Zimbabwe
Summary
Objectives:

Die Kooperation von staatlichen, nichtstaatlichen und privatwirtschaftlichen Akteuren zur Prävention von Gewalt gegen Frauen und Mädchen im

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Partnerorganisationen der deutschen politischen Stiftungen und kirchlichen Hilfswerke vor Ort

Financing organisation:

not available

 
Project value
Total financial commitment:20 000 000 Euro
Financial commitment for this project number:10 000 000 Euro
Cofinancing

Ford Foundation (ab 01.01.2012): 0Euro


 
Previous projects
2017.2161.2Prävention von Gewalt gegen Frauen und Mädchen im südlichen Afrika
Follow-on projects
not available
 
Term
Entire project:19.12.2017 - 30.11.2024
Actual project:01.12.2021 - 30.11.2024
other participants
Syspons GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt zielt vor allem auf Gleichberechtigung ab
Armutsorientierungnot available
CRS code
Überwindung der Gewalt gegen Frauen und Mädchen

Evaluation
not available
 
Project description (DE)

Ausgangssituation:
Geschlechtsspezifische Gewalt ist eine weitverbreitete Menschenrechtsverletzung, die sich während der Covid-19-Pandemie weiter deutlich verschärft hat. Im internationalen Vergleich kommt es im südlichen Afrika besonders häufig zu geschlechtsspezifischer Gewalt.
Vor allem Gewalt in Partnerschaften findet in der Gesellschaft häufig Akzeptanz. Die Ursachen hierfür liegen bei allen Formen geschlechtsspezifischer Gewalt in schädlichen sozialen Normen und Praktiken. Bis zu zwei Drittel aller Frauen in den Ländern des südlichen Afrikas geben an, körperliche oder emotionale Gewalt erlebt zu haben. Dabei ist die von einem Mann ausgehende Partnerschaftsgewalt die häufigste Form. Institutionelle und individuelle Kapazitäten, um innovative und kontextbezogene Präventionsmaßnahmen zu geschlechtsspezifischer Gewalt umzusetzen und zu koordinieren, müssen weiter gestärkt werden. Zudem ist der regionale Austausch zwischen Akteur*innen bisher unzureichend, um die Verbreitung und das Upscaling bewährter Ansätze zur Prävention geschlechtsspezifischer Gewalt voranzutreiben.

Ziel:
Regional, national und sub-national aktive Akteur*innen setzen gemeinsam sektorübergreifende und auf Forschung sowie Umsetzungserfahrung basierende Leuchtturmprojekte zur Prävention geschlechtsspezifischer Gewalt in Gemeinschaften des südlichen Afrikas um.

Vorgehensweise:
Das Regionalvorhaben Partnerschaften zur Prävention geschlechtsspezifischer Gewalt im südlichen Afrika (Partnerships for Prevention of Gender-Based Violence in Southern Africa, PfP) verfolgt einen gesamtgesellschaftlichen Ansatz, um ganzheitlich gegen geschlechtsspezifische Gewalt vorzugehen. PfP unterstützt den Zusammenschluss zwischen der Regierung, Privatwirtschaft und Zivilgesellschaft, um sektorübergreifende Multiakteurspartnerschaften aufzubauen. Diese setzen in Südafri-ka, Lesotho, Simbabwe und Sambia Leuchtturmprojekte zur Prävention geschlechtsspezifischer Gewalt um.
PfP unterstützt Initiativen, die gemäß den Bedarfen der jeweiligen Länder entwickelt wurden. Diese streben an, einen Beitrag zur Prävention geschlechtsspezifischer Gewalt zu leisten. Der regionale Charakter von PfP ist von großer Bedeutung, um bewährte Ansätze zur Prävention geschlechtsspezifischer Gewalt zu fördern und ermöglicht das gemeinsame Lernen in regionalen Austauschformaten. Darüber hinaus regt die regionale Dimension dazu an, neue Leuchtturmprojekte zu entwickeln und bereits bestehende weiter zu verbreiten und zu entwickeln.

Website: Partnerships for Prevention
Website: Partnership for Prevention Resource Hub: Useful resources on VAWG prevention
YouTube-Video: The effects of COVID-19 on gender-based violence (GBV)
https://www.youtube.com/watch?v=lthLW7Ik6Ck




 

 
Project description (EN)

Background:
Gender-based violence (GVB) is a widespread human rights violation, further exacerbated by the outbreak of Covid-19. In Southern Africa, the prevalence of GBV is high by international comparison. Especially intimate partner violence is widely accepted in society and is, as all forms of GBV, rooted in harmful social norms and practices. Up to two thirds of all women in the countries of Southern Afri-ca state that they have experienced physical and/or emotional violence with intimate partner violence at the hands of a man being the most common form. Institutional and individual capacities to imple-ment and coordinate innovative and context specific GBV prevention measures need to be strength-ened further. In addition, there is no regular regional exchange among stakeholders to promote up-scaling of good practices for GBV prevention

Objective:
Regional, national and sub-national stakeholders from different sectors jointly implement evidence informed and multisectoral GBV prevention flagships in Southern African communities.

Approach:
Partnerships for Prevention of Gender-Based Violence in Southern Africa (PfP) follows a whole-of-society approach to address GBV on a broad scale. PfP supports cooperation between government, the private sector and civil society to establish multi-stakeholder partnerships. They implement GBV prevention flagships in South Africa, Lesotho, Zimbabwe and Zambia. PfP supports new initiatives that have been developed according to the needs of each specific context. They aim to prevent all forms of GBV. The regional character of the PfP is essential to promote good practices for GBV pre-vention and ensures cross-country exchange and learning in regional exchange formats. Moreover, the regional dimension encourages the development of new and upscaling of existing flagships.

Results:
Website: Partnerships for Prevention
Website: Partnership for Prevention Resource Hub: Useful re-sources on VAWG prevention
YouTube-Video: The effects of COVID-19 on gender-based violence (GBV)
https://www.youtube.com/watch?v=lthLW7Ik6Ck

 





 
