  




 

Unterstützung des Dispositif National zur Prävention und zum Management von Ernährungskrisen
Food Security Niger
Appui au Dispositif National de Prévention et de Gestion des Catastrophes et des Crises Alimentaires
Project details
Project number:2015.2028.7
Status:Projekt beendet
Responsible Organisational unit: 1100 Westafrika 1
Contact person:Dr. Alexandre Sessouma alexandre.sessouma@giz.de
Partner countries: Niger, Niger
Summary
Objectives:

Die Funktionsfähigkeit der Nationalen Institution für Prävention und Management von Katastrophen und Ernährungskrisen (Dispositif National-DN) ist auf nationaler, regionaler und kommunaler Ebene gestärkt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Kabinett des Premierministers

Financing organisation:

not available

 
Project value
Total financial commitment:15 000 000 Euro
Financial commitment for this project number:4 000 000 Euro
Cofinancing

not available

 
Previous projects
2011.2101.1Unterstützung "Dispositif National de Prévention et de Gestion des Crises Alimentaires"
Follow-on projects
2017.2094.5Unterstützung des Dispositif National zur Prävention und zum Management von Ernährungskrisen in Niger
 
Term
Entire project:21.01.2013 - 30.06.2023
Actual project:01.01.2016 - 31.08.2018
other participants
Arge ECO Consult Sepp&Busacker
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Entwicklungsorientierte Nahrungsmittelhilfe

Evaluation
not available
 




 
