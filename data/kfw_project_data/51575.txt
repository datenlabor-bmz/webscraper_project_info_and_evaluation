LBN - UNICEF Berufliche Bildung Phase VI

Projektziel:
Verbesserung des Zugangs zu non-formalen Ausbildungs- und Qualifizierungsangeboten für libanesische Jugendliche und Heranwachsende und Flüchtlinge, sowie Erleichterung des Übergangs zwischen Ausbildung und Berufseinstieg, um die Lebensperspektiven der Zielgruppe zu verbessern.

Zielgruppe:
Das Vorhaben soll Maßnahmen der beruflichen Bildung für Libanesen und Flüchtlinge (hauptsächlich aus Syrien, den palästinensischen Gebieten und dem Irak) finanzieren.

Maßnahmen:
Das Projekt beinhaltet Trainings zur beruflichen Bildung, Vermittlung von bezahlten Praktika (sogenannte Cash for Work Einsätze), Vermittlung von "life skills", Vermittlung von grundlegenden Schreib- und Lesekenntnissen sowie von erweiterten Grundbildungskenntnissen (IT- und Sprachkenntnisse) sowie die Förderung der Nachhaltigkeit der Maßnahmen durch Anschlusskurse, Mentoring und Gründungsförderung. Damit knüpft die Phase VI inhaltlich weitgehend an die Vorgängerphase V an.

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde mit der USVP-Risikokategorie B bewertet. Die überschaubaren Risiken und moderaten potenziellen negativen Auswirkungen sind auf die jeweiligen Standorte der Einzelbaustellen begrenzt und können mit Gegenmaßnahmen nach dem Stand der Technik bzw. mit Standardlösungen auf ein akzeptables Maß minimiert werden. Die mit der Maßnahme für die teilnehmenden jugendlichen Praktikanten und Praktikantinnen sowie für die anderen Arbeitskräfte verbundenen Risiken liegen im Bereich Arbeitssicherheit und Arbeitsbedingungen. UNICEF wird zur Adressierung dieser Risiken Standort- und Maßnahmen- spezifische Umwelt- und Sozialmanagementpläne entwickeln, die u.a. auch Vorgaben zum Umweltschutz und Abfallmanagement, zur öffentlichen Sicherheit, zu Arbeitssicherheits-Training sowie Beschwerdemechanismen für die Öffentlichkeit - hier besonders Anrainergemeinden - sowie für die auf den Baustellen beschäftigten Arbeitskräfte enthalten. Alle Teilnehmer*innen der Maßnahmen erhalten durch UNICEF bzw. die implementierenden NGOs umfangreiche Einführungen in das Thema Arbeitssicherheit und sind für die gesamte Ausbildungszeit unfallversichert. UNICEF wird über die implementierenden NGOs - die an UNICEF berichten - die Umsetzung der U&S Managementpläne auf den einzelnen Baustellen nachhalten und ggfs. Korrektivmaßnahmen veranlassen. Sämtliche Maßnahmen werden unter Einhaltung von umfassenden Vermeidungs- und Schutzmaßnahmen durchgeführt, um die Risiken, die durch die gegenwärtige COVID19 Pandemie bestehen, angemessen zu adressieren.

Land / Region / Institution Libanon Nummer 51575 Schwerpunkt Bildung Sektor 11330 - Berufliche Bildung USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 15 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner UNITED NATIONS CHILDREN'S FUND Zuständige Abteilung Nahost
