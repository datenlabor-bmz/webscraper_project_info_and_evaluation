  




 

Lebenswerte und inklusive Städte für alle
Liveable and Inclusive Cities for All
Project details
Project number:2020.2125.1
Status:laufendes Projekt
Responsible Organisational unit: 2B00 Asien II
Contact person:Emilia Huss emilia.huss@giz.de
Partner countries: Bangladesh, Bangladesh
Summary
Objectives:

Die institutionellen Kapazitäten lokaler und nationaler Verwaltungen für inklusives und klimasensitives Umweltmanagement in Städten sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Local Government, Rural Development and Cooperatives

Financing organisation:

not available

 
Project value
Total financial commitment:5 500 000 Euro
Financial commitment for this project number:5 500 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:28.10.2022 - 31.10.2025
Actual project:01.11.2022 - 31.10.2025
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Stadtentwicklung und -verwaltung

Evaluation
not available
 




 
