  




 

Wasserprogramm
Water Programme for the Palestinian Territories
Water Programme for the Palestinian Territories
Project details
Project number:2009.2084.3
Status:Projekt beendet
Responsible Organisational unit: 3A00 Naher und Mittlerer Osten 2
Contact person:Dirk Schaefer dirk.schaefer@giz.de
Partner countries: Palestinian Territories
Summary
Objectives:

Die Wasser-und Sanitärversorgung ist verbessert.

Client:

BMZ

Project partner:

Palestinian Water Authority (Palästinensische Wasserbehörde) u. Ministry of Planing (Planungsministerium)

Financing organisation:

not available

 
Project value
Total financial commitment:19 615 894 Euro
Financial commitment for this project number:5 894 513 Euro
Cofinancing

not available

 
Previous projects
2005.2007.2KV-Wasserprogramm
Follow-on projects
2013.2257.7Wasserprogramm
 
Term
Entire project:02.03.2006 - 31.12.2018
Actual project:01.01.2010 - 30.09.2013
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Erhaltung von Wasser-ressourcen, einschl. Datenerhebung

Evaluation
not available
 




 
