Trinkwasserver- und Abwasserentsorgung in Stadtrandgebieten III

Bei diesem Vorhaben handelt es sich um ein offenes Programm, das mittlerweile fünf verschiedene Phasen umfasst. Die FZ-Mittel dieser Phasen werden zusammengefasst, um die folgenden einzelnen Infrastrukturprojekte in der schnell wachsenden Stadt Santa Cruz zu finanzieren:

Versorgungsgebiet der Wasserkooperative SAGUAPAC:
- Neubau des Abwassernetzes im Stadtteil "Distrito 12" für 20.000 Menschen (Abschluss der Bauarbeiten Ende 2019, finanziert durch Mittel der Phase I)
- Neubau der Kläranlage "Motacusal" im Nordosten von Santa Cruz zur Klärung der Abwässer von 250.000 Menschen (Phase II-V, in Durchführung seit Anfang 2020)
- Installation von ca. 30.200 Anschlüssen und knapp 400 km Sekundärnetzen, um die Abwässer des Gebietes der Kläranlage "Motacusal" zuzuführen (Phase V)
- Modernisierung der Kläranlage Ost und Bau eines weiteren Hauptsammlers zur Ableitung der geklärten Abwässer (bolivianischer Eigenbeitrag)

Versorgungsgebiet der Wasserkooperativen COSCHAL und COSPAIL:
- Verbesserung der Trinkwasserqualität (aktuell zu hohe Konzentration von Mineralien) für 20.000 Menschen
- Verbesserung der Versorgungsqualität (aktuell häufige Versorgungsunterbrechungen) für 20.000 Menschen

Dadurch soll ein Beitrag zur Verbesserung der Lebens- und Gesundheitsbedingungen der Bevölkerung sowie der Umweltsituation in der Stadt Santa Cruz geleistet werden.

Die Stadt Santa Cruz fungiert als Programmträger und wird die finanzierte Infrastruktur nach Abschluss der Baumaßnahmen auf die zuständigen Wasserkooperativen übertragen, die dann für Betrieb und Wartung verantwortlich sind. Die Kooperativen werden bereits in der Umsetzung stark bei Planung und Durchführung an den Projekten beteiligt.

Die Kosten des Programms betragen 124,7 Mio. EUR. Aus den Phasen I-V werden durch die Finanzielle Zusammenarbeit (FZ) 88,1 Mio. EUR übernommen. Der bolivianische Eigenbeitrag beläuft sich insgesamt auf 36,6 Mio. EUR.

Land / Region / Institution Plurinationaler Staat Bolivien Nummer 44340 Schwerpunkt Wasser und Abfall Sektor 14020 - Wasser-, Sanitärver. und Abwassermanagement USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 10 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner GOBIERNO AUTÓNOMO MUNICIPAL DE SANTA CRUZ DE LA SIERRA Zuständige Abteilung Lateinamerika und Karibik
