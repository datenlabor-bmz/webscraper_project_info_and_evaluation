  




 

Targeting - Identifizierung armer Haushalte
Support to the Identification of Poor Households (IDPoor) Programme II
Project details
Project number:2011.2247.2
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Ole Doetinchem ole.doetinchem@giz.de
Partner countries: Cambodia, Cambodia
Summary
Objectives:

Die systematisch unter der Steuerung des Ministry of Planning erhobenen und für die ländlichen und städtischen Gebiete verfügbaren Daten armer Haushalte werden als Hauptinformationsquelle für gezielt auf Arme ausgerichtete staatliche und nicht-staatliche Maßnahmen genutzt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Planning

Financing organisation:

not available

 
Project value
Total financial commitment:26 952 684 Euro
Financial commitment for this project number:4 598 072 Euro
Cofinancing

AusAID - alt bis 31.12.2011: 4 298 072Euro


 
Previous projects
2005.2178.1Targeting - Identifizierung armer Haushalten
Follow-on projects
2015.2093.1Targeting - Identifizierung armer Haushalte
 
Term
Entire project:16.02.2006 - 30.11.2022
Actual project:01.05.2012 - 30.04.2016
other participants
not available
 
Contact
Project websiteswww.giz-cambodia.com
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Soziale Sicherung

Evaluation
not available
 
Project description (DE)


Ausgangssituation
Der Kampf gegen die Armut ist eine der größten Herausforderungen für Kambodscha. Damit die nationale Armutsminderungsstrategie greift, müssen Entwicklungsmaßnahmen, Dienstleistungen und soziale Sicherungsprogramme auf die arme Bevölkerung ausgerichtet werden. Dafür werden Verfahren zur Identifizierung der armen Haushalte benötigt.

Ein einheitliches, von allen anerkanntes und landesweit praktiziertes Verfahren fehlte zu Projektbeginn in Kambodscha. So konnten arme Haushalte sich ihres Anspruchs auf grundlegende soziale Dienste nicht sicher sein. Selbst wenn sie rechtlich gesehen einen Anspruch hatten, zum Beispiel auf eine kostenlose medizinische Versorgung, gab es keine Möglichkeit, dieses Recht einzufordern.

Ziel
Ein standardisiertes Verfahren zur Identifizierung armer Haushalte ist von der Regierung Kambodschas offiziell verabschiedet. Die verfügbaren Daten dienen als zentrale Informationsquelle für staatliche und nichtstaatliche Organisationen, die Maßnahmen zugunsten der Armen durchführen.

Vorgehensweise
Nach einem intensiven Beratungs- und Abstimmungsprozess fiel während eines nationalen Forums im Jahr 2006 die Entscheidung, ein landesweit anwendbares Standardverfahren zur Erfassung armer Bevölkerungsgruppen zu entwickeln. Das Verfahren zur Bestimmung der Haushalte setzt auf Transparenz und Partizipation: In den Dörfern bestimmt die Gemeinschaft Vertreter aus ihrer Mitte, die an der Identifizierung der armen Haushalte mitwirken. In einem offenen Dialog mit den Dorfbewohnern wird dann für jedes Dorf anhand von Armutskriterien eine Liste mit armen Haushalten erstellt. Die gewählten Gemeinderäte prüfen sie, entscheiden über die aufzunehmenden Haushalte und veröffentlichen die Ergebnisse.

Das Vorhaben stärkt die Fähigkeiten der Mitarbeiter des Planungsministeriums, das Identifikationsverfahren in Zusammenarbeit mit regionalen Behörden zu koordinieren, wirkungsvoll umzusetzen und zu kontrollieren. Außerdem unterstützt die GIZ das Planungsministerium dabei, die Daten regelmäßig zu aktualisieren und das Verfahren weiterzuentwickeln. So soll nun auch ein Verfahren zur Erfassung armer Haushalte in Städten entwickelt werden. Bisherige Kooperationspartner waren die Europäische Kommission, die australische Entwicklungsorganisation AusAID und das Kinderhilfswerk der Vereinten Nationen UNICEF.

Wirkung – Was bisher erreicht wurde
2011 hat die Regierung das Verfahren als Standard für die gezielte Erfassung armer Bevölkerungsgruppen bestätigt. Die Daten werden nunmehr in allen Provinzen erhoben und alle drei Jahre aktualisiert. Über 12.000 Dörfer im ländlichen Raum (rund 90 Prozent aller Dörfer landesweit) haben das Verfahren zur Identifizierung armer Haushalte angewendet. Insgesamt wurden rund 776.000 Equity Cards (Identifizierungskarten) an arme Haushalte verteilt. Mit diesen Karten erhalten Arme Zugang zu zahlreichen sozialen Dienstleistungen, zum Beispiel zur kostenlosen Behandlung in öffentlichen Gesundheitseinrichtungen. Ab 2015 will das Planungsministerium das Verfahren eigenständig durchführen und ausschließlich aus dem Staatshaushalt finanzieren.
 

 
Project description (EN)


Context
The fight against poverty is one of the greatest challenges facing Cambodia. The national poverty reduction strategy can only take effect if development measures, services and social security programmes are targeted at the poor. In order to achieve this, procedures for identifying poor households are required.

Before the project began its work, there was no standardised, universally recognised and nationally applied procedure in Cambodia. This meant poor households could not be sure of access to basic social services. Although they were legally entitled to services such as free medical treatment, they were unable to assert their rights.

Objective
The Cambodian Government has officially adopted a standardised procedure for identifying poor households. The data collected serve as a central source of information for governmental and non-governmental organisations implementing measures to improve the situation of the poor.

Approach
During a national forum in 2006, after an intensive consultation and coordination process, it was decided that a nationally applicable, standardised procedure for collecting data on poor sections of the population would be developed. The procedure is based on transparency and participation. Village communities elect representatives from among the local population, who help to identify poor households. Following open discussions with village residents, a list of poor households is drawn up for each village in accordance with recognised poverty criteria. The elected representatives then check these lists, make a final decision on the households to be included, and publish the results.

The project is strengthening the capacity of staff at the Ministry of Planning to effectively coordinate and implement the identification process in cooperation with the regional authorities and to ensure it is being carried out properly. GIZ also supports the Ministry of Planning in the process of regularly updating the data and further developing the procedure. The next step will be to develop a procedure for identifying poor households in urban areas. So far, cooperation partners have included the European Commission, the Australian development organisation AusAID, and the United Nations Children’s Fund (UNICEF).

Results achieved so far
In 2011, the government confirmed that the procedure would continue to be used as the standard method of identifying poor sections of the population. The data are now collected in all provinces and updated every three years. Over 12,000 villages in rural areas (around 90% of all the villages in the country) have applied the procedure. Approximately 776,000 Equity Cards (identity cards) have been issued to poor households. These cards give poor people access to a number of social services such as free treatment in public health institutions. From 2015, the Ministry of Planning intends to carry out the procedure independently using exclusively state funds.
 





 
