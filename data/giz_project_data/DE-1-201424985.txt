  




 

Regenerative Energien und Energieeffizienz in den Provinzen Tata und Midelt (DKTI III)
DKTI 3 Renewable Energy and Energy Efficiency in the Provinces of Tata and Midelt
Project details
Project number:2014.2498.5
Status:Projekt beendet
Responsible Organisational unit: 3600 Nordafrika
Contact person:Simon Inauen simon.inauen@giz.de
Partner countries: Morocco, Morocco
Summary
Objectives:

Die Kapazitäten der Provinzen Midelt und Tata zur nachhaltigen Nutzung des Entwicklungspotenzials von Regenerativen Energien und Energieeffizienz (RE-EnEff) sind erhöht.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministère de l'Energie, des Mines, de l'Eau et de l'Environnement (MEMEE)

Financing organisation:

not available

 
Project value
Total financial commitment:6 000 000 Euro
Financial commitment for this project number:6 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:12.10.2015 - 30.01.2021
Actual project:12.10.2015 - 30.01.2021
other participants
Arge GFA Consuting Group-Thalys
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Energiepolitik und -verwaltung

Evaluation
not available
 
Project description (DE)

Ausgangssituation

Die Provinzen Midelt im Mittleren Atlas und Tata im Süden Marokkos verfügen über eine außergewöhnlich hohe Sonneneinstrahlung und ein erhebliches Potential für erneuerbare Energie. Der marokkanische Solarplan „Noor" sieht dort die Installation von Solarkraftwerken mit einer Gesamtkapazität von 2.000 Megawatt (MW) vor.
Gelegen auf einer Höhe zwischen 1.200 und 3.600 Metern, sind in der Provinz Midelt häufige Schneefälle im Winter üblich. Während dieser Phasen nutzt die Bevölkerung vor allem Brennholz, um sich Heizen überhaupt leisten zu können. Das Holz stammt überwiegend aus den Atlas-Wäldern. Wegen des hohen Bedarfs ist der Baumbestand zunehmend bedroht.
Die Provinz Tata hingegen zeichnet sich durch sehr trockene und heiße Wetterperioden aus. Nicht selten übersteigen die Temperaturen im Sommer 45 Grad Celsius. Die Provinz hat mit starker Wüstenbildung zu kämpfen, die die Palmenplantagen bedrohen. Sowohl Midelt als auch Tata sind stark von der Landwirtschaft abhängig, welche durch den Klimawandelt mit weniger Niederschlag rechnen muss. Viele junge Menschen wandern in die großen Ballungszentren an den Küsten Marokkos ab.

Ziel

Die Kapazitäten der Provinzen Midelt und Tata im Bereich erneuerbare Energien und Energieeffizienz sind verstärkt und erlauben es den Vertreter*innen in der Verwaltung, der Zivilgesellschaft und der Privatwirtschaft vom deren Potential zu profitieren.

Vorgehensweise

Das Projekt „Regenerative Energien und Energieeffizienz in den Provinzen Midelt und Tata" (EDMITA) zielt darauf ab, die Kompetenzen im Bereich der erneuerbaren Energien in der Bevölkerung, der Verwaltung sowie der Wirtschaft zu steigern. Die Regionen sollen sowohl vom Ausbau der Sonnen- und Windenergie als auch von der Verbesserung der Energieeffizienz wirtschaftlich profitieren. Erfahrungen und Wissen über Machbarkeit, Planung, Durchführung und Evaluierung werden anhand konkreter Maßnahmen in beiden Provinzen gesammelt und weitergetragen. Mittels 20 Installationen (zum Beispiel Solarpumpen für Bauen, effizientere Öfen in Schulen, neue öffentliche Beleuchtungen, energetische Sanierungen von Klassenzimmern, etc.) wird der Nutzen von erneuerbaren Energien und einer verbesserten Energieeffizienz veranschaulicht. EDMITA strebt dadurch eine Verbesserung der Lebensbedingungen insbesondere der armen Bevölkerung an und möchte neue lokale Wertschöpfungsketten schaffen, damit ein Kleingewerbe in der Energiebranche entsteht. Damit sich die Natur in der Gebirgskette wieder erholen kann, unterstützt das Projekt die Wiederaufforstung des Waldes.
Das Vorhaben fördert zudem den Erfahrungsaustausch untern den vier Pilotkommunen Tata, Fam El Hisn, Er-Rich und Midelt. Ausgebildete Energie-Teams in den Gemeinden und neu eingerichtete Energiemanagement-Systeme unterstützen die Entwicklung neuer Strategien für die Nutzung von erneuerbaren Energien und zur Einsparungen beim Energieverbrauch.
Um die Öffentlichkeit für das Thema zu interessieren und das Bewusstsein für die Umwelt zu stärken, unterstützt das Projekt den Aufbau von Energieinformationszentren und Plattformen. EDMITA hat an den Berufsschulen in Tata und Midelt einen neuen Ausbildungsgang für Solar-Installateur*innen (Photovoltaik) aufgebaut. Diese sollen mit Kursen zum Thema ‚Softskills‘ ergänzt werden, um die Eingliederung der Abgänger*innen in den Arbeitsmarkt zu erleichtern.
Das Projekt arbeitet in den Provinzen mit der GFA Consulting Group GmbH.

Wirkungen

• In den vier Pilotkommunen wurden Energie-Teams gegründet und ausgebildet, welche konkrete Handlungsfelder identifiziert haben. Dazu zählt insbesondere die Verbesserung der Energieeffizienz in der öffentlichen Straßenbeleuchtung.
• Gemeinsam mit der staatlichen Berufsausbildungsinstitution (Office de la formation professionelle et de la promotion du trav, OFPPT) wurde an der Berufsschulen in Tata und Midelt ein neuer Ausbildungsgang für Photovoltaik-Installateur*innen entwickelt.
• 20 Pilotprojekte demonstrieren die Machbarkeit und den Nutzen der Technologien
• In verschiedenen Grundschulen in Midelt werden effizientere Öfen installiert und Klassenräume energetisch saniert werden, um angemessene Temperaturen für die Schulklassen zu schaffen.

 

 
Project description (EN)

Context

The provinces of Midelt in the Middle Atlas Mountains and Tata in the south of Morocco have exceptionally high levels of sunshine and offer considerable potential for renewable energy sources. The Moroccan solar project ‘Noor’ comprises plans for the installation of solar power plants there with a total capacity of 2,000 megawatts (MW).
Situated at an altitude of between 1,200 and 3,600 metres above sea level, the province of Midelt is accustomed to frequent snowfall in the winter. During these cold spells, the population mainly uses firewood as an affordable fuel for heating. This wood primarily comes from Atlas forests. Tree stocks are increasingly threatened as a result of the high demand for firewood.
In contrast, the province of Tata is prone to very dry and hot periods. Temperatures often exceed 45 °C in summer. The province has to contend with severe desertification and the threat that this poses to the palm plantations. Both Midelt and Tata are heavily dependent on agriculture, which will have to deal with reduced precipitation as a consequence of climate change. Many young people migrate to the large conurbations on the coasts of Morocco.

Objective

The capacities of the provinces of Midelt and Tata in the area of renewable energy and energy efficiency have been strengthened and allow representatives from administration, civil society and the private sector to benefit from these potentials.

Approach

The ‘Sustainable Energy in the Provinces of Tata and Midelt’ project (EDMITA) aims to improve capacities in the area of renewable energy among the population, in administration and in the private sector. The regions are to benefit economically from the expansion of solar and wind energy and from the improvement of energy efficiency. Experience and knowledge relating to feasibility, planning, implementation and evaluation are being gathered and disseminated in both provinces on the basis of specific measures. The benefits of renewable energy sources and improved energy efficiency are being demonstrated using 20 installations (such as solar pumps for construction, more efficient stoves in schools, new public lighting, energy-efficient refurbishment of classrooms). In this way, EDMITA aims to improve people’s living conditions – particularly for the poor – and to establish new local value-added chains to foster the development of small businesses in the energy sector. The project supports reforestation so that nature in the mountain range can recover.
In addition, the project promotes the exchange of experience among the four pilot municipalities of Tata, Fam El Hisn, Er-Rich and Midelt. Trained energy teams in communities and newly established energy management systems support the development of new strategies for the use of renewable energy and for achieving savings in energy consumption.
To increase public interest in this issue and raise environmental awareness, the project supports the establishment of energy information centres and platforms. EDMITA has introduced a new training course for solar installation technicians (photovoltaics) at the vocational colleges in Tata and Midelt. Components on soft skills are to be added to these training programmes to help newly trained technicians to enter the labour market.
The project is cooperating with GFA Consulting Group GmbH in the provinces.

Results

• Energy teams have been set up and trained in the four pilot municipalities, and these teams have identified specific fields of action. In particular, these include the improvement of energy efficiency in public street lighting.
• A new training course for photovoltaic installation technicians has been developed at the vocational colleges in Tata and Midelt together with the state vocational training institution (Office de la formation professionnelle et de la promotion du travail, OFPPT).
• 20 pilot projects are demonstrating the feasibility and benefits of the new technologies.
• More efficient stoves are being installed and energy-efficient refurbishment of classrooms carried out at various primary schools in Midelt with the aim of achieving adequate temperatures for lessons.

 

 
Project description (FR)

Situation initiale

Situées respectivement dans le Moyen Atlas et au sud du Maroc, les provinces de Midelt et Tata bénéficient d’un ensoleillement exceptionnel et disposent d’un potentiel considérable en matière d’énergies renouvelables. Le plan solaire marocain « Noor » prévoit dans ces régions l’installation de centrales solaires d’une capacité totale de 2 000 mégawatts (MW).
Située à des altitudes comprises entre 1 200 et 3 600 mètres, la province de Midelt connaît généralement des chutes de neige fréquentes en hiver. Au cours de ces périodes, les populations locales utilisent surtout du bois pour se chauffer. Les besoins importants pour ce bois, qui provient principalement des montagnes de l’Atlas, ne cessent de mettre en danger la forêt de cette région.
En revanche, la province de Tata se distingue par des épisodes de grande sécheresse et de forte chaleur. Les températures y dépassent fréquemment les 45 °C en été. La province doit ainsi lutter contre une désertification importante qui menace les palmeraies.
Ces deux provinces dépendent largement d’un secteur agricole contraint par le changement climatique à s’adapter à de plus faibles précipitations. Par ailleurs, un grand nombre de jeunes partent dans les grandes agglomérations du littoral marocain.

Objectif
Les provinces de Midelt et Tata gagnent des compétences dans le domaine des énergies renouvelables et de l’efficacité énergétique, de telle sorte que les représentant·e·s de l’administration, de la société civile et du secteur privé profitent de ce nouveau domaine d’activité.

Approche
Le projet « Énergie durable dans les provinces de Midelt et Tata » (EDMITA) a pour objectif d’accroître les compétences de la population, de l’administration et des acteurs économiques dans le domaine des énergies renouvelables. Les régions doivent retirer des avantages économiques à la fois du développement de l’énergie solaire et éolienne et de l’amélioration de l’efficacité énergétique. Les expériences et les connaissances en matière de faisabilité, de planification, de mise en œuvre et d’évaluation sont recueillies et diffusées dans les deux provinces à l’aide de mesures concrètes. Une vingtaine d’installations (pompes solaires pour la construction, poêles plus efficaces dans les écoles, nouveaux éclairages publics, rénovations énergétiques de salles de classe, etc.) permettent d’illustrer les avantages des énergies renouvelables et d’une efficacité énergétique améliorée. Le projet EDMITA cherche ainsi à améliorer les conditions de vie des populations de la région, notamment des plus défavorisées, et à créer de nouvelles chaînes de valeur locales favorisant l’implantation de petites entreprises du secteur de l’énergie durable. Par ailleurs, le projet appuie le reboisement des forêts dans le but de permettre à la nature de se régénérer dans la chaîne de montagnes.
En outre, le projet encourage l’échange d’expériences entre les quatre communes pilotes de Tata, Fam El Hisn, Er-Rich et Midelt. Les équipes énergies formées et déployées dans ces communes et les nouveaux systèmes de gestion énergétique introduits appuient le développement de nouvelles stratégies en matière d’utilisation des énergies renouvelables et de réduction de la consommation d’énergie.
Afin d’attirer l’attention de l’opinion publique sur ce thème et de renforcer la sensibilisation à l’environnement, le projet soutient la mise en place de « centres info énergie » et d’autres types de plateformes d’informations. Le projet EDMITA a mis sur pied un nouveau cursus de formation pour installateur·rice·s solaires (photovoltaïque) dans les écoles professionnelles de Tata et Midelt. Ce cursus sera complété par des cours consacrés aux compétences non techniques (soft skills) afin de simplifier l’intégration des diplômé·e·s sur le marché du travail.
Le projet travaille dans les deux provinces en coopération avec la société GFA Consulting Group GmbH.

Résultats
• Plusieurs équipes énergies ont été créées et formées dans les quatre communes pilotes et sont parvenues à identifier des champs d’action concrets. Parmi ces derniers figure notamment l’amélioration de l’efficacité énergétique dans le domaine de l’éclairage public.
• En coopération avec l’Office de la formation professionnelle et de la promotion du travail (OFPPT), un nouveau cursus de formation pour installateur·rice·s photovoltaïques a été élaboré dans les écoles professionnelles de Tata et Midelt.
• Une vingtaine de projets pilotes démontrent la capacité de mise en œuvre des technologies et leurs avantages.
• Plusieurs écoles primaires de Midelt bénéficient de l’installation de poêles plus efficaces et de la rénovation énergétique de leurs salles de classe afin d’offrir des températures appropriées aux élèves et aux enseignant·e·s.

 





 
