Stärkung der Resilienz von vulnerablen Gruppen im Zusammenhang mit der Heuschreckenplage in Äthiopien

Das Vorhaben ist Teil des Programmvorschlags für Übergangshilfe-Vorhaben "Regionalansatz für Übergangshilfe zur Stärkung der Resilienz am Horn von Afrika". Ziel des Vorhabens ist es, die Resilienz der besonders von der Heuschreckenplage 2020 betroffenen Haushalte in den Regionen Afar, Amhara, Oromia und Somali zu erhöhen und ihre Entwicklung zu fördern. Dies soll durch verschiedene Maßnahmen erfolgen:
a) durch die Bereitstellung von Nahrungsmitteln in Form von Schulspeisungen für Grundschulkinder,
b) durch Conditional Cash-Transfers als Gegenleistung für die Mitarbeit an Kleinstprojekten zur Schaffung von Vermögenswerten für die Gemeinde oder den eigenen Haushalt,
c) Unconditional Cash-Transfers für Schwangere und stillende Mütter mit Kindern unter zwei Jahren zur Prävention von Minderwuchs,
d) durch die Stärkung klimaresilienter Lebensgrundlagen für Ackerbauern und Pastoralisten durch Trainings, Verteilung von Saatgut und anderer Güter sowie durch Markterschließungsprogramme für den Privatsektor und
e) durch technische Unterstützung der Regionalregierungen bei der Schaffung von Frühwarnsystemen für künftige Heuschreckenplagen und andere klimabedingte Schocks sowie des Ethiopian Institutes for Public Health (EPHI) für das Monitoring der Kosten nachhaltiger und gesunder Ernährung.

Das Vorhaben wird mit 21,5 Mio. EUR aus Übergangshilfe-Zuschussmitteln unterstützt und läuft von Anfang Oktober 2020 bis Ende Dezember 2023.

Land / Region / Institution Äthiopien Nummer 48195 Schwerpunkt Ernährung und Landwirtschaft Sektor 43072 - Haushalt Ernährungssicherung USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 26,5 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner WORLD FOOD PROGRAMME Zuständige Abteilung Ostafrika und Afrikanische Union
