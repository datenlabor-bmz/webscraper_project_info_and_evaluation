  




 

Unterstützung bei sozialer Sicherung, einschließlich Absicherung im Krankheitsfall
Support to social protection including social health protection
Project details
Project number:2015.2186.3
Status:Projekt beendet
Responsible Organisational unit: 2B00 Asien II
Contact person:Dr. Franz von Roenne franz.roenne@giz.de
Partner countries: Pakistan, Pakistan
Summary
Objectives:

Der Zugang der Bevölkerung, insbesonders armer und armutsgefährdeter Gruppen, zu bedarfsorientierten Diensten der sozialen Sicherung ist verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Finance and Economic Affairs, Economic Affairs Division Islamabad (EAD)

Financing organisation:

not available

 
Project value
Total financial commitment:20 900 000 Euro
Financial commitment for this project number:5 900 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2019.2140.2Unterstützung bei sozialer Sicherung, einschließlich Absicherung im Krankheitsfall
 
Term
Entire project:28.07.2016 - 30.09.2025
Actual project:01.01.2016 - 28.01.2021
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Soziale Sicherung

Evaluation
not available
 




 
