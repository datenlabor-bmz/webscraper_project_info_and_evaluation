  




 

Energiesektorreformprogramm: Energiepolitik und Energieeffizienz (EPEE)
Energy Sector Reform Programme: Energy Policy and Energy Efficiency (EPEE)
Project details
Project number:2007.2130.8
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Thomas Douglas Schmitz thomas.d.schmitz@giz.de
Partner countries: PR China
Summary
Objectives:

Die Energiepolitik von Schlüsselinstitutionen auf nationaler und auf subnationaler Ebene orientiert sich am Leitbild der Nachhaltigkeit und Maßnahmen zur Erhöhung der Ressourcen- und Energieeffizienz werden erfolgreich umgesetzt.

Client:

BMZ

Project partner:

National Development and Reform Commission

Financing organisation:

not available

 
Project value
Total financial commitment:7 900 000 Euro
Financial commitment for this project number:7 900 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:24.11.2008 - 06.06.2014
Actual project:01.12.2008 - 06.06.2014
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektziel-Ebene: Projekt zielt vor allem auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Energiepolitik und -verwaltung

Evaluation
not available
 




 
