  




 

Handel und Investitionen für nachhaltige Entwicklung
Sector Project Trade and Investment for Sustainable Development
Project details
Project number:2017.2038.2
Status:Projekt beendet
Responsible Organisational unit: G120 Nachhaltige Wirtschentw, Digitalisierung
Contact person:Sara Mohns sara.mohns@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh.
Summary
Objectives:

Das Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) bringt Ansätze und Positionen zur nachhaltigen Entwicklung durch Handel in relevante nationale und internationale Prozesse der Handels- und Entwicklungspolitik ein.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:29 501 292 Euro
Financial commitment for this project number:4 572 314 Euro
Cofinancing

not available

 
Previous projects
2015.2023.8SV Entwicklungsorientierte Handelspolitik, Handels- und Investitionsförderung
Follow-on projects
2020.2148.3SV Handel und Investitionen für nachhaltige Entwicklung
 
Term
Entire project:06.12.2001 - 31.07.2023
Actual project:01.11.2017 - 27.08.2020
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Handelspolitik und -verwaltung

Evaluation
not available
 
Project description (DE)

Ausgangssituation

Handel gilt als Schlüssel für nachhaltiges Wirtschaftswachstum und Wohlstand. Besonders wertschöpfungs- und technologieintensive Produkte wiederum gelten als dynamische Elemente des weltweiten Handels. Vielen Entwicklungsländern gelingt es aber nicht, sich so am internationalen Handel zu beteiligen, dass die erhofften positiven Wirkungen für Beschäftigung und Armutsminderung eintreffen. Entscheidende Faktoren sind die Ausgestaltung des internationalen Regelwerkes für den Handel, einheitliche Politik in Entwicklungsländern und Industrieländern (Politikkohärenz) sowie strukturelle Voraussetzungen in den Entwicklungsländern. Handlungsleitend für das deutsche Engagement ist daher das übersektorale Konzept handelsbezogener Entwicklungszusammenarbeit „Handel als Motor für Entwicklung – die deutsche Strategie für Aid for Trade".

Ziel

Handel ist als Querschnittsthema in der Politik- und Portfoliogestaltung der deutschen Entwicklungszusammenarbeit strukturell verankert und entwicklungspolitische Themen in die Handelspolitik integriert. Chancen für Beschäftigung und Wirtschaftswachstum in Entwicklungs- und Schwellenländern durch Handel werden stärker genutzt und möglichen negativen Effekten gezielt entgegengewirkt.

Vorgehensweise

Das vom Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) beauftragte Sektorvorhaben „Handel und Investitionen für nachhaltige Entwicklung" besteht seit 2002 und arbeitet in zwei Handlungsfeldern, die eng miteinander verknüpft sind: Politikberatung des BMZ für eine entwicklungsorientierte Handelspolitik und Ansätze sowie Instrumente zur Umsetzung handelsbezogener Entwicklungszusammenarbeit.

Wirkungen

Das Bundesentwicklungsministerium hat sich mit Unterstützung des Vorhabens in mehreren internationalen Prozessen und Diskussionen erfolgreich eingebracht: zum Beispiel bei der Umsetzung des Abkommens über Handelserleichterungen der Welthandelsorganisation WTO, bei weiteren Verhandlungen der Doha-Agenda und des Dienstleistungsabkommens (TiSA) sowie bei der Verlängerung des Übereinkommens über handelsbezogene Aspekte der Rechte des geistigen Eigentums (TRIPS) für am wenigsten entwickelte Länder.
Mit Projekten der deutschen Entwicklungszusammenarbeit wurden innovative Ansätze zu verschiedenen Handelsthemen getestet und verbreitet. So hat das Sektorvorhaben an der Konzeption zweier Vorhaben mitgewirkt, die Entwicklungsländer bei der Umsetzung des WTO-Abkommens zu Handelserleichterungen unterstützen. Außerdem beriet das Vorhaben in Zentralasien und Mosambik zu Handelserleichterungen (Trade Facilitation).
Die vielfältige Arbeit des Vorhabens trägt dazu bei, dass Partnerländer Handelspotenziale besser für ihre wirtschaftliche und soziale Entwicklung nutzen können. 

 
Project description (EN)

Background

Trade is considered key to sustainable economic growth and prosperity. In turn, products that are particularly technology-intensive and generate a considerable degree of added value are regarded as dynamic elements of global trade. However, many developing countries are unable to participate in international trade in a way that achieves the anticipated positive results for employment and poverty reduction. Crucial factors here include the way in which international rules relating to trade are structured, uniform policies in developing countries and industrialised countries (policy coherence) as well as structural requirements in the developing countries. Consequently, Germany’s activities are primarily guided by the cross-sectoral strategy of trade-related development cooperation, entitled Free and fair trade as a driver for development – The German strategy for Aid for Trade.

Objective

Trade is a cross-cutting issue that is structurally embedded in the policy-making and portfolio design of German development cooperation, and development issues are integrated into trade policy . Opportunities for employment and economic growth in developing countries and emerging economies through trade are being used more intensively, and potential negative impacts are being countered in a targeted manner.

Approach

Commissioned by the German Federal Ministry for Economic Cooperation and Development (BMZ), the Trade and Investment for Sustainable Development sector project has been in existence since 2002 and operates in two closely connected fields of activity: advising BMZ on development-oriented trade policy and approaches, and instruments for implementing trade-related development cooperation.

Impact

With the support of the project, BMZ has made successful contributions to a number of international processes and discussions. These include the implementation of the World Trade Organization’s Trade Facilitation Agreement, further negotiations on the Doha Development Agenda and the Trade in Services Agreement (TiSA), as well as the extension of the Agreement on Trade-Related Aspects of Intellectual Property Rights (TRIPS) for least developed countries.
Innovative approaches to various trade issues have been tested and disseminated by German development cooperation projects. For instance, the sector project has contributed to the design of two projects that help developing countries to implement the WTO Trade Facilitation Agreement. The project has also provided advice on trade facilitation in Central Asia and Mozambique.
The wide-ranging work of the project helps partner countries to make better use of trade potential for their economic and social development.
 





 
