Welttreuhandfonds für Kulturpflanzenvielfalt

Kernproblem:
Bereits heute hungern 800 Millionen Menschen. Um die wachsende Weltbevölkerung ernähren zu können, ist die landwirtschaftliche Pflanzenproduktion grundlegend. Gleichzeitig wird der Klimawandel weiter fortschreiten und die Rahmenbedingungen für die pflanzliche Nahrungsproduktion signifikant verändern. Dabei kann die pflanzliche Genvielfalt, durch Züchtung angepasster Sorten, Handlungsspielraum ermöglichen.

Modulziel:
Ziel des Vorhabens ist es, einen Beitrag zur langfristigen Nahrungssicherheit durch den Erhalt und die nachhaltige Nutzung pflanzengenetischer Ressourcen zu leisten. Hierzu wird der bereits bestehende Welttreuhandfonds für Kulturpflanzenvielfalt mit FZ-Mitteln ausgestattet werden.

Wesentliche Outputs:
Effizienzsteigerung beim Betrieb des Welttreuhandfonds für Kulturpflanzenvielfalt; langfristige Mittelbereitstellung durch den Welttreuhandfonds für Kulturpflanzenvielfalt; verbesserter Betrieb und Management der Saatgutbanken.

Zielgruppe:
Zielgruppe sind die Nutzer der geförderten Saatgutbanken. Der Erhalt des globalen Gutes "Kulturpflanzenvielfalt" dient der Ernährungssicherung für die Weltbevölkerung. Sie stellt somit die Zielgruppe im weiteren Sinne dar.

Beitrag zur nationalen Umsetzung der Agenda 2030 (nicht im MV):
Damit trägt das Vorhaben zu den Zielen 1 "Armut in jeder Form und überall dauerhaft beenden" , 2 "Hunger beenden und Ernährungssicherheit schaffen", 3 "Gesundheit für alle Menschen und ihr Wohlergehen fördern" , 13 "Maßnahmen zur Bekämpfung des Klimawandels ergreifen" und 15 "Ökosysteme schützen und Biodiversitätsverlust stoppen" für nachhaltige Entwicklung und damit zur Umsetzung der Agenda 2030 bei.

Land / Region / Institution Alle Entwicklungsländer Nummer 35215 Schwerpunkt Ernährung und Landwirtschaft Sektor 41030 - Biodiversität Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 25 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner - Zuständige Abteilung Westafrika
