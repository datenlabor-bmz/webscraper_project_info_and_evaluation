UNICEF, Bildung und Schutz von Kindern, Irak

Das vorliegende Programm führt die im Rahmen des Vorhabens "Stärkung von Resilienz von Flüchtlingen und aufnehmenden Gemeinden im Kontext der Syrien- und Irakkrise" (BMZ-Nr. 201567932) begonnene Unterstützung des UNICEF-Engagements im Irak im Bereich Kinderschutz fort. Dazu stellt die deutsche Seite einen Finanzierungsbeitrag von bis zu EUR 21 Mio. an UNICEF zur Verfügung. Der Zugang zu und die Qualität von Schulbildung sollen mit zusätzlichen Mitteln in weiteren Gemeinden in den Provinzen Duhok, Erbil und Sulaymaniya ausgebaut werden. Das vorliegende Programm hat eine Implementierungsdauer von 5 Jahren (Beginn Dezember 2015).

Aufgrund von Verzögerungen, im Zusammenhang mit der Ausbreitung des Corona-Virus wurde die Projektlaufzeit des Programms um 8 Monate bis zum 31.08.2021 kostenneutral verlängert.

Die Einzelmaßnahmen umfassen Neubau und Renovierung von Schulgebäuden, Beschaffung von Lehr- und Lernmaterialien, Bereitstellung von Cash Transfers für besonders betroffene Kinder, Trainings- und Fortbildungsmaßnahmen für Lehrkräfte sowie begleitende Qualitätssicherungs- und Monitoringmaßnahmen.

Land / Region / Institution Naher und mittlerer Osten Nummer 35188 Schwerpunkt Bildung Sektor 11220 - Grundschulbildung Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 24,243 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner UNITED NATIONS CHILDREN'S FUND Zuständige Abteilung Afghanistan, Pakistan
