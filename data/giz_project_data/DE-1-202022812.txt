  




 

EAC_Unterstützung der Lake Victoria Basin Commission der Ostafrikanischen Gemeinschaft (EAC/LVBC)
Support to the Lake Victoria Basin Commission
Project details
Project number:2020.2281.2
Status:laufendes Projekt
Responsible Organisational unit: 1500 Ostafrika
Contact person:Conrad Thombansen conrad.thombansen@giz.de
Partner countries: EAC EAST AFRICA, Burundi, Kenya, Rwanda, South Sudan, Tanzania, Uganda
Summary
Objectives:

Die institutionellen Rahmenbedingungen für eine integrierte Bewirtschaftung der grenzüberschreitenden Wasserressourcen in der Ostafrikanischen Staatengemeinschaft sind gestärkt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Secretariat of the East African Community (EAC)

Financing organisation:

not available

 
Project value
Total financial commitment:3 300 000 Euro
Financial commitment for this project number:3 300 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:10.12.2021 - 31.12.2024
Actual project:01.01.2022 - 31.12.2024
other participants
ARGE GFA Consulting Group GmbH-
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Wassersektorpolitik und -verwaltung

Evaluation
not available
 




 
