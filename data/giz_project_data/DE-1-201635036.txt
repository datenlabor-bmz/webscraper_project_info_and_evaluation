  




 

Studien- und Fachkräftefond Nepal
Study- and Expert-Fund Nepal
Project details
Project number:2016.3503.6
Status:laufendes Projekt
Responsible Organisational unit: 2A00 Asien I
Contact person:Mechthild Hansel mechthild.hansel@giz.de
Partner countries: Nepal, Nepal
Summary
Objectives:

Vorbereitung und Prüfung von Vorhaben der Technischen Zusammenarbeit (TZ), Finanzierung von Studien, Gutachten sowie Durchführung von Maßnahmen geringen Umfangs.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Economic Relation Division

Financing organisation:

not available

 
Project value
Total financial commitment:3 383 905 Euro
Financial commitment for this project number:3 383 905 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:15.08.2019 - 31.12.2025
Actual project:01.08.2019 - 31.12.2025
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
