ASEAN Biodiversitätszentrum Small Grants Programme Phase II

Die zweite Phase des Small Grants Programme (FZ-Modul) setzt sich aus einer nationalen und einer regionalen Komponente zusammen. Ziel der nationalen Komponente ist, dass erfolgreich umgesetzte Einzelmaßnahmen (Small Grants) (i) zur Verbesserung des Biodiversitätsschutzes in ausgewählten ASEAN Heritage Parks (AHP) und deren Randgebieten sowie (ii) zur Verbesserung der Lebensbedingungen der lokalen Bevölkerung beitragen. Außerdem sollen (iii) auf nationaler Ebene Koordinationsmechanismen und Managementkapazitäten für AHPs in Vietnam gestärkt werden. Ziel der regionalen Komponente ist es, (iv) dass das ACB als regionale Institution für den nachhaltigen Schutz der biologischen Vielfalt in den ASEAN Mitgliedsstaaten gestärkt ist.

Empfänger und Träger des Vorhabens ist das ASEAN Centre for Biodiversity (ACB). Verantwortlich für die Umsetzung vor Ort sind ausgewählte NGOs in Kooperation mit dem viet-namesischen Ministerium für Naturressourcen und Umwelt (Ministry of Natural Resources and Environment - MoNRE).

Die Zielgruppe des FZ-Moduls ist die lokale Bevölkerung, die für ihre Existenz direkt von den natürlichen Ressourcen in ausgewählten AHPs abhängt. Projektgebiet sind voraussichtlich vier der fünf vietnamesischen AHPs (Ba Be, Chu Mom Ray, Hoang Lien Son und Kon Ka Kinh) sowie die Flächen um diese Nationalparks.

Wie in der ersten Phase, können ländliche Kommunen und Parkverwaltungen in AHPs und deren Pufferzonen und NGOs sich um Zuschüsse zwischen 5.000 und 150.000 EUR (Small Grants) zur Finanzierung von Maßnahmen zum Erhalt von Biodiversität und der Verbesserung von Lebensbedingungen der Anrainerbevölkerung bewerben.

Die Gesamtkosten des FZ-Moduls werden auf EUR 7,4 Mio. geschätzt. Zur Finanzierung sind ein FZ-Zuschuss in Höhe von 6,39 Mio. EUR und ein Eigenbeitrag von rd. 1 Mio. EUR vorgesehen.

Land / Region / Institution ASEAN Centre for Biodiversity (ACB) Nummer 31864 Schwerpunkt Umwelt und Klima Sektor 41030 - Biodiversität USV-Kategorie B+ Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber Eigenbeitrag d. Landes/Own contribution of country Deutscher Finanzierungsbeitrag 6,39 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner ASEAN CENTRE FOR BIODIVERSITY Zuständige Abteilung Ostasien und Pazifik
