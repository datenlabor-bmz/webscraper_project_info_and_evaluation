  




 

Schutz der Agro-Biodiversität im ländlichen Raum Albaniens
Protected Area Management and Sustainable Use of Biodiversity in Mountain Areas of Albania
Project details
Project number:2011.2195.3
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Dr. Ralf Peveling ralf.peveling@giz.de
Partner countries: Albania
Summary
Objectives:

Die Bewirtschaftung von Schutzgebieten und nachhaltige Nutzung der Biodiversität durch öffentliche, private und zivilgesellschaftliche Akteure in ausgewählten Bergregionen Albaniens ist verbessert

Client:

BMZ

Project partner:

Ministry of Tourism and Environment

Financing organisation:

not available

 
Project value
Total financial commitment:4 529 415 Euro
Financial commitment for this project number:2 026 856 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2014.2199.9Schutz der Agrobiodiversität im ländlichen Raum
 
Term
Entire project:28.12.2011 - 30.04.2019
Actual project:28.12.2011 - 31.05.2015
other participants
GOPA Consultants GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektziel-Ebene: Projekt zielt vor allem auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Biodiversität

Evaluation
not available
 




 
