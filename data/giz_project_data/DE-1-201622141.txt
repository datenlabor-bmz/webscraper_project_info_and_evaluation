  




 

Programm zur Unterstützung des sambischen Dezentralisierungsprozesses III
Decentralisation for Development III
Project details
Project number:2016.2214.1
Status:Projekt beendet
Responsible Organisational unit: 1300 Südliches Afrika
Contact person:Dr. Wolff-Christian Peters wolff-christian.peters@giz.de
Partner countries: Zambia, Zambia
Summary
Objectives:

Relevante Akteure auf nationaler Ebene und Gemeinden sind besser in der Lage, den Dezentralisierungsprozess, insbesondere mit Blick auf Fiskaldezentralisierung, effizienter, transparenter, koordinierter, bürgernäher und armutsorientierter umzusetzen.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Cabinet Office: Management Development Division (MDD), (Unit: Decentralisation Secretariat DS)

Financing organisation:

not available

 
Project value
Total financial commitment:27 996 821 Euro
Financial commitment for this project number:6 465 469 Euro
Cofinancing

not available

 
Previous projects
2014.2074.4Programm zur Unterstützung des sambischen Dezentralisierungsprozesses
Follow-on projects
2020.2095.6Förderung effizienter und transparenter lokaler Regierungsführung in Sambia
 
Term
Entire project:30.01.2012 - 31.03.2025
Actual project:01.04.2018 - 30.04.2021
other participants
GFA Consulting Group GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Dezentralisierung und Förderung subnatio Gebietskörpers

Evaluation
not available
 
Project description (DE)

Ausgangssituation
In Sambia ist das Einkommen bislang recht ungleich verteilt, sodass fast zwei Drittel der Bevölkerung arm ist. Für ihr Überleben sind diese Menschen auf funktionierende kommunale Dienstleistungen angewiesen. Die Chancen, dass eine gelingende Dezentralisierung der staatlichen Dienstleistungen das Leben der Menschen in Sambia spürbar verbessert, stehen jedoch so gut wie nie. Sambias Wirtschaft wuchs bis vor kurzem stetig. Seit dem Regierungswechsel 2011 hat die Regierung den politischen Willen gezeigt, die Dezentralisierung des Landes voranzutreiben. Im Auftrag des Bundesministeriums für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) hat die Deutsche Gesellschaft für Internationale Zusammenarbeit (GIZ) GmbH seit 2012 die sambischen Partnerinstitutionen dabei unterstützt, die Grundlagen für die Implementation des gegenwärtigen Devolutionsprozesses zu legen. Die sambische Regierung hat umfangreiche administrative und rechtliche Vorbereitungen für die Umsetzung der Reformbeschlüsse zur weiteren Übertragung von Verantwortlichkeiten an Kommunen getroffen.

Ziel
Relevante nationale und kommunale Akteure sind besser in der Lage, die Dezentralisierung, vor allem mit Blick auf die Fiskaldezentralisierung, effizienter, transparenter, koordinierter, bürgernäher und armutsorientierter einzusetzen.

Vorgehensweise
Das Programm unterstützt die sambische Regierung bei der Steuerung der Dezentralisierungsreform und der Förderung der lokalen Verwaltung. Das Vorhaben zielt auf die Verbesserung der Verwaltungsfähigkeiten von 24 Gemeinden in der Süd- und der Nordwest-Provinz. Die Strategie baut auf den Initiativen, Erfahrungen und Ergebnissen des Vorgängervorhabens auf.

Das Vorhaben verbessert die Leistungsfähigkeit von Expert*innen und Manager*innen sowie von Kommunen, Institutionen und Ministerien. Das Vorhaben verwendet eine Kombination von Formaten, um die spezifischen Bedürfnisse der Partner*innen zu befriedigen (zum Beispiel kollaboratives Lernen, spezialisierte Trainingsmodule, Coaching). Damit werden Gemeinderät*innen, kommunale Angestellte sowie relevante Staatsbeamt*innen befähigt, ihre Aufgaben effektiver und Interessenskonflikte konstruktiv zu bewältigen.

Das Vorhaben vermittelt nachfragebasierte technisch-methodologische Beratung für das Dezentralisierungssekretariat, das Ministerium für Lokalverwaltung, die Provinzbüros für Lokalverwaltung, Partnergemeinden und Trainingsinstitute. Damit werden die Effektivität und Management der Dienstleistungserbringung sowie die Rechenschaftslegung der Partnerorganisationen verbessert. Innovative Verfahren und Instrumente werden standardisiert und in die kommunale Dienstleistungserbringung integriert (unter anderem Fiskal-Katasteroder ergebnis-basierte Leistungsbewertungen).

Das Vorhaben berät unter Verwendung praktischer Lehrbeispiele Gemeinden und Partnerministerien dabei, Defizite in der lokalen Dienstleistungserbringung zu beheben und legale und normative Vorgaben umzusetzen. Zudem unterstützt das Vorhaben auch die Verbindungsfunktion der Provinzinstitutionen zwischen den entsprechend höheren und niedergeordneten Behörden in den Bereichen Informationsvermittlung, gesetzliche, finanzielle und technische Überwachung.

Inhaltlich koordiniert das Programm seine Aktivitäten eng mit anderen von der GIZ in Sambia umgesetzten Vorhaben, vor allem mit dem Wasserprogramm sowie den Programmen zur Förderung guter finanzieller Regierungsführung und politischer Teilhabe. Da es sich um ein Gemeinschaftsprogramm mit der KfW-Entwicklungsbank handelt, werden die verschiedenen Maßnahmen durch die finanzielle Zusammenarbeit unterstützt.

Die drei Arbeitsbereiche des Vorhabens umfassen

• Steuerung der Dezentralisierungsreform
• Generierung und Verwaltung öffentlicher lokaler Einnahmen
• Verbesserung von Planung und Management kommunaler Dienstleistungen.

Die GFA Consulting Group unterstützt die Umsetzung der letzten beiden Punkte.

Wirkungen
Mehrere Vorgängerprogramme leisteten wichtige Vorarbeit für die aktuellen Maßnahmen. So wurde der Plan zur Umsetzung der Dezentralisierung (DIP) für 2013-2017 verabschiedet, in dem die einzelnen Schritte, die für die Realisierung der Nationalen Dezentralisierungsstrategie notwendig sind, definiert sind.

Das Vorhaben hat den Kommunen bei der Einführung von integrierten Entwicklungsplänen geholfen. Deren Anwendung erfolgt erstmalig 2019/2020. Der integrierte Planungsansatz harmonisiert die kommunale mit der nationalen Entwicklungsplanung, was wiederum den Devolutionsprozess innerhalb der Dezentralisierung stärkt.

In seinen drei Kernbereichen unterstützte das Programm:

• das sambische Dezentralisierungssektretariat in seinen Bemühungen, die Reformprozesse für kommunale Verwaltung zu koordinieren und umzusetzen, unter anderem durch Hilfe bei der Entwicklung eines neuen DIP.

• die Gemeinden in der Süd- und Nordwestprovinz bei der Steigerung eigener Einnahmen. Einige Gemeinden können dadurch wesentliche Teile ihres Gesamteinkommens aus eigenen Einnahmequellen generieren.

• die Koordination zwischen kommunalen Akteuren bei der Erbringung wichtiger Dienstleistungen, insbesondere in den Bereichen Wasserversorgung, Abwasser-Management und Gesundheit. Im Zuge dessen wurden Standardverfahren für eine geregelte Abfallentsorgung entwickelt und zum Beispiel in der Gemeinde Choma in der Südprovinz umgesetzt.
 

 
Project description (EN)

Context
With income distribution in Zambia remaining extremely inequitable, almost two thirds of the population live in poverty and are dependent for their survival on functioning municipal services. Nevertheless, the chance of making a perceptible improvement in the quality of life in Zambia through successful decentralisation of public services is better than ever before. Until recently, Zambia enjoyed steady economic growth and since the change of government in 2011 has demonstrated the political will to push ahead with decentralisation in the country. Since 2012,on behalf of the German Federal Ministry for Economic Cooperation and Development (BMZ), GIZ has supported efforts by its Zambian partner institutions to lay the foundations for the ongoing devolution process. The Zambian Government has made substantial administrative and legal preparations for the adoption of reform decisions on the further transfer of responsibilities to local governments.

Objective
The relevant national and municipal actors are better able to implement decentralisation, especially fiscal decentralisation, in a more efficient, transparent and coordinated way more closely geared to citizens’ needs and to poverty reduction.

Approach
The programme supports the efforts of the Zambian Government to steer the decentralisation reform process and to promote local administration. It aims to improve the administrative capabilities of 24 municipalities in the Southern and Northwestern provinces. The strategy builds on the initiatives, experiences and results of the predecessor programme.

The programme helps make experts and managers, as well as municipalities, institutions and ministries, more effective.It uses a combination of formats to meet the specific needs of partners (for example, collaborative learning, specialised training modules, coaching).These provide local councillors, municipal employees and government officials with skills enabling them to fulfil their tasks more effectively and find constructive solutions to conflicts of interest.

The programme provides demand-based, technical and methodological advice for the Decentralisation Secretariat, the Ministry of Local Government, the provincial local government offices, partner municipalities and training institutes, thereby improving the effectiveness and management of service provision as well as the accountability of the partner organisations. Innovative procedures and instruments are being standardised and integrated into municipal service provision (including a fiscal cadastre and results-based performance assessments).

Using examples from the field, the programme advises municipalities and partner ministries on ways of redressing deficits in local service provision and implementing normative requirements. It also supports the provincial institutions’ role as a liaison between upstream and downstream authorities in the areas of information provision and legal, financial and technical monitoring.

Programme activities are closely coordinated with the activities of other programmes being implemented by GIZ in Zambia, in particular the water programme and the programmes on good financial governance and political participation.Since this is a joint programme with KfW Development Bank, the various measures are supported through financial cooperation.

The three areas in which the programme is working are:
- Steering of decentralisation reform
- Generation and administration of local public revenue
- Better planning and management of municipal services

Results
Several predecessor programmes completed important preparatory work for the current measures.For example, the Decentralisation Implementation Plan (DIP) for 2013-2017 was adopted, which set out the steps required to fulfil the requirements of the National Decentralisation Policy.
The programme helped the municipalities introduce integrated development plans. These will be applied for the first time in 2019-2020. The integrated planning strategy alignsmunicipal with national development planning, which in turn is reinforcing the process of devolution in decentralisation.

In its three core areas, the programme:

Supported the Zambian Decentralisation Secretariat in its efforts to coordinate and implement municipal administration reform processes, also by helping it develop a new DIP;

Helped municipalities in the Southern and Northwestern provinces increase their own income, allowing some municipalities to generate a substantial proportion of their total revenue from their own sources.

Supported coordination between municipal actors in the provision of important services, especially water supply, wastewater management and health services.As a result, standard procedures have been developed for regulated waste disposal and implemented, for example in the municipality of Choma in the Southern Province. 





 
