  




 

Programm Überwindung von weiblicher Genitalverstümmelung
Combating Female Genital Mutilation in Ethiopia
Project details
Project number:2008.2093.6
Status:Projekt beendet
Responsible Organisational unit: 1700 Afrika Überregional und Horn von Afrika
Contact person:Dr. Christian Jahn christian.jahn@giz.de
Partner countries: Ethiopia
Summary
Objectives:

Individuelle und kollektive Einstellungen und Verhaltensweisen im Hinblick auf schädliche traditionelle Praktiken, insbesondere weilbliche Genitalverstümmelung, haben sich in der Projektregion verändert.

Client:

BMZ

Project partner:

Ministry of Women Affairs

Financing organisation:

not available

 
Project value
Total financial commitment:70 000 Euro
Financial commitment for this project number:70 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:12.09.2011 - 11.07.2014
Actual project:01.09.2011 - 11.07.2014
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt zielt vor allem auf Gleichberechtigung ab
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Menschenrechte

Evaluation
not available
 




 
