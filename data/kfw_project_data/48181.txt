COVID-19 Active Response and Expenditure Program (CARES)

Unter dem Corona-Soforthilfeprogramm "COVID-19 Active Response and Expenditure (CARES) Program" wird die indonesische Regierung insbesondere bei
a) der Verbesserung der medizinischen Versorgung,
b) der Ausweitung ihrer Sozialhilfeprogramme,
c) der Finanzierung von Einkommensteuersenkungen für Privathaushalte sowie
d) unterstützenden Konjunkturmaßnahmen für ausgewählte Wirtschaftssektoren
zur Abfederung der Folgen der Pandemie unterstützt.
Das Programm erfolgt in paralleler Kofinanzierung mit der ADB. Des Weiteren beteiligt sich die AIIB an dem Programm.

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde entsprechend der Nachhaltigkeitsrichtlinie der KfW Entwicklungsbank in die Umwelt- und Sozialrisikokategorie C eingeordnet, da vom Vorhaben voraussichtlich keine oder nur geringe nachteilige Auswirkungen und Risiken auf Umwelt und soziale Belange ausgehen. Es ist daher kein spezifischer Umwelt- und Sozialmanagementplan erforderlich. Der Partner wurde zur Einführung eines Beschwerdemechanismus verpflichtet und muss die KfW unverzüglich über umwelt- und sozialrelevante besondere Vorkommnisse informieren.

Land / Region / Institution Indonesien Nummer 48181 Schwerpunkt Wirtschaft und Finanzsektor Sektor 12264 - COVID-19-Bekämpfung USV-Kategorie C Finanzierungsinstrument Förderkredit Weitere Geber - Deutscher Finanzierungsbeitrag 250 Mio. EUR Status abgeschlossen Auftraggeber BMZ Projektpartner MINISTRY OF FINANCE OF THE REPUBLIC OF INDONESIA Zuständige Abteilung Ostasien und Pazifik
