  




 

Schutz der Biodiversität in Feuchtgebieten
Wetland Biodiversity Protection
Project details
Project number:2008.2213.0
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Shenqiang Luan shenqiang.luan@giz.de
Partner countries: PR China
Summary
Objectives:

Die Umsetzung eines integierten Ökosystemansatzes durch staatliche und zivilgesellschaftliche Akteure trägt zur Sicherung der Biodiversität und nachhaltigen Nutzung der Ressourcen in ausgewählten Feuchtgebieten Chinas bei.

Client:

BMZ

Project partner:

State Forestry Administration

Financing organisation:

not available

 
Project value
Total financial commitment:3 323 144 Euro
Financial commitment for this project number:3 323 144 Euro
Cofinancing

Shangri-la Institute for Sustainable Communities (SISC): 283 250Euro


 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:20.10.2009 - 31.12.2015
Actual project:01.07.2010 - 31.12.2015
other participants
Arge DFS / IUCN
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektziel-Ebene: Projekt zielt vor allem auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Biodiversität

Evaluation
not available
 




 
