  




 

Strategische Zusammenarbeit
Strategic cooperation with international Organisations
Project details
Project number:2012.6251.8
Status:Projekt beendet
Responsible Organisational unit: G410 Global Policy
Contact person:Dr. Anselm Schneider anselm.schneider@giz.de
Partner countries: Internat. Zusammenarbeit mit Regionen für Nachhalt
Summary
Objectives:

Verbesserte Mitgestaltung der Politikausrichtung bei internationalen Organisationen zu globalen Agenden

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:9 665 941 Euro
Financial commitment for this project number:4 665 941 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2015.6258.6Strategische Zusammenarbeit in und mit internationalen Organisationen
 
Term
Entire project:31.07.2012 - 11.05.2020
Actual project:31.07.2012 - 30.06.2016
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Multisektorale Hilfe

Evaluation
Deutschland und International: Globales Vorhaben: Strategische Zusammenarbeit in und mit Internationalen Organisationen. Projektevaluierung: Kurzbericht
 




 
