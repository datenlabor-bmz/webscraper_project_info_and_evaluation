  




 

Pan-Afrikanische Freihandelszone (CFTA)-GIZ-Modul - Phase II
Continental Free Trade Area (CFTA)
Project details
Project number:2020.2208.5
Status:laufendes Projekt
Responsible Organisational unit: 1700 Afrika Überregional und Horn von Afrika
Contact person:Svenja Ossmann svenja.ossmann@giz.de
Partner countries: African Union, Angola, Burkina Faso, Burundi, Benin, Botswana, Dem. Rep. Congo, Centr.Afr.Rep., Republic of the Congo, Côte d'Ivore, Cameroon, Cape Verde, Djibouti, Algeria, Egypt, Eritrea, Ethiopia, Gabon, Ghana, Gambia, Guinea, Equatorial Gui., Guinea-Bissau, Kenya, Comoros, Liberia, Lesotho, Libya, Morocco, Madagascar, Mali, Mauritania, Mauritius, Malawi, Mozambique, Namibia, Niger, Nigeria, Rwanda, Sudan, Saint Helena, Ascension and Tristan da Cunha (V/K), Sierra Leone, Senegal, Somalia, South Sudan, S.Tome,Principe, Eswatini, Chad, Togo, Tunisia, Tanzania, Uganda, South Africa, Zambia, Zimbabwe
Summary
Objectives:

Die Rahmenbedingungen für die panafrikanischen Verhandlungen der Continental Free Trade Area (CFTA) im Bereich Handelserleichterung und Zollkooperation sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Afrikanische Union

Financing organisation:

not available

 
Project value
Total financial commitment:64 146 348 Euro
Financial commitment for this project number:54 903 445 Euro
Cofinancing

Europäische Union (EU): 5 403 445Euro


 
Previous projects
2016.2000.4Pan-Afrikanischen Freihandelszone (CFTA)- GIZ-Modul
Follow-on projects
not available
 
Term
Entire project:19.02.2015 - 31.12.2024
Actual project:01.08.2020 - 31.12.2024
other participants
ARGE GOPA Worldwide Consultants GmbH-
GFA Consulting Group GmbH
GOPA Worldwide Consultants GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Regionale Handelsabkommen

Evaluation
not available
 
Project description (DE)

Ausgangssituation
Obwohl in Afrika für den Zeitraum 2019–2020 ein Wachstum von durchschnittlich 3,6 Prozent prognostiziert wird und die am schnellsten wachsenden Volkswirtschaften der Welt auf dem Kontinent zu finden sind, gibt es noch viel zu tun. Afrika ist nach wie vor stark von Rohstoff- und Agrarexporten abhängig. Investitionsgüter oder Lebensmittelerzeugnisse werden überwiegend von außerhalb des Kontinents importiert. Mit einem Welthandelsanteil von weniger als 3 Prozent hat Afrika eine Exportdiversifizierung erst noch zu leisten, da viele afrikanische Länder auf Einnahmen aus Rohstoffexporten angewiesen sind, während sie bei der Industrialisierung ins Hintertreffen geraten.

Vor diesem Hintergrund bleibt der innerafrikanische Handel hinter den gegebenen Möglichkeiten zurück. 2017 lag sein Anteil am gesamten afrikanischen Handelsvolumen bei rund 17 Prozent. Im Vergleich dazu liegt der Anteil des kontinentalen Handels in Nordamerika bei 51 Prozent der Exporte, in Asien bei 49 Prozent, in Lateinamerika bei 22 Prozent und in Westeuropa bei 69 Prozent. In einigen regionalen Wirtschaftsgemeinschaften wurde durch den Abbau von Zöllen die Handelsintegration verbessert, doch ist der afrikanische Markt nach wie vor fragmentiert. Nichttarifäre Hemmnisse wie unkoordinierte bürokratische Verfahren, lange Wartezeiten an der Grenze oder zeitraubende und hinderliche Ausfuhrbestimmungen erhöhen die Handelskosten auf dem Kontinent. Afrika hat sich daher schneller mit dem Rest der Welt als im Inneren verbunden.

Mit dem Vertrag von Abuja einigten sich die Mitgliedstaaten der Organisation für Afrikanische Einheit (OAU) 1991 auf einen Fahrplan zur Schaffung eines gemeinsamen afrikanischen Marktes. Um die Umsetzung des Vertrags zu beschleunigen und die regionale Integration zu stärken, kamen die Handelsminister der Afrikanischen Union (AU) überein, eine Panafrikanische Freihandelszone (African Continental Free Trade Area, AfCFTA) zu errichten. Seither ist die AfCFTA ein Leuchtturmprogramm der AU, und im Juni 2015 wurden die AfCFTA-Verhandlungen aufgenommen.

Auf dem außerordentlichen Gipfeltreffen der AU im März 2018 in Kigali (Ruanda) unterzeichneten 44 der 55 Mitgliedstaaten der AU das entsprechende Übereinkommen. Themen der ersten Verhandlungsphase sind Warenverkehr, Dienstleistungsverkehr und Streitbeilegung. In der zweiten Phase stehen Investitionen, Wettbewerbspolitik und Rechte des geistigen Eigentums auf der Tagesordnung. Einige offene Fragen der Phase I wie Zolltarife, Ursprungsregeln und Einzelheiten des Dienstleistungsverkehrs müssen noch abschließend behandelt werden. Gleichzeitig begann mit dem Gipfeltreffen von Kigali der Prozess der Ratifizierung des AfCFTA-Übereinkommens, und die Zahl der AU-Mitgliedstaaten, die das Übereinkommen in ihren nationalen Parlamenten ratifizieren, steigt. Mit der 22. Ratifizierung, die bei der AU hinterlegt wird, tritt das Übereinkommen in Kraft.

Ziel
Die AU koordiniert die laufenden Freihandelsverhandlungen und den Übergang zur Schaffung der Panafrikanischen Freihandelszone (AfCFTA).

Vorgehensweise
Die Kommission der AU, insbesondere ihre Abteilung für Handel und Industrie (Department of Trade and Industry, DTI), ist für die Koordinierung der BIAT- und AfCFTA-Aktivitäten zuständig. Im Rahmen des Vorhabens berät ein in den DTI-Strukturen eingesetzter Berater für regionale Handelspolitik den Handelskommissar der AU-Kommission in strategischen Fragen der Politik und Interessenvertretung. Zur Unterstützung der AfCFTA-Verhandlungsgruppe bei der Vorbereitung der Verhandlungen wird außerdem eine Fachkraft für Zollkooperation und Handelserleichterungen eingesetzt. Die Verhandlung offener Fragen aus Phase I über den Waren- und Dienstleistungsverkehr wird mit verschiedenen Mechanismen begleitet, etwa einer maßgeschneiderten fachlichen Beratung, der Organisation von Schulungen, Seminaren und Workshops oder gezielter finanzieller Unterstützung.

In bestimmten AfCFTA-Themenbereichen arbeitet die GIZ zusätzlich mit mehreren institutionellen Partnern zusammen. Die Kooperation mit der SheTrades-Initiative des ITC soll einen gleichstellungsorientierten Ansatz in den Verhandlungsprozess einbinden. Gemeinsam mit der Wirtschaftskommission der Vereinten Nationen für Afrika (United Nations Economic Comission for Africa, UNECA) und dem Zentrum für Handelsrecht (Trade Law Centre, TRALAC) werden Anstrengungen zur Sensibilisierung und Informationsverbreitung unternommen. Darüber hinaus ist eine enge Zusammenarbeit mit den regionalen Wirtschaftsgemeinschaften Ostafrikanische Gemeinschaft (EAC), Wirtschaftsgemeinschaft der westafrikanischen Staaten (ECOWAS) und Entwicklungsgemeinschaft des südlichen Afrika (SADC) vorgesehen, um die Vernetzung der kontinentalen und der regionalen Ebene zu gewährleisten.

Das Programm befasst sich im Zusammenhang mit der AfCFTA mit Themen wie der Einbindung der Interessengruppen, der Industrialisierung, Sonderwirtschaftszonen, ökologischem Wirtschaften oder elektronischem Handel. Ein sich rasch veränderndes Afrika braucht ein Handelsübereinkommen, das den aktuellen sozioökonomischen Gegebenheiten Rechnung trägt, um eine solide Grundlage für die wirtschaftliche Integration des gesamten Kontinents zu legen. 

 
Project description (EN)

Context
Although growth in Africa is forecasted at an average of 3.6 per cent for 2019–20, with the world’s fastest growing economies being on the continent, there is still much to be done. Africa is still heavily reliant on commodity and agricultural exports while importing capital goods or food products predominantly from outside the continent. With a global trade share of less than 3 per cent, export diversification has yet to be achieved, as many African countries still rely on rents from extractive exports, whilst falling behind on industrialisation efforts.
Against this backdrop, intra-African trade remains below its potential, accounting for about 17 per cent of the total African trade volume in 2017. In contrast, North American intracontinental trade accounts for 51 per cent of exports, 49 per cent in Asia, and 22 per cent in Latin America, while among West¬ern European countries this number reaches 69 per cent. Although some Regional Economic Communities (RECs) have achieved improvements in trade integration through tariff reductions, the African market remains fragmented. Non-tariff barriers such as uncoordinated bureaucratic procedures, long waiting times at the border or lengthy and cumbersome export requirements raise trade costs on the continent. As a result, Africa has integrated with the rest of the world faster than with itself.

With the Treaty of Abuja in 1991, the Member States of the Organisation for African Unity (OAU) agreed on a road map for the creation of a common African market. To accelerate the implementation of the Treaty and strengthen regional integration, the African Union (AU) Trade Ministers agreed to establish an African Continental Free Trade Area (AfCFTA). The AfCFTA has since been a flagship programme of the AU and AfCFTA negotiations were launched in June 2015.

Out of 55 Member States of the AU, 44 signed the Agreement in March 2018 at the AU Extraordinary Summit in Kigali, Rwanda. Phase I of negotiations covers trade in goods, trade in services and dispute settlement. Phase II is to cover investment, competition policy and intellectual property rights. Outstanding issues of Phase I such as tariff schedules, rules of origin and specifics on trade in services are yet to be completed. At the same time, the Kigali summit kicked off the ratification process of the AfCFTA, with an increasing number of AU Member States ratifying the Agreement in their national parliaments. The AU comes into force with the 22nd ratification deposited at the AU.

Objective
The AU is coordinating the ongoing free trade negotiations and the transition to implementation of the African Continental Free Trade Area (AfCFTA).

Approach
The AU Commission (AUC), specifically AUC´s Department of Trade and Industry (DTI) is in charge of coordinating in BIAT and AfCFTA related activities. The project advises the AUC Trade Commissioner on strategic policy and advocacy issues by seconding a regional trade policy advisor to DTI´s structures. It also supports the AfCFTA Negotiation Unit by seconding a customs cooperation and trade facilitation expert for the preparation of the negotiations. Outstanding negotiation issues of Phase I for trade in goods and trade in services are being supported by a range of mechanisms, e.g. by providing tailor made technical consultancy expertise, by organising trainings, seminars and workshops or by selectively rendering financial support.

In addition, GIZ is cooperating with a number of institutional partners on specific AfCFTA related topics. The cooperation with ITC’s SheTrades Initiative aims to embed a gender sensitive approach into the negotiating process. Joint efforts with the United Nations Economic Comission for Africa (UNECA) and the Trade Law Centre (TRALAC) are aimed at raising awareness and disseminating information. Furthermore, a close cooperation with the RECs East African Community (EAC), Economic Community of West African States (ECOWAS) and the Southern African Development Community (SADC) is envisaged to ensure the continental and the regional levels are linked.

The programme is targeting AfCFTA related topics such as stakeholder involvement, industrialisation, Special Economic Zones, Green Econ¬omy or eCommerce. In a rapidly changing Africa, a trade agreement that reflects current socioeconomic realities is needed to provide a solid foundation for economic integration across the continent. 





 
