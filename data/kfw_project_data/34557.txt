Trinkwasserversorgung und Abwasserentsorgung für syrische Flüchtlinge und aufnehmende Gemeinden V

Das Vorhaben ist Bestandteil des gemeinsam mit der französischen Agence Francaise de Developpement (AFD) geplanten Vorhabens:

"Improved access to water distribution performance and related sewerage disposal in Irbid Governorate for host communities and Syrian refugees"

Das Vorhaben mit einem Gesamtinvestitionsvolumen von voraussichtlich 152,0 Mio. EUR soll gemeinsam durch AFD, EU und Deutschland finanziert werden. Der deutsche Finanzierunganteil in Höhe von 80,0 Mio. EUR umfasst eine Zuschusskomponente von 40,0 Mio. EUR sowie eine Darlehenskomponente in Höhe von 40,0 Mio. EUR. Die Darlehenskomponente wird aus Mitteln der Vorhaben Wasserressourcen Management Programme III (BMZ Nr. 2012.66.725) und Wasserressourcen Ma-nagement Programme IV (BMZ Nr. 2014.67.869) finanziert. Die AFD hat ihrerseits bereits 40,0 Mio. EUR Zuschussmittel über den Regional Trust Fund in Response to the Syrian Crisis (MADAD) der EU und die Neighbourhood Investment Facility (NIF) der EU für das Vorhaben eingeworben und wird selbst ein konzessionäres Darlehen in Höhe von 32,0 Mio. EUR bereitstellen.

Im Rahmen des Vorhabens sollen Maßnahmen zur Verbesserung der Wasserverteilung sowie Abwasserentsorgung in den nördlichen Gouvernoraten Jordaniens finanziert werden. Zudem werden im Rahmen des Gesamtvorhabens Maßnahmen zur Stärkung der technischen Kapazitäten des regionalen Versorgers Yarmouk Water Company (YWC) finanziert, welche auf Wartung und Erhalt der finanzierten Infrastruktur abzielen. Eine weitere Projektkomponente umfasst den breitenwirksamen, stark armuts- und flüchtlingsorientierten Ausbau des Zugangs zur Wasserver- und Abwasserentsorgung durch die Bereitstellung von Investitionshilfen auf Haushaltsebene. Die Zielgruppe des Vorhabens besteht sowohl aus syrischen Flüchtlingen als auch aus der einheimischen Bevölkerung in der Projektregion.

Mit der Vorhaben soll die Trinkwasserver- und Abwasserentsorgung der einheimischen Bevölkerung und der Flüchtlinge im Norden Jordaniens verbessert werden, Konflikten vorgebeugt werden sowie ein Beitrag zum Schutz der Umwelt und zur Anpassung an den Klimawandel geleistet werden (Ziel des FZ-Moduls).

Das Vorhaben zur weiteren Unterstützung der Aufnahmegemeinden im Norden Jordaniens soll gemeinsam mit der französischen Agence Francaise de Developpement (AFD) im Rahmen der sognannten Mutual Reliance Initiative (MRI) umgesetzt werden. Die Laufzeit des Vorhabens beträgt voraussichtlich 60 Monate.

Hinweis: Die Zielindikatoren beziehen sich jeweils auf das Gesamtvorhaben.

Land / Region / Institution Jordanien Nummer 34557 Schwerpunkt Wasser und Abfall Sektor 14020 - Wasser-, Sanitärver. und Abwassermanagement USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 32 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner WATER AUTHORITY OF JORDAN Zuständige Abteilung Nahost
