  




 

Landmanagement und Dezentrale Planung
Landmanagement and Decentralised Planning in Lao PDR
Project details
Project number:2014.2123.9
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Dr. Thomas Taraschewski-Wiik thomas.taraschewski@giz.de
Partner countries: Laos, Laos
Summary
Objectives:

Die ländliche Bevölkerung in Zielgebieten verfügt über erhöhte Rechtssicherheit der Landnutzung bei verbesserter Steuerung der Investitionen im öffentlichen und privaten Sektor.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Planning and Investment

Financing organisation:

not available

 
Project value
Total financial commitment:15 424 110 Euro
Financial commitment for this project number:7 024 110 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2016.2192.9Landmanagement und Dezentrale Planung
 
Term
Entire project:05.11.2014 - 31.12.2023
Actual project:01.01.2015 - 31.03.2018
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Ländliche Entwicklung

Evaluation
not available
 
Project description (DE)

Ausgangssituation
Schon seit mehreren Jahren hat Laos ein starkes Wirtschaftswachstum zu verzeichnen. Die ländliche Bevölkerung hat davon jedoch bislang nicht nennenswert profitiert. Siebzig Prozent der Bevölkerung sind direkt oder indirekt von der Landwirtschaft abhängig. Die Lebensgrundlage vieler Subsistenzproduzenten ist durch die Vergabe umfangreicher Konzessionen an Investoren einerseits und fehlende Schutzvorkehrungen für Landnutzungsrechte andererseits bedroht. Vor allem Randgruppen und Frauen sind betroffen. Die Bevölkerung in ländlichen Gebieten wird an der Planung und Steuerung ihrer eigenen sozioökonomischen Lage nicht maßgeblich beteiligt. Öffentliche Einrichtungen verfügen nur begrenzt über die nötige Leistungsfähigkeit, um Strategien zur Armutsbekämpfung zu entwickeln und umzusetzen.
Die öffentlichen Akteure sind noch nicht dazu in der Lage, die gesetzlich verankerten Rechte der Landbevölkerung auf die Nutzung von Flächen und das Investieren in Land zu schützen. Der derzeitige Umgang mit öffentlicher und privater Investitionstätigkeit sorgt nicht dafür, dass Armut bekämpft wird und Nachhaltigkeit entsteht.

Ziel
Die Bevölkerung in den ländlichen Zielgebieten erhält umfangreichere, gesetzlich verankerte Landnutzungsrechte, während die Investitionstätigkeit im öffentlichen und privaten Sektor besser verwaltet wird.

Vorgehensweise
Das Vorhaben bietet politische Beratung im Bereich der Landverwaltung an. Gefördert werden die systematische Registrierung privater und kommunaler Landflächen und die Zuweisung von Landtiteln. Während einerseits die Qualität der Investitionen in Land verbessert und dezentrale Entwicklungsplanung gefördert wird, unterstützt das Projekt außerdem den Aufbau individueller und institutioneller Kompetenzen und Leistungsfähigkeit für eine armutsorientierte Planung und Entwicklung.
Das Projekt unterstützt seine Partner dabei, einen Dialog zwischen den Gebern, der Regierung und zivilgesellschaftlichen Organisationen herbeizuführen, und bei der Entwicklung bodenpolitischer und landrechtlicher Vorschriften und Gesetze.
Eine transparente und partizipatorische Entwicklungsplanung, unter Berücksichtigung aktueller Eigentumsverhältnisse und Nutzungsrechte, bedeutet nicht nur Rechtssicherheit für die lokale Bevölkerung, sondern sorgt auch allgemein für mehr politischen Schutz. Wenn Planung und Eigentumsrechte transparent gehandhabt werden, können Interessenkonflikte zwischen privaten Investoren (beispielsweise in Form von Landkonzessionen) und traditionellen, althergebrachten Landnutzungsrechten und Bedürfnissen der Existenzsicherung schon in frühen Stadien identifiziert und gelöst werden.
Das Programm unterstützt einen breit angelegten Aufbau von Kompetenzen und Leistungsfähigkeit auf allen Ebenen (Distrikt-, Provinz- und nationale Ebene), um die Leistung der öffentlichen Akteure zu verbessern. Dieser mehrdimensionale Ansatz fördert den Austausch von Informationen und Erfahrungen zwischen den drei Verwaltungsebenen einerseits und den Dorfbewohnern andererseits. Darüber hinaus unterstützt das Programm die Zusammenarbeit zwischen den Ministerien auf allen Ebenen.
Es werden besondere Anstrengungen unternommen, damit zivilgesellschaftliche Akteure soweit wie möglich einbezogen werden. Zudem ergeben sich zahlreiche Synergiepotenziale aus der Kooperation mit dem neuen regionalen GIZ-Vorhaben „Verbesserung des Landmanagements in der Mekong-Region", das wiederum eng mit dem Programm „Mekong Region Land Governance" der Schweizer Direktion für Entwicklung und Zusammenarbeit (DEZA) zusammenarbeitet.

Wirkung
Das Programm baut auf den Erfolgen der beiden GIZ-Vorhaben „Landmanagement und wirtschaftliche Entwicklung im ländlichen Raum" und „Ländliche Entwicklung von Armutsregionen in Laos" auf. Durch die intensive Beteiligung der Zielgruppen wurden im Rahmen dieser Projekte Prozesse und Instrumente zur Entwicklung von Entwicklungsplänen für Dörfer und Distrikte, Investitions- und Landnutzungsplänen getestet und eingeführt. Darüber hinaus wurden Systeme für die systematische Landregistrierung entwickelt und Foren für Dialoge geschaffen. Die Erfahrungen daraus sind außerdem in die Kommentare zu neuen bodenpolitischen und landrechtlichen Vorschriften eingeflossen. 

 
Project description (EN)

Context
Laos has reported high economic growth for several years. The rural population has so far benefited relatively little. 70 per cent of the population depends on agriculture either directly or indirectly. The awarding of large-scale concessions to investors and the lack of adequate protection for land use rights are threatening the livelihoods of many subsistence producers, especially marginalised groups and women. People in rural areas do not participate to any large extent in planning and managing their own socio-economic situation, and public institutions have only limited capacities to design and implement strategies to tackle poverty.
Public stakeholders are not yet able to safeguard the legal rights of rural people to use and invest in land. Nor are they able to manage public and private investments in a way that will alleviate poverty and be sustainable.

Objective
People in the rural target areas enjoy greater legal rights to land use, while the management of investments in the public and private sectors has improved.

Approach
The project provides policy advice on governance in the land sector. It is strengthening the systematic registration of individual and communal plots of land, and encouraging the allocation of land titles. While supporting improvements to the quality of investments in land and to decentralised development planning, it also supports individual and institutional capacity development for pro-poor development planning and management.
The project is assisting its partners in establishing a dialogue between donors, the government and non-governmental organisations, and on developing a land policy and land laws.
Transparent and participatory development planning that takes into account current ownership and usage rights of land not only ensures local people have legal security, it also provides them with a more general level of political protection. By ensuring transparency in planning and ownership it is possible to identify and resolve conflicts of interest between private investments (for example, in the form of land concessions) and traditional ancestral land use rights and subsistence needs at an early stage.
The programme promotes broad-based capacity development at all levels (district, provincial and national) so as to improve the performance of public stakeholders. With this multi-level approach it encourages the three layers of administration to exchange information and experiences with one another and also with village residents. The programme also supports inter-ministerial coordination at all levels.
Special efforts are made to involve civil society stakeholders wherever possible. Furthermore, many potential synergies are being opened up through cooperation with a new regional GIZ project, ‘Improving Land Management in the Mekong Region’, which in turn has close ties to the ‘Mekong Region Land Governance’ programme of the Swiss Agency for Development and Cooperation (SDC).

Results achieved so far
This programme builds on the results of two other GIZ projects, ‘Land Management and Rural Economic Development’ and the ‘Northern Upland Integrated Rural Development Programme’. With the greater involvement of the target groups, these projects successfully tested and introduced processes and instruments for creating village and district development plans as well as investment and land-use plans, and they developed systematic land registration systems and dialogue forums. These experiences have also been fed into comments on the new land policy and land law. 





 
