  




 

Programm lokale Selbstverwaltung und Wirtschaftsförderung
Programme for Local Self-government and Economic Development
Project details
Project number:2012.2457.5
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Karin Hoerhan karin.hoerhan@giz.de
Partner countries: Bosnia Herzeg., Bosnia Herzeg.
Summary
Objectives:

Die Wettbewerbsfähigkeit von ausgewählten Wirtschaftsstandorten ist verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministerium für Außenhandel und wirtschaftliche Beziehungen

Financing organisation:

not available

 
Project value
Total financial commitment:28 910 000 Euro
Financial commitment for this project number:6 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2015.2191.3Lokale Entwicklungsstrategien - Programm für lokale Selbstverwaltung und Wirtschaftsförderung
 
Term
Entire project:31.10.2012 - 30.11.2022
Actual project:31.10.2012 - 19.10.2016
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Geschäftspolitik und -verwaltung

Evaluation
Bosnien und Herzegowina: Programm lokale Selbstverwaltung und Wirtschaftsförderung (ProLocal). Projektevaluierung: Kurzbericht
 
Project description (DE)

Ausgangssituation
Bosnien und Herzegowina hat in den vergangenen Jahren bei der Umsetzung politischer und ökonomischer Reformen kaum Fortschritte erzielt. In internationalen Rankings, in denen die wirtschaftliche Leistungsfähigkeit beurteilt wird, belegt Bosnien und Herzegowina in Europa stets die hintersten Plätze, zum Beispiel den letzten Rang von 46 Ländern im Doing Business Index 2015 (Gesamt: 107 von 189 Ländern).
Die Produktivität und Qualität der Produkte lokaler Unternehmen genügent derzeit häufig nicht den Anforderungen internationaler Märkte. Andererseits besteht in einigen Sektoren – Möbelherstellung, Metallverarbeitungsindustrie, Lebensmittelproduktion, sowie landwirtschaftlicherm Anbau von Baum- und Beerenobst – das Potenzial, sowohl den heimischen Markt zu bedienen als auch Produkte in die EU und weitere Länder zu exportieren. Öffentliche Institutionen, Privatwirtschaft und Zivilgesellschaft müssten intensiver zusammenarbeiten, um die wirtschaftliche Leistung zu steigern und den Standort Bosnien und Herzegowina gemeinsam attraktiv zu gestalten. Gerade die öffentliche Verwaltung sieht sich nicht als Motor für wirtschaftliche Entwicklung und geht daher die strukturellen Probleme nicht in ausreichendem Maße an.
Ziel
Ausgewählte Standorte in Bosnien und Herzegowina verbessern ihre wirtschaftliche Leistungsfähigkeit und Wettbewerbsfähigkeit.
Vorgehensweise
Das Vorhaben konzentriert sich auf drei Pilotregionen in Bosnien und Herzegowina, in denen schrittweise Methoden und Konzepte der kommunalen Wirtschaftsförderung und Wertschöpfungskettenentwicklung umgesetzt werden. Gemeinden werden in ihrer interkommunalen Zusammenarbeit und in ihren Dialogen mit lokalen Unternehmen unterstützt, damit sie strategische Maßnahmen gemeinsam und partizipativ planen und umsetzen.
Darüber hinaus fördert die GIZ die Kooperation der Gemeinden mit weiteren relevanten Institutionen, zum Beispiel mit Regionalentwicklungsagenturen, Ministerien und Wirtschaftsverbänden. So werden die erarbeiteten Konzepte landesweit verbreitet und gemeinsam erarbeitete Initiativen zur Steigerung der Wettbewerbsfähigkeit umgesetzt.
Wirkung – Was bisher erreicht wurde
Im Norden Bosnien-Herzegowinas haben sich vier Gemeinden und deren KMU- Sektor zu einer Local Action Group (LAG) zusammengeschlossen, um insbesondere landwirtschaftliche Produkte der Region gemeinsam zu vermarkten und neue Vermarktungspotenziale zu erschließen.
In Zentralbosnien betreiben drei Gemeinden entitätsübergreifend Standortmarketing unter dem Namen „Business Excellence Area (BEAR)". Ihre Bemühungen sind im Frühjahr 2014 auch vom Foreign Direct Investment Magazine der Financial Times Group anerkannt worden. BEAR zählt demnach mittlerweile zu den attraktivsten Investitionsstandorten Europas, was sich in tatsächlichen Investitionen in den Gemeinden niederschlägt.
 

 
Project description (EN)

Context
In recent years, Bosnia and Herzegovina has made little progress in implementing political and economic reform. In international economic efficiency rankings, Bosnia and Herzegovina always occupies a low position among the European countries. For example, it came last out of 46 countries in the Doing Business 2015 index (overall ranking: 107th out of 189 countries).
The productivity of local companies and the quality of their products often do not meet the requirements of international markets. However, in certain sectors – including furniture manufacturing, the metal processing industry, food production and fruit and berry cultivation – the potential is there to satisfy the demand of the domestic market and export products to the EU and other countries. Public institutions, the private sector and civil society must cooperate more closely to improve economic performance and make Bosnia and Herzegovina an attractive business location. In particular, the public administration does not view itself as a driving force behind economic development and, consequently, does not address the structural problems sufficiently.
Objective
Selected locations in Bosnia and Herzegovina improve their economic efficiency and competitiveness.
Approach
The programme focuses on three pilot regions in Bosnia and Herzegovina, where methods and strategies in the fields of promoting local economic development and value chain development are gradually being implemented. It supports municipalities in cooperating with each other and engaging in dialogue with local companies, enabling them to participate jointly in planning and implementing strategic measures.
In addition, GIZ promotes the municipalities’ cooperation with other relevant institutions, such as regional development agencies, ministries and business associations. This ensures that the strategies developed are disseminated throughout the country and that joint initiatives to boost competitiveness are implemented.
Results
In the north of Bosnia and Herzegovina, four municipalities have joined forces with small and medium-sized enterprises to form a local action group (LAG) with a view to jointly marketing products from the region – in particular agricultural produce – and developing new marketing possibilities.
In central Bosnia, three municipalities now carry out inter-entity location marketing under the name Business Excellence Area (BEAR). Their efforts were recognised in early 2014 by Foreign Direct Investment Magazine published by the Financial Times Group. The magazine singled out BEAR as one of the most attractive investment locations in Europe – a claim supported by the real investments now coming into the municipalities.

 





 
