UNRWA - Basisdienstleistungen im Kontext der Syrienkrise im Libanon und Jordanien

Der seit 2012 andauernde Bürgerkrieg in Syrien hat massive Fluchtbewegungen in die Nachbarländer verursacht.
Das Ziel der FZ-Maßnahme ist es, die Resilienz der palästinensischen Flüchtlinge zu stärken und ihren Zugang zu Basisdienstleistungen (Bildung und Gesundheit) aufrecht zu erhalten. Dadurch wird ein Beitrag zur Stabilisierung der Nachbarländer der Syrienkrise geleistet und die Auswirkungen auf die Flüchtlinge verringert.
Es sollen Lehrer und Gesundheitspersonal von UNRWA (United Nations Relief and Works Agency for Palestine Refugees in the Near East
) im Libanon, als auch Lehrer von UNRWA in Jordanien finanziert werden. Damit erhalten palästinensische Flüchtlinge aus Syrien und palästinensische Flüchtlinge (1) Zugang zu Schulbildung in UNRWA-betriebenen Schulen im Libanon und Jordanien und (2) Zugang zu medizinischer Grundversorgung in UNRWA-Gesundheitszentren im Libanon.
Es wird dazu beigetragen, dass sich das Konfliktpotenzial, das sich aufgrund der zum Teil sehr armen Bevölkerung und dem sehr hohen Druck auf Ressourcen und Dienstleistungen innerhalb der Camps und für die dort lebenden Flüchtlinge ergibt, reduziert. Dadurch wird zur Stabilität in den Flüchtlingslagern beigetragen.

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde entsprechend der Nachhaltigkeitsrichtlinie der KfW Entwicklungsbank in die Umwelt- und Sozialrisikokategorie C eingeordnet, da vom Vorhaben voraussichtlich keine oder nur geringe nachteilige Auswirkungen und Risiken auf Umwelt und soziale Belange ausgehen. Es ist daher kein spezifischer Umwelt- und Sozialmanagementplan erforderlich. Der Partner wurde zur Einführung eines Beschwerdemechanismus verpflichtet und muss die KfW unverzüglich über umwelt- und sozialrelevante besondere Vorkommnisse informieren.

Land / Region / Institution Naher und mittlerer Osten Nummer 42406 Schwerpunkt Sonstige Sektor 16050 - Multisektorale Hilfe für soziale Grunddienste USV-Kategorie C Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 15 Mio. EUR Status abgeschlossen Auftraggeber BMZ Projektpartner UNITED NATIONS RELIEF AND WORKS AGENCY FOR PALESTINE REF Zuständige Abteilung Nahost
