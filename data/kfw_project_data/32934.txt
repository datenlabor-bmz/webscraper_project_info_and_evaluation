Schwerpunktprogramm Gesundheit, Komponente "Mutter-Kind Versorgung" Phase IX (Investition)

Modulziel: Verbesserung der Inanspruchnahme von fallgerechten Versorgungsleistungen für Mütter und Neugeborene sowie Stärkung des Referenzsystems in der Oblast Talas.

Zielgruppe: Risikoschwangere, Mütter sowie Früh- und erkrankte Neugeborene in der Oblast Talas.

Wesentliche Outputs: Bau und Ausstattung eines Anbaus für Geburtshilfe und Neonatologie am Oblast-Krankenhauses Talas (sekundäre Versorgungsebene zur Notfallversorgung von Geburten); Kapazitätsaufbau zur Verbesserung der klinischen Versorgungsqualität und des Referenzsystems (inkl. der Einführung einer einfachen telemedizinischen Plattform zur Unterstützung des Austauschs zu Risikofällen), der Wartung medizinischer Geräte (inkl. der Weiterentwicklung eines elektronischen Inventarisierungssystems) sowie des Krankenhausmanagements (inkl. Budgetplanung, Infektionsprävention und Abfallmanagement) - teilweise in Form von Post-Completion Support.

Land / Region / Institution Kirgisistan Nummer 32934 Schwerpunkt Gesundheit Sektor 13020 - Förderung reproduktiver Gesundheit USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber Eigenbeitrag d. Landes/Own contribution of country Deutscher Finanzierungsbeitrag 6 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner MINISTRY OF HEALTH REPUBLIC OF KIRGISTAN Zuständige Abteilung Osteuropa, Kaukasus und Zentralasien
