  




 

Studien- und Fachkräftefonds
Studies and Experts Fund
Fonds d'Etudes et d'Experts
Project details
Project number:1995.3602.0
Status:Projekt beendet
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Verena Blickwede verena.blickwede@giz.de
Partner countries: Haiti, Haiti
Summary
Objectives:

Vorbereitung und Prüfung von Vorhaben der Technischen Zusammenarbeit (TZ), Finanzierung von Studien, Gutachten sowie Durchführung von TZ-Maßnahmen geringen Umfangs.

Client:

BMZ

Project partner:

Ministerium für Planung und externe Kooperation

Financing organisation:

not available

 
Project value
Total financial commitment:1 803 141 Euro
Financial commitment for this project number:1 792 915 Euro
Cofinancing

not available

 
Previous projects
1991.2082.5Studien- und Fachkräftefonds
Follow-on projects
not available
 
Term
Entire project:26.05.1992 - 30.01.2021
Actual project:30.10.1995 - 30.01.2021
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
