  




 

Globalvorhaben Wiederherstellung waldreicher Landschaften und gute Regierungsführung im Waldsektor (Forests4Future)
Forests4Future
F4F
Project details
Project number:2019.0125.5
Status:laufendes Projekt
Responsible Organisational unit: G330 Umweltpolitik, Biodiversität, Wald
Contact person:Susanne Wallenoeffer susanne.wallenoeffer@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh., Côte d'Ivore, Cameroon, Germany, Ethiopia, Laos, Madagascar, Togo, Viet Nam
Summary
Objectives:

Internationale, nationale und lokale Akteure tragen zunehmend zur Wiederherstellung von Waldlandschaften und zu guter Regierungsführung im Waldsektor bei

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Partnerministerien

Financing organisation:

not available

 
Project value
Total financial commitment:58 655 752 Euro
Financial commitment for this project number:58 655 752 Euro
Cofinancing

DFID (bis 30.08.2020) - Neu: FCDO: 2 655 752Euro


 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:10.12.2019 - 30.06.2027
Actual project:01.01.2020 - 30.06.2027
other participants
ARGE DFS Deutsche Forstservice GmbH-
DFS - Deutsche Forst-
GFA Consulting Group GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Forstentwicklung

Evaluation
not available
 
Project description (DE)

Ausgangssituation:

Rund ein Drittel der Erdoberfläche ist von Wäldern bedeckt. Ihr Wert ist unermesslich: Rund 80 Prozent der bekannten Tier- und Pflanzenarten außerhalb der Ozeane befinden sich in Wäldern. Gleichzeitig ist der Wald für jeden fünften Menschen die Lebensgrundlage.
Jährlich gehen jedoch mehr als 7,6 Millionen Hektar Wald verloren – ein Großteil davon in den Tropen und Subtropen für die Ausweitung landwirtschaftlicher Flächen. Mit dem Verlust der Waldflächen geht ein wichtiger Kohlenstoffspeicher verloren, zudem setzt ihre Zerstörung zusätzlich klimaschädliche Treibhausgase frei.
Der Waldschutz stellt eine wichtige Maßnahme im Kampf gegen den Klimawandel dar: Natürliche Regeneration, Aufforstung sowie Agroforstmaßnahmen, die Ackerbau mit Bäumen kombinieren, können Kohlenstoff langfristig binden. Erosionsschutz und eine verbesserte Wasserverfügbarkeit tragen zudem maßgeblich zur Anpassung an den Klimawandel bei.
Auf internationaler Ebene gibt es eine Vielzahl von Vereinbarungen zum Schutz, zur nachhaltigen Nutzung und zur Wiederherstellung von Wäldern. Hierzu gehört die im Jahr 2014 verabschiedete „New York Declaration on Forests" (NYDC), die zum Ziel hat, Entwaldung vollständig zu stoppen. Zusätzlich beabsichtigt sie, dass Wälder und baumreiche Landschaften auf einer Fläche von 350 Millionen Hektar bis zum Jahr 2030 wiederhergestellt werden sollen. Damit knüpft sie an die Ziele der Bonn Challenge an – eine weitere globale Vereinbarung für die Wiederherstellung von waldreichen Landschaften.
Die ehrgeizigen Ziele zum Waldschutz konnten bisher nur unzureichend umgesetzt werden. Der politische Wille ist in den Partnerländern vorhanden, allerdings bremsen schwache Regierungsstrukturen im Waldbereich und mangelnde Koordination von Einzelmaßnahmen die Zielerreichung.

Ziel:
Der Wiederaufbau von Wäldern und baumreichen, produktiven Landschaften wird in Äthiopien, Madagaskar und Togo zunehmend umgesetzt. Die Regierungsführung im Waldbereich ist verbessert. Der EU-Prozess zur Rechtsdurchsetzung, Politikgestaltung und zum Handel in der Forstwirtschaft ist in Zusammenarbeit mit den Ländern Côte d’Ivoire und Laos maßgeblich vorangebracht.

Vorgehensweise:
Im Auftrag und in enger Abstimmung mit dem Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) verfolgt das Vorhaben Forests4Future unterschiedliche Handlungsfelder.
Es berät das Bundesministerium zu den Themen Forest Landscape Restoration (FLR) sowie zur Rechtsdurchsetzung, Politikgestaltung und dem Handel in der Forstwirtschaft (FLEGT). Weiterhin begleitet das Vorhaben die African Forest Landscape Restoration Initiative (AFR100) konzeptionell und beteiligt sich an der Steuerung der Initiative. Forests4Future kümmert sich um Fragestellungen im Zusammenhang mit der Bonn Challenge und weiteren internationalen Initiativen mit FLR-Bezug. Zudem werden europäische und internationale Prozesse zur Verbesserung der Regierungsführung im Waldsektor begleitet, vor allem FLEGT. Wichtige Beiträge, um die nationalen FLR-Ziele in ausgewählten Partnerländern zu erreichen, werden umgesetzt.
Auf einer Fläche von 2.000 Hektar sollen in Äthiopien, Madagaskar und Togo Aufforstungsmaßnahmen realisiert werden. Diese verbessern die Biomasse und verringern die Erosion – welches sich positiv auf die Biodiversität und Wasserverfügbarkeit auswirkt.
Weiterhin soll das Einkommen aus der Nutzung von Wäldern und baumreichen, produktiven Landschaften bei 1.700 Haushalten in den Regionen um durchschnittlich 10 Prozent gesteigert werden. Frauen und junge Menschen sollen besonders unterstützt werden.


 





 
