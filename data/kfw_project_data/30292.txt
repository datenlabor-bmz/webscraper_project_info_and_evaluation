Nachhaltige Finanzierung des nationalen Schutzgebietssystems in Peru

Die Finanzierung der öffentlichen Verwaltung wurde in Peru vor einigen Jahren auf eine Ergebniserreichung hin ausgerichtet. Im Bereich Naturschutz wurde hierfür das Haushaltsprogramm 057 (Programa Presupuestal 057) aufgestellt. Das Haushaltsprogramm enthält eine Zielmatrix mit Indikatoren für die wesentlichen Hauptaufgaben der peruanischen Schutzgebietsbehörde SERNANP. Das hier vorliegende FZ-Modul nutzt die peruanische Logik des Haushaltsprogramms für eine ergebnisbasierte Finanzierung.
Ziel des FZ-Vorhabens ist es, einen Beitrag zu einem effizienten und wirkungsvollen Management der ausgewählten Schutzgebiete im nördlichen Amazonasgebiet zu leisten. Damit soll der Erhalt der biodiversen Ökosysteme und Ihrer Dienstleistungen gefördert werden.

Das Vorhaben orientiert sich an zwei der fünf Hauptaufgaben des Haushaltsprogramms 057, welchen eine besonders hohe Bedeutung für die Amazonasregion zukommt: "Kontrolle und Überwachung" sowie "Partizipative Mechanismen". Die ersten beiden Komponenten des FZ-Vorhabens entsprechen den beiden Hauptaufgaben aus dem Haushaltsprogramm. Die Zielindikatoren wurden aus dem Haushaltsprogramm 057 übernommen und ambitioniertere jährliche Zielwerte vereinbart. Die Erreichung dieser ambitionierteren Zielwerte ist Voraussetzung für eine Auszahlung der entsprechenden FZ-Mittel. Darüber hinaus wurde eine Komponente vereinbart, welche die Effizienz der Arbeit von SERNANP sowie der Regionalregierung von Loreto auf Systemebene fördert. Das Projektmanagement ist die vierte Komponente.

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde der Umwelt und Sozial (U&S) Risikokategorie B+ zugeordnet

Land / Region / Institution Peru Nummer 30292 Schwerpunkt Umwelt und Klima Sektor 41030 - Biodiversität USV-Kategorie B+ Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 20 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner REPUBLIK PERU Zuständige Abteilung Lateinamerika und Karibik
