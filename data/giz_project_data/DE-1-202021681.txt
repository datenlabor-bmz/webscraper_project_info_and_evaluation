  




 

Prävention von Gewalt gegen Frauen II
Prevention of violence against women
Prevención de la violencia contra las mujeres
Project details
Project number:2020.2168.1
Status:laufendes Projekt
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Viviana Maldonado Posso viviana.maldonado@giz.de
Partner countries: Ecuador, Ecuador
Summary
Objectives:

Der Beitrag staatlicher Akteure, des Privatsektors und der Zivilgesellschaft zur Prävention von Gewalt gegen Frauen ist gestärkt

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Secretaría de Derechos Humanos

Financing organisation:

not available

 
Project value
Total financial commitment:4 400 000 Euro
Financial commitment for this project number:2 000 000 Euro
Cofinancing

not available

 
Previous projects
2017.2196.8Prävention von Gewalt gegen Frauen I
Follow-on projects
not available
 
Term
Entire project:06.07.2018 - 31.12.2023
Actual project:01.06.2021 - 31.12.2023
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt zielt vor allem auf Gleichberechtigung ab
Armutsorientierungnot available
CRS code
Überwindung der Gewalt gegen Frauen und Mädchen

Evaluation
not available
 




 
