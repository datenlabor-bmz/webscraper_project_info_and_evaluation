  




 

Energiesysteme der Zukunft
Energy Systems of the Future
Sistemas de Energia do Futuro
Project details
Project number:2015.2126.9
Status:Projekt beendet
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Markus Exenberger markus.exenberger@giz.de
Partner countries: Brazil, Brazil
Summary
Objectives:

Die Voraussetzungen für die systematische Integration von Erneuerbaren Energien und Energieeffizienz in das brasilianische Energiesystem sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministério de Minas e Energia

Financing organisation:

not available

 
Project value
Total financial commitment:25 870 000 Euro
Financial commitment for this project number:4 829 844 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2017.2215.6Energiesysteme der Zukunft II
 
Term
Entire project:09.12.2015 - 31.12.2025
Actual project:09.12.2015 - 22.07.2019
other participants
Arge Lahmeyer International GmbH-
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Energiepolitik und -verwaltung

Evaluation
not available
 




 
