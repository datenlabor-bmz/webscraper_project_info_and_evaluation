"Welttreuhandfonds für Kulturpflanzenvielfalt - Crop Trust"

Kernproblem:
Bereits heute hungern 800 Millionen Menschen. Um die wachsende Weltbevölkerung ernähren zu können, ist die landwirtschaftliche Pflanzenproduktion grundlegend. Gleichzeitig wird der Klimawandel weiter fortschreiten und die Rahmenbedingungen für die pflanzliche Nahrungsproduktion signifikant verändern. Dabei kann die pflanzliche Genvielfalt, durch Züchtung angepasster Sorten, Handlungsspielraum ermöglichen.

Modulziel:
Ziel des Vorhabens ist es, einen Beitrag zur langfristigen Nahrungssicherheit durch den Erhalt und die nachhaltige Nutzung pflanzengenetischer Ressourcen zu leisten. Hierzu wird der bereits bestehende Welttreuhandfonds für Kulturpflanzenvielfalt mit FZ-Mitteln ausgestattet werden.

Wesentliche Outputs:
Aus dem Kapitalfonds generierte Kapitalerträge werden den ausgewählten regionalen/internationalen Saatgutbanken zur Finanzierung der Betriebskosten bereitgestellt.

Zielgruppe:
Zielgruppe im engeren Sinne sind die Nutzer der Saatgutbanken (nationale Forschungseinrichtungen, ZüchterInnen, SaatgutvermehrerInnen). Zielgruppe im weiteren Sinne ist die Weltbevölkerung, insbes. die ländliche Bevölkerung in Entwicklungsländern.

Beitrag zur nationalen Umsetzung der Agenda 2030 (nicht im MV):
Damit tragen die Maßnahmen zum Ziel 2.5: "Genetische Vielfalt bewahren" für nachhaltige Entwicklung und damit zur Umsetzung der Agenda 2030 bei.

Land / Region / Institution Alle Entwicklungsländer Nummer 46605 Schwerpunkt Ernährung und Landwirtschaft Sektor 41030 - Biodiversität Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 10 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner GLOBAL CROP DIVERSITY TRUST (GCDT) Zuständige Abteilung Westafrika
