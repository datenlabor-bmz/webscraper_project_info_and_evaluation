  




 

Sektorvorhaben Agenda 2030 für nachhaltige Entwicklung
2030 Agenda for Sustainable Development
Project details
Project number:2015.2166.5
Status:Projekt beendet
Responsible Organisational unit: G410 Global Policy
Contact person:Dr. Angela Paul angela.paul@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh., ZZZ
Summary
Objectives:

Das Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) spielt eine strategische Rolle bei der Operationalisierung, Umsetzung und Überprüfung der Post 2015 Agenda im internationalen Kontext und in Deutschland.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:37 600 000 Euro
Financial commitment for this project number:5 026 183 Euro
Cofinancing

not available

 
Previous projects
2012.2498.9SV Post-2015-Agenda
Follow-on projects
2019.2265.7Sektorvorhaben Agenda 2030 für nachhaltige Entwicklung
 
Term
Entire project:10.09.2012 - 29.02.2024
Actual project:28.10.2015 - 28.02.2019
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Multisektorale Hilfe

Evaluation
Zentrale Projektevaluierung. Sektorvorhaben Agenda 2030 für nachhaltige Entwicklung. Evaluierungsbericht
 




 
