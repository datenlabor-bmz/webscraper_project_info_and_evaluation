Kommunales Infrastrukturprogramm III

Das FZ-Modulziel des Programms "Kommunale Infrastruktur III" ist die Sicherstellung einer zuverlässigen und hygienisch unbedenklichen Trinkwasserversorgung für die Bevölkerung in ausgewählten Programmstädten Albaniens zu kostendeckenden und sozialverträglichen Preisen sowie die Gewährleistung einer akzeptablen Siedlungshygiene durch die ordnungsgemäße Abwasserentsorgung - bei gesteigerter Leistungsfähigkeit/Kostendeckung der Versorgungsunternehmen.

Das Programm "Kommunale Infrastruktur III" baut auf den etablierten Strukturen und dem Durchführungsansatz der Vorgängerprogramme "Kommunale Infrastruktur I-II" (Kom I-II) auf und beinhaltet Elemente der konzeptionellen Weiterentwicklung.

Auf Wunsch der albanischen Regierung orientieren sich Finanzierung und Förderung der Investitionsmaßnahmen der Programme III und IV an der Priorisierung des nationa-len Masterplans im Wasser- und Abwassersektor.

Das Programm III richtet sich an die Wasserver- und Abwasserentsorgungsunternehmen der Projektstandorte UK Berat-Kucova und UK Librazhd als Projektträger (Project-Executing Agency, PEA) und an das Sektorministerium Ministry of Engery and Infrastructure als Programmträger (Programme Coordination Unit, PCU). Dabei wird mit Librazhd ein neuer Standort aufgenommen, der nicht Teil von Kom I und II war.

Kernelement der Programmkonzeption der "Kommunalen Infrastruktur I - II" ist ein Anreizmechanismus zur Kostendeckung der Versorgungsunternehmen durch Verknüp-fung von Leistungssteigerungen und Investitionsmaßnahmen (sog. "Performance-based approach"). Die Programmimplementierung erfolgt nach Erreichung der individuellen Kostendeckungsziele an den einzelnen Standorten ("Milestone-Konzept").

Die Umsetzung von Kom III erfolgt in einem 2-Phasen-Modell: Im Rahmen einer Programmvorbereitungsphase (Phase 1) werden die Projektstandorte bei der Erstellung von technischen Feasibilitystudien und der Verbesserung ihrer Organisation und Leistungsfähigkeit durch internationale Consultants unterstützt. In der Durchführungsphase (Phase 2) werden die Investitionen mit Consultantunterstützung umgesetzt.

Die Finanzierung des Investitionsprogramms III erfolgt durch einen FZ-Entwicklungskredit (ZV) in Höhe von EUR 15 Mio. sowie durch Zuschussmittel i.H.v. ca. EUR 4 Mio. der Schweizer SECO. SECO hat der deutschen FZ ein Mandat zur Implementierung erteilt. SECO wird so für Kom III und Kom IV insgesamt EUR 11,0 Mio. Zuschüsse zur Verfügung stellen.
Das Vorhaben steht auch in engem Zusammenhang mit der Begleitmaßnahme des Programms "Kommunale Infrastruktur IV", aus der ausgewählte Versorgungsunternehmen bei der Verbesserung ihrer betriebswirtschaftlichen Leistungsfähigkeit unterstützt werden.

Land / Region / Institution Albanien Nummer 27626 Schwerpunkt Wasser und Abfall Sektor 14020 - Wasser-, Sanitärver. und Abwassermanagement Finanzierungsinstrument Entwicklungskredit Weitere Geber - Deutscher Finanzierungsbeitrag 15 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner MINISTRY OF TRANSPORT AND INFRASTRUCTURE Zuständige Abteilung Südosteuropa und Türkei
