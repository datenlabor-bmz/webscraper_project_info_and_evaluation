  




 

Breitenwirksame Wachstums- und Beschäftigungsförderung, Phase III
Pro-Poor Growth and Promotion of Employment in Nigeria
Project details
Project number:2016.2120.0
Status:laufendes Projekt
Responsible Organisational unit: 1600 Westafrika 2, Madagaskar
Contact person:Markus Wauschkuhn markus.wauschkuhn@giz.de
Partner countries: Nigeria, Nigeria
Summary
Objectives:

Die Beschäftigungs- und Einkommenssituation von Kleinst-, kleinen und mittleren Unternehmen (KKMU) in ausgewählten Sektoren ist verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Federal Ministry for Budget and National Planning

Financing organisation:

not available

 
Project value
Total financial commitment:106 182 151 Euro
Financial commitment for this project number:57 320 000 Euro
Cofinancing

Europäische Union (EU): 9 000 000Euro


 
Previous projects
2012.2208.2Breitenwirksame Wachstums- und Beschäftigungsförderung
Follow-on projects
not available
 
Term
Entire project:11.03.2011 - 30.09.2023
Actual project:01.04.2017 - 30.09.2023
other participants
ARGE GOPA Worldwide Consultants GmbH-
Arge AFC / GOPA
Arge AFC Agriculture and Finance
Arge GOPA Woldwide Consultants-GOPA-
Arge GOPA-AFC
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Geschäftspolitik und -verwaltung

Evaluation
not available
 
Project description (DE)

Ausgangssituation

Armut und Unterbeschäftigung kennzeichnen die wirtschaftliche und soziale Situation Nigerias. 44 Prozent der arbeitsfähigen Bevölkerung sind entweder arbeitslos oder unterbeschäftigt. 60 Prozent von ihnen sind junge Menschen zwischen 15 und 29 Jahren. 84 Prozent der Beschäftigten in Nigeria arbeiten in zumeist privaten Kleinbetrieben, die nicht optimal in die Wertschöpfungsketten integriert sind. Hohe Regulierungskosten und eingeschränkte Finanzierungsmöglichkeiten hemmen deren Wachstum. Mikrofinanzbanken spielen noch keine wichtige Rolle bei der Finanzierung von Kleinbetrieben. Die hohen Betriebskosten und das mangelhafte Risikomanagement der Banken verteuern die Kredite, so dass sie für viele Kleinbetriebe unwirtschaftlich sind.

Ziel

Die Beschäftigungs- und Einkommenssituation von Kleinbetrieben in ausgewählten Sektoren ist verbessert.

Vorgehensweise

Das Vorhaben und seine Partner unterstützen die Entwicklung von Kleinst-, Klein- und mittleren Unternehmen (KKMU). Es setzt in drei Bereichen an: die Verbesserung des Zugangs zu Finanzdienstleistungen, die Verbesserung von rechtlichen Rahmenbedingungen, die Erhöhung der unternehmerischen Kompetenzen und die Stärkung von Dienstleistern (zum Beispiel Beratungsstellen) für KKMU. Im Mittelpunkt stehen dabei die Wirtschaftszweige Landwirtschaft (Kartoffeln, Maniok, Reis, Tomaten, Chili und Ingwer), Bekleidung, Leder und sozialer Wohnungsbau. Das Vorhaben führt Aktivitäten in den nördlichen Bundesstaaten Kaduna, Kano, Niger und Plateau sowie in den südlichen Bundesstaaten Abia, Lagos, Ogun und Oyo durch.
Für einen besseren Zugang zu Finanzdienstleistungen arbeitet das Vorhaben mit der nigerianischen Zentralbank, Mikrofinanzbanken und Trainingsanbietern zusammen. Es berät 15 Mikrofinanzbanken, unterstützt die Fortbildung ihrer Mitarbeiter*innen und führt Maβnahmen zur Verbesserung der finanzwirtschaftlichen Kenntnisse der Bevölkerung durch. Es unterstützt staatliche, private und zivilgesellschaftliche Organisationen darin, ein nachhaltiges Trainingsangebot zu entwickeln.
Für die Verbesserung der Rahmenbedingungen von KKMU führt das Vorhaben öffentlich-private Dialoge durch und berät staatliche Stellen zu Themen wie Registrierung, Landerwerb, Baugenehmigung und Besteuerung. Dienstleister für KKMU werden beim Aufbau von Beratungsstellen unterstützt.
Für die Entwicklung von unternehmerischen Kompetenzen arbeitet das Vorhaben mit Trainingsanbietern zusammen, um verschiedene Fortbildungskurse, wie etwa Gründerkurse und einen 6-monatigen „Training & Coaching Loop", durchzuführen und dauerhaft anzubieten. In Zusammenarbeit mit Sekundarschulen fördert das Vorhaben die berufliche Orientierung und die unternehmerischen Fähigkeiten von Jugendlichen.
Das Vorhaben wird von der Europäischen Gemeinschaft kofinanziert und seine Durchführung wird von den Beratungsfirmen GOPA Consultants und AFC Agriculture and Finance Consultants unterstützt.

Wirkungen

Von 2016 bis 2018 wuchs das Kreditportfolio der geförderten 15 Mikrofinanzbanken um 13 Prozent auf circa 7,6 Millionen Euro. Ihr Einkommen wuchs um 47 Prozent und die Anzahl ihrer Kreditnehmer*innen um 31 Prozent auf 59.352. 59 Prozent der Kreditnehmer waren Frauen.
Durch die seit 2017 mit Hilfe des Vorhabens durchgeführten Trainingskurse verbesserten bisher 69.827 Menschen ihre finanzwirtschaftlichen Kenntnisse. Zudem gelang es, die finanzwirtschaftliche Grundbildung in Schullehrpläne zu integrieren.
Der Aufbau von 37 Gruppen zur Interessensvertretung und Lobbyarbeit sowie von acht Schiedsstellen zur Beilegung von Rechtsstreitigkeiten trug zur Verbesserung des Investitionsklimas und der Rahmenbedingungen von KKMU bei. Auf Basis neuer bundesstaatlicher Gesetze zur Steuerharmonisierung konnten 49 von 61 unerlaubten Gebühren abgeschafft und somit die Kosten der Kleinbetriebe gesenkt werden. Die zum Landerwerb erforderliche Zeit wurde von drei Jahre auf sechs Wochen reduziert. Die Zeit zum Erhalt einer Baugenehmigung wurde halbiert.
Von den 346 Männern und Frauen (50 Prozent), die an Gründungskursen teilnahmen, haben bisher ein Drittel den Geschäftsbetrieb aufgenommen. 32 neu registrierte Unternehmen haben 131 zusätzliche Jobs geschaffen. Von den 562 Unternehmer*innen (30 Prozent Frauen), die am sechsmonatigen „Training & Coaching Loop" teilnahmen, haben bisher 53 Prozent ihre Unternehmen formalisiert. 3.173 Nigerianer*innen, darunter 402 rückkehrende Migrant*innen, haben in anderen Trainingskursen ihre unternehmerischen Kompetenzen weitergebildet. Zudem haben 14.859 Schüler*innen (56 Prozent Mädchen) unternehmerische Fähigkeiten in Arbeitsgemeinschaften von 232 Sekundarschulen entwickelt.
In den geförderten landwirtschaftlichen Wertschöpfungsketten wurden 5.366 neue Jobs geschaffen. Zwölf neue Kartoffelsorten wurden eingeführt und 5.200 Kartoffelbäuerinnen und -bauern nahmen an Trainingskursen teil. Dies resultierte in Produktivitätssteigerungen und das durchschnittliche Einkommen der Kartoffelbäuerinnen und -bauern erhöhte sich um 50 Prozent.
 

 
Project description (EN)

Context

Poverty and underemployment characterise Nigeria’s current economic and social situation. 44 per cent of the potential working population is either unemployed or underemployed. 60 per cent of them are young people between the ages of 15 and 29. 84 per cent of those employed in Nigeria work in mostly private small businesses that are not optimally integrated into the value chains. High regulatory costs and limited financing opportunities hinder their growth. Microfinance banks have yet to play an important role in financing small businesses. The high operating costs and inadequate risk management in banking make loans more expensive, rendering them uneconomical for many small businesses.

Objective

The employment and income situation of small enterprises in selected sectors is improved.

Approach

The project and its partners support the development of micro, small and medium-sized enterprises (MSMEs). It focuses on three areas: improving access to financial services, improving the legal framework, boosting entrepreneurial skills and strengthening service providers (e.g. advice centres) for MSMEs. The focus here is on the agriculture (potatoes, manioc, rice, tomatoes, chilli and ginger), clothing, leather goods and social housing construction sectors. The project carries out activities in the northern states of Kaduna, Kano, Niger and Plateau along with the southern states of Abia, Lagos, Ogun and Oyo.
In order to improve access to financial services, the project is working with the Central Bank of Nigeria, microfinance banks and training service providers. It advises 15 microfinance banks, supports staff training, and implements measures to improve the population’s financial literacy. It supports government, private and civil society organisations in developing sustainable training courses.
To improve the general conditions for MSMEs, the project encourages public-private dialogue formats and advises government agencies on issues such as registration, land acquisition, building permits and taxation. It supports MSME service providers in setting up advice centres.
To boost entrepreneurial skills, the project is working with training providers to run various training courses such as start-up courses and a six-month Training and Coaching Loop, and to offer these on a permanent basis. The project is also working with secondary schools to support young people with careers guidance and entrepreneurial skills.
The project is cofinanced by the European Community, and the consulting firms GOPA Consultants and AFC Agriculture and Finance Consultants support its implementation. 

Results

From 2016 to 2018, the credit portfolio of the 15 microfinance banks supported by the project grew by 13 per cent to approximately EUR 7.6 million. Their income increased by 47 per cent and the number of borrowers rose by 31 per cent to 59,352. The proportion of women borrowers was 59 per cent.
To date, 69,827 people have improved their financial literacy through training courses that have been held with the project’s support since 2017. Basic financial literacy has also been incorporated into school curricula.
The investment climate and conditions for MSMEs improved thanks in part to the establishment of 37 groups responsible for representing interests and lobbying along with eight arbitration bodies for settling legal disputes. Based on new federal legislation to harmonise taxes, 49 out of 61 unauthorised fees have been abolished, thus reducing costs for small businesses. The time required for land acquisition has been reduced from three years to six weeks. The time needed to obtain a building permit has been halved.
Of the 346 men and women (50 per cent) who participated in start-up courses, one third have started business operations. A total of 32 newly registered companies have created 131 additional jobs. Of the 562 entrepreneurs (30 per cent women) who participated in the six-month Training and Coaching Loop, 53 per cent have formalised their companies. In other training courses, 3,173 Nigerians, of whom 402 are returning migrants, have improved their business skills. In addition, 14,859 school pupils (56 per cent girls) boosted their entrepreneurial skills in working groups involving 232 secondary schools.
In the agricultural value chains supported, 5,366 new jobs were created. Twelve new potato varieties were introduced and 5,200 potato farmers attended training courses. This resulted in productivity gains and the average income of the potato farmers increased by 50 per cent.
 





 
