  




 

Kapazitätsentwicklung durch städtische Infrastrukturmaßnahmen
Capacity Building trough Urban Infrastructure Development
Project details
Project number:2016.2168.9
Status:laufendes Projekt
Responsible Organisational unit: 3600 Nordafrika
Contact person:Francois Menguele francois.menguele@giz.de
Partner countries: Egypt, Egypt
Summary
Objectives:

Die Lebensbedingungen in ausgewählten informellen Gebieten mit hoher Migrationsrelevanz sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Housing, Utilities and Urban Communities

Financing organisation:

not available

 
Project value
Total financial commitment:22 500 000 Euro
Financial commitment for this project number:22 500 000 Euro
Cofinancing

Europäische Union (EU): 17 000 000Euro


 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:03.08.2019 - 31.12.2024
Actual project:01.03.2020 - 31.12.2024
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Stadtentwicklung und -verwaltung

Evaluation
not available
 




 
