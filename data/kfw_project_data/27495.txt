Kleinbewässerung im Großraum West

Kernproblem
Burkina Faso sieht sich bei der dauerhaften Sicherung seiner Ernährungsbasis einer dreifachen Problemlage gegenüber: einer steigenden Nahrungsmittelnachfrage aufgrund des nach wie vor hohen Bevölkerungswachstums, einer schrumpfenden Verfügbarkeit landwirtschaftlicher Nutzflächen aufgrund fortschreitender Bodendegradierung und einer hieraus resultierenden hohen Abhängigkeit von importierten Lebensmitteln bei gleichzeitiger Erwartung langfristig steigender und volatiler Weltmarktpreise für Grundnahrungsmittel. Hieraus leiten sich für den Agrarsektor konkrete Handlungsbedarfe ab, die von staatlicher Seite wie von privaten Akteuren durch entsprechende Investitionen in Infrastruktur und Ausbildung zu adressieren sind.

Modulziel
Die Bevölkerung in der Umgebung der Talauen und Bewässerungsperimeter verfügt über Einkommensmöglichkeiten in der landwirtschaftlichen Produktion, Verarbeitung und Vermarktung und über höhere Ernährungssicherheit.

Wesentliche Outputs
Finanziert werden sollen die Planung und Umsetzung von Bewässerungsmaßnahmen in Talauen sowie die Realisierung von Infrastruktur zur besseren Marktanbindung der beteiligten Dörfer. Darüber hinaus werden die Bauern bei Nutzung und Instandhaltung der Bewässerungsanlagen, bei der Vermarktung ihrer Produktion sowie bei der Inanspruchnahme einfacher Finanzierungsinstrumente begleitet und beraten.

Zielgruppe
Die ländliche Bevölkerung in der Umgebung der Talauen in der Region Sud-Ouest und der Provinz Sissili. Produzenten / Produzentinnen und Verarbeiter / Verarbeiterinnen sind direkt begünstigt, darunter mehr als die Hälfte Frauen. Indirekt profitieren außerdem Reis- und Gemüsehändler, Reismühlenbetreiber und Personen, die den geernteten Reis zu Parboiled-Reis weiterverarbeiten, durch eine Vergrößerung des lokal verfügbaren Angebots.

Beitrag zur nationalen Umsetzung der Agenda 2030
1. Armut in jeder Form und überall dauerhaft beenden
2. Hunger beenden und Ernährungssicherheit schaffen

Land / Region / Institution Burkina Faso Nummer 27495 Schwerpunkt Ernährung und Landwirtschaft Sektor 31120 - Landwirtschaftsentwicklung USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber Eigenbeitrag d. Landes/Own contribution of country Deutscher Finanzierungsbeitrag 11 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner MINISTERE DE L'AGRICULTURE RES. ANIMALES ET HALIEUTIQUES Zuständige Abteilung Westafrika
