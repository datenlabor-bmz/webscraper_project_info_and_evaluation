Schutz und nachhaltiges Management in Indigenengebieten (FUNAI)

Ziel des Vorhabens "Schutz und nachhaltiges Management in Indigenengebieten" ist die Verbesserung von Schutz und Management in ausgewählten Terras Indígenas (TIs) Amazoniens.
Als Empfänger des Finanzierungsbeitrags ist ein Finanzagent vorgesehen. Der Finanzagent wird den Projektträger, die brasilianische Indigenenbehörde FUNAI (Fundação Nacional do Índio), bei der Umsetzung des Vorhabens unterstützen.
Direkte Zielgruppen des Vorhabens sind i) die indigene Bevölkerung in den ausgewählten TIs sowie ii) die dezentralen Einheiten der FUNAI in diesen Gebieten. Von der Stärkung der dezentralen Einheiten der FUNAI profitieren indirekt auch weitere indigene Bevölkerungsgruppen und ihre Organisationen sowie der Hauptsitz der FUNAI in Brasília.
Die Gesamtkosten des Vorhabens belaufen sich auf 8,8 Mio. EUR. Sie teilen sich in 8 Mio. FZ-Finanzierungsbeitrag und 0,8 Mio. EUR Eigenbeitrag der FUNAI auf.

Land / Region / Institution Brasilien Nummer 24690 Schwerpunkt Umwelt und Klima Sektor 41040 - Gebietsschutz Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 8 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner FUNDACAO NACIONAL DO INDIO Zuständige Abteilung Lateinamerika und Karibik
