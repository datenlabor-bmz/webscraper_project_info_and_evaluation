Förderung klimafreundlicher Biogastechnologien

Bei der zumeist anaeroben Abwasserbehandlung in Brasilien entstehen u.a. methanhaltige Klärgase, die zum überwiegenden Teil als Treibhausgase in die Atmosphäre gelangen und damit zum Klimawandel beitragen. An Kläranlagen wird das Potenzial zur Energiegewinnung aus Biogas in geringem Umfang genutzt. Zudem werden Abwässer nicht vollständig und gemäß vorhandener Umweltvorschriften behandelt, was zur Verunreinigung der Umwelt beiträgt.

Im Rahmen des Vorhabens mit dem Wasserversorgungsunternehmen (WVU) SANEPAR sind investive Maßnahmen für die Verbesserung und den Ausbau von insgesamt bis zu 11 Kläranlagen vorgesehen. Die Reinigungsleistung der Kläranlagen wird dabei streng an bestehenden Umweltrichtlinien ausgerichtet. Darüber hinaus soll das im Klärprozess entstehende Biogas umweltgerecht gefasst und weitestgehend energetisch verwertet werden. Für eine erfolgreiche Durchführung des Vorhabens wird SANEPAR bei der Planung und Umsetzung durch Beratungsleistungen unterstützt.

Die Kosten für die Maßnahmen im Rahmen des Programms mit SANEPAR belaufen sich auf umgerechnet 75 Mio. EUR und werden zu zwei Dritteln (50 Mio. EUR) aus Mitteln der FZ (ZV-Darlehen) und zu einem Drittel aus Eigenmittel von SANEPAR (25 Mio. EUR) finanziert.

Nach erfolgten Investitionen und Betriebsaufnahme der Kläranlagen sollen jährlich 10 tausend Tonnen CO2eq weniger an Treibhausgasen (THG) ausgestoßen und 4 GWh elektrische und thermische Energie genutzt werden.

Land / Region / Institution Brasilien Nummer 29734 Schwerpunkt Energie Sektor 23270 - Biogas-/Biomasse (Strom u. Wärme) USV-Kategorie B Finanzierungsinstrument Entwicklungskredit Weitere Geber Eigenbeitrag d. Landes/Own contribution of country Deutscher Finanzierungsbeitrag 50 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner COMPANHIA DE SANEAMENTO DO PARANÁ Zuständige Abteilung Lateinamerika und Karibik
