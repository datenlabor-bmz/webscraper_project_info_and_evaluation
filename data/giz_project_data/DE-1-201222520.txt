  




 

Regionale Maßnahmen - Afrika südlich der Sahara (II)
Human Capacity Development (HCD)-Programme Africa
Project details
Project number:2012.2252.0
Status:Projekt beendet
Responsible Organisational unit: 1700 Afrika Überregional und Horn von Afrika
Contact person:Sabine Diallo sabine.diallo@giz.de
Partner countries: AFRICA, South Africa
Summary
Objectives:

Umsetzung der genehmigten Human Capacity Development (HCD)-Maßnahmen in Afrika bis zum Jahr 2014.

Client:

BMZ

Project partner:

Partnerorganisationen der deutschen politischen Stiftungen und kirchlichen Hilfswerke vor Ort

Financing organisation:

not available

 
Project value
Total financial commitment:13 374 089 Euro
Financial commitment for this project number:13 374 089 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:28.11.2011 - 30.06.2015
Actual project:28.11.2011 - 30.06.2015
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Fortbildung von Fach- und Führungskräften

Evaluation
not available
 




 
