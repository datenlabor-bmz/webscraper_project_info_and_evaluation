  




 

Betreiberplattform zur Stärkung von Partnerschaften kommunaler Unternehmen im Wassersektor weltweit
Project details
Project number:2019.2156.8
Status:laufendes Projekt
Responsible Organisational unit: 3930 Region Ost
Contact person:Heiko Heidemann heiko.heidemann@giz.de
Partner countries: Germany, Jordan, Tanzania, Ukraine, South Africa, Zambia
Summary
Objectives:

Der Zugang von Betreibern aus Partnerländern zu praxiserprobtem Erfahrungswissen sowie dem fachlichen und institutionellen Know-how deutscher kommunaler Wasserbetreiber hat sich verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:8 301 075 Euro
Financial commitment for this project number:8 301 075 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:18.06.2019 - 30.06.2024
Actual project:01.07.2019 - 30.06.2024
other participants
Engagement Global gGmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Wasser- und Sanitär-versorgung und Abwassermgt, große S

Evaluation
not available
 




 
