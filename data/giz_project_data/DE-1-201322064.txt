  




 

Inklusive und ökologische Wirtschaftsförderung in ausgewählten ländlichen Regionen in Usbekistan
Support to Sustainable Economic Development in selected regions of Uzbekistan
Project details
Project number:2013.2206.4
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Beate Schoreit beate.schoreit@giz.de
Partner countries: Uzbekistan, Uzbekistan
Summary
Objectives:

Die Entwicklung und das ökologische Wachstum kleinerer und mittelständischer Betriebe sowie die Einkommens- und Beschäftigungsmöglichkeiten benachteiligter ländlicher Bevölkerungsgruppen in den ausgewählten Regionen und Wertschöpfungsketten (WSK) sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Wirtschaftsministerium der Republik Usbekistan

Financing organisation:

not available

 
Project value
Total financial commitment:39 125 869 Euro
Financial commitment for this project number:17 328 532 Euro
Cofinancing

Europäische Union (EU): 9 328 532Euro


 
Previous projects
2007.2059.9Schwerpunktprogramm Nachhaltige Wirtschaftsförderung; Komp. Wirtschaftsförderung in Regionen Usbekistans
Follow-on projects
2019.2259.0Unterstützung von Wirtschaftsreformen und einer nachhaltigen Wirtschaftsentwicklung in Regionen Usbekistans
 
Term
Entire project:08.12.2004 - 31.03.2025
Actual project:06.11.2014 - 12.04.2021
other participants
COFAD Beratungsgesellschaft für
mascontour GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Geschäftspolitik und -verwaltung

Evaluation
not available
 
Project description (DE)

Das Vorhaben unterstützt die usbekische Regierung bei der Umsetzung der Maßnahmen zur Förderung beschäftigungswirksamer Wertschöpfungsketten (WSK). Insbesondere sollen die Entwicklung und das ökologische Wachstum kleinerer und mittelständischer Betriebe gefördert und damit Einkommens- und Beschäftigungsmöglichkeiten benachteiligter ländlicher Bevölkerungsgruppen verbessert werden. Das Vorhaben konzentriert sich auf die Regionen Karakalpakstan, Khorezm, Surchandarya, auf die drei Regionen des Ferganatals Namangan, Andijan und Fergana sowie auf die Regionen Djizak, Syrdarya und Kashkadarya.
Das Vorhaben umfasst drei Handlungsfelder: A) Kompetenzentwicklung für eine inklusive, ökologische und marktbasierte Wirtschaftsförderung, B) Wertschöpfungskettenförderung; C) Einführung neuer (ökologischer) Technologien, EU-Agrarstandards sowie Sozial- und Umweltstandards.
In der Kooperation mit ausgewählten Interessensvertretungen der Wirtschaft sowie Fachinstitutionen (Kammern, Verbände, Agrarinstitute) werden Kapazitäten für eine regionale Wirtschaftsentwicklung gestärkt und das Dienstleistungsangebot (Information, Aus- und Weiterbildung, Training, Beratung) weiterentwickelt. Die lokale und regionale Verwaltung sowie politische Entscheidungsträger sind in diesen Prozess eingebunden, so dass nicht nur die Fähigkeiten dieser Akteure im Bereich regionaler/lokaler Wirtschaftsentwicklung gestärkt, sondern auch der Dialog zwischen privaten und öffentlichen Akteuren verbessert wird. Auf der Makro-Ebene arbeitet das Vorhaben mit relevanten Fachministerien, Forschungseinrichtungen sowie nationalen Akademien zusammen. Mittel- bis langfristig werden dadurch die regulativen und institutionellen Rahmenbedingungen für eine inklusive und ökologische Wirtschaftsentwick-lung in den Regionen verbessert. 

 
Project description (EN)

The project is supporting the Uzbek Government in implementing measures to promote job-creating Value Chains (VCs). In particular, the aim is to promote the development and ecological growth of small and medium-sized enterprises, thereby improving the income and employment opportunities of disadvantaged rural groups. The project is focusing on the regions Karakalpakstan, Khorezm, Surchandarya, the three regions of the Fergana Valley, Namangan, Andijan and Fergana, and the regions Djizak, Syrdarya and Kashkadarya.

The project consists of three components: A) Competence development for inclusive, ecological and market-based economic development, B) Value Chain development; C) Introduction of new (ecological) technologies, EU agricultural standards and social and environmental standards.

In cooperation with selected business interest groups and specialised institutions (chambers, associations, agricultural institutes), the capacities for regional economic development are strengthened and the range of services offered (information, advanced education, training, consulting) is expanded. Local and regional administrations and policy-makers are involved in this process, so that not only the capabilities of these actors in the field of regional/local economic development are strengthened, but also the dialogue between private and to public actors. At the macro level, the project cooperates with relevant ministries, research institutions and national academies. In the medium to long term, this will improve the regulatory and institutional framework conditions for inclusive and ecological economic development in the regions. 





 
