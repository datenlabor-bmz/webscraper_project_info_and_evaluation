Unterstützung des nationalen Programms zur nachhaltigen Kleinbewässerungslandwirtschaft II

Kernproblem
Die Situation in Mali, vor allem im Norden, ist bis heute maßgeblich durch die Nachwirkungen der Krise von 2012/2013 geprägt und die zunehmende Invasion islamischer Extremisten. Die Steigerung der landwirtschaftlichen Produktion und die Schaffung von Beschäftigung spielen eine tragende Rolle für die Entwicklung Nord-Malis. Hier setzt das Vorhaben durch den Bau und die Bewirtschaftung von Bewässerungsinfrastruktur an.

Modulziel
Beitrag zur Nutzung des wirtschaftlichen Potentials in der Kleinbewässerung für eine nachhaltige und rentable Landwirtschaft sowie eine erhöhte Nahrungssicherheit und damit zur Stabilität in den jeweiligen Zielregionen (insbesondere Timbuktu, Mopti, Koulikoro und Sikasso).

Wesentliche Outputs
Die Anzahl an Bewässerungsperimetern, neu ausgebauten natürlichen Senken, Gemüsebauperimeter, Lagerhallen und Transportkähne soll erhöht und damit die landwirtschaftlicher Produktion gefördert werden.

Zielgruppe
Direkte Zielgruppe der FZ Maßnahme ist die ländliche Bevölkerung in den Projektregionen Timbuktu, Mopti, Sikasso und Koulikoro, die in absoluter Armut lebt. Die Jugend ist die größte arbeitssuchende Gruppe in Mali und profitiert damit in besonderem Maße von landwirtschaftlichen Investitionen. So auch die Malierinnen, die entscheidend für die Diversifizierung des Haushaltseinkommens und die Ernährung der Familien sind.

Beitrag zur nationalen Umsetzung der Agenda 2030
Die Maßnahmen tragen damit zum Ziel "Den Hunger beenden, Ernährungssicherheit und eine bessere Ernährung erreichen und eine nachhaltige Landwirtschaft fördern" für nachhaltige Entwicklung bei.

Land / Region / Institution Mali Nummer 39888 Schwerpunkt Ernährung und Landwirtschaft Sektor 31140 - Landwirtschaftliche Wasserressourcen USV-Kategorie B+ Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 12 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner MINISTÈRE DU DÉVELOPPEMENT RURAL Zuständige Abteilung Westafrika
