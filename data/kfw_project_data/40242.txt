Energieeffizienz in öffentlichen Gebäuden

Im Rahmen des FZ-Moduls sollen Bestandsgebäude des öffentlichen Sektors in den Gemeinden energetisch rehabilitiert werden. Es besteht massiver Investitionsbedarf, da der Gebäudebestand alt ist und Instandhaltungsmaßnahmen in den letzten Jahrzehnten oftmals vernachlässigt wurden.
Das Ziel der FZ-Maßnahme ist die Verbesserung der Energieeffizienz öffentlicher Gebäude in Bosnien und Herzegowina sowie die Verbesserung der Qualität öffentlicher Dienstleistungen im Bildungssektor inklusive Armutsreduzierung.
Finanziert wird das Vorhaben mit Haushaltsmitteln in Höhe von 19,5 Mio. EUR, wovon bis zu 10 Mio. EUR in der Republika Srpska und bis zu 9,5 Mio. EUR in der Föderation von Bosnien und Herzegowina umgesetzt werden sollen.

Land / Region / Institution Bosnien und Herzegowina Nummer 40242 Schwerpunkt Energie Sektor 23183 - Energieeffizienz (nachfrageseitig) USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 19,5 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner REPUBLIKA SRPSKA, FOED. BOSNIEN-HERZEGOWINA VIA FED. MINISTRY OF FINANCE Zuständige Abteilung Südosteuropa und Türkei
