  




 

Städte-Plattform
Community of Practice for Sustainable Urban Development
Connective Cities
Project details
Project number:2015.6257.8
Status:Projekt beendet
Responsible Organisational unit: 3910 Region West
Contact person:Dr. Manfred Poppe manfred.poppe@giz.de
Partner countries: Internat. Zusammenarbeit mit Regionen für Nachhalt, Georgia, Indonesia, Jordan, Kenya, Lebanon, Mexico, Peru, Philippines, Thailand, Turkey, Tanzania, South Africa
Summary
Objectives:

Kommunale Praktikerinnen aus Städten und Gemeinden weltweit haben lokal angepasste Lösungen für nachhaltige Stadtentwicklung unter Einbeziehung internationaler Expertise.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:13 416 217 Euro
Financial commitment for this project number:4 585 118 Euro
Cofinancing

not available

 
Previous projects
2012.6263.3Community of Practice for Sustainable Urban Development
Follow-on projects
2019.6253.9Internationale Städte-Plattform für nachhaltige Entwicklung
 
Term
Entire project:30.05.2013 - 30.09.2022
Actual project:09.11.2015 - 31.03.2019
other participants
Engagement Global gGmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Stadtentwicklung und -verwaltung

Evaluation
not available
 




 
