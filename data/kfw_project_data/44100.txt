Unterstützung des nationalen Trinkwasser-und Sanitärversorgungsprogramms - Sektorbudgetfinanzierung III (VPT2)

Kernproblem:
Knapp fünf Millionen Burkinabe haben keinen Zugang zu einwandfreiem Trinkwasser und mehr als vierzehn Millionen haben keinen Zugang zu angemessener Sanitärversorgung. Aufgrund von Budgetknappheit ist der Handlungsspielraum der burkinischen Regierung bei der Finanzierung der Verbesserung und Ausweitung der Wasser- und Sanitärversorgungsinfrastruktur eingeschränkt.

Modulziel:
Ziel des FZ-Vorhabens ist die Unterstützung der burkinischen Regierung bei der Umsetzung der nationalen Wasser- und Sanitärstrategie in den Jahren 2021, 2022 und 2023. Das Ziel der Maßnahme (EZ-Programmziel) ist es, zur Verbesserung des Zugangs der Bevölkerung insbesondere in Stadtrandgebieten und stadtnah gelegenen Dörfern zu Trinkwasser- und Sanitärversorgung beizutragen.

Wesentliche Outputs:
Die Finanzierung des deutschen Beitrags in Höhe von 15,6 Mio. EUR erfolgt über drei Jahre auf der Grundlage der Einstiegskriterien für die Gewährung von sektoraler Budgetfinanzierung gemäß Budgethilfekonzept des BMZ und nach Zustimmung durch den Haushaltsausschuss des Deutschen Bundestages. Die Auszahlung erfolgt in drei jährlichen Tranchen auf Basis der Erreichung von festgelegten Zielen im Sektor.

Zielgruppe:
Zielgruppe ist die gesamte Bevölkerung in Burkina Faso in Stadtrandgebieten und stadtnah gelegenen Dörfern.

Beitrag zur nationalen Umsetzung der Agenda 2030 (nicht im MV):
Damit trägt das Vorhaben zu den Zielen 1 "Armut in jeder Form und überall dauerhaft beenden", 3 "Gesundheit für alle Menschen und ihr Wohlergehen fördern", 6 "Verfügbarkeit von Wasser und Sanitärversorgung ermöglichen" für nachhaltige Entwicklung und damit zur Umsetzung der Agenda 2030 bei.

Land / Region / Institution Burkina Faso Nummer 44100 Schwerpunkt Wasser und Abfall Sektor 14020 - Wasser-, Sanitärver. und Abwassermanagement USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 15,6 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner ONEA Zuständige Abteilung Westafrika
