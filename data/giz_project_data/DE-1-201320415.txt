  




 

Programm Stärkung indigener Organisationen in Lateinamerika
Strengthening Indigenous Organizations in Latin America
Fortalecimiento de Organizaciones Indígenas en Latinoamérica
Project details
Project number:2013.2041.5
Status:Projekt beendet
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Dr. Angela Meentzen angela.meentzen@giz.de
Partner countries: AMERICA, Bolivia, Colombia, Ecuador, Peru, Paraguay
Summary
Objectives:

Die Grundlagen für Achtung, Schutz und Gewährleistung der Rechte indigener Völker, vor allem ihrer Territorialrechte, sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

VN Hochkommissariat für Menschenrechte

Financing organisation:

not available

 
Project value
Total financial commitment:14 418 529 Euro
Financial commitment for this project number:4 500 000 Euro
Cofinancing

not available

 
Previous projects
2010.2083.3Programm Stärkung indigener Organisationen in Lateinamerika
Follow-on projects
not available
 
Term
Entire project:31.10.2006 - 11.09.2017
Actual project:23.12.2013 - 11.09.2017
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Menschenrechte

Evaluation
Amerika NA: Stärkung indigener Organisationen in Lateinamerika (PROINDIGENA). Projektevaluierung: Kurzbericht
America supraregional: Strengthening of indigenous organisations in Latin America (PROINDÍGENA). Project evaluation: summary report
 
Project description (DE)

Ausgangssituation
Indigene Völker haben neben den universalen Menschenrechten einen Anspruch auf indigene Rechte. Die Allgemeine Konferenz der Internationalen Arbeitsorganisation (ILO) hat im Jahr 1989 ein „Übereinkommen über eingeborene und in Stämmen lebende Völker in unabhängigen Ländern" („Übereinkommen 169") getroffen. Darin sind unter anderem weitreichende Kollektivrechte wie kulturelle und Territorialrechte, das Recht auf freie, vorherige und informierte Zustimmung bei sie betreffenden Politiken und Programmen und letztlich das Recht auf Selbstbestimmung verankert. Diese Rechte sind im Jahr 2007 mit der Erklärung der Vereinten Nationen über die Rechte der indigenen Völker noch vertieft und erweitert worden.
Die ILO-Konvention 169 wurde zwar von fast allen lateinamerikanischen Ländern ratifiziert, ihre Inhalte wurden teilweise auch in den neuen Verfassungen verankert, ihre systematische Umsetzung stellt viele Länder aber noch vor große Herausforderungen. Es gibt Fortschritte in der rechtlichen Anerkennung indigener Territorien, doch die Umsetzung der staatlichen Unterstützung für die indigene Selbstverwaltung kommt nur schleppend voran.
Auch in Bezug auf die nachhaltige Nutzung natürlicher Ressourcen stehen indigene Territorien unter starkem Druck, da die Staaten der Nutzung von Bodenschätzen Priorität einräumen und mögliche negative Folgen für die Umwelt und die kulturelle Vielfalt nicht berücksichtigen. Die betroffenen indigenen Völker haben oftmals wenige Vorteile von den Bodenschätzen. Zu Konflikten zwischen indigenen Gemeinschaften, Privatunternehmen und öffentlichen Trägern kann es insbesondere dann kommen, wenn der Staat sich die Erteilung von strategischen Konzessionen vorbehält, ohne die Einwände der indigenen Gruppen zu berücksichtigen und ohne die Beteiligung indigener Gruppen an Gewinnen abzustimmen.
Den großen Herausforderungen begegnen die Staaten noch zu selten mit eigenen Politikansätzen und ausreichend spezialisierten Institutionen.
Ziel
Die Grundlagen für Achtung, Schutz und Gewährleistung der Rechte indigener Völker, vor allem ihrer Territorialrechte, sind verbessert.
Vorgehensweise
Das Regionalvorhaben PROINDÍGENA unterstützt die Förderung von Gelegenheiten zum Erfahrungsaustausch auf lokaler, nationaler und regionaler Ebene zwischen verschiedenen indigenen Autoritäten und Fachkräften, staatlichen Vertretern, NRO, Wissenschaftlerinnen und Wissenschaftlern und anderen relevanten Akteuren zum Thema indigene Selbstverwaltung. Das Vorhaben fördert den Vergleich von Normen und Erfahrungen mit ihrer Umsetzung und die Verbreitung von Wissen zum Thema indigener Selbstverwaltung aus regionaler Perspektive.
Fortbildungsangebote für indigene Autoritäten und Fachkräfte im Zusammenhang mit indigener Selbstverwaltung werden ausgebaut. PROINDÍGENA fördert die verstärkte finanzielle Unterstützung aus öffentlichen Mitteln für die Selbstverwaltung indigener Territorien und unterstützt regionale indigene Dachverbände dabei, auf internationale Beschlüsse, die indigene Rechte betreffen, Einfluss zu nehmen. Das Wissensmanagement zu indigenen Rechten innerhalb und außerhalb der deutschen internationalen Zusammenarbeit wird weiterentwickelt.
Ein besonderer Fokus liegt auf der systematischen Beteiligung von indigenen Frauen und Jugendlichen an allen Aktivitäten im Rahmen des Vorhabens.
Wirkung
• Überregionale vergleichende Untersuchungen der rechtlichen Rahmenbedingungen für die Selbstverwaltung indigener Territorien in der Andenregion und des aktuellen Stands der Umsetzung mit konkreten Empfehlungen für Verbesserungen.
• Koordination mit einem Netzwerk von Focalpoints für indigene Rechte in bilateralen Projekten der GIZ in Lateinamerika.
• Aufbau der Zusammenarbeit mit dem UN-Hochkommissariat für Menschenrechte als zukünftiger Counterpart von PROINDIGENA
• Förderung der Erarbeitung einer Frauenagenda der COICA.
• Beratung der COICA und CAOI und ihrer Mitgliedsorganisationen bei der Vorbereitung ihrer Teilnahme an der COP 20 in Lima.
• Verbreitung relevanter Prozesse und Informationen für die Selbstverwaltung indigener Territorien und ihrer Naturressourcen über die Webseite infoindigena.org gemeinsam mit Partnern und anderen interessierten Akteuren.
 

 
Project description (EN)

Context
In addition to universal human rights, indigenous peoples also have a claim to indigenous rights. In 1989, the General Conference of the International Labour Organisation (ILO) agreed on a ‘convention concerning indigenous and tribal peoples in independent countries’ (Convention No. 169). Extensive collective rights, for example cultural and territorial rights, are anchored in this convention along with the right to free, prior and informed consent with respect to policies and programmes that affect them, and also the right of self-determination. In 2007, these rights were extended and broadened further through the declaration of the United Nations on the rights of indigenous peoples.
The ILO No. 169 convention has been ratified by almost all Latin American countries and its contents have also been anchored in some cases in new constitutions; but the systematic implementation of the convention still poses major challenges for many countries. Progress has been made with respect to the legal recognition of indigenous territories, but the implementation of state support for indigenous self-administration is making only slow progress.
Also with respect to the sustainable utilisation of natural resources, indigenous territories find themselves under significant pressure as states give priority to the exploitation of natural resources without considering the possible negative impact on the environment and cultural diversity. The indigenous peoples affected often receive little benefit from natural resources. Conflicts can emerge between communities, private companies and public authorities, particularly where a state reserves for itself the right to distribute strategic concessions without giving due consideration to indigenous groups’ objections and without agreeing to share the resulting profits with them.
Only too rarely do nations use their own policy approaches and sufficiently specialised institutions to address major challenges.
Objective
To improve the foundations for respecting, protecting and ensuring the rights of indigenous peoples, and particularly their territorial rights.
Approach
The PROINDÍGENA regional project supports the promotion of opportunities for sharing experiences on the local, national and regional levels between different indigenous authorities and experts, government representatives, NGOs, scholars and other relevant parties on the topic of indigenous self-administration. The project fosters the comparison of standards and experiences with their implementation and the dissemination of knowledge on the topic of indigenous self-administration from a regional perspective.
Training courses for indigenous authorities and specialists in connection with indigenous self-administration will be expanded. PROINDÍGENA promotes increasing the financial support from public funds for the self-administration of indigenous territories, and it supports regional indigenous umbrella organisations in their efforts to impact international agreements that affect indigenous rights. The knowledge management of indigenous rights both within and outside of German international cooperation activities is to be developed further.
A special focus is placed on the systematic participation of indigenous women and youths in all activities within the scope of the project.
Results achieved so far
• Supra-regional comparative studies on the legal framework for the self-administration of indigenous territories in the Andes region, and the current status of implementation with specific recommendations for improvements;
• Coordination with a network of focal points for indigenous rights in bilateral GIZ projects in Latin America;
• Establishment of cooperation with the UN High Commission for Human Rights as a future counterpart of PROINDÍGENA;
• Promotion of the elaboration of a women's agenda for COICA;
• Advising COICA and CAOI and their member organisations on their preparations for the COP 20 in Lima;
• Dissemination of relevant processes and information for the self-administration of indigenous territories and their natural resources via the infoidigena.org website, together with partners and other interested parties.
 

 
Project description (ES)

Situación de partida
A los pueblos indígenas les corresponden, además de los derechos humanos universales, los derechos indígenas. En el año 1989, la Conferencia General de la Organización Internacional del Trabajo (OIT) adoptó el "Convenio sobre pueblos indígenas y tribales en países independientes" (Convenio 169), en el que están consagrados, entre otros, derechos colectivos de amplio alcance como derechos culturales y territoriales, el derecho al consentimiento previo, libre e informado en relación con políticas y programas que les afectan, y el derecho a la autodeterminación. En 2007, estos derechos fueron reforzados y ampliados adicionalmente con la "Declaración de las Naciones Unidas sobre los derechos de los pueblos indígenas".
El Convenio 169 de la OIT ha sido ratificado por casi todos los países latinoamericanos y su contenido ha sido integrado en parte en las nuevas Constituciones. No obstante, su aplicación sistemática sigue planteando grandes retos a muchos países. Se está progresando en el reconocimiento jurídico de territorios indígenas, pero la implementación del apoyo estatal para la autonomía indígena sólo avanza lentamente.
También en relación con el uso sostenible de los recursos naturales se ejerce una gran presión sobre los territorios indígenas, pues los Estados dan prioridad al aprovechamiento de los recursos minerales, sin considerar posibles consecuencias negativas para el medio ambiente y la diversidad cultural. Muchas veces, los pueblos indígenas afectados se benefician poco de estos recursos. En especial se generan conflictos entre comunidades indígenas, empresas privadas y entidades públicas cuando el Estado se reserva el derecho de adjudicar concesiones estratégicas sin atender a las objeciones de los grupos indígenas y sin concertar la participación de estos grupos en los beneficios.
Estos grandes retos pocas veces son afrontados por los Estados con enfoques de política propios e instituciones debidamente especializadas.
Objetivo
Se han mejorado los fundamentos para el respeto, la protección y la garantía de los derechos de los pueblos indígenas, sobre todo de sus derechos territoriales.
Procedimiento
El programa regional PROINDÍGENA apoya el fomento de oportunidades para el intercambio de experiencias a nivel local, nacional y regional entre diferentes autoridades indígenas y expertos, representantes estatales, organizaciones no gubernamentales, científicas y científicos y otros actores pertinentes en relación con el tema de la autonomía indígena. El programa promueve la comparación de las normas con las experiencias de su implementación, así como la difusión de conocimientos sobre el tema de la autonomía indígena desde una perspectiva regional.
Se desarrollan ofertas de capacitación para autoridades indígenas y expertos sobre autonomía indígena. PROINDÍGENA fomenta un mayor apoyo financiero con fondos públicos a la administración autónoma de los territorios indígenas, y apoya a federaciones indígenas regionales para que influyan en las decisiones internacionales concernientes a los derechos indígenas. Además, mejora la gestión del conocimiento sobre derechos indígenas dentro y fuera de la cooperación internacional alemana.
El programa da especial importancia a la participación sistemática de mujeres y jóvenes indígenas en todas sus actividades.
Resultados logrados hasta la fecha
• Estudios comparativos suprarregionales de las condiciones marco jurídicas para la autonomía de los territorios indígenas en la región andina y del estado actual de la implementación, con recomendaciones concretas para mejoras.
• Coordinación con proyectos bilaterales de la GIZ en América Latina a través de una red de puntos focales (focal points) sobre derechos indígenas.
• Organización de la cooperación con el Alto Comisionado de las Naciones Unidas para los Derechos Humanos como futura contraparte de PROINDÍGENA.
• Fomento de la elaboración de una agenda para la mujer de la Coordinadora de las Organizaciones Indígenas de la Cuenca Amazónica (COICA).
• Asesoramiento a la COICA y la Coordinadora Andina de Organizaciones Indígenas (CAOI) y sus organizaciones miembro en la preparación de su participación en la Conferencia de las Partes 20 (COP 20) en Lima.
• Difusión de procesos e informaciones pertinentes para la administración autónoma de los territorios indígenas y sus recursos naturales a través del sitio web infoindigena.org junto con las contrapartes y otros actores interesados.
 





 
