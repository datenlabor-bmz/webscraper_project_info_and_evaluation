SFD XII Beschäftigungsförderung

Das FZ-Vorhaben SFD XII Beschäftigungsförderung setzt die 2003 begonnene Unterstützung des jemenitischen 'Social Fund for Development' (SFD) fort. Es knüpft direkt an die Vorhaben 'SFD IX und X (ländliche Entwicklung und Beschäftigung)' an. Der SFD finanziert damit landesweit Maßnahmen zur Rehabilitierung und Erweiterung der sozialen und wirtschaftlichen Infrastruktur in Armutsgebieten.

Die Verwendung der FZ-Mittel ist ausschließlich für arbeitsintensive Infrastrukturmaßnahmen des SFD mit einem Lohnanteil von mindestens 60% vorgesehen. Ziel der FZ-Maßnahme ist es, einen Beitrag zur Stabilisierung des Jemen und zur Abfederung der Auswirkungen der gegenwärtigen Krise für arme Bevölkerungsgruppen v.a. in ländlichen Gebieten zu leisten. Durch die armutsorientierte Bereitstellung von Basisinfrastruktur leistet der SFD einen wesentlichen Beitrag zur Linderung der Folgen der politischen Krise und der Auswirkungen der Kriegshandlungen in großen Teilen des Jemens für arme Bevölkerungsgruppen.

Im Rahmen der FZ-Maßnahme wird ein Finanzierungsbeitrag in Höhe von 5,0 Mio. EUR zur Verfügung gestellt.

Land / Region / Institution Jemen Nummer 33864 Schwerpunkt Sonstige Sektor 43010 - Multisektorale Hilfe Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 5 Mio. EUR Status abgeschlossen Auftraggeber BMZ Projektpartner SOCIAL FUND FOR DEVELOPMENT Zuständige Abteilung Nahost
