Libanon-Beschäftigungsförderung durch arbeitsintensive Infrastrukturmaßnahmen (EIIP)

Phase III setzt auf den Erkenntnissen von Phase I und II auf.

Das Vorhaben wird durch die International Labor Organization (ILO) und das United Nations Development Programme (UNDP) umgesetzt.

Während sich die Aktivitäten der Phase I und II auf die bedürftigsten Gemeinden der Gouvernorate Bekaa, Nord-Libanon und Mount Lebanon konzentriert haben, werden in der Phase III die Aktivitäten landesweit durchgeführt werden.

Die Gesamtkosten der Phase III betragen 14 Mio. EUR und werden über einen Zuschuss an den Projektträger ILO finanziert. Phase III soll im Dezember 2018 starten und wird voraussichtlich eine Laufzeit von 18 Monaten haben.

Umwelt- und Sozialverträglichkeit

Das libanesische Umweltschutzgesetz schreibt für alle Vorhaben die Durchführung einer Umweltverträglichkeitsprüfung (Environmental Impact Assessment) vor. Deshalb wird im Vorhaben ein vorhabenspezifisches Schutzkonzept entwickelt und im Rahmen der Durchführung umgesetzt. Über ILO-geführte Trainings der Bauunternehmer und Arbeitnehmer zu Arbeitssicherheit werden entsprechende Risiken angemessen adressiert. Die USVP-Vorprüfung ergab die Kategorie USVP: B. Eine vertiefte Umwelt-/Soziaiverträglichkeits- und Klimaprüfung wird nicht durchgeführt.

Land / Region / Institution Libanon Nummer 38975 Schwerpunkt Friedensentwicklung und Krisenprävention Sektor 16010 - Soziale Sicherung USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 14 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner INT. LABOUR ORGANIZATION REGIONAL OFFICE FOR THE ARAB S Zuständige Abteilung Nahost
