  




 

Transformation der Administration, Stärkung von Innovation (TransformASI)
Transforming administration - Strengthening innovation
Project details
Project number:2015.2115.2
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Philipp Johannsen philipp.johannsen@giz.de
Partner countries: Indonesia, Indonesia
Summary
Objectives:

Das Personalmanagementsystem sowie die Kapazität, kontinuierliche Innovationen im Dienstleistungsbereich vorzunehmen sind in der Öffentlichen Verwaltung verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Staatsministerium für Verwaltungsreformen

Financing organisation:

not available

 
Project value
Total financial commitment:10 256 297 Euro
Financial commitment for this project number:4 250 000 Euro
Cofinancing

not available

 
Previous projects
2013.2120.7Transformation Administration, Stärkung von Innovation (TransformASI)
Follow-on projects
not available
 
Term
Entire project:19.02.2014 - 30.01.2021
Actual project:01.01.2017 - 30.01.2021
other participants
Arge GFA Consultinggroup GmbH-
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Politik und Verwaltung in Bezug auf den öffentl Sektor

Evaluation
not available
 




 
