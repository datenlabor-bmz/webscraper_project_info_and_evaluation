  




 

Programm zur Unterstützung der African Governance Architecture (AGA)
African Governance Architecture (AGA)
Project details
Project number:2013.2234.6
Status:Projekt beendet
Responsible Organisational unit: 1700 Afrika Überregional und Horn von Afrika
Contact person:Sophia Gallina sophia.gallina@giz.de
Partner countries: African Union, Angola, Burkina Faso, Burundi, Benin, Botswana, Dem. Rep. Congo, Centr.Afr.Rep., Republic of the Congo, Côte d'Ivore, Cameroon, Cape Verde, Djibouti, Algeria, Egypt, Eritrea, Ethiopia, Gabon, Ghana, Gambia, Guinea, Equatorial Gui., Guinea-Bissau, Kenya, Comoros, Liberia, Lesotho, Libya, Morocco, Madagascar, Mali, Mauritania, Mauritius, Malawi, Mozambique, Namibia, Niger, Nigeria, Rwanda, Sudan, Saint Helena, Ascension and Tristan da Cunha (V/K), Sierra Leone, Senegal, Somalia, South Sudan, S.Tome,Principe, Eswatini, Chad, Togo, Tunisia, Tanzania, Uganda, South Africa, Zambia, Zimbabwe
Summary
Objectives:

Die Afrikanische Governance Architektur (AGA) fördert effektiv die Umsetzung der Ziele der AU in den Bereichen Demokratie und Menschenrechte.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Afrikanische Union

Financing organisation:

not available

 
Project value
Total financial commitment:29 763 699 Euro
Financial commitment for this project number:13 400 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2016.2063.2Programm zur Unterstützung der African Governance Architecture (AGA) – Phase II
 
Term
Entire project:08.11.2013 - 30.06.2023
Actual project:08.11.2013 - 31.12.2016
other participants
GFA Consulting Group GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Demokratische Teilhabe und Zivilgesellschaft

Evaluation
54 Mitgliedstaaten der Afrikanischen Union (alle afrikanischen Länder mit Ausnahme von Marokko): Programm zur Unterstützung afrikanischen Governance-Architektur (AGA), Afrikanische Union. Projektevaluierung: Kurzbericht
 
Project description (DE)

Ausgangssituation
Gute Regierungsführung und die Einhaltung von Menschenrechten sind Kernvoraussetzungen für friedliches und nachhaltiges Wachstum in Afrika. Sie sind deshalb auch ein Schwerpunkt der Arbeit der Afrikanischen Union und der deutschen Entwicklungszusammenarbeit.

Nationale Regierungen und regionale Gemeinschaften befürworten und verpflichten sich zur Wahrung von Menschenrechten und demokratischer Regierungsführung. Oftmals fallen sie jedoch bei der Umsetzung hinter ihre Versprechen zurück. Nicht zuletzt ist dies mangelnder institutioneller Leistungsfähigkeit geschuldet.

Innerhalb der Afrikanischen Union gibt es eine Vielzahl von Institutionen und Organen, die mit einem Mandat zur Förderung von demokratischer Regierungsführung ausgestattet sind. Diese Akteure koordinieren sich jedoch kaum, Mandate überlappen sich, sodass ein Mehrwert für die Bevölkerung nur selten spürbar wird. Die Umsetzung politischer Entscheidungen, die für die Bürger nachhaltige Veränderungen bedeuten, wird so erschwert.

Die AU-Versammlung der Staats- und Regierungschefs beschloss 2010 den Aufbau einer Afrikanischen Governance-Architektur (African Governance Architecture, AGA). Sie soll auf nationaler und regionaler Ebene für die zahlreichen Initiativen und Prozesse einen Rahmen schaffen und sie miteinander verbinden. Ziel ist es, koordiniert und wirkungsvoll zu demokratischer Regierungsführung, Menschenrechten und Rechtsstaatlichkeit in Afrika beizutragen.

Die AGA hat das Potenzial, zu einer Schnittstelle und einem Motor zu werden für die Umsetzung von Politiken sowie für die Formulierung neuer Programme zu demokratischer Regierungsführung und Menschenrechten. Zurzeit befindet sich die AGA noch im Aufbau und kann ihrer Aufgabe oft noch nicht gerecht werden. Neben Ressourcen mangelt es auch am konstruktiven, planvollen Miteinander der vier wichtigsten AGA-Mitglieder: der Abteilung für politische Angelegenheiten (DPA), der Kommission der Afrikanischen Union, dem Afrikanischen Menschenrechtsgerichtshof (AfCHPR), der Afrikanischen Menschenrechtskommission (ACHPR) und dem Panafrikanischen Parlament (PAP).

Ziel
Die Afrikanische Governance-Architektur (AGA) ist ein von Staaten und Bürgern anerkannter Mechanismus, der zur Harmonisierung und Umsetzung von Demokratie- und Menschenrechtsstandards in den Ländern Afrikas beiträgt.

Vorgehensweise
Die Abteilung für politische Angelegenheiten der AU-Kommission, der Afrikanische Menschenrechtsgerichtshof, die Afrikanische Menschenrechtskommission und das Panafrikanische Parlament bilden das Rückgrat der Afrikanischen Governance-Architektur (AGA). Die vier wichtigsten Mitglieder sind langjährige Partner der GIZ, die mit dem neuen Programm an eine erfolgreiche Zusammenarbeit anknüpfen kann.

Aufbauend auf vertrauensvolle Beziehungen und gemeinsam erzielte Fortschritte wird die GIZ, mithilfe deutscher und internationaler Berater sowie finanzieller Beiträge, ihre Unterstützung weiterführen. Besondere Schwerpunkte sind die Verbesserung der Managementfähigkeiten und institutionellen Kapazitäten sowie die Förderung der Zusammenarbeit der vier AGA-Mitglieder sein. Standorte des Vorhabens sind Addis Abeba in Äthiopien, Arusha in Tansania sowie Midrand in Südafrika und Banjul in Gambia. 





 
