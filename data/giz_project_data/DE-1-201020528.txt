  




 

Schwerpunktprogramm Klima und Energie - TZ-Komponente
South African-German ENergy Programme - SAGEN
Project details
Project number:2010.2052.8
Status:Projekt beendet
Responsible Organisational unit: 1300 Südliches Afrika
Contact person:Dr. Soeren David soeren.david@nama-facility.org
Partner countries: South Africa
Summary
Objectives:

Investitionen in Energieeffizienz und erneuerbarer Energien sind gestiegen

Client:

BMZ

Project partner:

Department of Energy

Financing organisation:

not available

 
Project value
Total financial commitment:54 710 251 Euro
Financial commitment for this project number:12 285 250 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2014.2081.9Schwerpunktprogramm Klima und Energie - SAGEN
 
Term
Entire project:29.06.2011 - 31.12.2024
Actual project:01.10.2011 - 31.12.2014
other participants
Arge DFIC / STEAG /
Prognos AG
energy & meteo systems GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Energieerzeugung, erneuerbare Quellen – verschiedene ..

Evaluation
not available
 




 
