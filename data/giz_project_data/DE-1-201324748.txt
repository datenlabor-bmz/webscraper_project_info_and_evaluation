  




 

Förderung der Freiwilligenarbeit von UN Volunteers
Cooperation with United Nations Volunteers
Project details
Project number:2013.2474.8
Status:Projekt beendet
Responsible Organisational unit: G410 Global Policy
Contact person:Kay Andraschko kay.andraschko@giz.de
Partner countries: Weltweite Massnahmen
Summary
Objectives:

Zusammenarbeit mit UNV fortführen und den erforderlichen finanziellen Beitrag leisten

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:7 494 970 Euro
Financial commitment for this project number:2 129 000 Euro
Cofinancing

not available

 
Previous projects
2012.2495.5Förderung der Freiwilligenarbeit von UN Volunteers
Follow-on projects
2015.2146.7Förderung der Freiwilligenarbeit von UNV
 
Term
Entire project:16.08.2012 - 30.10.2020
Actual project:01.01.2014 - 31.12.2018
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Multisektorale Hilfe

Evaluation
not available
 
Project description (DE)

Ausgangssituation
Deutschland ist der größte bilaterale Geber des United Nations Volunteers Programme (UNV). Das Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) unterstützt UNV seit den 70er Jahren u.a. durch die Vermittlung deutscher Freiwilliger. Grundlage war ein Abkommen zwischen dem Deutschen Entwicklungsdienst (DED) und UNV aus dem Jahr 2003. Pro Jahr wurden ca. 10 deutsche Freiwillige voll finanziert und entsandt. In einer Pilotphase zur Schaffung geeigneter Rahmenbedingungen für die Fortführung der Kooperation unter den neuen Bedingungen nach der Fusion der drei deutschen Entwicklungshilfeorganisationen ergaben sich bereits erste positive Ansätze einer engeren bi- und multilateralen Zusammenarbeit in einigen Ländern.

Ziel
Die Kooperationsbeziehungen zwischen UNV und der GIZ in Deutschland sowie UNV und den anderen UN-Organisationen und Vorhaben der GIZ in den Ländern des Interventionsgebietes sind ausgebaut.

Vorgehensweise
Das Vorhaben unterstützt UNV beim Entsendeprozess von überwiegend deutschen Freiwilligen finanziell und organisatorisch. Es besteht im Wesentlichen aus einem Finanzierungsbeitrag an UNV und einer begleitenden Prozessberatung. Um gute Voraussetzungen für Synergien zwischen bilateraler und multilateraler Entwicklungszusammenarbeit zu schaffen, werden die aus Vorhabenmitteln finanzierten Freiwilligen in Länder entsandt, bei denen anhand eines zu erarbeitenden Strategiepapiers Potential identifiziert wurde.

Wirkungen
Mit einer Weiterführung der strategischen Kooperation mit UNV leistet Deutschland als Sitzstaat der Vereinten Nationen (UN) einen sichtbaren Beitrag zu UNV und zur UN Familie und signalisiert damit Verlässlichkeit. Die Zusammenarbeit zwischen deutscher bilateraler und multilateraler Entwicklungszusammenarbeit wird verstärkt.
 

 
Project description (EN)

Context
Germany is the largest bilateral donor for the United Nations Volunteers Programme (UNV). Since the 1970s the Federal Ministry for Economic Cooperation and Development (BMZ) supports UNV inter alia by placement of volunteers respectivly funding of volunteer assignments. On the basis of an agreement between the Deutscher Entwicklungsdienst (German Development Service, DED) and UNV from the year 2003 ten German UN volunteers have been fully funded each year. In a pilot phase prerequisites were created to pursue and extend the cooperation with UNV under the new conditions following the merger of the three German development agencies. There are first positive signs for closer cooperation between bilateral and multilateral development cooperation in some countries of intervention.

Objectives
The cooperative relationship between UNV and GIZ in Germany as well as between UNV or other United Nations organizations and GIZ projects in the countries of intervention is improved.

Approach
The project provides financial and organizational support to UNV for the recruitment of mainly German volunteers. It primarily consists of a financial contribution to UNV and accompanying process consulting. UN volunteer assignments funded by the project will take place in countries where potential for synergies between bilateral and multilateral development cooperation has been identified based on a strategy paper.

Results
Maintaining the strategic cooperation with UNV, Germany as one of the locations of the United Nations makes a visible contribution to UNV and the UN family and thereby ensures reliability. Synergies between bilateral and multilateral development cooperation are generated.
 





 
