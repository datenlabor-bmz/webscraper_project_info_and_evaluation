Städtische Wasserversorgung Sekundärstädte III (Inv.)

Die mangelhafte Versorgung der Bevölkerung mit hygienisch einwandfreiem Trinkwasser und Sanitäranlagen behindert ganz wesentlich die soziale und wirtschaftliche Entwicklung der Demokratischen Republik Kongo. Die FZ-Maßnahme ist ein offenes Programm und finanziert in den ersten beiden Phasen die Wiederherstellung der Trinkwasserversorgung in bis zu 13 Sekundärstädten in 3 Regionen im Rahmen des Programms "Städtische Wasserversorgung Sekundärstädte". Im Rahmen der dritten Phase (und folgende) werden die Baumaßnahmen in sechs weiteren Sekundärstädten in den Regionen Kasaï und Bandundu und weitere Baumaßnahmen in Kikwit vorbereitet.

Ziel des Vorhabens ist die nachhaltige Versorgung der Zielgruppe mit hygienisch unbedenklichem Trinkwasser und angemessenen Basissanitäranlagen. Damit soll als indirekte Wirkung ein Beitrag zur Verbesserung der Gesundheitssituation der Zielgruppe geleistet werden. Zielgruppe ist unmittelbar die urbane Bevölkerung der unterstützten Programmstädte. Die einzelnen Maßnahmen umfassen Sofortmaßnahmen zur Wiederherstellung einer regelmäßigen Trinkwasserversorgung, eine Anschubfinanzierung für die Betriebsphase, die Wiederinstandsetzung bzw. den Neubau der Wasserversorgungsanlagen, sowie den Bau öffentlicher Basissanitäreinrichtungen.

Im Rahmen der Begleitmaßnahme wird ein besonderer Fokus auf die technische Unterstützung in den Sekundärzentren gelegt und die auf Leistungsverträgen basierende autonome Betriebsverantwortung der Sekundärzentren gestärkt.

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde entsprechend der Nachhaltigkeitsrichtlinie der KfW Entwicklungsbank in die Umwelt- und Sozialrisikokategorie C eingeordnet, da vom Vorhaben voraussichtlich keine oder nur geringe nachteilige Auswirkungen und Risiken auf Umwelt und soziale Belange ausgehen. Es ist daher kein spezifischer Umwelt- und Sozialmanagementplan erforderlich. Der Partner wurde zur Einführung eines Beschwerdemechanismus verpflichtet und muss die KfW unverzüglich über umwelt- und sozialrelevante besondere Vorkommnisse informieren.

Land / Region / Institution Demokratische Republik Kongo Nummer 24375 Schwerpunkt Wasser und Abfall Sektor 14030 - Trinkwasser, Sanitär u Abwasser-Grundl Vers USV-Kategorie C Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 14 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner REGIE DE DISTRIBUTION D'EAU DE LA REPUBLIQUE DU CONGO Zuständige Abteilung Zentralafrika
