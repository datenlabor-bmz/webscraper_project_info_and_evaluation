  




 

Schwerpunktprogramm Gesundheit, Komponente Fortbildung von Ärzten und medizinischem Personal
Advanced Training for Medical Professionals on High-Tech Medical Equipment
Project details
Project number:2013.2205.6
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Dr. Evelina Toteva evelina.toteva@giz.de
Partner countries: Uzbekistan, Uzbekistan
Summary
Objectives:

Im öffentlichen Gesundheitssystem ist die Nutzung moderner Diagnostik und Therapie durch das usbekische Gesundheitspersonal gestiegen.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Gesundheitsministerium der Republik Usbekistan

Financing organisation:

not available

 
Project value
Total financial commitment:16 626 415 Euro
Financial commitment for this project number:5 097 395 Euro
Cofinancing

not available

 
Previous projects
2009.2265.8Schwerpunktprogramm Gesundheit, Komponente Fortbildung von Ärzten und medizinischem Personal
Follow-on projects
2018.2177.6Management medizinischer Hochtechnologie in Usbekistan
 
Term
Entire project:06.05.2011 - 31.03.2023
Actual project:01.09.2014 - 31.03.2019
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Medizinische Aus- und Fortbildung

Evaluation
not available
 
Project description (DE)

Bezeichnung: Fortbildung erfahrener medizinischer und technischer Fachleute in Methoden moderner Hochtechnologiemedizin.
Auftraggeber: Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)
Land: Usbekistan
Politischer Träger: Ministerium für Gesundheit
Gesamtlaufzeit: 2014 bis 2016
Ausgangssituation
Die öffentlichen Ausgaben für das Gesundheitswesen in Usbekistan sind in den letzten Jahren erheblich gestiegen. Seit Anfang 2000 erhöhten sich Investitionen in die Infrastruktur und die medizinische Ausstattung der Gesundheitseinrichtungen um durchschnittlich sieben Prozent pro Jahr. Zahlreiche medizinische Geräte wurden durch die Regierung und im Rahmen geberfinanzierter Projekte erworben. Die neue Ausstattung umfasst moderne Geräte der Hochtechnologiemedizin für bildgebende Verfahren, Labordiagnostik, Anästhesie, Intensivmedizin und Endoskopie. Der effiziente und effektive Einsatz der Geräte wird allerdings durch Mangel an qualifiziertem Personal, schlechtes Management im Vorfeld der Beschaffung, fehlende Vereinbarungen für Wartungsdienstleistungen und unzureichende Logistik beeinträchtigt. Die Folge sind Qualitätseinbußen bei den Gesundheitsdienstleistungen und Verzögerungen bei der Einführung und Anwendung fortschrittlicher Diagnostik und neuer Therapien.
Ziel
Hochtechnologiemedizin wird in ausgewählten klinischen Bereichen effektiv und effizient verwendet.
Vorgehensweise
Das Projekt unterstützt die Qualifizierung von medizinischem und technischem Personal in ausgewählten klinischen Bereichen. Es umfasst folgende Komponenten:
1. Fortbildung des medizinischen Personals in der Anwendung moderner medizinischer Technologie
2. Einführung und Etablierung eines Inventarsystems als zentrales Bestandsverzeichnis für medizinische Ausrüstung in Usbekistan
3. Ausbildung von erfahrenen Technikern und Ingenieuren in planmäßiger vorbeugender Wartung medizinischer Geräte
Wirkung – Was bisher erreicht wurde
Seit August 2012 wurden im Rahmen des Projekts zahlreiche Fort- und Weiterbildungen im In-und Ausland organisiert. Am Republikanischen Wissenschaftlichen Zentrum für Notfallmedizin konnte ein Schulungszentrum für bildgebende Verfahren (Computertomografie/Magnetresonanztomografie / digitales Röntgen) und für Laparoskopie erfolgreich eingerichtet und in Betrieb genommen werden. Das Projekt hat außerdem laparoskopische Trainingsmodelle, weitere Ausbildungshilfen, Arbeitsplatzrechner und Büromöbel zur Verfügung gestellt.
Eine Arbeitsgruppe bestehend aus Mitarbeitern des Instituts für postgraduale medizinische Ausbildung, des republikanischen Perinatalzentrums und des Interdisziplinären Notfallzentrums (RSCEMC) hat ein Konzept zur Sicherung der Nachhaltigkeit der Trainingsmaßnahmen als Teil des Curriculums ausgearbeitet.
Das Projekt unterstützt parallel die Verbesserung der Qualifikation der Lehrkräfte durch enge Kooperation mit deutschen und französischen Universitätskliniken und fachlichen Ausbildungszentren sowie Fortbildungszentren der Medizingerätehersteller Karl Storz und Siemens, deren Trainingszentren in Russland genutzt werden konnten.
2013 und 2014 wurden 296 Chirurgen, 159 Radiologen, 64 Medizintechniker und 123 OP-Schwestern im Rahmen des Projekts geschult.
Das vom Schweizerischen Zentrum für Internationale Gesundheit (SCIH), Dienstleister des Schweizerischen Tropen- und Public Health Instituts (Swiss TPH) entwickelte computergestützte Inventarsystem "open MEDIS", konnte erfolgreich an einer der Pilotinstitutionen des Projekts getestet werden.
Workshops zur Planung und Beschaffung medizinischer Geräte wurden in Zusammenarbeit mit internationalen Instituten wie ECRI in London, Swiss TPH und dem Resah Institut, Frankreich, durchgeführt.
WEITERE INFORMATIONEN
1. Tätigkeitsbericht des Projekts für 2012-2014
2. Bericht über Lebenszykluskosten von Ausrüstungsgegenständen
3.Factsheet 

 
Project description (EN)

Title: Advanced training for medical and technical professionals to work with modern high-technology equipment in Uzbekistan
Commissioned by: German Federal Ministry for Economic Cooperation and Development (BMZ)
Country: Uzbekistan
Lead executing agency: Ministry of Health
Overall term: 2014 to 2016
Context
Public spending on health in Uzbekistan has increased significantly in recent years. Since the beginning of 2000, investments in infrastructure and medical equipment for health facilities have increased by an average of seven per cent each year. Many items of medical equipment have been acquired by the government and through donor-funded projects. The new equipment includes some of the latest high-tech medical devices for imaging, laboratory diagnostics, anaesthetics, intensive care and endoscopy. So far, however, the efficient and effective use of this equipment has been hindered by the lack of qualified personnel and management shortcomings in the procurement processes, including a lack of maintenance contracts and inadequate logistics. This directly affects the quality of the health services and is impeding the introduction of advanced diagnoses and treatments.
Objective
Modern, high-tech medical equipment is used effectively and efficiently in selected clinical areas.
Approach
The project supports the training of medical and technical personnel in a number of clinical areas, and includes the following components.
1. Advanced training for medical staff in the use of modern medical technologies
2. Introduction and establishment of an inventory system to keep a central record of existing medical equipment in Uzbekistan
3. Advanced training for experienced technicians and engineers in systematic, preventive maintenance of medical equipment
Results
As part of the project, numerous training courses have been organised since August 2012, both in Uzbekistan and abroad. A new training centre for medical imaging methods (computerised tomography/magnetic resonance imaging/digital X-ray) and less invasive forms of surgery has been established at the Republican Scientific Centre of Emergency Medical Care. The project has provided training materials, such as laparoscopic mannequins, training aids, desk computers and office furniture.
A working group, consisting of staff from the Tashkent Institute of Postgraduate Medical Education, the Republican Perinatal Centre and the Republican Scientific Centre of Emergency Medical Care, has developed a strategy for ensuring the sustainability of the training measures as part of the curriculum.
At the same time, through close cooperation with German and French teaching hospitals and specialised training centres, the project has supported improvements in the training provided to Uzbek teaching staff. In this respect, collaboration has also taken place with medical device manufacturers, Karl Storz and Siemens, whose training centres in Russia were used.
In 2013 and 2014, as part of the project, nearly 300 specialists in endo-surgery were trained, as well as 159 radiologists, 64 medical technicians and 123 theatre nurses.
The computerised inventory system ‘openMEDIS’, developed by the Swiss Centre for International Health – the service provider of the Swiss Tropical and Public Health Institute (Swiss TPH) – has been introduced and successfully tested at one of the pilot facilities.
Workshops on planning and the procurement of medical equipment have been held in cooperation with international institutes such as ECRI in London, Swiss TPH and the French Resah Institute.

 





 
