Promoting Innovative Financial Inclusion Program

Kernproblem:
Die Covid-19 bremst den weltweiten Handel und wirkt sich negativ auf die Wirtschaftskraft der meisten Länder aus, wie auch auf Indonesien. Dies betrifft vor allem Bevölkerungsgruppen, die vor der Pandemie schon einen erschwerten Zugang zum Finanzsektor hatten.

Ziel:
Unter dem Sektorprogramm (PIFIP) soll die Republik Indonesien bei der Verbesserung der finanziellen Inklusion von Frauen, kleinsten, kleinen und mittleren Unternehmen (KKMU) sowie weiteren bisher benachteiligten Bevölkerungsgruppenunterstützt werden. Hierzu sollen im Rahmen eines Reformprogramms Kredite von ADB und KfW zur Verfügung gestellt werden. Die Durchführung des PIFIP ist an die Umsetzung einer Vielzahl von Einzelreformen geknüpft.

Wesentliche Outputs:
1) Auf- und Ausbau von innovativer digitaler Infrastruktur und Serviceprogrammen zur Steigerung der finanziellen Inklusion;
2) Verbesserter Zugang zu Finanzprodukten für bisher benachteiligte Bevölkerungsgruppen und KKMU;
3) Verbesserte Aufsicht und Verbraucherschutz im Sinne einer "Responsible Financial Inclusion"

Zielgruppe:
Durch das Projekt sollen vor allem Frauen, KKMUs im ländlichen Raum und weitere benachteiligte Bevölkerungsgruppen fördern.

Umwelt- und Sozialverträglichkeit

Das Vorhaben wurde entsprechend der Nachhaltigkeitsrichtlinie der KfW Entwicklungsbank in die Umwelt- und Sozialrisikokategorie C eingeordnet, da vom Vorhaben voraussichtlich keine oder nur geringe nachteilige Auswirkungen und Risiken auf Umwelt und soziale Belange ausgehen. Es ist daher kein spezifischer Umwelt- und Sozialmanagementplan erforderlich. Der Partner wurde zur Einführung eines Beschwerdemechanismus verpflichtet und muss die KfW unverzüglich über umwelt- und sozialrelevante besondere Vorkommnisse informieren.

Land / Region / Institution Indonesien Nummer 47552 Schwerpunkt Wirtschaft und Finanzsektor Sektor 15111 - Management der öffentlichen Finanzen USV-Kategorie C Finanzierungsinstrument Förderkredit Weitere Geber - Deutscher Finanzierungsbeitrag 200 Mio. EUR Status abgeschlossen Auftraggeber BMZ Projektpartner MINISTRY OF FINANCE OF THE REPUBLIC OF INDONESIA Zuständige Abteilung Ostasien und Pazifik
