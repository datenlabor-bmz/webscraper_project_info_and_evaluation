  




 

Förderung der nachhaltigen Entwicklung und des sozialen Zusammenhalts in Lateinamerika und der Karibik
Promoting Sustainable Development and social cohesion in Latin America and the Caribbean: Investing in Regional Public Goods
Fomento del Desarrollo sostenible y de la cohesión social en AL y el Caribe: Inversión en bienes publicos regionales
Project details
Project number:2009.2245.0
Status:Projekt beendet
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Juergen Klenk juergen.klenk@giz.de
Partner countries: L.AMERICA ECLAC
Summary
Objectives:

Regierungen Lateinamerikas haben Handlungsmöglichkeiten für politische Reform- und Verhandlungsprozesse auf nationaler und regionaler Ebene in den Bereichen Klimawandel, Fiskalpolitik zur Verbesserung sozialer Kohäsion und regionaler Kooperration und Integration in Themen Handel Innovationspol sichtbar erweitert.

Client:

BMZ

Project partner:

Economic Commission for Latin America and the Caribbean (ECLAC)

Financing organisation:

not available

 
Project value
Total financial commitment:3 514 597 Euro
Financial commitment for this project number:3 514 597 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:17.08.2010 - 02.12.2013
Actual project:17.08.2010 - 02.12.2013
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Politik und Verwaltung in Bezug auf den öffentl Sektor

Evaluation
not available
 




 
