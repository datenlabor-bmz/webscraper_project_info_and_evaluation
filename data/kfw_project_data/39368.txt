UNDP Libanon: Beschäftigung im ländlichen Raum

Der Bürgerkrieg in Syrien hat massive Fluchtbewegungen in den Libanon verursacht. Der hohe Zustrom stellt eine zusätzliche Belastung für die wirtschaftliche und sozioökomische Situation des Landes dar. Die Spannungen zwischen den Geflüchteten und der Lokalbevölkerung sind dementsprechend hoch, wobei die Konkurrenz um Beschäftigung und Verdienstmöglichkeiten in den Aufnahmegemeinden eine herausgehobene Rolle spielt. Durch die anhaltende Krise besteht hoher Investitionsbedarf. Viele Flüchtlinge aus Syrien sowie die aufnehmenden Gemeinden sind auf nicht absehbare Zeit auf Unterstützung angewiesen.
Ziel der FZ-Maßnahme ist es daher, kurz- und mittelfristige Einkommensmöglichkeiten für Flüchtlinge aus Syrien und bedürftige Libanesen zu schaffen. Im Mittelpunkt des Vorhabens stehen dabei Maßnahmen zur Rehabilitierung bzw. zum Bau von Bewässerungsinfrastruktur (Bewässerungskanäle und Rückhaltebecken). Neben den kurzfristigen Beschäftigungsmöglichkeiten auf den Baustellen soll die Effizienz und Rentabilität der landwirtschaftlichen Produktion in den Projektregionen gesteigert werden, was weitere Beschäftigungseffekte nach sich zieht.

Das Vorhaben wird durch die United Nations Development Programme (UNDP) umgesetzt.

Die Kosten dieser Phase belaufen sich auf 10 Mio. EUR. Die FZ-Mittel werden als Zuschuss bereitgestellt.

Land / Region / Institution Libanon Nummer 39368 Schwerpunkt Friedensentwicklung und Krisenprävention Sektor 73010 - Kurzfristiger Wiederaufbau USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 10 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner UNITED NATIONS DEVELOPMENT PROGRAMME (UNDP) Zuständige Abteilung Nahost
