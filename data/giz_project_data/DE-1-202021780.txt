  




 

Nachhaltige Wirtschafts- und Regionalentwicklung, Beschäftigungsförderung und Berufliche Bildung II
Sustainable Economic and regional development, employment promotion and Vocational Education and Training
Project details
Project number:2020.2178.0
Status:laufendes Projekt
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Dr. Irina Kausch irina.kausch@giz.de
Partner countries: Albania, Albania
Summary
Objectives:

Die Beschäftigungssituation von Arbeitskräften und Selbstständigen, insbesondere junger qualifizierter Menschen, auf dem albanischen Arbeitsmarkt ist verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministerium für Finanzen und Wirtschaft

Financing organisation:

not available

 
Project value
Total financial commitment:43 420 006 Euro
Financial commitment for this project number:19 442 764 Euro
Cofinancing

Europäische Union (EU): 8 069 144Euro


 
Previous projects
2016.2094.7Nachhaltige Wirtschafts- und Regionalentwicklung, Beschäftigungsförderung und Berufliche Bildung
Follow-on projects
not available
 
Term
Entire project:02.12.2016 - 30.09.2026
Actual project:01.04.2022 - 30.09.2026
other participants
ARGE CEFE International-PEM Consult
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Entwicklung von kleinen und mittleren Unternehmen (KMU)

Evaluation
not available
 




 
