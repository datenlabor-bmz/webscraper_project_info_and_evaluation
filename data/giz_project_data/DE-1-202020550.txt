  




 

Multilaterale Entwicklungspolitik und G7-_G20-Entwicklungsagenden
Multilateral Development and G7-G20-Development Agenda Policy Support
Project details
Project number:2020.2055.0
Status:laufendes Projekt
Responsible Organisational unit: G410 Global Policy
Contact person:Marike Ferguson marike.ferguson@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh., Indonesia
Summary
Objectives:

Stärkung der strategischen Ausrichtung, Gestaltung und Verankerung deutscher entwicklungspolitischer Prioritäten in multilateralen Foren und

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

kein

Financing organisation:

not available

 
Project value
Total financial commitment:7 854 290 Euro
Financial commitment for this project number:4 874 520 Euro
Cofinancing

not available

 
Previous projects
2016.2131.7Unterstützung der G7- und G-20 Entwicklungsagenden
Follow-on projects
not available
 
Term
Entire project:20.06.2016 - 31.01.2024
Actual project:01.02.2020 - 31.01.2024
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
Armutsorientierungnot available
CRS code
Multisektorale Hilfe

Evaluation
not available
 
Project description (DE)

Ausgangssituation

In einer zunehmend vernetzten Welt können entwicklungspolitische Ziele immer weniger nur durch nationales Handeln erreicht werden. Das Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) gestaltet daher eine globale nachhaltige Entwicklung in Kooperation mit zahlreichen internationalen Partnern im Rahmen seiner multilateralen Entwicklungspolitik mit. Dazu arbeitet es mit staatlichen und nichtstaatlichen Akteur*innen in und über internationale Organisationen, Foren und Netzwerke zusammen.

Die Gruppe der Sieben (G7, Industrieländer) und die Gruppe der Zwanzig (G20, Industrie- und Schwellenländer) sind als Foren der wichtigsten Industrie- und Schwellenländer dabei von besonderer Bedeutung. Sie vereinen einen großen Teil der Weltwirtschaftsleistung und Weltbevölkerung auf sich und stimmen gemeinsame Positionen zu globalen politischen Fragestellungen ab. Die jährlich wechselnden Präsidentschaften der G7 und G20 übernehmen eine wichtige Aufgabe bei der Ausgestaltung und Umsetzung der jeweiligen Agenden. Jedes Mitgliedsland nutzt seine Präsidentschaft, um eigene Schwerpunkte zu setzen und Themen zu platzieren.

Deutschland tritt dafür ein, dass entwicklungs- und nachhaltigkeitspolitische Ziele wie die Umsetzung der Agenda 2030 und des Pariser Klimaabkommens im G7- und G20-Prozess einen hohen Stellenwert behalten. Insbesondere im Zuge der Vorbereitung auf den deutschen G7-Vorsitz 2022 und während diesem ergeben sich besondere Gestaltungsmöglichkeiten – und Herausforderungen.

Ziele

Das BMZ gestaltet die entwicklungspolitischen Agenden der G7 und G20 sowie weiterer multilateraler Foren und Prozesse mit. An der Agenda 2030 orientierte entwicklungs- und nachhaltigkeitspolitische Ziele sind in den G7 und G20 stärker verankert. Zivilgesellschaftliche Akteur*innen aus Deutschland und dem globalen Süden sind stärker in die Entwicklungsprozesse der G7 und G20 eingebunden.

Vorgehensweise

Das Vorhaben unterstützt das BMZ bei der themenübergreifenden Ausrichtung, Gestaltung und Verankerung deutscher entwicklungspolitischer Prioritäten in den G7 und G20 sowie in weiteren multilateralen Foren und Prozessen. Dies realisiert es über drei Handlungsfelder:

1.) Multilaterale Entwicklungspolitik gestalten

Das Vorhaben berät das BMZ bei der Umsetzung der neuen multilateralen Strategie. Dazu identifiziert es beispielsweise Möglichkeiten zur besseren Verzahnung der bilateralen und multilateralen Entwicklungszusammenarbeit, analysiert Trends in der Entwicklungspolitik und dem globalen Governance-System und ordnet die Positionierung von Schwellenländern strategisch ein. Zudem unterstützt das Vorhaben beim verstärkten Engagement in multilateralen Netzwerken. Es berät darüber hinaus zur Ausgestaltung von Geberkooperationen mit G7-Ländern, die nicht Mitglieder der Europäischen Union (EU) sind.

2.) Entwicklungsagenden der G7 und G20 unterstützen

Das Vorhaben berät das BMZ dazu, die entwicklungspolitischen Erfolge seiner G7- und G20-Vorsitze 2015 und 2017 nachzuhalten, Umsetzungsbemühungen aufrechtzuhalten sowie deutsche Prioritäten in die aktuellen Entwicklungsagenden der G7 und G20 einzubringen. Durch ausgewählte Personalentsendungen an G7-/ G20-Partner sowie relevante internationale Organisationen werden entwicklungs- und nachhaltigkeitspolitische Ziele stärker in den G7 und G20 verankert.
Ab 2021 trägt das Vorhaben dazu bei, das deutsche entwicklungspolitische G7-Programm inhaltlich vorzubereiten und den deutschen G7-Vorsitz 2022 durchzuführen. Dies geschieht in Abstimmung und im Austausch mit anderen relevanten entwicklungspolitischen Akteur*innen.

3.) Legitimation der G7 und G20 fördern

Ergänzend berät das Vorhaben das BMZ dazu, die globalen Agenden der G7 und G20 stärker mit Anliegen lokaler Akteur*innen zu verknüpfen und zivilgesellschaftliche Vertreter*innen aus Deutschland und dem globalen Süden intensiver einzubinden. Dafür entwickelt und führt das Vorhaben Dialogformate durch, erstellt Umfeldbeobachtungen, analysiert Dokumente und wertet diese aus. Zudem unterstützt es ausgewählte Partnerorganisationen aus dem globalen Süden.
 

 
Project description (EN)

Context

In an increasingly networked world, it is becoming less and less possible to achieve development policy objectives through national action alone. The German Federal Ministry for Economic Cooperation and Development (BMZ) is therefore helping to shape sustainable global development in cooperation with numerous international partners as part of its multilateral development policy. To this end, it is working together with governmental and non-governmental actors in and through international organisations, forums and networks.

Against this backdrop, the Group of Seven (G7, industrialised nations) and the Group of Twenty (G20, industrialised nations and emerging economies), as forums for the leading industrialised nations and emerging economies, are of particular significance. They represent a large proportion of the world’s population and economic output and agree on joint positions on global policy issues. The annually rotating presidencies of the G7 and G20 perform an important task in shaping and implementing the respective agendas. Thus, every member country uses its presidency to set its own agenda and policy priorities.

Germany advocates that development and sustainability policy objectives such as the implementation of the 2030 Agenda and the Paris Climate Agreement remain highly important in the G7 and G20 processes. In this context, particular opportunities – and challenges – will arise during the preparations for and in the course of Germany's G7 presidency in 2022.

Objective

BMZ helps to shape the development policy agendas of the G7 and G20 and other multilateral forums and processes. Development and sustainability policy objectives geared towards the 2030 Agenda are mainstreamed to a greater extent in the G7 and G20. Civil society stakeholders from Germany and the global south are involved to a greater extent in the development processes of the G7 and G20.

Approach

The project supports BMZ in aligning, shaping and mainstreaming German development policy priorities in the G7 and G20 and in other multilateral forums and processes. It does this through three fields of activity:

1.) Shaping multilateral development policy

The project advises BMZ on the implementation of the new multilateral strategy. To this end, for example, it identifies possibilities for improving the integration of bilateral and multilateral development cooperation, analyses trends in development policy and the global governance system, and strategically assesses the positioning of emerging economies. The project also supports BMZ in the increased involvement in multilateral networks. In addition, it provides advice with regards to designing donor cooperation arrangements with G7 countries that are not members of the European Union (EU).

2.) Supporting the G7 and G20 development agendas

The project advises BMZ on following up on the development policy successes of its G7 and G20 presidencies in 2015 and 2017, maintaining implementation efforts and incorporating German priorities into the current G7 and G20 development agendas. Through selected seconding of personnel to G7 and G20 partners as well as relevant international organisations, development and sustainability policy objectives are mainstreamed to a greater extent in the G7 and G20. As of 2021, the project will be helping to prepare the German development policy G7 programme. In 2022, it will be supporting the realisation of the German G7 presidency. This will be done in consultation and exchange with other relevant development policy actors.

3.) Promoting the legitimation of the G7 and G20

To supplement the above fields of action, the project advises BMZ on connecting the global agendas of the G7 and G20 to a greater extent with the concerns of local stakeholders and on involving civil society representatives from Germany and the global south more intensively. To this end, the project develops and implements dialogue formats, monitors relevant civil society activities and analyses and evaluates documents. Moreover, it supports selected partner organisations from the global south.
 





 
