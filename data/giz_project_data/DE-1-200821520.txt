  




 

Stärkung der lokalen Selbstverwaltung
Strengthening Local Self-government
Jačanje lokalne samouprave
Project details
Project number:2008.2152.0
Status:Projekt beendet
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Klaus Schmidt klaus.schmidt1@giz.de
Partner countries: Serbia
Summary
Objectives:

Kommunale Planungs- und Managementfähigkeiten für eine effiziente Erbringung von kommunalen Dienstleistungen (Abfall, Wasser/Abwasser und Energie) und für die Schaffung nachhaltiger Infrastruktur sind gestärkt.

Client:

BMZ

Project partner:

Ministerium für Umwelt und Raumplanung

Financing organisation:

not available

 
Project value
Total financial commitment:3 000 000 Euro
Financial commitment for this project number:3 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:06.01.2009 - 08.02.2013
Actual project:06.01.2009 - 08.02.2013
other participants
not available
 
Contact
Project websiteswww.sls.rs
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Dezentralisierung und Förderung subnatio Gebietskörpers

Evaluation
not available
 




 
