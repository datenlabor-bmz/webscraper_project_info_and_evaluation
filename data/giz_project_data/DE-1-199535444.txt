  




 

Studien- und Fachkräftefonds
Studies and Experts Fund
Project details
Project number:1995.3544.4
Status:Projekt beendet
Responsible Organisational unit: 1300 Südliches Afrika
Contact person:Dr. Matthias Rompel matthias.rompel@giz.de
Partner countries: Malawi
Summary
Objectives:

Vorbereitung und Prüfung von Vorhaben der Technischen Zusammenarbeit (TZ), Finanzierung von Studien, Gutachten sowie Durchführung von TZ-Maßnahmen geringen Umfangs.

Client:

BMZ

Project partner:

Ministry of Finance

Financing organisation:

not available

 
Project value
Total financial commitment:13 008 547 Euro
Financial commitment for this project number:4 517 138 Euro
Cofinancing

not available

 
Previous projects
1989.2142.1Studien- und Fachkräftefonds (SFF)
Follow-on projects
2011.3510.2Studien- und Fachkräftefonds
 
Term
Entire project:20.02.1990 - 31.03.2026
Actual project:01.01.1995 - 12.03.2015
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
