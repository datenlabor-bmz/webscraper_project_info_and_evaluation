  




 

Grenzübergreifende Katastrophenvorsorge am Flußlauf des Nigers in Niger und Mali
Cross-border disaster preparedness at the riverbank of Niger in Niger and Mali
Project details
Project number:2013.1803.9
Status:Projekt beendet
Responsible Organisational unit: G220 Frieden und Sicherheit
Contact person:Steffen Mueller steffen.mueller@giz.de
Partner countries: Niger
Summary
Objectives:

Die Bevölkerung und ihre Verwaltungseinheiten entlang des Nigers nutzen die Unterstützung, sich den sich veränderten Risiken anzupassen und sich vor den Überschwemmungen zu schützen

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministère du Plan, de l'Amenagement du Territoire et du Développement Communautaire

Financing organisation:

not available

 
Project value
Total financial commitment:3 499 730 Euro
Financial commitment for this project number:3 499 730 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:19.07.2013 - 15.05.2017
Actual project:19.07.2013 - 15.05.2017
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Multi-Hazard Katastrophenhilfe/Vorbereitung Katast-fall

Evaluation
not available
 
Project description (DE)

Ausgangssituation

Mali und Niger gehören zu den am stärksten von Dürre bedrohten Ländern weltweit. Abseits der großen Flusssysteme werden landwirtschaftliche Tätigkeiten durch ständigen Wassermangel, voranschreitende Desertifikation sowie Bodenerosion zunehmend erschwert.
Nach den Aufzeichnungen der Nigerbeckenbehörde (Autorité du Bassin du Niger, ABN) nimmt die Wassermenge des Flusses Niger tendenziell ab. Gleichzeitig treten zunehmend punktuelle Hochwasser auf, die auf verkürzte jährliche Regenzeiten mit unregelmäßigen Starkregen sowie die Versandung der Flussläufe zurückzuführen sind. Auf malischer Seite ist die Region Gao und in Niger die Region Tillabéri besonders von periodischen Überschwemmungen betroffen. Die überwiegend ländliche Bevölkerung in diesen Regionen ist aufgrund ihrer Armut und des hohen Bevölkerungswachstums besonders anfällig und bisher nicht in der Lage, sich den veränderten Risiken anzupassen. Die Überschwemmungen und ihre Folgen bedrohen zunehmend die Lebensgrundlagen der Bevölkerung.

Ziel

Die Bevölkerung und kommunale Verwaltungseinheiten entlang des Nigers sind in die Lage versetzt, sich den veränderten Risiken anzupassen und sich vor Überschwemmungen zu schützen.

Vorgehensweise

Entsprechend seines ganzheitlichen, inklusiven Ansatzes unterstützt das Vorhaben zunächst die Durchführung von Vulnerabilitätsanalysen in Gemeinden entlang des Flusslaufs. Auf der Grundlage dieser partizipativen Erhebungen werden kommunale Aktionspläne erstellt. Sie enthalten einerseits Einzelmaßnahmen zur Vorbeugung und Abmilderung von Katastrophen – in dem zum Beispiel Ufer befestigt, Dämme angelegt, Erosionsschutzmaßnahmen durchgeführt sowie der Bodenbewuchs verbessert wird. Hinzu kommen Maßnahmen zur Vorbereitung auf den Katastrophenfall. Risikokartierungen sind ein wesentliches Instrument der Analyse. Der Einführung von wirksamen, regional abgestimmten Frühwarnsystemen und Notfallplänen kommt eine besondere Bedeutung zu.
Bei der Beratung und der Durchführung von bedarfsgerechten Maßnahmen konzentriert sich das Vorhaben auf zwei Handlungsfelder:
1. Steigerung der Leistungsfähigkeit aller relevanten staatlichen Institutionen zum Management der Überschwemmungsrisiken
2. Unterstützung der Gemeinden bei der inhaltlich-fachlichen Planung und der Umsetzung von Maßnahmen, die dazu beitragen, Risiken zu reduzieren.

Wirkungen

Nach Auswertung der Hochwasserereignisse 2012 und 2013 wurden 40 überschwemmungsgefährdete Dörfer in zehn Gemeinden identifiziert. Im Rahmen von begleiteten Vulnerabilitätsanalysen erstellten die Dörfer Risikokartierungen und Aktionspläne. Infolge dieser partizipativen Prozesse verstärkte sich sowohl bei den unmittelbar Betroffenen als auch bei den staatlichen Partnern das Interesse für die Gesamtproblematik.
Erste Maßnahmen, die dazu beitragen, die Katastrophenrisiken zu reduzieren, wurden durchgeführt: Eine Flussschwelle wurde gebaut, Ufer- und Furt befestigt, Erosionsschutzmaßnahmen durchgeführt und Dämme ausgebessert. Die Rolle des Bauherrn übernahmen die Partnergemeinden. Behörden des Landwirtschaftsministeriums wurden in die Umsetzung einbezogen. Alle Maßnahmen wurden arbeitsintensiv durchgeführt, sodass die Bevölkerung zusätzliche Einkommen erzielen konnte.
Ein Hochwasser-Frühwarnsystem sowie Notfallpläne liegen in abgestimmter Form vor. Um eine optimale Wirksamkeit zu gewährleisten, setzt das Frühwarnsystem sowohl lokale (Niederschlags- und Pegelmessung) als auch nationale Datenquellen (Wetter- und Abflussvorhersagen) in Wert. Derzeit werden die Gemeinden und Dörfer im Rahmen einer mit der Region Tillabéri vereinbarten Pilotphase unterstützt, diese Instrumente einzuführen.
Alle projektrelevanten Entscheidungen werden von einem regionalen Steuerungskomitee getroffen. Diese Plattform, an der die Partnergemeinden wie auch die übergeordneten Distrikte sowie die Region Tillabéri beteiligt sind, dient dazu ein Bewusstsein für Auswirkungen des Klimawandels zu schaffen. Fortbildungen verstärken diesen für Veränderungsprozesse entscheidenden Aspekt.

 

 
Project description (EN)

Context
Mali and Niger are among the countries most at risk of drought worldwide. Away from the major river systems, constant water shortages, progressive desertification and soil erosion are increasingly hampering agricultural activities.
According to the records kept by the Niger Basin Authority (NBA), the volume of water in the River Niger is decreasing on the whole. However, peak flow levels are reached with increasing frequency due to shorter annual rainy seasons accompanied by irregular heavy rainfall and siltation in the river. Gao Region in Mali and Tillabéri Region in Niger are particularly badly affected by periodic flooding. The predominantly rural population in these regions is poor and growing rapidly. This group is therefore particularly vulnerable and has been unable to adapt to the new risks. Floods and their effects are increasingly threatening the livelihoods of the people in these regions.

Objective
The population and municipal administrative units along the Niger River are able to adapt to the new risks and protect themselves from flooding.

Approach
The project takes a holistic and inclusive approach that begins with providing support for conducting vulnerability assessments in communities located along the river. Municipal action plans are prepared on the basis of these participatory surveys. They include individual measures to prevent and mitigate the impacts of disasters – for instance reinforcing slopes, constructing dams, conducting erosion protection measures, and increasing vegetation levels. Disaster preparedness measures are also part of the plans. Risk mapping is an important analysis instrument. The introduction of effective, regionally coordinated early warning systems and contingency plans is a particular priority.
The advice and needs-based measures provided under the project focus on two main areas:
1. Improving the capability of all relevant government institutions to manage flood risks
2. Supporting municipalities in the technical planning and implementation of risk mitigation measures

Results
On the basis of an analysis of the 2012 and 2013 floods, 40 villages at risk of flooding were identified in ten municipalities. The villages used vulnerability assessments to produce maps for risk analyses and action plans. This participatory process increased interest in the overall problem, not only among the directly affected parties, but also among the governmental partners.
Initial measures that contribute to disaster risk reduction were carried out: a river weir was constructed, riverbanks and fords were reinforced, erosion protection measures were conducted, and the dams were repaired. The partner municipalities commissioned the building work, and Ministry of Agriculture officials were involved in the implementation process. All of the measures were labour-intensive, which generated additional income for the local population.
A flood early warning system and emergency plans have been created and approved. In order to make the system as effective as possible, it utilises both local (rainfall and river level measurements) and national data sources (weather and runoff forecasts). As part of a pilot phase agreed with partners in Tillabéri Region, the project is supporting municipalities and villages in introducing these instruments.
All decision-making of relevance to the project is carried out by a regional steering committee. This platform, which involves stakeholders from the partner municipalities as well as the districts and Tillabéri Region, helps to raise awareness of the impacts of climate change. Training activities are being used to enhance this knowledge, which plays a key role in change processes.
 





 
