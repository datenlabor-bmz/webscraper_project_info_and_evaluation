  




 

Förderung eines klimafreundlichen Strommarktes in der ECOWAS-Region
Promotion of a climate-friendly market in the ECOWAS region (ProCEM)
Promotion d'un marché d'électricité respectueux du climat dans la région CEDEAO (ProMERC)
Project details
Project number:2017.2065.5
Status:Projekt beendet
Responsible Organisational unit: 1600 Westafrika 2, Madagaskar
Contact person:Assani-Massourou Dahouenon mansour.dahouenon@giz.de
Partner countries: ECOWAS, Burkina Faso, Benin, Côte d'Ivore, Cape Verde, Ghana, Gambia, Guinea, Guinea-Bissau, Liberia, Mali, Mauritania, Niger, Nigeria, Sierra Leone, Senegal, Togo
Summary
Objectives:

Die Voraussetzungen für einen klimafreundlichen Strommarkt in der ECOWAS-Region sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

ECOWAS Kommission

Financing organisation:

not available

 
Project value
Total financial commitment:31 650 000 Euro
Financial commitment for this project number:14 726 973 Euro
Cofinancing

Europäische Union (EU): 5 450 000Euro


 
Previous projects
2012.2530.9Förderung eines klimafreundlichen Stromverbundes in Westafrika
Follow-on projects
2021.2053.3Förderung eines klimafreundlichen Strommarktes in der ECOWAS-Region
 
Term
Entire project:08.11.2013 - 31.12.2024
Actual project:29.11.2017 - 30.01.2022
other participants
GOPA International Energy
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Energieerzeugung, erneuerbare Quellen – verschiedene ..

Evaluation
not available
 
Project description (DE)

Ausgangssituation

Die ECOWAS (Economic Community of West African States) ist ein Zusammenschluss von 15 westafrikanischen Staaten: Benin, Burkina Faso, Cabo Verde, Elfenbeinküste, Gambia, Ghana, Guinea, Guinea Bissao, Liberia, Mali, Niger, Nigeria, Senegal, Sierra Leone und Togo. Trotz eines ansehnlichen Wirtschaftswachstums der Staaten-emeinschaft von jährlich 5-8 % seit dieser Dekade, ist die mangelnde und unzuverlässige Versorgung mit Strom in der Region nach wie vor ein wesentliches Entwicklungshemmnis. Von den mehr als 340 Mio. Einwohnern haben nur etwa 42 % der Bevölkerung Zugang zu Strom. Hoffnungen werden in einen gemeinsamen Strommarkt der ECOWAS-Mitgliedstaaten gesetzt, der es erlauben würde, RE- und andere Energieressourcen stärker grenzüberschreitend zu nutzen.

Durch das Vorgängervorhaben sind zum heutigen Stand bereits konkrete Verbesserungen der Rahmenbedingungen in Bezug auf RE erzielt worden. Den Partnern stehen Best Practice-Erfahrungen für die Wissensverbreitung und Bewusstseinsstärkung zur Verfügung und die Stromerzeuger können auf eine Vielzahl von Lösungsmöglichkeiten zur Reduzierung ihrer technischen und kommerziellen Verteilungsverluste zurückgreifen. Mit Ergebnissen auf Ebene der übergeordneten entwicklungspolitischen Wirkungen ist erst zu einem späteren Zeitpunkt zu rechnen. Umgesetzt wird das Vorhaben mit drei gleichberechtigt nebeneinander stehenden Durchführungspartnern: ECOWAS-Zentrum für erneuerbare Energien und Energieeffizienz (ECOWAS Centre for Renewable Energy and Energy Efficiency, ECREEE) mit Sitz in Cabo Verde, Westafrikanischer Stromverbund (West African Power Pool, WAPP) mit Sitz in Benin und die regionale Regulierungsbehörde für den Elektrizitätssektor der ECOWAS (ECOWAS Regional Electricity Regulatory Authority, ERERA) mit Sitz in Ghana. Alle drei Institutionen verfügen über ein starkes politisches Mandat und sind in der Region gut vernetzt. Das Vorhaben richtet sich an alle 15 Mitgliedsstaaten und fördert eine breitenwirksame, technisch und wirtschaftlich effiziente sowie sozial und ökologisch verträgliche Energieversorgung.

Kern des Vorhabens ist es, den Schub und die Gestaltungskraft des gemeinsamen energiepolitischen Handelns der Mitgliedstaaten für eine beschleunigte Energiewende auf nationaler Ebene zu nutzen. Das bedeutet, die regionalen Fachinstitutionen der ECOWAS in der Ausübung ihrer Mandate und Funktionen zu stärken und über sie regional abgestimmte energiepolitische und -fachliche Vorgaben und Empfehlungen in die nationale Umsetzung zu tragen. Mit der EU ist eine Kooperation zur Entwicklung von Energiekorridoren, RE-Qualitäts- und Energieeffizienzstandards und zu ländlicher Elektrifizierung durch RE vereinbart.

Ziel
Die Voraussetzungen für einen klimafreundlichen Strommarkt in der ECOWAS Region sind verbessert

Vorgehensweise
Das Vorhaben verfolgt einen Mehr-Ebenen- und Multi-Akteurs-Ansatz. Es verknüpft energiefachliche, methodische und Prozessberatung bei den Durchführungspartnern mit ausgewählten Maßnahmen zur Förderung der Implementierung regionaler Vorgaben auf der nationalen Ebene. Die relevanten Akteure, nationalstaatliche oder lokale Organisationen sowie ausgewählte EVU werden in der Ausübung ihrer jeweiligen Rollen und Funktionen sowie ihrer Dialog- und Konsensbildungsfähigkeit gestärkt. Organisationen des Privatsektors oder der Verbraucher werden anlassgemäß einbezogen. Die CD-Strategie sieht dazu Maßnahmen der strategischen Kompetenzentwicklung (sowie auch CD-Maßnahmen zur Entwicklung von Organisationen und Kooperationen im Sektor und die Gestaltung von Rahmenbedingungen auf regionaler oder nationalstaatlicher Ebene vor.

Das Vorhaben gliedert sich in drei Handlungsfelder: (1) Ausbau erneuer-arer Energien, (2) Ausbau Energieeffizienz und (3) Funktionalität des regionalen Strommarktes. Es verknüpft energiefachliche, methodische und Prozessberatung bei den Durchführungspartnern mit Maßnahmen zur Förderung der Implementierung regionaler Vorgaben auf nationaler Ebene. Die Partner und nationale Akteure werden in der Ausübung ihrer jeweiligen Rollen und Funktionen sowie ihrer Dialog- und Konsensbildungs-fähigkeit gestärkt.

Das Vorhaben ergänzt das Engagement der KfW Entwicklungsbank bei der Finanzierung von Erzeugungs- und Übertragungsinfrastruktur zum Aufbau des WAPP. Es stimmt sich intensiv mit bilateralen und globalen Vorhaben ab, vor allem in Nigeria, Ghana, Senegal und Benin. Eine enge Abstimmung erfolgt ebenfalls mit der Europäischen Union, der US-amerikanischen Entwicklungsagentur (USAID), der Weltbank und anderen Gebern.





 

 
Project description (EN)

Context
Despite considerable economic growth of 5% to 8% annually since 2010 in the member states of the Economic Community of West African States (ECOWAS), the inadequate and unreliable electricity supply in the region continues to be a major obstacle to development. Only about 42% of the more than 340 million people living in the region have access to electricity. In the common electricity market represented by the 15 ECOWAS member states, greater cross-border use of renewable energy and other energy resources could increase and stabilise the electricity supply.

The current project builds on an earlier project that improved the framework for renewable energies in the region. Partners already have access to best practices in disseminating knowledge and raising awareness. Electricity producers have a variety of possible solutions for reducing their technical and commercial distribution losses. Overarching development results (impact) are not expected until a later stage.

Objective
The conditions for a climate-friendly electricity market and a widely effective, technically and economically efficient and socially and environmentally sustainable energy supply in the ECOWAS region are improved. This will mitigate climate change and reduces greenhouse gas emissions. The increasing use of renewable energies also indirectly reduces the pressure on fossil resources and the resulting environmental damage.

Approach
The main goal of the project is to use the impetus and the creative power of member states’ joint energy policy actions to accelerate energy transition at national level. This means strengthening ECOWAS’s regional expert institutions in exercising their mandates and functions. Regionally coordinated energy policy and technical specifications as well as recommendations can then be implemented nationally through the specialist institutions.

The project is aimed at all 15 ECOWAS member states and works in three fields of action: expansion of renewable energies, better energy efficiency, and functionality of the regional electricity market.
The project combines energy-related, methodological and process advice to the implementation partners. It also uses selected measures to promote the implementation of regional guidelines in the individual states.

The project strengthens the relevant actors – national and local organisations as well as selected energy utilities – in performing their role and their ability to engage in dialogue and reach consensus. Private sector organisations and consumers are involved where necessary.
The project is being implemented with three equal implementation partners: the ECOWAS Centre for Renewable Energy and Energy Efficiency (ECREEE), based in Cabo Verde; the West African Power Pool (WAPP), based in Benin; and the ECOWAS Regional Electricity Regulatory Authority (ERERA), based in Ghana.

The project complements contributions by KfW Development Bank to funding power generation and transmission infrastructure to develop WAPP. It harmonises its activities closely with bilateral and global projects, particularly in Benin, Ghana, Nigeria and Senegal. There is also close cooperation between the project and the European Union, the United States Agency for International Development (USAID), the World Bank and other donors.

Co-financing for the development of energy corridors, quality and energy efficiency standards, and rural electrification through renewable energies has been agreed with the European Union.





 

 
Project description (FR)

Contexte
En dépit de la croissance économique importante de 5 à 8 % par an que connaissent les pays de la Communauté économique des États de l’Afrique de l’Ouest (CEDEAO) depuis 2010, l’approvisionnement insuffisant et peu fiable en électricité constitue toujours un frein majeur au développement de la région. Sur une population de plus de 340 millions d’habitants, seuls 42 % environ ont un accès à l’électricité. Un marché de l’électricité commun aux quinze États membres de la CEDEAO permettrait de renforcer l’utilisation transfrontalière des énergies renouvelables et des autres sources d’énergie puis d’améliorer et de stabiliser l’approvisionnement en électricité.

Le programme actuel s’appuie sur un programme précédent qui a permis d’améliorer les conditions d’ensemble pour les énergies renouvelables dans la région. Les partenaires ont d’ores et déjà acquis de solides expériences en matière de bonnes pratiques pour la diffusion des connaissances et la sensibilisation. Quant aux compagnies de distribution d’électricité, elles peuvent recourir à un grand nombre de solutions et approches à succès adaptées pour réduire les pertes techniques et commerciales. Les résultats au niveau de l’impact global en matière de développement ne sont prévus qu’à long terme.

Objectif
Le programme vise l’amélioration des conditions clés pour la mise en place d’un marché de l’électricité respectueux du climat en vue d’un approvisionnement en énergie à la fois durable et efficace d’un point de vue technico-économique et respectueux des enjeux sociaux et écologiques dans l’ espace CEDEAO. Il en résulte une atténuation du changement climatique et une réduction des émissions de gaz à effet de serre. D’une manière indirecte, l’utilisation croissante des sources d’énergie renouvelables fait également baisser la pression exercée sur les ressources fossiles et les atteintes à l’environnement qui en découlent.

Approche
Le programme a essentiellement vocation à exploiter l’élan et la force créatrice de l’action commune des États membres en matière de politique énergétique afin d’accélérer la transition énergétique de chaque pays. Autrement dit, les institutions régionales spécialisées de la CEDEAO doivent être renforcées dans l’exercice de leurs mandats et de leurs fonctions. A travers ces Institutions appuieront ensuite la mise en œuvre, au niveau national, des orientations et des recommandations politiques et techniques concertées à l’échelle régionale.

Le programme adresse les quinze États membres de la CEDEAO et se concentre sur les trois champs d’action suivants : le développement des énergies renouvelables, le développement de l’efficacité énergétique et la fonctionnalité du marché régional de l’électricité.
Le programme fournit pour ce faire aux partenaires de mise en œuvre un appui-conseil à la fois technique, méthodologique et axé sur les processus. De plus, il promeut par des mesures appropriées la mise en œuvre d’orientations régionales dans chaque État membre. Le programme renforce les acteurs pertinents (organisations nationales ou locales et acteurs clés du secteur de l’énergie) dans l’exercice de leurs fonctions respectives, ainsi que dans leur capacité à dialoguer et à construire des consensus. Les organisations du secteur privé ou les consommateurs seront, au besoin, associées à la mise en œuvre du programme.

Le programme sera mis en œuvre en coopération avec trois partenaires : le Centre pour les Energies Renouvelables et l’Efficacité Energétique de la CEDEAO (CEREEC), le Système d’Echanges d’Energie Electrique Ouest Africain (EEEOA) et l’Autorité de Régulation Régionale du secteur de l’Electricité de la CEDEAO (ARREC). Les sièges de ces trois institutions se trouvent respectivement au Cap-Vert, au Bénin et au Ghana.

Le programme complète l’engagement de la banque de développement allemande KfW portant sur le financement des infrastructures de production et de transport visant à développer le système d’échanges d’énergie électrique ouest-africain. Une étroite concertation avec des projets bilatéraux et globaux, en particulier au Nigéria, au Ghana, au Sénégal et au Bénin est établie. Une concertation étroite existe également avec l’Union européenne, l’Agence des États-Unis pour le développement international (USAID), la Banque mondiale et d’autres partenaires techniques et financiers.

Un cofinancement est convenu avec l’Union Européenne. destiné au développement des corridors d’énergies propres, la qualité et normes dans le secteur des énergies renouvelables et de l’efficacité énergétique ainsi que l'électrification rurale à partir de sources d'énergies renouvelables.
 





 
