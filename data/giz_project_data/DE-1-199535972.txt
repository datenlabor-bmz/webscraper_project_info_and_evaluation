  




 

Studien- und Fachkräftefonds
Studies and Experts Fund
Fondo de Estudios y Expertos
Project details
Project number:1995.3597.2
Status:Projekt beendet
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Verena Blickwede verena.blickwede@giz.de
Partner countries: Dominican Repuplic, Dominican Repuplic
Summary
Objectives:

Vorbereitung und Prüfung von Vorhaben der Technischen Zusammenarbeit (TZ), Finanzierung von Studien, Gutachten sowie Durchführung von TZ-Maßnahmen geringen Umfangs.

Client:

BMZ

Project partner:

Dirección Planificación de la Presidencia

Financing organisation:

not available

 
Project value
Total financial commitment:2 634 053 Euro
Financial commitment for this project number:1 720 300 Euro
Cofinancing

not available

 
Previous projects
1985.2185.8Studien- und Fachkräftefonds
Follow-on projects
not available
 
Term
Entire project:09.08.1985 - 30.01.2022
Actual project:24.04.1995 - 30.01.2022
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
