  




 

Unterstützung bei der Umsetzung der Kooperativen Berufsbildung (Tamheen II)
Support for the implementation of cooperative vocational training (Morocco)
Appui à la mise en Œuvre de la Formation Professionnelle en Milieu de Travail (Maroc)
Project details
Project number:2020.2077.4
Status:laufendes Projekt
Responsible Organisational unit: 3600 Nordafrika
Contact person:Dr. Ruth Belkhadir ruth.belkhadir@giz.de
Partner countries: Morocco, Morocco
Summary
Objectives:

Die arbeitsmarktorientierte kooperative Berufsbildung ist ausgeweitet.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministère Inclusion d'Economique / Ministère d'Interieur

Financing organisation:

not available

 
Project value
Total financial commitment:15 500 000 Euro
Financial commitment for this project number:8 000 000 Euro
Cofinancing

not available

 
Previous projects
2016.2058.2Unterstützung bei der Umsetzung der Kooperativen Berufsbildung (Marokko - Tamheen)
Follow-on projects
not available
 
Term
Entire project:06.12.2017 - 31.05.2025
Actual project:01.06.2022 - 31.05.2025
other participants
Arge GOPA Worldwide Consultants GmbH-
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Berufliche Bildung

Evaluation
not available
 




 
