  




 

Organisationsberatung der Tschadseebeckenkommission (GIZ)
LCBC (Lake Chad Basin Commission) Capacity Development
Project details
Project number:2014.2271.6
Status:Projekt beendet
Responsible Organisational unit: 1400 Zentralafrika
Contact person:Annika Amelung annika.amelung@giz.de
Partner countries: Lake Chad Basin Commission, Centr.Afr.Rep., Cameroon, Libya, Niger, Nigeria, Chad
Summary
Objectives:

Die Tschadseebeckenkommission unterstützt die Mitgliedsstaaten kompetent und eigenständig dabei insbesondere klimawandelbedingte Herausforderungen im Einzugsgebiet des Tschadsees zu bewältigen.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Commission du bassin du lac Tchad

Financing organisation:

not available

 
Project value
Total financial commitment:20 777 385 Euro
Financial commitment for this project number:6 000 000 Euro
Cofinancing

not available

 
Previous projects
2010.2273.0Programm Nachhaltiges Wassermanagement Tschadseebecken (GIZ-Komponente)
Follow-on projects
2018.2224.6Angewandtes Wasserressourcenmanagement im Tschadseebecken
 
Term
Entire project:11.02.2005 - 31.12.2025
Actual project:27.06.2014 - 30.06.2019
other participants
Integrated Consulting Group
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Flussgebietsentwicklung und -regulierung

Evaluation
not available
 
Project description (DE)

Ausgangssituation
Die Tschadseebeckenkommission wurde 1964 von den vier Anrainerstaaten des Tschadsees Niger, Nigeria, Kamerun und Tschad gegründet. 1985 trat die Zentralafrikanische Republik und 2007 Libyen der Kommission bei. Die Kommission ist die einzige von den Anrainerstaaten akzeptierte Institution zur Lösung grenzüberschreitendender Probleme. Sie hat das Mandat der Mitgliedsstaaten, grenzübergreifende Wasserprojekte zu überwachen und zu koordinieren, die natürlichen Ressourcen zu schützen, die Wassernutzung zu regeln, zu kontrollieren und Streitfragen zu schlichten. Die Organisationsstruktur der Kommission wurde 2009/2010 umgestaltet, doch die Reorganisation wird den Anforderungen nicht gerecht. Hinzu kommen neue Aufgabenbereiche, die eine weitere Anpassung der Managementstrategie und eine Erweiterung der Kompetenzen erforderlich machen.

Ziel
Die Tschadseebeckenkommission nutzt ihre erweiterten Planungs-, Kooperations- und Kommunikationsfähigkeiten zur Verbesserung der Zusammenarbeit mit den Mitgliedsländern.

Vorgehensweise
Das Projekt zur Organisationsberatung der Tschadseebeckenkommission ist Teil des Programms „Nachhaltiges Wassermanagement Tschadseebecken", in Kooperation mit der Bundesanstalt für Geowissenschaften und Rohstoffe. Die Tschadseebeckenkommission soll zu einem Informationszentrum und zum Dienstleister für die Mitgliedsländer ausgebaut werden und ihre Aufgaben mit höherer Effizienz, Effektivität und erweitertem Know-how bewältigen.
Das Vorhaben investiert in institutionelle Reformen der Kommission, in die Konsolidierung der Planungsprozesse, in den Aufbau eines Monitoring- und Evaluierungssystems, eines Informationssystems (regionale Datenbank) sowie in die Verbesserung der internen und externen Kommunikation. Es unterstützt die Personalentwicklung und fördert den fachlichen Austausch mit anderen internationalen Kommissionen sowie die Erstellung von Medienprodukten und Publikationen. Durch eine verbesserte Geberkoordinierung sollen die verfügbaren Ressourcen künftig besser genutzt werden.

Unterstützt wird das Programm bei der Neugestaltung, Pflege der Webseite und beim Aufbau der internen Kommunikationsplattform der Kommission durch die Firma Compower, Karlsruhe, die auch die Schulungen hierzu durchführt. Der Ausbau der IT-Infrastruktur wird durch die AHT GROUP AG unterstützt, die auch das Informationssystem (LIS Lake Chad Information System) mit den regionalen Fachdatenbanken entwickelt hat. Der Aufbau des Hochtechnologieservers und die Schulung des IT-Personals wurden und werden durch die Ibes AG, Jena, begleitet. Unterstützung in der Beratung zur Organisationsentwicklung (Reform der Kommission, Kommunikation und Capacity Development) erfolgen durch Königswieser & Network, Wien, synetz international, Rösrath, und Integrated Consulting Group Deutschland, Berlin.

Der Ausbau der Fachkompetenz erfolgt über die kollegiale Begleitung der Fachabteilungen bei der Erstellung des ersten Statusberichtes zum ökologischen und sozioökonomischen Zustand des Tschadseebeckens und der jährlichen Monitoringberichte, die auch von der AHT GROUP AG unterstützt wird. Die Abteilungen werden in der Anwendung und Pflege des Informationssystems und der internen Kommunikationsplattform geschult und fortgebildet. Daneben wird die Fachkompetenz der Kommission zum Klimawandel erweitert.

Wirkung
Durch die Gründung der abteilungsübergreifenden „Internal Experts Meeting" sowie der „Water and Climate Coordination Group" erfolgt die fachliche Zusammenarbeit mit anderen Institutionen aus den Mitgliedsländern und ist international bereits jetzt koordinierter und auf gegenseitige Unterstützung ausgerichtet.

Das an die neuen Herausforderungen der Kommission angepasste Informationsmanagementsystem – mit verschiedenen Datenbanken, verbesserter interner IT-Infrastruktur (Hightechserver) sowie verbesserter Kommunikation des Personals über eine eigene interne Kommunikationsplattform – erlaubt eine effizientere fach- und abteilungsübergreifende Zusammenarbeit mit vereinheitlichten Daten. Aus diesen Daten wird der erste Statusbericht zum ökologischen und sozioökonomischen Zustand des Tschadseebeckens erstellt, dessen Fertigstellung 2015 geplant ist.

Durch die Einführung der abteilungsübergreifenden Planung und des Monitorings der Aktivitäten, Programme und Projekte ist erstmals die fristgerechte Erstellung strategischer Jahresoperationspläne für die jährlichen Ministerratssitzungen möglich.

Der Ministerrat hat im April 2015 zugestimmt, den vom Vorhaben erarbeiteten Reformvorschlag umzusetzen, um Effektivität, Effizienz und Fachwissen innerhalb der Kommission zu erhöhen.
 

 
Project description (EN)

Context
The Lake Chad Basin Commission (LCBC) was set up in 1964 by the four riparian states Niger, Nigeria, Cameroon and Chad, with the Central African Republic and Libya joining in 1985 and 2007 respectively. LCBC is the only institution that enjoys the acceptance of all these countries when it comes to resolving cross-border issues. Its mandate is to supervise and coordinate transboundary water projects, protect natural resources, regulate and monitor water usage, and settle disputes. The reorganisation undertaken in 2009/2010 has not equipped LCBC to meet the demands placed on it. Additionally, new areas of activity are emerging which require the institution to further adapt its management strategy and expand its capacities.

Objective
LCBC leverages its enhanced planning, cooperation and communication capacities to improve cooperation with its member states.

Approach
This project forms part of the programme Sustainable Water Resources Management of the Lake Chad Basin, which is being carried out in cooperation with the German Federal Institute for Geosciences and Natural Resources. The intention is for LCBC to evolve into an information centre and a service provider for its member states and to become more efficient and effective in performing its work using its enhanced expertise.

The project is investing in the institutional reform of LCBC, the consolidation of planning processes, the setting up of a monitoring and evaluation system and an information system (regional database), and the improvement of internal and external communication. It supports human resources development, encourages specialist dialogue with other international commissions and promotes the creation of media products and publications. Improved donor coordination is to ensure that the available resources are used more effectively in future.
Compower, a company based in Karlsruhe, is assisting the programme with the redesign and maintenance of the website and the establishment of LCBC's internal communication platform, providing the necessary training along the way. AHT GROUP AG, which developed the LIS Lake Chad Information System with the specialist regional databases, is supporting the expansion of LCBC's IT infrastructure. The company ibes AG from Jena has assisted with the development of the hi-tech server and the training of IT staff and will continue to do so. Königswieser & Network (Vienna), synetz international (Rösrath) and the Integrated Consulting Group Deutschland (Berlin) are providing consultancy services in organisational development, for instance with respect to the reform of LCBC, communication and capacity development activities.

LCBC's expertise is being enhanced by means of peer support to the relevant departments with the preparation of the initial report on the environmental and socio-economic status of the Lake Chad Basin and the annual monitoring reports. AHT GROUP AG is also providing assistance in this context. The departments are being trained in using and maintaining the information system and the internal communication platform. At the same time, activities are conducted to enhance LCBC's expertise in the area of climate change.

Results
The cross-departmental Internal Experts Meeting and the Water and Climate Coordination Group, which were set up to facilitate cooperation with other institutions from the member states, have already improved coordination at international level and encouraged mutual support among the different actors.

The information management system has been adapted to meet the new challenges faced by LCBC through the addition of a range of databases, improvements to the internal IT infrastructure (hi-tech server) and the establishment of a dedicated internal communication platform to improve staff communication. This is improving the efficiency of interdisciplinary and cross-departmental cooperation through the use of standardised data. This data is being utilised as the basis for the first report on the environmental and socio-economic status of the Lake Chad Basin, which is due to be completed in 2015.

Thanks to the introduction of cross-departmental planning and monitoring of activities, programmes and projects, it is now possible to draw up strategic annual operational plans in time for the annual meetings of the Council of Ministers.

The Council of Ministers agreed in April 2015 to implement the reform proposals drafted by the project in order to increase the effectiveness, efficiency and expertise of LCBC.
 





 
