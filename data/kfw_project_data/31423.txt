responsAbility Participations

Das Investitionsziel der responsAbility Participations AG sind Beteiligungen an Finanzinstitutionen in Entwicklungs- und Schwellenländern, die sich auf die Bereitstellung von Finanzdienstleistungen für Niedrigeinkommenshaushalte und kleinste, kleine und mittlere Unternehmen (KKMU) spezialisiert haben. Durch die Investitionen soll der Zugang zu zielgruppenorientierten Finanzdienstleistungen ausgeweitet werden und indirekt zur Steigerung der Erwerbstätigkeit bei KKMU beigetragen werden.

Land / Region / Institution Alle Entwicklungsländer Nummer 31423 Schwerpunkt Wirtschaft und Finanzsektor Sektor 24030 - Finanzintermediäre des formellen Sektors Finanzierungsinstrument Förderbeteiligung Weitere Geber - Deutscher Finanzierungsbeitrag 25,004 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner RESPONSABILITY PARTICIPATIONS AG Zuständige Abteilung Beteiligungsfinanzierung
