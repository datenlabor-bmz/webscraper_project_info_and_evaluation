  




 

Bildung für Medienschaffende
Capacity Building for Media Practitioners
Project details
Project number:2012.2128.2
Status:Projekt beendet
Responsible Organisational unit: G210 Flucht, Migration, Rückkehr
Contact person:Astrid Kohl astrid.kohl@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh.
Summary
Objectives:

Das Sektorreferat kann auf innovative Handlungsansätze zum Thema Bildung für Medienschaffende, die Handlungs- empfehlungen zur Stärkung der Medienkompetenz auf allen Bildungsstufen einschließen, Bezug nehmen.

Client:

BMZ

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:3 280 000 Euro
Financial commitment for this project number:3 280 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:27.12.2011 - 16.12.2013
Actual project:27.12.2011 - 16.12.2013
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Fortbildung von Fach- und Führungskräften

Evaluation
not available
 




 
