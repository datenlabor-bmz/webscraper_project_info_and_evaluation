  




 

Pan-Afrikanischen Freihandelszone (CFTA)- GIZ-Modul
Continental Free Trade Area (CFTA)
Project details
Project number:2016.2000.4
Status:Projekt beendet
Responsible Organisational unit: 1700 Afrika Überregional und Horn von Afrika
Contact person:Svenja Ossmann svenja.ossmann@giz.de
Partner countries: African Union, Ethiopia, Tanzania, South Africa
Summary
Objectives:

Die Rahmenbedingungen für die panafrikanischen Verhandlungen der Continental Free Trade Area (CFTA) im Bereich Handelserleichterung und Zollkooperation sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Afrikanische Union

Financing organisation:

not available

 
Project value
Total financial commitment:9 242 903 Euro
Financial commitment for this project number:9 000 000 Euro
Cofinancing

not available

 
Previous projects
1995.3552.7/118Projektprüfung "Panafrikanische Freihandelszone (CFTA)"
Follow-on projects
not available
 
Term
Entire project:19.02.2015 - 14.04.2021
Actual project:01.02.2017 - 14.04.2021
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
Armutsorientierungnot available
CRS code
Regionale Handelsabkommen

Evaluation
not available
 
Project description (DE)

NEUE ID >> Regionale Zusammenarbeit in Afrika >> Verbindungsbüro zur Afrikanischen Union >> Regionale wirtschaftliche Integration
Panafrikanische Freihandelszone (CFTA)
Bezeichnung: Unterstützung der Kommission der Afrikanischen Union (AUK) in der Steuerung und Koordinierung des CFTA-Prozesses sowie in der Verbesserung der Rahmenbedingungen für die pan-afrikanischen Verhandlungen im Bereich Handelserleichterungen und Zollkooperation
Ausgangssituation
Mit einem Bruttosozialprodukt von 2,4 Prozent im globalen Kontext ist Afrika der ärmste und unterentwickeltste Kontinent. Nach einer Dekade anhaltenden Wachstums ist das Wirtschaftswachstum 2015 im Vergleich zum Vorjahr erstmals von 5 auf 3,6 Prozent gesunken. Noch immer ist Afrika durch Rohstoff- und Agrarexporte sowie durch Importe von Kapitalgütern asymmetrisch in globale Handelsdynamiken eingebunden.
Die geringe Größe vieler afrikanischer Länder ist ebenfalls ein Problem. Mit häufig weniger als 20 Millionen Einwohnern und einem volkswirtschaftlichen Volumen unter 10 Milliarden US-Dollar sind viele nationale Märkte zu klein, um große Investitionen zu akquirieren.
Darüber hinaus bleibt der Handel zwischen den Ländern Afrikas hinter seinen Möglichkeiten zurück und macht nur etwa 10 Prozent des gesamten Handelsvolumens aus. Im Vergleich dazu beträgt der kontinentale Handel in Nordamerika 40 Prozent. Die Länder Westeuropas wickeln sogar zu 63 Prozent Geschäfte mit ihren Nachbarn ab.
In einigen regionalen Wirtschaftsgemeinschaften (Regional Economic Communities, RECs) wurde durch den Abbau von Zöllen die Handelsintegration verbessert. Der afrikanische Markt ist dennoch nach wie vor fragmentiert. Überlappende Mitgliedschaften in unterschiedlichen Wirtschaftsgemeinschaften haben institutionelle Konkurrenzen, hohe Transaktionskosten und eine Hemmung der regionalen und kontinentalen Integration zur Folge. Entsprechend verursachen zahlreiche nichttarifäre Handelshemmnisse weiterhin hohe Handelskosten auf dem Kontinent und sorgen dafür, dass Afrika mit dem Rest der Welt stärker verbunden ist als seine Länder untereinander.
Ziel
Die Rahmenbedingungen für die panafrikanischen Verhandlungen der Continental Free Trade Area im Bereich Handelserleichterung und Zollkooperation sind verbessert.
Vorgehensweise
Mit dem Vertrag von Abuja (1991) vereinbarten die damaligen Mitgliedsstaaten der Organisation für Afrikanische Einheit (OAU), Vorgängerorganisation der Afrikanischen Union (AU), erstmals einen Fahrplan zur Schaffung eines afrikanischen Binnenmarktes. Um das Abuja-Abkommen zu beschleunigen und die regionale Integration zu stärken, beschlossen die AU-Handelsminister 2012 die Schaffung einer panafrikanischen Freihandelszone (Continental Free Trade Area, CFTA) bis 2017.
Im Auftrag des Bundesministeriums für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) unterstützt die GIZ die zuständige AU-Kommission dabei, den CFTA-Prozess zu steuern und zu koordinieren sowie die Rahmenbedingungen für die panafrikanischen Verhandlungen über Handelserleichterungen und Zollkooperation zu verbessern. Schwerpunkte der Zusammenarbeit sind die Stärkung der Abteilung für Handel und Industrie (DTI) der Kommission der Afrikanischen Union sowie die inhaltlich-fachliche Vorbereitung der Verhandlungen. So berät das Vorhaben den Handelskommissar in strategischen Fragen und unterstützt das DTI, unter anderem durch Experten für Handelserleichterungen im Verhandlungsprozess.
Zielgruppen des Vorhabens sind, neben der Bevölkerung, Eigentümer und Beschäftigte außenhandelsorientierter Unternehmen in den Mitgliedsstaaten der AU, vor allem aus kleinen und mittelständischen Unternehmen. Die Bevölkerung profitiert von den Kosteneinsparungen für Handelstransaktionen, die in Form von günstigeren Preisen für Güter und Dienstleistungen an den Endverbraucher weitergegeben werden.
Durch die strategische Beratung der Abteilung für Handel und Industrie (DTI) der Kommission der Afrikanischen Union zur Gestaltung des CFTA-Prozesses konnten Dynamik und Schwerpunkte der Verhandlungen aufrechterhalten werden. Das DTI stellte zusätzliches technisch versiertes Projektpersonal ein, um den Prozess voranzubringen und die von den AU-Mitgliedsstaaten bei den Verhandlungsrunden geforderten Leistungen zu liefern, beispielsweise Studien, fachliche Unterstützung, Training der Verhandlungsführer.
Mit einer Senkung der Zolltarife um 90 Prozent haben sich die Mitgliedsstaaten auf einen überraschend ambitionierten Weg der Handelsliberalisierung geeinigt. Durch den Abbau von Handelshemmnissen, wie komplizierte Abwicklungsverfahren an den Grenzen, können die hohen intraregionalen Handelskosten gesenkt werden. Marktzugang und Wettbewerbsfähigkeit regionaler Unternehmen und Produkte werden so verbessert. Vor allem Länder mit kleinen Binnenmärkten können bestehende Handelspotenziale besser nutzen und sich neue Absatzmärkte auf dem Kontinent erschließen. Langfristig soll durch das CFTA-Abkommen der intraafrikanische Handel von 10,2 Prozent (2010) auf 15,5 Prozent (2022) gesteigert werden. Die Vereinbarung wird sich auch auf die Armutsreduzierung positiv auswirken: Durch das entstehende Wirtschaftswachstum werden Arbeitsplätze und neue Einkommensmöglichkeiten geschaffen. 





 
