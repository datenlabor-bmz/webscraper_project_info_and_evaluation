  




 

PROKLIMA-Substitution von ozonzerstörenden Substanzen
Proklima -substitution of ozone depleting substances
Project details
Project number:2013.2103.3
Status:Projekt beendet
Responsible Organisational unit: G330 Umweltpolitik, Biodiversität, Wald
Contact person:Bernhard Siegele bernhard.siegele@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh., Germany
Summary
Objectives:

Die Kooperationsländer ersetzen frühzeitig ozonzerstörende und klimaschädliche Substanzen durch umweltfreundlichere Alternativen.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:21 834 802 Euro
Financial commitment for this project number:2 074 898 Euro
Cofinancing

not available

 
Previous projects
2007.2123.3PROKLIMA-Substitution von ozonzerstörenden Substanzen (ODS)
Follow-on projects
2016.2219.0PROKLIMA - Integrierter Ozon- und Klimaschutz
 
Term
Entire project:11.12.1995 - 30.11.2021
Actual project:15.03.2013 - 28.02.2017
other participants
not available
 
Contact
Project websiteswww.giz.de/proklima
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektziel-Ebene: Projekt zielt vor allem auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Schutz der Biosphäre

Evaluation
Überregional: Globales Sektorvorhaben PROKLIMA Integrierter Ozon- und Klimaschutz. Projektevaluierung: Kurzbericht
 




 
