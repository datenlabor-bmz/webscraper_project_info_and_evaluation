  




 

Stärkung der Menschenrechte
Strenghthening Human Rights in Uganda
Project details
Project number:2010.2196.3
Status:Projekt beendet
Responsible Organisational unit: 1500 Ostafrika
Contact person:Bolaji Abigail Aina bolaji.aina@giz.de
Partner countries: Uganda
Summary
Objectives:

Ausgewählte staatliche und nicht-staatliche Akteure stärken die Menschenrechte, insb. von benacht. Gruppen, in ihren jew. Aufgabenfeldern.

Client:

BMZ

Project partner:

National Planning Authority (NPA)

Financing organisation:

not available

 
Project value
Total financial commitment:6 703 015 Euro
Financial commitment for this project number:2 064 542 Euro
Cofinancing

Danish International Development Agency (DANIDA) alt bis 31.12.2011: 310 057Euro


 
Previous projects

not available

Follow-on projects
2013.2203.1Stärkung der Menschenrechte
 
Term
Entire project:31.10.2011 - 31.03.2017
Actual project:06.02.2012 - 06.02.2015
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Menschenrechte

Evaluation
not available
 




 
