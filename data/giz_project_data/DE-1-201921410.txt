  




 

Verbesserung der Arbeits-, Sozial- und Umweltstandards in der pakistanischen Textilindustrie
Improvement of labour-, social- and environment standards in the Pakistani textile industry
Project details
Project number:2019.2141.0
Status:laufendes Projekt
Responsible Organisational unit: 2B00 Asien II
Contact person:Romina Kochius romina.kochius@giz.de
Partner countries: Pakistan, Pakistan
Summary
Objectives:

Die Rahmenbedingungen für die Einhaltung von Arbeits- und Sozialstandards in der Provinz Punjab sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Commerce and Industry

Financing organisation:

not available

 
Project value
Total financial commitment:24 300 000 Euro
Financial commitment for this project number:14 350 000 Euro
Cofinancing

not available

 
Previous projects
2016.2029.3Verbesserung der Arbeits- und Sozialstandards in der pakistanischen Textilindustrie
Follow-on projects
not available
 
Term
Entire project:14.12.2016 - 31.12.2023
Actual project:01.01.2021 - 31.12.2023
other participants
ARGE GOPA Worldwide Consultants GmbH-
ARGE cinco.systems Lakoni & Thiel
Como Consult GmbH
Deutsche Gesetzliche Unfallversicherung
adelphi consult GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Verantwortungsvolles unternehmerisches Handeln

Evaluation
not available
 




 
