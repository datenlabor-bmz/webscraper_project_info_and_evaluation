  




 

Gute Regierungsführung durch GovTech und Transparenz
Good Governance by GovTech and Transparency
Project details
Project number:2021.2146.5
Status:laufendes Projekt
Responsible Organisational unit: 3900 Deutschland, Europa, Südkaukasus
Contact person:Julia Schappert julia.schappert@giz.de
Partner countries: Ukraine, Ukraine
Summary
Objectives:

Die Nutzung daten- und technologiebasierter Lösungen für eine transparente und inklusive Gestaltung des Wiederaufbaus ist zwischen Schlüsselakteuren aus dem öffentlichen Sektor und der Zivilgesellschaft gestärkt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministerium für Digitale Transformation

Financing organisation:

not available

 
Project value
Total financial commitment:9 000 000 Euro
Financial commitment for this project number:9 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:29.12.2022 - 31.12.2025
Actual project:01.01.2023 - 31.12.2025
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Politik und Verwaltung in Bezug auf den öffentl Sektor

Evaluation
not available
 




 
