  




 

Sektorvorhaben Wasserstoffstrategie
Hydrogen Strategy
Project details
Project number:2022.6690.6
Status:Projekt beendet
Responsible Organisational unit: G130 Zusammenarbeit mit der Wirtschaft
Contact person:Farhanja Wahabzada farhanja.wahabzada@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh.
Summary
Objectives:

Entwicklung lokaler Märkte und Wertschöpfungsketten für grünen Wasserstoff und Umsetzung von großskaligen Projekten schaffen, die Wettbewerbsfähigkeit der Länder im globalen Markthochlauf stärken und den Rahmen für konkrete Investitionen verbessern.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:2 200 000 Euro
Financial commitment for this project number:2 200 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:31.12.2021 - 30.01.2023
Actual project:09.02.2022 - 30.01.2023
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
Armutsorientierungnot available
CRS code
Energieerzeugung, erneuerbare Quellen – verschiedene ..

Evaluation
not available
 




 
