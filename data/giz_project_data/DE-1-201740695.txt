  




 

Förderung von Handel für Beschäftigung
Trade for development
Project details
Project number:2017.4069.5
Status:laufendes Projekt
Responsible Organisational unit: 3300 Naher und Mittlerer Osten 1
Contact person:Sherif Younis Hassan sherif.younis@giz.de
Partner countries: Jordan
Summary
Objectives:

Die Bedingungen für jordanische Unternehmen zur beschäftigungswirksamen Steigerung ihrer Handelsleistung sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Industry and Trade

Financing organisation:

not available

 
Project value
Total financial commitment:32 897 584 Euro
Financial commitment for this project number:32 897 584 Euro
Cofinancing

DFID (bis 30.08.2020) - Neu: FCDO: 1 948 169Euro


Ministry of Foreign Affairs (MFA/DGIS) Niederlande (ab 01.01.2012): 5 800 000Euro


World Economic Forum: 1 149 415Euro


 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:22.08.2017 - 30.10.2025
Actual project:13.09.2017 - 30.10.2025
other participants
ARGE PEM CONSULT GmbH-Khalid Hassan
GOPA Worldwide Consultants GmbH
ICON-Institut GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Geschäftspolitik und -verwaltung

Evaluation
not available
 




 
