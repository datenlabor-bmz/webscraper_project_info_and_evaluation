  




 

Kooperative Berufsbildung im Rohstoffsektor
Cooperative Vocational Training in the Mineral Resources Sector
Project details
Project number:2015.2225.9
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Beate Dippmar beate.dippmar@giz.de
Partner countries: Mongolia, Mongolia
Summary
Objectives:

Mongolische Jugendliche und Erwachsene nutzen die Chancen zur Beschäftigung auf dem Arbeitsmarkt im Rohstoffsektor einschließlich vor- und nachgelagerter Industrien besser.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Labour and Social Protection

Financing organisation:

not available

 
Project value
Total financial commitment:38 180 133 Euro
Financial commitment for this project number:12 714 629 Euro
Cofinancing

Direktion für Entwicklung und Zusammenarbeit (DEZA/engl. SDC): 4 851 698Euro


Dep. of Foreign Affairs and Trade (DFAT) - ehemals AusAID: 2 443 643Euro


 
Previous projects
2012.2523.4Kooperative Berufsbildung im Rohstoffsektor
Follow-on projects
2018.2120.6Kooperative Berufsbildung
 
Term
Entire project:12.12.2012 - 31.12.2023
Actual project:01.03.2016 - 31.05.2019
other participants
TUVD Agentur
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Berufliche Bildung

Evaluation
not available
 




 
