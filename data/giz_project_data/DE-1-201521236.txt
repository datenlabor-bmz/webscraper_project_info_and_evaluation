  




 

Programm zur Effizienzverbesserung der Trinkwasserver- und Abwasserentsorgung in Nicaragua
Programme to improve the efficiency of the drinking water supply and the sewage treatment
Mejoramiento de la eficiencia en agua potable y eliminación de aguas residuales en Nicaragua
Project details
Project number:2015.2123.6
Status:laufendes Projekt
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Gereon Hunger gereon.hunger@giz.de
Partner countries: Nicaragua, Nicaragua
Summary
Objectives:

Das Wasserressourcenmanagement und die städtische Trinkwasser- und Sanitärversorgung sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Secretaría de Relaciones Económicas y Cooperación (Ministerio de Relaciones Exteriores)

Financing organisation:

not available

 
Project value
Total financial commitment:31 660 890 Euro
Financial commitment for this project number:20 030 890 Euro
Cofinancing

Empresa Nicaraguense de Acueductos y Alcantarillados (ENACAL): 3 135 890Euro


Europäische Union (EU): 9 450 000Euro


Direktion für Entwicklung und Zusammenarbeit (DEZA/engl. SDC): 695 000Euro


 
Previous projects
2009.2121.3Programm zur Effizienzverbesserung der Trinkwasser- und Abwasserentsorgung in Nicaraugua
Follow-on projects
not available
 
Term
Entire project:27.10.2010 - 31.12.2023
Actual project:01.10.2015 - 31.12.2023
other participants
Akut Umweltschutz
 
Contact
Project websiteswww.proatas.org.ni
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Grundl. Versorg Trinkwasser, Sanitärversorgung, Abwass

Evaluation
not available
 
Project description (DE)


Ausgangssituation

Nicaragua hat die Trinkwasser-Zielvorgabe der Millenniums-Entwicklungsziele erreicht: der Anteil der Menschen, die keinen Zugang zu sauberem Trinkwasser haben, hat das Land um die Hälfte reduziert. Nach wie vor existieren jedoch teilweise starke Engpässe bei der Versorgung der Bevölkerung. Vielerorts fließt das kostbare Nass nur für wenige Stunden und auch nur an einigen Tagen in der Woche durch die Leitung. Der staatliche Wasserversorgungsbetrieb, der drei Millionen Menschen mit Trinkwasser beliefert, hat seinen Service zwar verbessert, jedoch gilt das nicht für alle Teile des Landes. Bislang schafft es der staatliche Versorger, nur eine geringe Zahl seiner Kläranlagen ordnungsgemäß zu betreiben. Viele Abwässer landen weiterhin ungeklärt in Flüssen und Seen - eine Belastung für Umwelt und Bevölkerung. Extreme Wetterereignisse, wie beispielsweise die durch „El Niño" verursachte Dürre 2014 und 2015 mit stark reduzierten Niederschlägen in der Regenzeit, erschweren die Versorgung mit sicherem Trinkwasser zusätzlich. Ein integriertes Wasserressourcenmanagement können die verantwortlichen Institutionen bisher erst in Ansätzen stemmen.

Ziel

Das Wasserressourcenmanagement und die städtische Trinkwasser- und Sanitärversorgung sind verbessert.

Vorgehensweise

Das Projekt hat seine Beratung bis Ende 2015 auf vier Außenstellen des staatlichen Wasserversorgers (Masaya, Rivas, Boaco und Chontales) und seinen Geschäftssitz in Managua konzentriert. Zusammen mit der nationalen Wasserbehörde und den Gemeindeverwaltungen hat es neue Ansätze und Methoden in der Bewirtschaftung der Wasserressourcen zweier Wassereinzugsgebieten des Landes (in der Region Rivas und der Region Chontales) entwickelt. Seit Anfang 2016 weitet das Projekt die Methoden zusammen mit den Partnern auf weitere Gemeinden und Wassereinzugsgebiete aus. Im Fokus stehen folgende Schwerpunkte:
— Städtische Trinkwasserversorgung und Abwasserentsorgung
Zur Verbesserung der städtischen Trinkwasser- und Sanitärversorgung unterstützt das Projekt den nationalen staatlichen Versorger ENACAL. Der durch die Beratung angestoßene Modernisierungs- und Reorganisationsprozess im Unternehmen führte zu Verbesserungen seiner Serviceleistungen und finanziellen Situation. Die erfolgreich eingeführten Prozesse überträgt das Unternehmen mit Unterstützung des Projektes nun auf elf weitere Städte. In diesen Städten befinden sich neue Trinkwasser- und Sanitäranlagen im Bau, die von internationalen Gebern im Rahmen des nationalen Wasserprogramms finanziert werden.

— Integriertes Wasserressourcenmanagement
In zwei Wassereinzugsgebieten berät das Projekt die lokalen Stadtverwaltungen, die gegründeten Wasserkomitees und die Nationale Wasserbehörde ANA. Es geht vor allem darum, den aufgestellten Plan zur Bewirtschaftung der Wasserressourcen weiter zu verwirklichen. Er beinhaltet klimasensible Maßnahmen zur Verbesserung der Situation der Wasserressourcen und ihrer nachhaltigen Nutzung. Bisher erfolgreich erprobte Methoden des integrierten Wasserressourcenmanagements in den zwei Projektgebieten werden auf eines der wirtschaftlich wichtigsten Einzugsgebiete im Nordwesten des Landes übertragen. Neben den Gemeindeverwaltungen wurde die Beratung auf weitere wichtige Sektorinstitutionen erweitert.

— Politische Beratung
Mandate und Zuständigkeiten der für das Abwassermanagement relevanten Institutionen sind geklärt. Das Projekt unterstützt jetzt das Umweltministerium MARENA und den nationalen Wasserversorger ENACAL dabei, die Einleitung von Abwässern in Oberflächengewässer zu überwachen und zu regulieren. Hierzu wird ein digitales online Monitoringsystem beim Umweltministerium aufgebaut.


Fortbildung von Umwelttechnikern der Stadtverwaltungen zur Gewässerüberwachung © GIZ


Wirkung

In den vier Außenstellen ist es dem staatlichen Wasserversorgungsbetrieb gelungen, 110.000 Menschen etwa zehn Stunden täglich mit sauberem Trinkwasser zu beliefern – zuvor waren es gerade einmal durchschnittlich zwei Stunden. Die illegalen Anschlüsse an das Versorgungsnetz haben deutlich abgenommen. Der Wasserversorgungsbetrieb konnte 13.000 neue Kunden gewinnen. Die jährlichen Einnahmen der Außenstellen des Versorgers in den Gemeinden haben sich durch die Verbesserung im kommerziellen Bereich und durch besseres Management mehr als verdoppelt. Die Kostendeckung ist von durchschnittlich 42 auf 60 Prozent angestiegen.

Das Projektteam hat mit 16 Kläranlagen zusammengearbeitet. Diese halten die Umweltnormen stärker ein, so dass die Wasserqualität beim Ablauf aus der Kläranlage zugenommen hat.

In den zwei Wassereinzugsgebieten wurde ein fünfjähriger Bewirtschaftungsplan aufgestellt. Mehr als ein Drittel der vorgesehenen Maßnahmen haben die Gemeindeverwaltungen bereits verwirklicht. Alle ausgearbeiteten Maßnahmen dienen der nachhaltigen Nutzung der Wasserressourcen und der Anpassung an die Auswirkungen des Klimawandels. Mit Unterstützung des Projektes wurden in den beiden Einzugsgebieten Wassernutzerkomitees gegründet, in denen Repräsentanten der Gemeindeverwaltung, von Unternehmen und der Zivilgesellschaft sitzen. Nach diesem Vorbild hat die Wasserbehörde eine Richtlinie erlassen, die zur Gründung von Wassernutzerkomitees landesweit angewandt wird. Die Umwelt- und Wasserabteilungen der Stadtverwaltungen überwachen in beiden Einzugsgebieten die Gewässer hinsichtlich ihrer Qualität und Quantität. So können sie negative Veränderungen im Wasserhaushalt feststellen und mit konkreten klimasensiblen Maßnahmen darauf reagieren.

Den verschiedenen Institutionen des Wassersektors liegt ein erster Entwurf vor, der die Zuständigkeiten bei Schnittstellen zwischen den Behörden und Institutionen zur Umsetzung des 2010 verabschiedeten Wassergesetzes regelt.
 





 
