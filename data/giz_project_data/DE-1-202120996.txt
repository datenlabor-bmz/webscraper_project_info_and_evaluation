  




 

Ländliche Entwicklung und produktive Landwirtschaft - Förderung der produktiven Landwirtschaft (PROMAP), Phase IV
Promotion of the productive agriculture Niger
Project details
Project number:2021.2099.6
Status:laufendes Projekt
Responsible Organisational unit: 1100 Westafrika 1
Contact person:Matthias Banzhaf matthias.banzhaf@giz.de
Partner countries: Niger, Niger
Summary
Objectives:

Landwirtinnen und Landwirte der Kleinbewässerungslandwirtschaft in den Regionen Agadez, Tahoua und Tillaberi verwenden innovative und agrarökologische Bewirtschaftungsmethoden zur nachhaltigen Produktions- und Ernährungssicherung.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministère du Plan

Financing organisation:

not available

 
Project value
Total financial commitment:92 696 717 Euro
Financial commitment for this project number:19 739 574 Euro
Cofinancing

Ministry of Foreign Affairs (MFA/DGIS) Niederlande (ab 01.01.2012): 6 000 000Euro


 
Previous projects
2017.2093.7Programm ländliche Entwicklung und produktive Landwirtschaft - Förderung der produktiven Landwirtschaft(PROMAP) Phase II
Follow-on projects
not available
 
Term
Entire project:31.10.2006 - 31.12.2024
Actual project:01.01.2022 - 31.12.2024
other participants
ARGE ECO Consult GmbH & Co. KG-
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungSelbsthilfeorientierte Armutsbekämpfung
CRS code
Landwirtschaftsentwicklung

Evaluation
not available
 




 
