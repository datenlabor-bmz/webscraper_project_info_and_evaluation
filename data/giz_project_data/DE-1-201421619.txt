  




 

Innovationsfonds
Innovation Fund
Fondo de Innovación
Project details
Project number:2014.2161.9
Status:laufendes Projekt
Responsible Organisational unit: 2C00 Lateinamerika, Karibik
Contact person:Barbara Marta Oehler barbara.oehler@giz.de
Partner countries: Ecuador, Ecuador
Summary
Objectives:

Innovative Ansätze tragen zur Umsetzung der ecuadorianischen Reformagenda bei und fördern die Beteiligung und Stärkung der verschiedenen staatlichen und privatwirtschaftlichen Akteure auf nationaler und dezentraler Ebene.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Secretaria Tecnica de Cooperacion Internacional

Financing organisation:

not available

 
Project value
Total financial commitment:4 000 000 Euro
Financial commitment for this project number:4 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:24.06.2016 - 30.06.2023
Actual project:01.07.2016 - 30.06.2023
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Politik und Verwaltung in Bezug auf den öffentl Sektor

Evaluation
not available
 
Project description (DE)

Ausgangssituation

Ecuador hat auf Grundlage seines nationalen Entwicklungsplans eine Strategie zur Neugestaltung seiner Wirtschaft erarbeitet. Damit ist vor allem der Übergang von Primärgüter-Export und Ausbeutung von Bodenschätzen hin zu einer diversifizierten, ökoeffizienten Produktion mit höherer Wertschöpfung gemeint – zum Beispiel Dienstleistungen in einer wissensbasierten Wirtschaft. Dazu sollen künftig stärker die Fähigkeiten und das Wissen der Bevölkerung sowie der öffentlichen Angestellten genutzt werden. Voraussetzung für das Gelingen dieser Reformanstrengung ist, dass die lokalen Verwaltungsebenen ihre Fähigkeiten und Kompetenzen ausbauen, enger mit dem Privatsektor zusammenarbeiten und die Zivilgesellschaft einbinden.

Ziel

Private und öffentliche Akteure setzen durch verbesserte Zusammenarbeit innovative Projekte zur lokalen Wirtschaftsförderung, Beschäftigungsförderung und zur dualen Berufsausbildung um.

Vorgehensweise

Der Innovationsfonds finanziert Projekte, die öffentlich-private Zusammenarbeit im produktiven Sektor fördern. Die Projektlaufzeit liegt zwischen sechs und zwölf Monaten und das Volumen zwischen 100.000 bis 150.000 Euro. Themen von bisher zwei öffentlichen Ausschreibungen waren berufliche Bildung/Beschäftigungsförderung und lokale Wirtschaftsentwicklung.

Die Auswahl der Projekte erfolgt jeweils durch das Sekretariat für Internationale Kooperation im ecuadorianischen Außenministerium und die Deutsche Botschaft. Auf Basis eines Kriterienkatalogs und nach klaren Regeln wurden insbesondere Projektvorschläge ausgewählt, die konkrete Wirkungen auf lokaler Ebene erzielen und gleichzeitig auch in anderen Landesteilen Ecuadors anwendbar sind.

Die GIZ berät bei der Erstellung der Projektanträge und begleitet alle Projekte eng in der Umsetzungs­phase.

Wirkungen

Die über den Fonds geförderten innovativen Projekte erproben neue Prozesse, Methoden und Formen der Zusammenarbeit auf lokaler Ebene und mit unterschiedlichen Akteuren. Sie haben positive Effekte auf die Gleichberechtigung der Geschlechter und/oder die Generationengerechtigkeit.

In der Provinz Manabí gibt es drei Projekte zur wirtschaftlichen Wiederbelebung und lokalen Wirtschaftsförderung:
—Für genossenschaftlich organisierte Produzentinnen und Produzenten in der Provinz Manabí werden digitale Tools zur Belebung der lokalen Wirtschaft entwickelt. Zum einen können sie sich über einen Online-Kurs zu praktischen Themen genossenchaftlicher Vereinigungen fortbilden. Zum anderen wird die Website "SuperTienda" zu einem Online Marktplatz mit Bezahlfunktion weiterentwickelt, auf der landesweit Produzenten ihre Produktpalette anbieten sowie direkt an- und verkaufen können. Mithilfe einer Smartphone-Applikation können die Kleinproduzentinnen in Manabí zudem ihre Produkte untereinander - ohne Zwischenhändler - online anbieten sowie an- und verkaufen.
—In San Isidro (Gemeinde Sucre) steigern Produzenten, die in der landwirtschaftlichen Vermaktungsvereinigung „Los Maraciyeros" zusammengeschlossen sind, den Mehrwert in den Wertschöpfungsketten für Inka-Nuss, Ají, Kaffee, Kakao und Mais. Dies erreichen sie über den Erwerb von Maschinen, zum Beispiel für die Weiterverarbeitung der Kaffebohnen zu abgepacktem Kaffee, eine Presse zur Gewinnung von Speiseöl aus der Inka-Nuss (sacha inchi) und eine Mühle zur weiteren Verabeitung der Ají-Schoten zu Saucen.
—Der mehrheitlich von Frauen geführte Gastronomiesektor ist der wirtschaftliche Motor für die Entwicklung in Cojimíes (Gemeinde Pedernales). Die Anbieterinnen entwickeln mit Unterstützung der GIZ ihr organisatorisches und gastronomisches Potenzial, um den Küstenort für Touristen attraktiver zu machen. In Kooperation mit dem Dachverband der Provinzverwaltungen erhalten die Frauen außerdem Ausstattung, wie zum Beispiel Tiefkühltruhen zur hygienischen Aufbewahrung von Fisch und Meeresfrüchten, Entsafter für frische Obstsäfte sowie Töpfe, Pfannen und Geschirr für verbesserten Service ihrer Restaurants, Außerdem gibt es Unterstützung bei der Organisation von Gastronomiefestivals.

Im Bereich Berufliche Bildung fördert der Innovationsfonds diese Projekte:
—In Azuay richtet der nationale Verband der Holzindustrie gemeinsam mit dem staatlichen Berufsausbildungsinstitut eine „Digitale Lernfabrik" (FabLab) im dualen Berufsausbildungsgang "Industrielle Holzverarbeitung" ein. Das Projekt verknüpft erstmals in Ecuador einen dualen Ausbildungsgang mit einer überbetrieblichen Ausbildervereinigung. Innovativ ist auch, dass Azubis im „FabLab" den praktischen Teil ihrer Berufsausbildung in einem digitalen Umfeld - unter anderem mit 3D-Drucker - absolvieren können.
—Der Ausbildungsbetrieb Transoceánica und das ecuadorianische Erziehungsministerium organisieren erstmalig in Ecuador und landesweit Berufsorientierungstage mit Schulen und dualen Ausbildungsbetrieben. An diesen Tagen können sich Schülerinnen und Schüler der Oberstufe über das Spektrum der angebotenen dualen Berufsausbildungsgänge informieren. Mindestens 2500 Schülerinnen und Schüler an 50 Schulen sowie zahlreiche Ausbildungsbetriebe der Privatwirtschaft sollen sich beteiligen.
—Der nationale Verband der Textilindustrie reichte gemeinsam mit dem staatlichen Berufsausbildungsinstitut Sucre einen innovativen Projektvorschlag ein, der eine integrale Ausbildung an einem überbetrieblichen Ausbildungszentrum schaffen soll. Azubis im dualen Ausbildungsgang „Textilproduktion" in der Stadt Quito durchlaufen zukünftig im Rotationsbetrieb verschiedene Unternehmen mit unterschiedlichen Prozessschritten. Zudem wird für die Azubis ein modernes Textil-Laboratorium eingerichtet.

 

 
Project description (EN)

Context

On the basis of its National Development Plan, Ecuador has devised a strategy for revamping its economy. In particular, this strategy seeks to achieve a shift from the focus on exporting primary commodities and exploiting natural resources to engaging in diversified and environmentally efficient production with higher added value. This includes, for example, service industries founded on a knowledge-based economy. The aim is to make greater use in future of the skills and knowledge of the population and of employees in the public sector. For these reform efforts to succeed, it is essential that staff in local administrations develop their skills and expertise, and cooperate more closely with the private sector and organisations in civil society.

Objective

Private and public sector actors cooperate more effectively on implementing innovative projects in the fields of local economic development, employment promotion and dual vocational training.

Approach

The innovation fund was set up to finance projects that foster public-private cooperation in the productive sector. Eligible projects have terms of 6 to 12 months and volumes ranging from EUR 100,000 to EUR 150,000. Two public tenders to date have focused on vocational education/employment promotion and local economic development.

Projects are selected by the Technical Secretariat for International Cooperation (SETECI) in the Ecuadorian Ministry of Foreign Affairs and the German Embassy. Applications are assessed according to an agreed set of criteria and clear selection rules. Special consideration is given to projects that bring about visible results at local level. At the same time, these should also be suitable for transfer to other parts of the country.

GIZ advises interested parties on how to prepare a project proposal and provides close support during project implementation.

Results

The assisted projects are innovative in that they trial new processes, methods and forms of cooperation at local level and with a range of different actors. They have a positive impact on gender and/or generational equality.

Three projects focusing on economic revival and promotion of the local economy are under way in Manabí Province:
—Digital tools aimed at boosting the local economy are being developed for producers in cooperatives in Manabí Province. Producers have the opportunity to complete an online training course on practical issues relating to cooperative associations. In addition, the ‘SuperTienda’ website is being developed into an online marketplace with a payment function. Producers throughout the country can offer their product range on this marketplace and can buy and sell products directly. A smartphone app has also been created that allows small-scale producers in Manabí to offer their products online and to buy and sell products among themselves without involving intermediaries.
—In San Isidro (Sucre canton), producers that belong to the agricultural marketing association ‘Los Maraciyeros’ are successfully increasing the added value in the value chains for Inca nuts, ají chilli peppers, coffee, cocoa and maize. They have accomplished this by acquiring machinery for processing coffee beans into packaged coffee, a press for extracting cooking oil from the Inca nut (sacha inchi) and a mill for processing ají pods into sauces.
—In Cojimíes (Pedernales canton), the catering sector – which is run mainly by women – is the driving force for local economic development. With support from GIZ, local providers are developing their organisational and culinary potential to help attract more tourists to the coastal town. In cooperation with the umbrella organisation for provincial administrations, the women also have access to equipment such as freezers to store fish and other seafood under hygienic conditions, juicers to extract fresh fruit juice, and the pots, pans and dishes they need for their restaurants. They also receive support with organising food festivals.
—The innovation fund also supports the following projects in the area of vocational training:
—In Azuay, the national wood industry association and the state vocational training institute are joining forces to set up a ‘digital learning factory’ (FabLab) for students pursuing dual vocational training in industrial wood processing. For the first time in Ecuador, this project brings together a dual vocational training programme and an industry-wide training association. Another innovative aspect of the project is that trainees in the ‘FabLab’ can complete the practical part of their vocational training in a digital environment equipped with the latest features such as 3D printers.
—The training workplace Transoceánica and the Ecuadorian Ministry of Education are organising Ecuador’s first ever country-wide career-guidance days for schools and training workplaces. At these events, senior pupils can find out about the range of dual vocational training options available. At least 2,500 pupils from 50 schools as well as numerous apprenticing companies from the private sector are expected to take part.
—The national textile industry association and the state vocational education institute in Sucre have jointly submitted an innovative project proposal aimed at providing integrated training at an industry-wide vocational training centre. Apprentices undertaking dual vocational training in textile production in the city of Quito will in future rotate through a number of different companies to learn about different steps in the textile production process. A modern textiles laboratory is also being set up for the apprentices.

 





 
