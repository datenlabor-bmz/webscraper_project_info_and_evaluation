  




 

Nachhaltige Wasserpolitik
Sustainable Water Policy
Project details
Project number:2017.2027.5
Status:Projekt beendet
Responsible Organisational unit: G310 Energie, Wasser, Verkehr
Contact person:Dieter Rothenberger dieter.rothenberger@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh., Burundi, Dem. Rep. Congo, Chile, Egypt, Mali, Niger, Rwanda, Chad
Summary
Objectives:

Ansätze der deutschen Entwicklungszusammenarbeit (EZ) sind in der Öffentlichkeit und der politischen Debatte verankert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:63 502 862 Euro
Financial commitment for this project number:13 443 148 Euro
Cofinancing

Europäische Union (EU): 2 273 000Euro


 
Previous projects
2014.2264.1Internationale Wasserpolitik
Follow-on projects
2020.2147.5Nachhaltige Wasserpolitik
 
Term
Entire project:27.10.2008 - 30.06.2023
Actual project:01.07.2017 - 30.07.2020
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Wassersektorpolitik und -verwaltung

Evaluation
not available
 
Project description (DE)

Ausgangssituation

Eine der zentralen Herausforderungen des 21. Jahrhunderts besteht darin, die Verfügbarkeit ausreichend sauberen Wassers dauerhaft zu sichern. Die Menschenrechte auf Wasser und Sanitärversorgung sind in vielen Regionen der Welt noch nicht verwirklicht. Laut den Vereinten Nationen (UN) fehlt 2,1 Milliarden Menschen der Zugang zu sicherem Trinkwasser. 4,5 Milliarden Menschen verfügen nicht über eine sichere Sanitärversorgung. Bevölkerungswachstum, Wirtschaftswachstum, Klimawandel und Verstädterung erhöhen den Druck auf global und lokal begrenzte Wasserressourcen. Expert*innen gehen davon aus, dass der weltweite Wasserbedarf bis 2050 um über 50 Prozent ansteigt.
Deutschland gehört zu den wichtigsten Gebern und ist in globale Strategieentwicklungs- und Entscheidungsprozesse zur Erreichung der wasserbezogenen Nachhaltigkeitsziele der Agenda 2030 intensiv eingebunden. Dafür orientiert sich die deutsche Entwicklungszusammenarbeit vor allem an der Wasserstrategie des Bundesministeriums für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ). Deren Konzepte und innovativen Ansätze weiterzuentwickeln, umzusetzen und in die internationale Diskussion einzubringen ist eine Kernherausforderung.


Ziel

Das Bewusstsein für die globale Wasserkrise ist gewachsen und Lösungsansätze der deutschen Entwicklungszusammenarbeit werden von vielen Partner*innen weltweit aufgegriffen.
Die integrierte Betrachtung zur Sicherstellung der Wasser-, Energie- und Ernährungssicherheit („Nexus-Ansatz") werden in den Partnerländern und Programmen der Entwicklungszusammenarbeit entwickelt und umgesetzt.


Vorgehensweise

Das Vorhaben unterstützt das BMZ durch fachliche Beratung. Es erarbeitet Konzepte und Ansätze zur Umsetzung der Wasserstrategie. Dazu werden zum Beispiel innovative Modelle zur Finanzierung von Wasser und Sanitärversorgung sowie Ansätze zur Förderung von guter Regierungsführung erarbeitet. Es werden Konzepte zur Rolle von Wasser bei der Anpassung an den Klimawandel und zur Minderung von Treibhausgasen entwickelt, welche den gesamten Wasserkreislauf berücksichtigen. Diese Ansätze unterstützen internationale Prozesse, wobei ein besonderes Augenmerk den Querbezügen zwischen Wasser und anderen Bereichen gilt, wie Landwirtschaft und Energie sowie Migration und Fluchtursachen.
Das Projekt entwickelt Strategien für die Sicherung der langfristigen Wasserverfügbarkeit für alle. Unter anderem unterstützt das Vorhaben das BMZ und die Europäische Union beim regionalen und internationalen Austausch zum Wasser-Energie-Ernährungssicherungs-Nexus (WEF-Nexus). Mit diversen Akteuren und regionalen Organisationen, zum Beispiel in Lateinamerika mit der Wirtschaftskommission für Lateinamerika und die Karibik, im Mittleren Osten und Nordafrika mit der Liga der arabischen Staaten und in Westafrika mit der Nigerbeckenbehörde (ABN), werden politische Dialoge zur Erarbeitung integrierter Lösungsansätze durchgeführt und daraus Politikempfehlungen abgeleitet.
Seit Januar 2019 wurde das Vorhaben um zwei weitere Projekte am Kivu See und Ruzizi Fluss zum nexusbasierten Wassermanagement sowie zur Verbesserung von Sicherheit und Klimaresilienz in fragilen Regionen in der Sahel Zone erweitert.


Wirkungen

Im internationalen Dialog hat das Vorhaben die notwendige Entwicklung neuer Mechanismen zur Förderung und Finanzierung des Zugangs zu sicherer Wasser- und Sanitärversorgung angestoßen und geprägt. In Kooperation mit der KfW, der OECD und der Weltbank wird derzeit ein neuer Fonds erarbeitet, um Mittel effektiver, effizienter und armutswirksamer als bisher einsetzen zu können.
Die engen Bezüge zwischen Wasser und Klima wurden gezielter in den internationalen Diskurs eingebracht. Dadurch ist die entscheidende Bedeutung des Wassermanagements vor allem bei Anpassungs- und Minderungsmaßnahmen bei den jährlichen UN-Klimakonferenzen und der Global Commission on Adaptation verankert worden.
Politikempfehlungen zum Nexus-Ansatz wurden in drei Regionen von Partnern eingeführt. Aktionspläne für die Umsetzung von Investitions-Projekten zur Wasser-, Energie- und Ernährungssicherheit wurden gemeinsam erarbeitet.
Mehr als 450 Teilnehmer*innen wurden im Rahmen des Capacity Buildings geschult. Darin wurden Zusammenhänge zwischen Wasser-, Energie- und Ernährungssicherheit sowie integrierte Lösungsansätze nähergebracht.
 

 
Project description (EN)

Context

One of the central challenges of the 21st century consists in ensuring the permanent availability of sufficiently clean water. In many regions of the world, people are as yet unable to exercise their human rights to water and sanitation. According to the United Nations (UN), 2.1 billion people do not have access to safe drinking water, while 4.5 billion people lack safely managed sanitation. Population growth, economic growth, climate change and urbanisation increase the pressure on globally and locally limited water resources. Experts assume that the worldwide demand for water will increase by more than 50 per cent by 2050.
Germany is one of the most important donors and is heavily involved in global strategy development and decision-making processes for achieving the water-related Sustainable Development Goals of the 2030 Agenda. German development cooperation focuses here primarily on the water strategy of the German Federal Ministry for Economic Cooperation and Development (BMZ). One key challenge lies in developing and implementing BMZ’s concepts and innovative approaches and including them in the international discussion.


Objective

There is greater awareness of the global water crisis, and the approaches proposed by German development cooperation are being adopted by many partners around the world. The integrated approach to securing water, energy and food (nexus approach) is being developed in the partner countries and development cooperation programmes.


Approach

The project supports BMZ by providing expert advice, in addition to devising strategies and approaches for implementing the water strategy. To achieve this, innovative models for financing water and sanitation, as well as approaches to encourage good governance, are being developed, for example. Strategies focusing on the role of water in adapting to climate change and reducing greenhouse gases are being developed that take account of the entire water cycle. These approaches support international processes, with particular attention paid to the interrelationships between water and other areas, such as agriculture and energy, or migration and the root causes of displacement.
The project develops strategies for securing long-term water availability for all. Among other things, the project supports BMZ and the European Union (EU) in regional and international exchange on the water-energy-food nexus (WEF nexus). Political discourse with the aim of developing integrated approaches is conducted with diverse actors and regional organisations, for example in Latin America with the Economic Commission for Latin America and the Caribbean (ECLAC), in the Middle East and North Africa with the Arab League and in West Africa with the Niger Basin Authority (ABN). Policy recommendations will then be derived from this discourse.
In January 2019, two further projects were added: one at Lake Kivu and the Ruzizi River for nexus-based water management, the other to improve security and climate resilience in fragile regions of the Sahel zone.


Results

In international dialogue, the project has triggered and shaped the development of new mechanisms required to support and finance access to safe water and sanitation. In cooperation with KfW Development Bank, the Organisation for Economic Co-operation and Development (OECD) and the World Bank, a new fund is currently being developed to enable resources to be deployed more effectively, efficiently and in a manner better able to combat poverty.
The international discourse specifically addressed the close relationship between water and climate. As a result, the crucial importance of water management, particularly in adaptation and mitigation measures, has been firmly embedded in the annual UN Climate Change Conferences and the Global Commission on Adaptation.
Policy recommendations for the nexus approach were introduced by partners in three regions. Action plans for implementing investment projects for water, energy and food security were drawn up jointly.
More than 450 participants were provided with training as part of capacity-building measures. Participants were taught about the relationship between water, energy and food security and about integrated approaches.
 





 
