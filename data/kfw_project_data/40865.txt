Verbesserung ländlicher und kleinstädtischer Wasser- und Sanitärversorgung in den Regionen Mopti, Koulikoro und Kayef IV

Kernproblem
Die Unterversorgung der Bevölkerung Malis mit hygienisch einwandfreiem Trinkwasser und die unzureichende Verfügbarkeit sanitärer Einrichtungen stellen zentrale Entwicklungshemmnisse für den malischen Staat dar. Die Bereitstellung von Wasser- und Sanitärversorgung im ländlichen und kleinstädtischen Bereich hat in den letzten Jahren nachgelassen. Ursache hierfür sind mangelnde Kapazitäten und fehlendes Zusammenspiel der dekonzentrierten Strukturen, ausgeprägte regionale Versorgungsdefizite, ein starkes Bevölkerungswachstum kombiniert mit krisenbedingter Binnenmigration sowie ein zwischenzeitiger, ebenfalls krisenbedingter Investitionsstopp.

Modulziel
Das Vorhaben soll den Zugang der armen, ländlichen und kleinstädtischen Bevölkerung in Mali zu hygienisch unbedenklichem Trinkwasser und angemessener Sanitärversorgung verbessern. Ziel ist die nachhaltige Sicherstellung einer ganzjährigen Versorgung der in der Programmregion in Klein- und Mittelstädten ansässigen Bevölkerung mit hygienisch einwandfreiem Wasser und angemessenen Sanitäranlagen.
Ziel ist es, bis 2025 den globalen Zugang zur Wasserversorgung in Mali zu ermöglichen. Mit der FZ-Maßnahme wird in effizienter Weise gemäß den Vorgaben zu den Quantifizierbaren Versorgungszielen (QVZ) zur Erreichung der Weltentwicklungsziele (MDG) beigetragen.

Wesentliche Outputs
Das Vorhaben umfasst ein Bohrprogramm, den Neubau und die Rehabilitierung von Wasserversorgungssystemen, die Einrichtung von Basissanitärlösungen sowie Begleitmaßnahmen, Informations- und Sensibilisierungs-Maßnahmen.

Zielgruppe
Zielgruppe ist die Bevölkerung von Klein- und Mittelstädten mit einer Einwohnerzahl zwischen 2.000 und 30.000 insbesondere in den malischen Regionen Kayes und Koulikoro.

Beitrag zur nationalen Umsetzung der Agenda 2030
- Keine Armut
- Sauberes Wasser und Sanitäreinrichtungen
- Nachhaltige Städte und Gemeinden

Land / Region / Institution Mali Nummer 40865 Schwerpunkt Wasser und Abfall Sektor 14030 - Trinkwasser, Sanitär u Abwasser-Grundl Vers Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 12 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner DIRECTION NATIONALE DE L'HYDRA ULIQUE MIN.DE L'ENERGIE /L'EAU Zuständige Abteilung Westafrika
