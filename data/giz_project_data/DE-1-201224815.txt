  




 

Bankenförderung und Finanzsystementwicklung
Banking and Financial Systems Development
Project details
Project number:2012.2481.5
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Armin Hofmann armin.hofmann@giz.de
Partner countries: Myanmar, Myanmar
Summary
Objectives:

Der formelle Bankensektor in Myanmar wird dabei unterstützt, kleinen und mittleren Unternehmen nachhaltige und bedarfsgerechte Kredite zur Verfügung zu stellen.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Planning and Finance

Financing organisation:

not available

 
Project value
Total financial commitment:18 350 000 Euro
Financial commitment for this project number:8 791 936 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
2016.2135.8Bankenförderung und Finanzsystementwicklung
 
Term
Entire project:01.10.2012 - 07.10.2021
Actual project:01.10.2012 - 28.07.2017
other participants
Arge Icon-Sparkassenstiftung-
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Finanzsektorpolitik und -verwaltung

Evaluation
Projektevaluierung: Kurzbericht. Myanmar: Bankenförderung und Finanzsystementwicklung
Project evaluation: summary report. Myanmar: Banking and Financial Sector Development
 
Project description (DE)

Ausgangssituation

Seit Beginn der Öffnung Myanmars 2011 leitete die Regierung Reformschritte in der Wirtschaftspolitik ein, um einen marktwirtschaftlichen Strukturwandel zu vollziehen. Die im März 2016 vereidigte neue Regierung mit Aung San Suu Kyi und Präsident Htin Kyaw wird diesen Wandel voraussichtlich fortführen und intensivieren. Kleine und mittlere Unternehmen (KMU) spielen für die nachhaltige Wirtschaftsentwicklung dabei eine zentrale Rolle. Sie stellen auch in Myanmar die breite Mehrheit der privaten Betriebe und sind von ausschlaggebender Bedeutung für den Arbeitsmarkt. Ein zentrales Wachstumshindernis für KMU ist der mangelnde Zugang zu adäquaten Finanzdienstleistungen. Der Finanzsektor ist generell stark entwicklungsbedürftig und zudem nur unzureichend an die Bedürfnisse von KMU angepasst. Ein nachhaltiges und bedarfsorientiertes Angebot an Finanzdienstleistungen für KMU existiert nicht. Wesentliche Ursachen hierfür sind die einschränkende Regulierung des Bankensektors sowie die institutionellen und fachlichen Mängel in den Geschäftsbanken.


Ziel

Der formelle Bankensektor stellt kleinen und mittleren Unternehmen nachhaltige und bedarfsgerechte Kredite zur Verfügung.


Vorgehensweise

Das Projekt unterstützt lokale Partnerinstitutionen auf verschiedenen Ebenen des Finanzsektors. Dies reicht von der Unterstützung der Zentralbank bei der Schaffung förderlicher regulativer und aufsichtsrechtlicher Rahmenbedingungen über Förderung von Trainingsanbietern für den Bankenmarkt bis zur Kooperation mit einzelnen Banken zur Pilotierung von KMU-Finanzierungsansätzen.


Wirkungen

Mit Unterstützung des Projektes wurde das Bankengesetz überarbeitet und Anfang 2016 vom Parlament verabschiedet. Das Gesetz schafft die Basis für eine schrittweise Liberalisierung des Bankenwesens und damit auch für eine umfassende und nachhaltige Bedienung von KMU durch den Bankensektor.
Die Partnerbanken des Projektes konnten ihre Kreditportfolien für kleine und mittlere Unternehmen verdreifachen. Insgesamt haben aktuell über 12.000 KMU Zugang zu Krediten, beinahe die Hälfte von ihnen ist im ländlichen Raum angesiedelt. Außerdem unterstützt das Projekt seine Partner, wie den lokalen Bankenverband oder die Yangoner Wirtschaftsuniversität, durch umfassende Trainingsangebote zur Personalentwicklung. Bis Februar 2016 wurden mehr als 11.000 Teilnehmer-Trainingstage angeboten.
Der Bankenverband bietet regelmäßig einen Zertifizierungskurs zur Kreditvergabe an kleine und mittlere Unternehmen an. Einige Geldinstitute bewerben sich beim Projekt mit eigenen Vorschlägen um Unterstützung und tragen große Teile der entstehenden Kosten selbst. Ihre positiven Erfahrungen und ihr Know-how stellen sie dem gesamten Finanzsektor zur Verfügung. Die Yangoner Wirtschaftsuniversität überarbeitet den Master-Studiengang zum Bankenwesen; das Projekt berät sie dabei.
Das Banking Sector Financial Reporting Standards Implementation Committee (BFRIC) hat mithilfe des Projektes seine Arbeit im Januar 2016 aufgenommen. Die Kommission will Hilfestellung bei der Einführung internationaler Standards für die Rechnungslegung von Banken leisten. Das Projekt berät den Ausschuss.
 





 
