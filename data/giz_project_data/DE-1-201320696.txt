  




 

Förderung von Initiativen der Sozialen Integration und Transformation (FLICT)
Facilitating Local Initiatives for Social Cohesion and Transformation (FLICT)
Project details
Project number:2013.2069.6
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Christoph Feyen christoph.feyen@giz.de
Partner countries: Sri Lanka, Sri Lanka
Summary
Objectives:

Staatliche und nicht-staatliche Schlüsselakteure arbeiten zusammen, um Kernelemente sozialer Integration in die Praxis umzusetzen

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of National Languages and Social Integration

Financing organisation:

not available

 
Project value
Total financial commitment:20 179 144 Euro
Financial commitment for this project number:3 983 162 Euro
Cofinancing

Europäische Union (EU) - alt, bis 31.12.2011: 869 221Euro


 
Previous projects
2010.2193.0Förderung von Initiativen zur sozialen Integration und Transformation
Follow-on projects
not available
 
Term
Entire project:14.08.2002 - 29.01.2018
Actual project:01.01.2014 - 29.01.2018
other participants
not available
 
Contact
Project websiteswww.flict.org
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungSelbsthilfeorientierte Armutsbekämpfung
CRS code
Zivile Friedensentw., Krisenpräv. und Konfliktlösung

Evaluation
Sri Lanka: Förderung von Initiativen zur sozialen Integration und Transformation (FLICT). Projektevaluierung: Kurzbericht
 
Project description (DE)

Ausgangssituation
2002 vereinbarten die sri-lankische Regierung und die Liberation Tigers of Tamil Eelam (LTTE) einen Waffenstillstand. In dieser Phase des Bürgerkrieges startete das Vorhaben zur Förderung von Initiativen der Integration und Transformation (FLICT). Ziel war es, durch die Unterstützung zivilgesellschaftlicher Akteure zur Konflikttransformation beizutragen. Allerdings wurde die Waffenruhe über die Jahre gebrochen und die Kampfhandlungen eskalierten erneut. Im März 2009 wurde der Bürgerkrieg durch die Regierung gewaltsam beendet. Die Grundursachen des Konfliktes sind jedoch nicht verschwunden und neue Konfliktlinien sind hinzu gekommen. Politische und ökonomische Benachteiligung verschiedener ethnischer, religiöser und sozialer Bevölkerungsgruppen haben zu ungleicher Entwicklung und struktureller Diskriminierung geführt.

Ziel
Staatliche und nichtstaatliche Schlüsselakteure setzen gemeinsam Kernelemente sozialer Integration in die Praxis um.

Vorgehensweise
Das Ministerium für nationale Sprachen und soziale Integration (MNLSI) hat das Mandat, die soziale Integration in Sri Lanka zu fördern und zur Versöhnung beizutragen. Die GIZ unterstützt das Partnerministerium bei der Politikformulierung, der Umsetzung der Politik auf lokaler Ebene sowie bei der Personalentwicklung.

Partner aus Kunst und Kultur werden dabei unterstützt, mit ihren Methoden das Thema soziale Integration zu bearbeiten und zur Sensibilisierung beizutragen. Das Vorhaben fördert gleichzeitig Vernetzung und Dialog zwischen und innerhalb der verschiedenen Ebenen von Staat, Zivilgesellschaft und Privatsektor, um Politikstrategien landesweit zu verankern und die Erfahrungen bei der Umsetzung in die politischen Prozesse einfließen zu lassen.

Wirkungen
Ein besonderer Erfolg für FLICT war das Inkrafttreten der National Policy for Social Integration (NPSI) 2012, deren Formulierung vom Vorhaben beratend unterstützt wurde.

FLICT nimmt in 5 Pilotdistrikten die Rolle des Intermediärs und Moderators zwischen staatlichen und nichtstaatlichen Akteuren ein. So wurden in den letzten 3 Jahren 16 interreligiöse Dialogveranstaltungen mit rund 3.000 Teilnehmenden durchgeführt.

Zurzeit unterstützt FLICT die Umsetzung von 54 Kleinstmaßnahmen durch technische Beratung und finanzielle Beiträge. Zu den Maßnahmen gehören Bau und Rehabilitierung von Schulen und Gemeindezentren, die insbesondere marginalisierten Gruppen zugutekommen.

Das Vorhaben hat kontinuierlich innovative, erfolgreiche Konzepte zur Förderung sozialer Integration und zur Entwicklung von Kompetenzen verschiedener Akteure entwickelt, darunter der sehr gefragte Trainingskurs Art of Coexistence, der seit 2011 25-mal mit insgesamt über 500 Teilnehmenden durchgeführt wurde.
 

 
Project description (EN)

Context
The Sri Lankan Government and the Liberation Tigers of Tamil Eelam (LTTE) agreed to a ceasefire in 2002. It was at this stage in the civil war that the Facilitating Initiatives for Social Cohesion and Transformation project (FLICT) was launched. The objective was to help bring about conflict transformation by supporting civil-society actors. Over the years, however, the ceasefire broke down and hostilities began to escalate again. In March 2009, the government forcibly ended the civil war. The fundamental causes of the conflict have never disappeared, though, and new conflicts have emerged. Political and economic discrimination between different ethnic, religious and social groups has led to unequal development and structural discrimination.

Objective
Key state and civil-society actors are working together to implement the core elements of social integration on a practical level.

Approach
The Ministry of National Languages and Social Integration (MNLSI) has a mandate to promote social integration in Sri Lanka and help bring about reconciliation. GIZ is supporting the partner ministry in formulating policies, implementing them at local level and developing human resources.

Partners from the field of art and culture are being given support to work on the topic of social integration using their own methods and thereby help to make the population more aware of the issues. At the same time, the project is also supporting networking and dialogue between and within the different levels of the state, civil society and the private sector, with the aim of putting policy strategies on a solid national footing and ensuring that experience gained during implementation is fed back into the policy-making process.

Results
One of FLICT’s biggest successes was the introduction of the National Policy for Social Integration (NPSI) in 2012. The project provided advice on formulating this policy.

In five pilot districts, FLICT has taken on the role of intermediary and moderator between state and civil-society actors, with 16 inter-religious dialogue events held over the last three years for around 3,000 participants.

FLICT is currently supporting the implementation of 54 micro-measures by offering technical assistance and financial contributions. These measures include building and renovating schools and community centres, which particularly benefits marginalised groups.

The project has continuously developed innovative and successful concepts for promoting social integration and improving the skills of various actors, including the very popular Art of Coexistence training course, which has been run 25 times since 2011 and been attended by a total of over 500 participants.
 





 
