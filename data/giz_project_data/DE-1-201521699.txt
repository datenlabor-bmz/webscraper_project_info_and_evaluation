  




 

Integrierte Planung und Energieeffizienz zur Stärkung der Anwendung von Klimatechnologien
Integrated planning and energy efficiency to strengthen the application of climate technologies
Efficacité Energétique au Maroc
Project details
Project number:2015.2169.9
Status:Projekt beendet
Responsible Organisational unit: 3600 Nordafrika
Contact person:Fatiha EL-MAHDAOUI fatiha.el-mahdaoui@giz.de
Partner countries: Morocco, Morocco
Summary
Objectives:

Diverse Akteure in Marokko sowie verbesserte Rahmenbedingungen tragen zur Umsetzung der Energieeffizienzstrategie bei.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministère de la Transition Energétique et du Développement Durable

Financing organisation:

not available

 
Project value
Total financial commitment:5 000 000 Euro
Financial commitment for this project number:5 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:14.10.2016 - 30.01.2021
Actual project:14.10.2016 - 30.01.2021
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektziel-Ebene: Projekt zielt vor allem auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
Armutsorientierungnot available
CRS code
Energieeinsparung und Effizienz (Nachfrageseite)

Evaluation
Zentrale Projektevaluierung - Kurzfassung. Energieeffizienz in Marokko (DKTI IV)
Évaluation centrale de projet - Synthèse. Efficacité énergétique au Maroc (DKTI IV)
Zentrale Projektevaluierung - Auf einen Blick. Energieeffizienz in Marokko (DKTI IV)
Évaluation centrale de projet - En bref. Efficacité énergétique au Maroc (DKTI IV)
Évaluation centrale de projet. Initiative allemande pour les technologies favorables au climat : Efficacité énergétique au Maroc (DKTI IV). Rapport d'évaluation
 




 
