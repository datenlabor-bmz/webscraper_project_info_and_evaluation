  




 

Unterstützung landwirtschaftlicher Produktion und Qualitätssicherung
Support of Agricultural Production and Quality Assurance
Project details
Project number:2019.2248.3
Status:laufendes Projekt
Responsible Organisational unit: 1500 Ostafrika
Contact person:Dr. Gabriele Maria Prinz gabriele.prinz@giz.de
Partner countries: Somalia, Somalia
Summary
Objectives:

Die Rahmenbedingungen für die Steigerung der Produktion qualitativ hochwertiger Produkte in Somalia sind verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Planning, Investment and Economic Development (Ministerium für Planung, Investment und wirtschaftl. Entwickl

Financing organisation:

not available

 
Project value
Total financial commitment:10 100 000 Euro
Financial commitment for this project number:10 100 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:08.03.2021 - 31.03.2024
Actual project:01.04.2021 - 31.03.2024
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Landwirtschaftspolitik und -verwaltung

Evaluation
not available
 




 
