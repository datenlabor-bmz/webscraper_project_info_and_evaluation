  




 

Gesundheitsprogramm/Kampf gegen die Müttersterblichkeit
Health programme - Fight against maternal mortality
Project details
Project number:2016.2031.9
Status:Projekt beendet
Responsible Organisational unit: 1400 Zentralafrika
Contact person:Josselin Guillebert josselin.guillebert@giz.de
Partner countries: Cameroon, Cameroon
Summary
Objectives:

Die Versorgung der Bevölkerung mit Gesundheitsdiensten guter Qualität ist verbessert, insbesondere in sexueller und reproduktiver Gesundheit und Rechten (SRGR).

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministère de Santé

Financing organisation:

not available

 
Project value
Total financial commitment:42 313 476 Euro
Financial commitment for this project number:7 400 000 Euro
Cofinancing

not available

 
Previous projects
2013.2140.5Gesundheitsprogramm - Kampf gegen die Müttersterblichkeit
Follow-on projects
2019.2072.7Unterstützung des Gesundheitssystems und der Familienplanung für die Resilienz in Kamerun
 
Term
Entire project:21.12.2011 - 31.01.2024
Actual project:01.01.2018 - 30.01.2021
other participants
Arge GOPA Wordwide-Health Focus GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungSonstige unmittelbare Armutsbekämpfung
CRS code
Förderung reproduktiver Gesundheit

Evaluation
not available
 
Project description (DE)

Gesundheitsprogramm / Kampf gegen die Müttersterblichkeit in Kamerun (PASaR III)

Ausgangssituation

Die Stärkung des Gesundheitssystems kommt in Kamerun nur langsam voran. Der Zugang zu einer qualitativ hochwertigen Gesundheitsversorgung ist, besonders für Frauen und Kinder, nach wie vor begrenzt. So nutzen nur 15,8% (EDS 2018) der kamerunischen Paare moderne Verhütungsmittel und die Müttersterblichkeit ist sehr hoch (596/100000; EDS 2015). Neben den soziokulturellen Hemmnissen werden folgende Faktoren genannt: begrenzte Verfügbarkeit von Kontrazeptiva, eine unzureichende technische Ausstattung, die niedrige Qualität der in den Gesundheitseinrichtungen angebotenen Dienste sowie ein Mangel an qualifiziertem Gesundheitspersonal.

Ziel

Die Versorgung der Bevölkerung mit Gesundheitsdiensten in guter Qualität ist verbessert, insbesondere in Sexueller und Reproduktiver Gesundheit und Rechte (SRGR).

Vorgehensweise

Das PASaR-Vorhaben ist insbesondere in drei Regionen tätig: Adamaoua, Westen und Süd-Westen und arbeitet mit dem Gesundheitsministerium (MINSANTE) und insbesondere mit den entsprechenden Regionaldelegationen (DRSP) zusammen. Um die Mütter- und Kindersterblichkeit zu reduzieren, zielen die Hauptaktivitäten auf eine Stärkung der Dienste zur Familienplanung und der sexuellen und reproduktiven Gesundheit und Rechte. Hierbei wird ein besonderes Augenmerk auf die Zielgruppe der Jugendlichen gelegt.
Zur Stärkung des Gesundheitssystems werden die Kompetenzen des Gesundheitspersonals durch einen strukturierten Qualitätsmanagementprozess erweitert. Nationale Ausbildungseinrichtungen wie Universitäten und Gesundheitsstrukturen sind ebenso Teil des Prozesses wie die Bevölkerung. Der Ansatz stärkt bewusst die Eigeninitiative des Gesundheitspersonals und befasst sich sowohl mit klinischen als auch mit organisatorischen Aspekten.
Die Aktivitäten zur Förderung der Gesundheit auf Gemeindeebene werden über die Regionalfonds zur Gesundheitsförderung (FRPS) unterstützt, die den Gewinn aus dem Verkauf von Medikamenten zur Umsetzung von gesundheitsfördernden Aktivitäten in den Gemeinden nutzen. Die deutsche technische Zusammenarbeit konzentriert sich in ihrer Beratung auf administrative, organisationsstärkende und logistische Kapazitäten der FRPS sowie auf eine verbesserte Koordination zwischen den Regionalfonds.
Ein weiterer Schwerpunkt zur Stärkung des Gesundheitssystems ist die Unterstützung der Partner in der Nutzung des kamerunischen Gesundheitsdateninformationssystems, dessen Daten auch das Gesundheitsprogramm für das Projektmonitoring nutzt.

Ergebnisse

Familienplanung:
Etwa 500 Gesundheitseinrichtungen wurden mit Material für Aktivitäten der Familienplanung ausgestattet und darin fortgebildet, 130 Gesundheitsfachkräfte wurden im Einsetzen und Entfernen von Spiralen und Implantaten sowie 25 Ausbilder für Familienplanung wurden weitergebildet und zertifiziert.

Durch die gezielte Einbindung verschiedener Kommunikationsmedien wird die Bevölkerung für das Thema sexuelle und reproduktive Gesundheit und Rechte sensibilisiert.

Seit 2012 hat sich die Nutzung von modernen Familienplanungsmethoden in den Einsatzregionen vervierfacht.


Stärkung von Personalressourcen und Qualitätsmanagement:
30 Teams aus Gesundheitseinrichtungen in der Westregion und 12 Teams in der Region Adamaoua nehmen am Prozess "Challenge Qualité" teil;

Auf nationaler Ebene wird damit begonnen, gemeinsam mit anderen Partnern das Thema Qualitätsstandards aufzugreifen und Synergien mit anderen Instrumenten (Performance-Based-Financing, Chèque Santé) sicherzustellen.

Regionalfonds für Gesundheitsförderung (FRPS):
Die FRPS gelten landesweit als Modell für die dezentrale Verwaltung und den Vertrieb von Medikamenten und insbesondere für einen besseren Zugang zu Kontrazeptiva zu einem erschwinglichen Preis für die Bevölkerung.
Durch die Fonds wurde eine Koordinationsplattform für die Programme der anderen technischen und finanziellen Partner (KfW, AFD und Weltbank) geschaffen.

Die Integration von Querschnittsthemen wie Gleichberechtigung der Geschlechter, HIV, sexuelle und reproduktive Rechte sorgen für eine umfassende Ausrichtung der Aktivitäten.
 

 
Project description (EN)

Health Programme / Fight against maternal mortality in Cameroon (PASaR III)

Baseline situation

The strengthening of the health system in Cameroon is progressing only slowly. Access to high-quality health care remains limited, especially for women and children. For example, only 15,8% (EDS 2018) of Cameroonian couples use modern contraceptives and maternal mortality is high (596/100000; EDS 2015). In addition to socio-cultural barriers, the following factors are mentioned: limited availability of contraceptives, inadequate technical equipment, the low quality of the services offered in the health facilities and a lack of qualified health personnel.

Goal

The provision of good quality health services to the population has been improved, especially in sexual and reproductive health and rights (SRGR).

The approach

The PASaR project is particularly active in three regions: Adamaoua, West and South-West and works with the Ministry of Health (MINSANTE), in particular with the relevant regional delegations (DRSP). In order to reduce maternal and child mortality, the main activities are aimed at strengthening family planning services and sexual and reproductive health and rights. Special attention is paid to the target group of young people.
In order to strengthen the health system, the competencies of health personnel are expanded through a structured quality management process. National training institutions such as universities and health structures are a part of the process as well as the population. The approach deliberately strengthens the initiative of health personnel and deals with both clinical and organisational aspects.
Health promotion activities at community level are supported by the Regional Promotion Funds for Health Promotion (FRPS), which uses the proceeds from the sale of medicines to implement health promotion activities in the communities. German technical cooperation focuses its advisory services on the administrative, organisational and logistical capacities of the FRPS and on improved coordination between the regional funds.
Another priority for strengthening the health system is to support the partners in the use of the Cameroonian health data information system, the data of which is also used by the health programme for project monitoring.

Family planning:

Some 500 health facilities have been equipped with and trained in family planning activities, 130 health professionals have been trained in the insertion and removal of spirals and implants, and 25 family planning trainers have been trained and certified.

Through the targeted integration of various communication media, the population is made aware of the issue of sexual and reproductive health and rights.

Since 2012, the use of modern family planning methods has quadrupled in the regions of operation.

Strengthening human resources and quality management:

30 teams from health facilities in the western region and 12 teams in the Adamaoua region participate in the quality management process;

At national level, work is underway to address the issue of quality standards together with other partners and to ensure synergies with other instruments (performance-based financing, Chèque Santé).

Regional Funds for Health Promotion (FRPS):

The FRPS are regarded throughout the country as a model for decentralised management and distribution of medicines and, in particular, for better access to contraceptives at an affordable price for the population.

The funds created a coordination platform for the programmes of the other technical and financial partners (KfW, AFD and World Bank).


The integration of cross-cutting issues such as gender equality, HIV, sexual and reproductive rights ensures a comprehensive orientation of activities.
 





 
