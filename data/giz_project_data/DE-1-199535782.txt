  




 

Studien- und Fachkräftefonds
Studies and Experts Fund Uganda
Project details
Project number:1995.3578.2
Status:Projekt beendet
Responsible Organisational unit: 1500 Ostafrika
Contact person:Christian Schnurre christian.schnurre@giz.de
Partner countries: Uganda, Uganda
Summary
Objectives:

Vorbereitung und Prüfung von Vorhaben der Technischen Zusammenarbeit (TZ), Finanzierung von Studien, Gutachten sowie Durchführung von TZ-Maßnahmen geringen Umfangs.

Client:

BMZ

Project partner:

Ministry of Finance

Financing organisation:

not available

 
Project value
Total financial commitment:9 368 728 Euro
Financial commitment for this project number:8 032 188 Euro
Cofinancing

not available

 
Previous projects
1991.2134.4Studien- und Fachkräftefonds
Follow-on projects
not available
 
Term
Entire project:26.06.1980 - 30.11.2017
Actual project:05.12.1994 - 30.11.2017
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Multisektorale Hilfe

Evaluation
not available
 




 
