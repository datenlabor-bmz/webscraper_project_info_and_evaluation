  




 

Sektorvorhaben One Health
Sector Programme One Health
Project details
Project number:2020.2249.9
Status:laufendes Projekt
Responsible Organisational unit: G110 Gesundheit, Bildung, Soziales
Contact person:Nils Gade nils.gade@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh., ZZZ
Summary
Objectives:

Beratung des Bundesministeriums für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) und Agenda-Setting One Health.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:6 000 000 Euro
Financial commitment for this project number:6 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:03.11.2020 - 31.03.2024
Actual project:01.01.2021 - 31.03.2024
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Gesundheitspolitik und Verwaltung des Gesundheitswesen

Evaluation
not available
 




 
