  




 

Kommunales Resourcenmanagement
Community-based natural resource management
Project details
Project number:2015.2209.3
Status:Projekt beendet
Responsible Organisational unit: 1300 Südliches Afrika
Contact person:Klemens Riha klemens.riha@giz.de
Partner countries: Namibia, Namibia
Summary
Objectives:

Die kohärente Umsetzung eines gemeindebasierten Managements natürlicher Ressourcen (CBNRM) ist auf allen Ebenen (national, regional, lokal) verbessert.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Environment and Tourism

Financing organisation:

not available

 
Project value
Total financial commitment:6 800 000 Euro
Financial commitment for this project number:6 800 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:25.11.2016 - 30.05.2021
Actual project:01.01.2017 - 30.05.2021
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektziel-Ebene: Projekt zielt vor allem auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Biodiversität

Evaluation
not available
 
Project description (DE)

Ausgangssituation

Mit der kommunalen Verwaltung von natürlichen Ressourcen (Community-Based Natural Resource Management, CBNRM) werden die Voraussetzungen für die aktive Bewirtschaftung der Ökosysteme durch die Gemeindegebiete geschaffen. Die Anwohner der registrierten Hegegebiete dürfen durch touristische Angebote, die nachhaltige Nutzung der Wildbestände und die Ernte von Naturprodukten innerhalb der Hegegebiete ein Einkommen erwirtschaften. Durch die Möglichkeit, einen Nutzen aus ihrer natürlichen Umwelt zu ziehen, besteht bei den Anwohnern ein starker Anreiz, ihr Hegegebiet zu schützen und zu erhalten. Derzeit sind insgesamt 86 Hegegebiete, ein Verein und 32 Gemeindewälder offiziell registriert und dokumentiert. Sie bieten rund 230.000 Menschen aus ländlichen Gebieten die Möglichkeit, ein eigenes Einkommen zu erzielen. Dabei hat der Tourismus die Trophäenjagd inzwischen als Haupteinnahmequelle abgelöst.

Ziel

Die CBNRM-Strategie wird auf nationaler, regionaler und lokaler Ebene besser umgesetzt.

Vorgehensweise

Im Auftrag des Bundesministeriums für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) unterstützt die GIZ das namibische Ministerium für Umwelt und Tourismus (Ministry of Environment and Tourism, MET) dabei, die CBNRM-Strategie besser umzusetzen. Dazu hat die GIZ das vorliegende Projekt auf den Weg gebracht, das einen Mehrebenenansatz verfolgt. Es richtet sich auf nationaler, regionaler und lokaler Ebene an rund 100.000 Einwohner von Hegegebieten und Gemeindewäldern in den Regionen Kavango und Kunene sowie in den Regionen in Nord- und Zentral-Namibia. Auf nationaler Ebene wird das gesamte CBNRM-Programm durch Politik- und Rechtsberatungsleistungen unterstützt.

•Ausbau von Kompetenzen

Durch das Projekt wird die erfolgreiche Umsetzung der CBNRM-Strategie unterstützt. Dabei werden insbesondere die Kompetenzen des MET im Bereich der Compliance-Überwachung ausgebaut. Dafür wird ein webbasiertes Compliance-Managementsystem eingeführt, über das die Regionalzweigstellen des MET in Echtzeit mit dem Hauptsitz des Ministeriums verbunden sind. Darüber hinaus fördert das Projekt die Formalisierung der Zusammenarbeit zwischen dem MET, den ausführenden Organen (NROs), den Projektpartnern in der Privatwirtschaft und den kommunalen Hegegebieten.

•Verantwortungsvolle Regierungsführung

Im Rahmen des Projekts werden die Hegegebiete dabei unterstützt, die Prinzipien guter Regierungsführung einzuhalten. Dazu gehören transparente und effiziente Entscheidungsprozesse, die Umsetzung der Hegegebietsverfassungen, eine transparente und effiziente Finanzverwaltung, die Gleichstellung aller Geschlechter, die nachhaltige Verwaltung des Wildbestands sowie die faire Versorgung aller Bürger*innen. Die Hegegebiete erhalten außerdem Unterstützung bei der Übermittlung ihrer Daten aus dem Compliance-Managementsystem sowie bei Maßnahmen zur Verbesserung ihrer Anpassungsfähigkeit der an den Klimawandel.

•Von der Natur leben

Namibia ist das trockenste Land südlich der Sahara; die Niederschlagsmengen sind unvorhersehbar. Trotzdem bietet das Land Lebensraum für unzählige Wildtiere und einheimische Pflanzen, die von der Bevölkerung der Hegegebiete als wichtige Einkommensquelle genutzt werden könnten. Um den Auswirkungen des Klimawandels entgegenzuwirken, wird die Diversifizierung der Einkommensquellen und die Entwicklung von Wertschöpfungsketten auf Grundlage der natürlichen Ressourcen gefördert. Eines der Projektziele ist es, Investitionen zu erhöhen und auf lokalen und internationalen Märkten mit dem Vertrieb einheimischer Produkte Fuß zu fassen. So sollen die Erträge der Hegegebiete gesteigert werden. Zusätzlich bietet das Projekt Wirtschafts- und Rechtsberatungsleistungen für Hegegebiete, die Tourismus-, Jagd-, oder BioTrade-Verträge mit Partnern in der Privatwirtschaft abschließen wollen. Außerdem werden Monitoringmaßnahmen etabliert, die sicherstellen, dass beide Parteien die vertraglichen Abmachungen erfüllen.

Wirkungen

• Rund 228.000 Bewohner*innen von kommunalen Hegegebieten wurden dabei unterstützt, ihre natürlichen Ressourcen aktiv selbst zu verwalten.
• Ein webbasiertes Compliance-Managementsystem für CBNRM wurde entwickelt und eingeführt – es wurden bereits mehr als 6.700 Dokumente digitalisiert.
• Im Tourismus- und Jagdbereich wurden neue Joint-Venture-Verträge abgeschlossen, von denen mehr als 60.000 Gemeindemitglieder profitieren.
• Die Strategie zum Umgang mit Konflikten zwischen Menschen und Wildtieren (Human Wildlife-Conflict, HWC) wurde überarbeitet, und es wurden entsprechende Leitlinien entwickelt.
• Es wurden Maßnahmen zur Anpassung an den Klimawandel durchgeführt, und das Vorhaben hat neue Ansätze zur Minderung von HWC-Vorfällen sowie zur Steigerung der Anpassungsfähigkeit der Gemeindegebiete umgesetzt.
• Die Einkommensbasis der Erntearbeiter*innen wurde verbessert. Dies gelang durch die Steigerung der Erträge, die in den Hegegebieten aus einheimischen natürlichen Ressourcen, besonders der Teufelskralle und Myrrhe, gewonnen wurden
 

 
Project description (EN)

Context

Community Based Natural Resource Management (CBNRM) creates an environment where people in communal areas can actively manage their ecosystems. Within the boundaries of formally registered conservancies, residents are allowed to generate revenue through tourism, the sustainable use of wildlife and from harvesting natural products. The opportunity to derive benefits from their natural environment provides a strong incentive for conservancies to protect and safeguard their environment. Today, a total of 86 conservancies, one association and 32 community forests are registered and documented, benefiting about 230,000 rural people. Meanwhile, tourism has replaced trophy hunting as the main source of income.

Obejctive

The coherent implementation of the CBNRM policy has improved at the national, regional and local levels.

Approach

Commissioned by the German Federal Ministry for Economic Cooperation and Development (BMZ), Deutsche Gesellschaft für Internationale Zusammenarbeit (GIZ) GmbH supports the Namibian Ministry of Environment and Tourism (MET) in improving the implementation of the CBNRM policy. The project applies a multi-level approach at national, regional and local level, it targets approximately 100,000 residents in conservancies and integrated community forests in the Kavango, Kunene and North-Central regions of Namibia. At the national level, it supports the entire CBNRM programme through policy and legal advice.

• Strengthening capacities
The Project supports the successful implementation of the CBNRM policy, in particular strengthening compliance monitoring by MET. The project implements a web-based monitoring compliance management system that connects MET’s regional offices with headquarters in real time. The project further assists in formalising the cooperation between MET, implementing organisations (NGOs), private sector and communal conservancies.

• Good governance
The project supports conservancies to meet good governance standards, including transparent and efficient decision making, implementation of the conservancy constitution, transparent and efficient financial management, gender mainstreaming, sustainable management of wildlife as well as fair and equitable sharing of benefits. Conservancies are also assisted to submit their compliance management data and improving resilience of conservancies by implementing climate change adaptation measures.

• Benefitting from nature
Although Namibia is the most arid country south of the Sahara, with highly variable rainfall, it is endowed with an abundance of wildlife and indigenous plants that can potentially be an important source of revenue for conservancies. In the face of the effects of climate change, the project provides technical support to diversify income generating sources and develop value chains linked to natural resources. The project aims at increasing investments and expanding into local and international markets for indigenous products in order to boost the revenue generated by the conservancies. Furthermore, the project provides economic and legal support to conservancies entering into tourism, hunting or biotrade contractual agreements with the private sector and implements monitoring measures that ensures contract satisfaction on both sides.

Results

• Support to about 228,000 people living in communal conservancies to actively manage their natural resources
• Development and operationalisation of a web-based CBNRM compliance monitoring system - more than 6,700 documents are digitised
• New tourism and hunting based joint ventures were signed, benefiting more than 60,000 community members
• Revising of the Human Wildlife-Conflict (HWC) Policy and development of Guidelines for management of HWC
• Implementation of climate change adaptation measures -new approaches to mitigate HWC and increase resilience of communities are implemented
• Improved revenue base for harvesters - derived from indigenous natural resources in conservancies , especially Devils Claw and Commiphora wildii
 





 
