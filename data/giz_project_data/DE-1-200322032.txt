  




 

Verwaltungsausbildung
Training for Administrative Authorities (CTCSPMO)
Project details
Project number:2003.2203.2
Status:Projekt beendet
Responsible Organisational unit: 2A00 Asien I
Contact person:Dr. Juergen Steiger juergen.steiger@giz.de
Partner countries: PR China
Summary
Objectives:

Die neue Führungsakademie der Oragnisationsabteilung der Partei ist in der Lage, Reformen des öffentlichen Personalsystems auf allen administrativen Ebenen umzusetzen.

Client:

BMZ

Project partner:

China Training Centre for Senior Personnel Management Officials; National Development and Reform Commission

Financing organisation:

not available

 
Project value
Total financial commitment:3 018 178 Euro
Financial commitment for this project number:3 018 178 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:09.09.2005 - 31.07.2013
Actual project:01.10.2005 - 31.07.2013
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Politik und Verwaltung in Bezug auf den öffentl Sektor

Evaluation
not available
 




 
