  




 

Fonds Förderung Internationale Agrarforschung (FIA)
Fund International Agricultural Research (FIA)
Project details
Project number:2018.7860.2
Status:laufendes Projekt
Responsible Organisational unit: G530 Globale Agenden für Ernährungssicherung
Contact person:Dr. Stefan Kachelriess-Matthess stefan.kachelriess@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh.
Summary
Objectives:

Kleinbäuerinnen und Kleinbauern profitieren vermehrt von auf soziale, wirtschaftliche und ökologische Nachhaltigkeit ausgerichteten Innovationen der CGIAR (plus).

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Kein politischer Träger

Financing organisation:

not available

 
Project value
Total financial commitment:522 792 314 Euro
Financial commitment for this project number:126 490 000 Euro
Cofinancing

not available

 
Previous projects
2017.7860.4Förderung der internationalen Agrarforschung
Follow-on projects
not available
 
Term
Entire project:09.03.1994 - 31.12.2026
Actual project:01.01.2019 - 31.12.2026
other participants
Madiba Consult GmbH
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Landwirtschaftliche Forschung

Evaluation
not available
 
Project description (DE)

Ausgangssituation

Durch innovative Technologien, verbesserte Pflanzensorten und umweltschonende Anbaupraktiken hilft die internationale Agrarforschung, die Herausforderungen nachhaltiger Produktion und der Einkommenserwirtschaftung besser zu meistern: Zum einen kämpfen Kleinbäuerinnen und Kleinbauern in Entwicklungsländern mit dem Rückgang landwirtschaftlicher Nutzflächen, Bodenunfruchtbarkeit, Wasserknappheit und dem Verlust der biologischen Vielfalt. Faktoren, die der Klimawandel zusätzlich verschlimmert. Zum anderen gehen viele Nahrungsmittel auf den Feldern oder nach der Ernte durch Schädlinge und Krankheiten verloren oder entsprechen nicht den Anforderungen der Lebensmittelverarbeitenden und der Verbraucher*innen. Die internationale Agrarforschung hilft dabei die steigende Nachfrage nach gesunden Lebensmitteln zu befriedigen.


Ziel

Kleinbäuerinnen und Kleinbauern setzen zunehmend innovative Produkte aus der internationalen Agrarforschung ein. Dadurch erzielen sie ein höheres Einkommen, tragen zu einer gesunden Ernährung bei und schonen natürliche Ressourcen.


Vorgehensweise

Der Fonds zur Förderung Internationaler Agrarforschung (FIA) finanziert und führt Maßnahmen in drei Handlungsfeldern durch:

1. Forschungsmanagement: Das Vorhaben verwaltet die Auswahl und Abwicklung der geförderten Projekte in 15 internationalen Forschungsinstituten. Dazu zählen 13 Institute der globalen Forschungspartnerschaft für eine ernährungssichere Zukunft (Consultative Group on International Agricultural Research, CGIAR) sowie das World Vegetable Center (WorldVeg) und das Internationale Zentrum für Insektenphysiologie und -ökologie (International Centre of Insect Physiology and Ecology, ICIPE). Dabei stellt das Vorhaben ihre wissenschaftliche Qualität, Entwicklungsorientierung, Berücksichtigung der Gleichberechtigung der Geschlechter sowie das Potenzial für eine breite Anwendung der Projekte sicher.

Aktuell werden die CGIAR-Zentren zusammengelegt, um die Agrarforschung effizienter und effektiver zu gestalten. Dieser Prozess findet unter dem Namen „ONE CGIAR" statt und wird von Deutschland unterstützt und vom Vorhaben beraten. Ab 2021 betreibt das Vorhaben daher keine direkte Projektförderung mehr, sondern unterstützt großangelegte Initiativen mit Querschnittsthemen.

Zu den Schwerpunkten der geförderten Agrarforschung zählen:
· Pflanzensorten züchten, die ertragsreicher und nahrhafter sind und auch extremen Umweltbedingungen standhalten
· Angepasste Technologien und landwirtschaftliche Anbaupraktiken entwickeln, die Boden, Wasser und Biodiversität schonen
· Agrobiodiversität in zwölf Saatgut- und Genbanken erhalten
· Forschungsergebnisse in der Praxis verbreiten

2. Beratung und politisches Agenda-Setting: Das Vorhaben unterstützt das Bundesministerium für wirtschaftliche Entwicklung und Zusammenarbeit (BMZ) dabei, die deutschen Beiträge für die internationale Agrarforschung zu gestalten und sich in aktuellen Entwicklungen zu positionieren. FIA ist außerdem Mitglied der Europäischen Initiative für entwicklungsorientierte Agrarforschung (European Initiative for Agricultural Research for Development, EIARD).

3. Personalförderung: Zusammen mit dem Centrum für Internationale Migration und Entwicklung (CIM) fördert das Vorhaben den Einsatz von europäischen Fachkräften in internationalen Forschungszentren. Das Team der „Task Force on Scaling" arbeitet an der Herausforderung, innovative Technologien und Methoden zu verbreiten und in die Praxis zu bringen. Ausgewählten Masterstudierenden wird ein Forschungsaufenthalt an einem der Institute ermöglicht, koordiniert von der Arbeitsgemeinschaft für Tropische und Subtropische Agrarforschung (ATSAF).


Wirkungen

Die Erkenntnisse und Produkte der Agrarforschung tragen dazu bei, die Ernährung und Lebensgrundlage vieler Kleinbäuerinnen und -bauern zu verbessern. Im Jahr 2020 abgeschlossene Projekte erzielten folgende Wirkungen:
• In Kooperation mit Privatpartnern hat das Internationale Zentrum der tropischen Landwirtschaft (International Institute of Tropical Agriculture, IITA) Ansätze zu klimafreundlichem Kaffeeanbau entwickelt und etwa 20.000 Bäuerinnen und Bauern dazu geschult. Zusätzlich wurde eine Applikation namens Stepwise entwickelt. Die App steigerte in Uganda die Kaffeeerträge zwischen 53 Prozent für Arabica- und 73 Prozent für Robustasorten. Zudem führte sie zu einem geringeren Auftreten von Schädlingen und Krankheiten.
• Ein Projekt des Internationalen Reisforschungsinstitut (International Rice Research Institute, IRRI) entwickelte hitzetolerante Reissorten (MAGICheat) für Kleinbäuerinnen und -bauern in Bangladesch und Myanmar. Die Reisproduktion konnte durch die Verwendung der Sorte um 1,5 bis zwei Tonnen pro Hektar gesteigert werden.
• Das RDP-Projekt (Resist Detect Protect) von WorldVeg entwickelte Tomaten- und Paprikasorten, die eine hohe Resistenz gegenüber Schädlingen wie der weißen Fliege aufweisen. Diese Sorten haben darüber hinaus einen besseren Wuchs und ein gefragteres Aussehen als herkömmliche Sorten. Die dadurch erhöhte Annahme durch die Zielgruppe verringerte die Produktionskosten um 20 bis 30 Prozent. Insgesamt wurden 1.482 Kleinbäuerinnen und -bauern in Indien und Bangladesch in Anbaumethoden von krankheitsresistenten Gemüsesorten weitergebildet.

 

 
Project description (EN)

Context

Thanks to innovative technologies, improved crop varieties and environmentally friendly farming methods, international agricultural research helps smallholders in developing countries to be better placed to tackle the challenges of establishing sustainable production and generating income. The problems smallholders face include a decline in the size of farmland, infertile soil, water shortages and the loss of biodiversity, all of which are further exacerbated by climate change. Furthermore, a lot of food is either lost on the fields or after harvesting as a result of pests and diseases, or it fails to meet the requirements of food processing companies and consumers. International agricultural research helps here in meeting the growing demand for healthy food.


Objective

Smallholders are increasingly using innovative products from international agricultural research. These are enabling them to generate a higher income, contribute to healthy nutrition and conserve natural resources.


Approach

The Fund for International Agricultural Research (FIA) finances and implements measures in three fields of activity:

1. Research management:
The project manages the selection and processing of the funded projects in 15 international research institutions. These include 13 centres of the Consultative Group on International Agricultural Research (CGIAR), as well as the World Vegetable Center (WorldVeg) and the International Centre of Insect Physiology and Ecology (ICIPE). In this regard, the project ensures the scientific quality of the funded measures, their development orientation, consideration of gender equality and the projects’ potential for broad-based application.
The CGIAR centres are currently being merged in order to make agricultural research more efficient and effective. This process, which is taking place under the name ‘ONE CGIAR’, is supported by Germany and benefits from advisory services provided by the project. From 2021 onwards, therefore, the GIZ project will no longer fund other projects directly but instead support large-scale initiatives involving cross-cutting issues.

The priority areas for the funding of agricultural research include:
• Cultivating more nutritious crop varieties that produce a higher yield and are also resistant to extreme environmental conditions
• Developing adapted technologies and farming methods that conserve soil, water and biodiversity
• Preserving agrobiodiversity in 12 seed and gene banks
• Sharing research results in practice

2. Advisory services and policy agenda-setting:
The project supports the German Federal Ministry for Economic Cooperation and Development (BMZ) in shaping Germany’s contributions to international agricultural research and adopting a position in respect of current developments. FIA is also a member of the European Initiative for Agricultural Research for Development (EIARD).

3. Staff development:
Together with the Centre for International Migration and Development (CIM), the project facilitates the placement of European experts in international research centres. The Task Force on Scaling team is working on the challenge of disseminating innovative technologies and methods and getting them used in practice. Selected Master’s students are given the opportunity to spend a period of time conducting research at one of the institutes, coordinated by the Council for Tropical and Subtropical Agricultural Research (ATSAF).


Results

The findings and products of agricultural research help to improve nutrition and the livelihoods of many smallholders. Projects completed in 2020 achieved the following results:
• In cooperation with private partners, the International Institute of Tropical Agriculture (IITA) developed approaches for climate-friendly coffee cultivation and trained around 20,000 farmers to apply them. An application called Stepwise was also developed. This app increased coffee yields in Uganda by between 53 per cent for the Arabica variety and 73 per cent for the Robusta variety. It also led to a reduced prevalence of pests and diseases.
• A project conducted by the International Rice Research Institute (IRRI) developed a heat-resistant rice variety (MAGICheat) for smallholders in Bangladesh and Myanmar. Using this variety, it was possible to increase rice production by 1.5 to two tonnes per hectare.
• The RDP (Resist Detect Protect) project conducted by WorldVeg developed varieties of tomato and pepper that exhibit a high level of resistance to pests such as whitefly. These varieties are also characterised by better growth and a more attractive appearance than conventional varieties. The resulting increase in acceptance among the target group has cut production costs by 20 to 30 per cent. A total of 1,482 smallholders in India and Bangladesh were given training in cultivation methods for disease-resistant varieties of vegetables.

 





 
