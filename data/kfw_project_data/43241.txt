Integriertes Programm zur Resilienzförderung in Somalia (UNICEF)

Kernproblem:
Die fragile Sicherheitssituation in Südwesten Somalia und die Häufung von extremen Naturereignissen hat die Lebensgrundlagen der Bevölkerung teilweise zerstört und ihre Vulnerabilität stark erhöht. Die schwachen staatlichen Institutionen sind dabei kaum in der Lage, diesen Herausforderungen effektiv zu begegnen.

Modulziel:
Stärkung der sozioökonomischen Resilienz von Individuen, Haushalten und Gemeinden durch die Förderung ihrer Existenzgrundlagen, insbesondere der landwirtschaftlichen Produktion, sowie durch die Verbesserung des Zugangs zu angemessenen Basisdienstleistungen in den Bereichen Ernährung, Gesundheit, WASH und Bildung. Langfristig sollen der Zielgruppe damit bessere Bewältigungs- und Anpassungsmechanismen hinsichtlich ökonomischer und klimabedingter Schocks zur Verfügung stehen.

Wesentliche Outputs:
Das Programm wird gemeinschaftlich von UNICEF, WEP und FAO in der Region Gedo im Südwesten Somalias umgesetzt und umfasst folgende vier Komponenten:
i) Schaffung der Voraussetzungen für ein sicheres und förderliches Lernumfeld und Bereitstellung von qualitativen Bildungsangeboten, insbesondere für benachteiligte Kinder und Jugendliche;
ii) Verbesserung des Zugangs zu grundlegenden Dienstleistungen in den Bereichen Ernährung, Wasser-, Sanitärversorgung und Hygiene (WASH), um die Widerstandsfähigkeit der Bevölkerung zu stärken;
iii) Stärkung und Diversifizierung von Lebensgrundlagen über die nachhaltige Steigerung der agro-pastoralen Produktion und den Aufbau landwirtschaftlicher Wertschöpfungsketten;
iv) Förderung lokaler Regierungs- und Verwaltungskapazitäten, um sie in die Lage zu versetzen, ihrer Bevölkerung eigenständig und nachhaltig angemessene Basisdienstleistungen anzubieten

Zielgruppe:
Die Zielgruppe sind bedürftige Haushalte, Kinder, schwangere und stillende Frauen sowie Binnenvertriebene in ausgewählten Distrikten der Region Gedo im Bundestaat Jubaland. Jährlich sollen ca. 146.000 Menschen von den Leistungspaketen profitieren.

Beitrag zur nationalen Umsetzung der Agenda 2030:
Die somalische Regierung bekennt sich zu den Sustainable Development Goals (SDG) und hat ihren nationalen Entwicklungsplan auf diese abgestimmt. Das Vorhaben wird zu SDG 2, 3, 4, 5, 6 sowie SDG 15 beitragen.

Umwelt- und Sozialverträglichkeit

Die Umwelt- und Sozialrisiken und zu erwartenden Auswirkungen des Vorhabens sind überwiegend moderat (Kategorie B) und mit Standardmaßnahmen und guter fachlicher Praxis zu adressieren. Potentielle negative Auswirkungen können in den Bereichen Arbeitssicherheit/Arbeitsbedingungen, Umweltschutz und Abfallmanagement bei Bauarbeiten sowie öffentliche Sicherheit während der Bauarbeiten auftreten. Gegebenenfalls kann es in Einzelfällen zu kleinflächigem, zusätzlichen Landbedarf kommen, im Rahmen der Umsetzung der von der FAO geplanten Maßnahmen. Vor diesem Hintergrund ist die Risikokategorie regelmäßig zu überprüfen und ggfs. anzupassen, mit der entsprechenden Entwicklung der dann relevanten Safeguard-Instrumente (=Landakquisitions- und Kompensationspläne). Die Partnerorganisationen sind verpflichtet, für alle Interventionen ein umfängliches Screening der Umwelt- und Sozialrisiken durchzuführen, mit der KfW zu teilen und den jeweiligen Risiken und potenziellen Umwelt- und Sozialauswirkungen angemessene Safeguard-Instrumente zu entwickeln und umzusetzen bzw. deren umfängliche Umsetzung durch Bauunternehmen nachzuhalten und an die KfW zu berichten. Die Partnerorganisationen werden jeweils ihre eigenen Umwelt- und Sozialstandards sowie die Vorgaben der KfW Nachhaltigkeitsrichtlinie anwenden, einschließlich der Anforderungen des von der KfW zur Verfügung gestellten "ESMP Guidance Document". FAO, UNICEF und WFP werden für ihre Interventionen jeweils ihre eigenen bereits bestehenden Beschwerdemechanismen zugänglich machen. Über besondere Vorkommnisse mit Umwelt- und Sozialbezug ist der KFW jeweils unverzüglich zu berichten.

Land / Region / Institution Somalia Nummer 43241 Schwerpunkt Ernährung und Landwirtschaft Sektor 16050 - Multisektorale Hilfe für soziale Grunddienste USV-Kategorie B Finanzierungsinstrument Zuschuss / Darlehen aus Haushaltsmitteln Weitere Geber - Deutscher Finanzierungsbeitrag 17,5 Mio. EUR Status aktiv Auftraggeber BMZ Projektpartner UNITED NATIONS CHILDREN'S FUND Zuständige Abteilung Ostafrika und Afrikanische Union
