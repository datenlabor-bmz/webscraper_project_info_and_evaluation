  




 

Offener Regionalfonds für Südosteuropa - Energie, Verkehr und Klimaschutz
Open Regional Fund for Southeast Europe - Energy , Traffic and Climate Protection
Project details
Project number:2020.2209.3
Status:laufendes Projekt
Responsible Organisational unit: 3700 Westbalkan, Zentralasien, Osteuropa
Contact person:Sibylle Strahl sibylle.strahl@giz.de
Partner countries: Western balkans/Eastern Partnership, Albania, Bosnia Herzeg., Montenegro, North Macedonia, Serbia, Kosovo
Summary
Objectives:

Relevante politische und zivilgesellschaftliche Akteure der Länder SOEs nutzen im Prozess der Umsetzung klimaschutzrelevanter EU-Vorgaben zunehmend regionale Netzwerke.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Sekretariat des Regionalen Kooperationsrats

Financing organisation:

not available

 
Project value
Total financial commitment:29 348 126 Euro
Financial commitment for this project number:10 348 242 Euro
Cofinancing

Europäische Union (EU): 5 000 000Euro


 
Previous projects
2017.2026.7Offener Regionalfonds für Südosteuropa - Energieeffizienz (ORF-EE)
Follow-on projects
not available
 
Term
Entire project:16.06.2008 - 31.05.2025
Actual project:01.08.2020 - 31.05.2025
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektergebnis-Ebene: Projektkomponente zielt auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Energiepolitik und -verwaltung

Evaluation
not available
 




 
