  




 

Entwicklung eines Regionalen Zentrums für Erneuerbare Energien und Energieeffizienz (RCREEE)
Regional Center for Renewable Energy and Energy Efficiency
Project details
Project number:2012.2060.7
Status:Projekt beendet
Responsible Organisational unit: 3600 Nordafrika
Contact person:Momen Meckawy momen.meckawy@giz.de
Partner countries: Egypt, Egypt
Summary
Objectives:

Die energiepolitischen Rahmenbedingungen sind in den Mitgliedsländern mit Hilfe des Erfahrungs- und Wissensaustauschs zu erneuerbaren Energien und Energieeffizienz durch das Regionale Zentrum für erneuerbaren Energien und Energieeffizienz (RCREEE) verbessert .

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Electricity and Renewable Energy

Financing organisation:

not available

 
Project value
Total financial commitment:8 000 000 Euro
Financial commitment for this project number:2 850 000 Euro
Cofinancing

not available

 
Previous projects
2008.2020.9Regionales Zentrum für Erneuerbare Energien und Energieeffizienz
Follow-on projects
not available
 
Term
Entire project:07.07.2008 - 05.11.2018
Actual project:23.10.2013 - 05.11.2018
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektergebnis-Ebene: Projektkomponente zielt auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt birgt nachgewiesen kein Potenzial zur Förderung der Gleichberechtigung
ArmutsorientierungAllgemeine entwicklungspolitische Ausrichtung
CRS code
Energiepolitik und -verwaltung

Evaluation
Ägypten: Unterstützung eines regionalen Zentrums für Erneuerbare Energien und Energieeffizienz (RCREEE). Projektevaluierung: Kurzbericht
Egypt: Regional Centre for Renewable Energies and Energy Efficiency (RCREEE). Project evaluation: summary report
 
Project description (DE)

Ausgangssituation
Erneuerbare Energien und Energieeffizienz sind in den Ländern des Nahen Ostens und Nordafrikas (MENA) zentrale Themen der Energiepolitik. Dabei ist insbesondere die Sicherung einer nachhaltigen Energieversorgung eine gemeinsame Herausforderung. Bislang versuchte jedoch jedes Land, diese Themen in Eigenregie zu lösen, eine gemeinsame Plattform fehlte. Vor diesem Hintergrund wurde das Regionale Zentrum für erneuerbare Energien und Energieeffizienz (RCREEE) in Kairo durch die GIZ gegründet.

Ziel
Durch das RCREEE ist der Anteil an erneuerbaren Energien in den Ländern der MENA-Region maßgeblich erhöht und die effiziente Nutzung der Energie verbessert.

Vorgehensweise
Das Programm unterstützt das RCREEE in seinem strukturellen Aufbau und seiner institutionellen Leistungsfähigkeit. Dazu gehört beispielsweise die Entwicklung eines Businessplans mit Geschäftsstrategien, internem Regelwerk und Arbeitsplänen.

Zur Steuerungsstruktur des RCREEE gehören ein Aufsichtsrat und ein Exekutivkomitee. Im Aufsichtsrat sind die Mitgliedsstaaten des RCREEE vertreten: Ägypten, Algerien, Bahrain, Djibouti, Irak, Jemen, Jordanien, Kuwait, Libanon, Libyen, Marokko, Mauretanien, Palästinensische Gebiete, Somalia, Sudan, Syrien und Tunesien. Das operative Geschäft führt ein von einem arabischen Direktor geleitetes Sekretariat.

Das RCREEE initiiert regionale Politdialoge und fördert Strategien und Partnerschaften, die die Verbreitung erneuerbarer Energien und die Verbesserung von Energieeffizienz in den Mitgliedsstaaten begünstigen. Zudem werden Trainingsmaßnahmen, Workshops und Wissensaustausch sowohl innerhalb (Süd-Süd) als auch außerhalb der Region (Süd-Nord) durchgeführt.

Durch weitere Aktivitäten sollen private Investoren für ein Engagement im Bereich erneuerbare Energie und Energieeffizienz gewonnen werden.

Darüber hinaus liefert das RCREEE zuverlässige Energiedaten und Fakten, die sowohl für mehr Transparenz sorgen, als auch die Fortschritte der Mitgliedsländer bei erneuerbaren Energien und Energieeffizienz aufzeigen.

Wirkungen
Das RCREEE ist eine regionale Institution und sowohl innerhalb der Region als auch darüber hinaus anerkannt. Die Mitgliedsländer haben zugesichert, ab 2014 einen maßgeblichen Anteil an der Finanzierung des RCREEE zu leisten.

Erfolge der Aktivitäten des RCREEE sind beispielsweise die mit Unterstützung des Zentrums eingeführten acht nationalen Aktionspläne zur Energieeffizienz in den Mitgliedsstaaten. Parallel unterstützt das RCREEE die Umsetzung einer arabischen Strategie zur erneuerbaren Energie, die den Weg für nationale Aktionspläne ebnet. Ein regionales Zertifizierungssystem für solare Warmwassererzeuger ist in Vorbereitung.

Zudem ist das RCREEE ein gefragter Kooperationspartner von internationalen Organisationen für regionale Aktivitäten zum Thema erneuerbare Energien und Energieeffizienz. 

 
Project description (EN)

Context
Renewable energies and energy efficiency are key topics for the energy policies of the countries in the Middle East and North Africa (MENA). Safeguarding a sustainable energy supply is a special challenge common to all the countries of this region. Until now, however, each country has attempted to solve the related problems on its own, as no joint platform existed. Against this background, GIZ established the Regional Center for Renewable Energies and Energy Efficiency (RCREEE) in Cairo.

Objective
Through the efforts of RCREEE the share of renewable energies has increased significantly throughout the MENA region, while energy use has become more efficient.

Approach
The programme supports the structural organisation and institutional capacity of RCREEE. This involves, for example, developing a business plan that uses commercial strategies and drafting internal policies and work plans.

RCREEE’s management structure consists of a Supervisory Board and an Executive Committee, with the Center’s member states all represented on the Supervisory Board. These are: Algeria, Bahrain, Djibouti, Egypt, Iraq, Jordan, Kuwait, Lebanon, Libya, Mauritania, Morocco, the Palestinian Territories, Somalia, Sudan, Syria, Tunisia and Yemen. Business operations are managed by a secretariat headed by an Arab director.

RCREEE initiates regional policy dialogues and promotes strategies and partnerships to facilitate the spread of renewable energies and efficient energy use throughout the member states. It also implements training measures, workshops and knowledge exchanges within the region (South-South) and outside the region (South-North).

Additional activities are designed to encourage private investors to become involved in the field of renewable energy and energy efficiency.

Furthermore, RCREE provides reliable facts and figures on energy. This promotes greater transparency while also revealing the progress made by the member states in this field.

Results
RCREEE is a regional institution and has achieved recognition as such, both within the region and beyond it. The member states pledged to provide a significant proportion of the financing for RCREEE starting in 2014.

Some of RCREEE’s successes include the eight national energy efficiency action plans introduced in the member states with its support. At the same time, the Center is supporting the implementation of the Arab Energy Efficiency Directive, which paves the way for these action plans, and it is working to develop a certification system for solar water heaters.

RCREEE is in high demand as a cooperation partner for international organisations that run regional activities related to renewable energies and energy efficiency. 





 
