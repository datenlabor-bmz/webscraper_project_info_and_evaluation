  




 

Integriertes Management der Küsten- und Meeresgebiete der Sundarbans und Swatch of No Ground (SoNG)
Integrated Management of the Coastal and Marine Areas of the Sundarbans and Swatch of No Ground
Project details
Project number:2020.2124.4
Status:laufendes Projekt
Responsible Organisational unit: 2B00 Asien II
Contact person:Dr. Stefan Alfred Groenewold stefan.groenewold@giz.de
Partner countries: Bangladesh, Bangladesh
Summary
Objectives:

Die Koordination relevanter Akteure für den Schutz und die nachhaltige Nutzung des Meeresschutzgebiets Swatch of No Ground (SoNG) im Golf von Bengalen ist gestärkt.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Ministry of Environment, Forest and Climate Change

Financing organisation:

not available

 
Project value
Total financial commitment:4 000 000 Euro
Financial commitment for this project number:4 000 000 Euro
Cofinancing

not available

 
Previous projects

not available

Follow-on projects
not available
 
Term
Entire project:30.06.2022 - 31.07.2025
Actual project:01.08.2022 - 31.07.2025
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projekt ist nicht auf PD/GG ausgerichtet bzw. lässt sich (noch) nicht einstufen
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjektziel-Ebene: Projekt zielt vor allem auf Umwelt- und/oder Ressourcenschutz
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
Armutsorientierungnot available
CRS code
Biodiversität

Evaluation
not available
 




 
