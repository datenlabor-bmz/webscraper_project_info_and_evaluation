  




 

Menschenrechte umsetzen in der Entwicklungszusammenarbeit
Programme Realizing Human Rights in Development Cooperation
Programa 'Aplicar los derechos humanos en la cooperación para el desarrollo'
Project details
Project number:2014.2251.8
Status:Projekt beendet
Responsible Organisational unit: G420 Governance, Menschenrechte
Contact person:Juliane Osterhaus juliane.osterhaus@giz.de
Partner countries: Globale Vorhaben, Konventions-/Sektor-/Pilotvorh., Ecuador, Guatemala, Cambodia, Pakistan, Rwanda
Summary
Objectives:

Das Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) und seine Durchführungsorganisationen richten ihr entwicklungspolitisches Handeln systematischer an menschenrechtlichen Standards und Prinzipien aus.

Client:

Bundesministerium für wirtschaftliche Zusammenarbeit u. Entwicklung

Project partner:

Bundesministerium für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ)

Financing organisation:

not available

 
Project value
Total financial commitment:39 822 224 Euro
Financial commitment for this project number:5 820 000 Euro
Cofinancing

not available

 
Previous projects
2011.2065.8Menschenrechte umsetzen in der Entwicklungszusammenarbeit
Follow-on projects
2017.2011.9Menschenrechte umsetzen in der Entwicklungszusammenarbeit
 
Term
Entire project:02.03.2005 - 31.10.2023
Actual project:01.11.2014 - 29.03.2018
other participants
not available
 
Contact
Project websitesnot available
 
Policy markers
Partizipative Entwicklung und Gute Regierungsführung:Projektziel-Ebene: Projekt zielt vor allem auf PD/GG
Umwelt- und Ressourcenschutz, ökologische NachhaltigkeitProjekt nicht auf Umwelt-/Ressourcenschutz gerichtet bzw. (noch) nicht zu kennzeichnen
Gleichberechtigung der GeschlechterProjekt hat nachweislich positive Wirkung auf Gleichberechtigung
ArmutsorientierungÜbergreifende Armutsbekämpfung auf Makro- und Sektorebene
CRS code
Menschenrechte

Evaluation
Sektorprogramm: Menschenrechte umsetzen in der Entwicklungszusammenarbeit. Projektevaluierung: Kurzbericht
Sector programme: Realising Human Rights in Development Cooperation. Project evaluation: summary report
 
Project description (DE)

Menschenrechtsansatz in der Entwicklungszusammenarbeit
Ausgangssituation
Die Partnerländer der deutschen Entwicklungszusammenarbeit haben die wesentlichen internationalen Menschenrechtsabkommen ratifiziert und sich damit – wie Deutschland selbst – zu ihrer Umsetzung verpflichtet. Die faktische Menschenrechtslage in vielen Entwicklungs- und Schwellenländern ist jedoch durch gravierende politisch-bürgerliche sowie wirtschaftliche, soziale und kulturelle Menschenrechtsverletzungen geprägt.
Das Menschenrechtskonzept des Bundesministeriums für wirtschaftliche Zusammenarbeit und Entwicklung (BMZ) aus dem Jahr 2011 unterstreicht die zentrale Bedeutung der Menschenrechte für die deutsche Entwicklungspolitik als Leitprinzip und Querschnittsaufgabe. Nur mit einer Verbesserung der Menschenrechtslage ist global die Überwindung von Armut und gewaltsamen Konflikten zu erreichen.
Ziel
Das BMZ und seine Durchführungsorganisationen tragen mit ihrem entwicklungspolitischen Handeln noch systematischer zur Verbesserung der Menschenrechte bei.
Vorgehensweise
Das Projekt unterstützt das BMZ bei der Positionierung zum Thema Menschenrechte national und international – so beispielsweise bei der Reform der Umwelt- und Sozialstandards der Weltbank oder in der Gestaltung der Post-2015-Agenda. Außerdem berät es das BMZ bei der Integration der Menschenrechte in entwicklungspolitische Konzepte und Strategien. Fortbildungen und Beratungen der Mitarbeiterinnen und Mitarbeiter in den deutschen Durchführungsorganisationen sorgen dafür, dass diese ihr menschenrechtliches Fachwissen erweitern und in ihrem jeweiligen Arbeitskontext umsetzen können. Erfolgreiche Praxisbeispiele zur Realisierung des Menschenrechtansatzes aus der Auslandsarbeit arbeitet das Projekt auf und stellt sie als vielversprechende Beispiele den Kollegen vor. Mit der Deutschen Welle Akademie führt das Projekt Pilotmaßnahmen zur Förderung des Menschenrechts auf Meinungsfreiheit in verschiedenen Kooperationsländern durch.
Auch mit dem Deutschen Institut für Menschenrechte (DIMR) arbeitet das Projekt seit vielen Jahren eng zusammen. Das DIMR hat im Rahmen der Kooperation ein umfassendes elektronisches Informationsportal zum Thema Menschenrechte und Entwicklungszusammenarbeit eingerichtet.
Wirkungen (kann bei gerade begonnenen Projekten zunächst entfallen)
Menschenrechte werden inzwischen systematisch bei der Vorbereitung aller vom BMZ finanzierten Entwicklungsprojekte berücksichtigt. Das Bundesentwicklungsministerium thematisiert darüber hinaus menschenrechtliche Herausforderungen und Förderansätze in allen Konzepten, Positionspapieren und Strategien, genauso wie in seinen Dialogen mit den Regierungen der Partnerländer.
Seit 2009 sind 13 neue Projekte zur Stärkung regionaler und nationaler Menschenrechtsinstitutionen entstanden – eines davon am afrikanischen Menschenrechtsgerichtshof. Auch stieg die Anzahl der Projekte und Programme der deutschen Entwicklungszusammenarbeit, die einen Menschenrechtsansatz in ihrem Fachgebiet verfolgen, beispielsweise in den Bereichen Wasser, Gesundheit oder Bildung.
 

 
Project description (EN)

Context
The partner countries of German development cooperation have ratified the major international human rights agreements and, like Germany, committed themselves to implementing them. However, the human rights situation on the ground in many developing countries and emerging economies is characterised by serious human rights abuses in the political, civic, economic, social and cultural spheres.
The human rights concept published by the German Federal Ministry for Economic Cooperation and Development (BMZ) in 2011 underscores the centrality of human rights as a guiding principle and cross-cutting task of German development policy. Only by improving the human rights situation will it be possible to overcome poverty and violent conflicts at global level.
Objective
BMZ and its implementing organisations contribute systematically to improving human rights through their development policy activities.
Approach
The programme supports BMZ in positioning the topic of human rights at national and international level, for example, in the context of the reform of the World Bank's environmental and social safeguard policies and the shaping of the post-2015 agenda. It also advises BMZ on how to mainstream human rights into development-policy concepts and strategies. By providing training and advice to staff in the German implementing organisations, the programme enables them to expand their human rights expertise and apply it in their work. It reviews case studies of human rights approaches implemented successfully abroad and presents them to staff members as models of good practices. The programme works with the Deutsche Welle Akademie to pilot measures designed to promote the human right to freedom of expression in a number of cooperation countries.
It has also collaborated closely with the German Institute for Human Rights (DIMR) for many years. As part of this cooperation, the DIMR has set up a comprehensive electronic information portal for human rights and development cooperation.
Results
Human rights are now being mainstreamed into the preparation process of all BMZ-funded development projects. BMZ is also addressing human rights-related challenges and approaches to promoting human rights in its concepts and strategies, and in dialogue with the governments of its partner countries.
Thirteen new projects for strengthening human rights institutions at regional and national level have been established since 2009, one of them at the African Court on Human and Peoples' Rights. There has also been an increase in the number of projects and programmes of German development cooperation that pursue a human rights approach in their respective fields, including the water, health and education sectors.

 





 
